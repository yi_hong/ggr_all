function [p,v,p_iter,v_iter,C_iter] = cdc_gd(Q,p0,v0,t,epsilon,max_iter,alpha,verbose)
% CDC_GD - Compute a best-approximating geodesic to a collection of m 
% time-indexed points on the Grassmannian G(d,N)
%
% The algorithm implements the gradient descent (GD) approach (w.r.t. the
% Sasaki metric between two geodesics (given by intial points + velocity) 
% originally proposed in
%
% Q. Rentmeesters
% 'A gradient method for geodesic data fitting on some 
% symmetric Riemmannian manifolds'
% IEEE CDC '11
% 
% Call syntax:
% ------------
%
%   Variant 1 (no verbose returns):
%   
%   [p,v,~,~,~] = cdc_gd(Q,p0,v0,t,epsilon,max_iter,alpha,0)
%
%   Variant 2 (with verbose returns)
%
%   [p,v,p_iter,v_iter,C_iter] = cdc_gd(Q,p0,v0,t,epsilon,max_iter,alpha,1)
%
%   This not only returns (p,v) but also the progression of (p,v) over all 
%   iterations as well as the cost over all iterations.
%
% Input:
% ------
%
%   Q - (m x 1) cell array of input points (i.e., the regressors)
%       
%       Each point Q{i} is supposed to be a representer (i.e, an 
%       orthonormal N x d matrix) for a d-dimensional subspace of R^N
%       
%   t - associated 'time' with each point (i.e., the dependent variable)
%
%       Note that time needs to be such that  t(1):=0 < t(i) <= t(end):=1,
%       in other words, t in [0,1]
%
%   p0 - initial estimate of geodesic base point
%   v0 - initial estimate of velocity
%       
%       Initial points can be found trying pairs of points and computing
%       the energy term, i.e., the sum-of-squared distances to the curve
%
%   epsilon - termination crit.
%   max_iter - another termination crit.
%   alpha - step size for GD
%
% Output:
% -------
%
%   The best approximating geodesic a(t) given 
%
%   p - p = a(0)
%   v - v = d/dt a(t)|_{t=0} 
%
% Author(s): R. Kwitt, Y. Hong, N. Singh, M. Niethammer, 2014

    p = -1;
    v = -1;
    
    % note: [p_k,v_k] in TM
    m = length(Q);
    p_k = p0; % initial p
    v_k = v0; % initial v
    
    assert(m>1); % need more than 1 point
    N = size(Q{1},1); % rows
    d = size(Q{1},2); % cols
    
    % helper functions
    G = grassmannfactory(N,d);
    
    % some defaults
    p_iter = {};
    v_iter = {};
    C_iter = [];
    
    iter=0;
    while 1
        gp = Inf; % change of p (lives in the tangent space)
        gv = Inf; % change of v (lives in the tangent space)
        
        
        % iterate over all q_1,...q_m points and compute the required
        % gradients
        for i=1:m
            [gp_i,gv_i] = gradf(G,Q{i},t(i),p_k,v_k);
            if i==1
                gp = gp_i;
                gv = gv_i;
            else
                gp = gp + gp_i;
                gv = gv + gv_i;
            end
        end
        
        alpha=alpha*2;
        [p_new,v_new] = retract(G,p_k,v_k,-alpha*gp,-alpha*gv);
        
        % compute cost function for (p,v)
        cost_old = cdc_cost(G,Q,p_k,v_k,t);
        while 1
            % compute cost of new estimates (p_new, v_new)
            cost_new = cdc_cost(G,Q,p_new,v_new,t);
            if (cost_new < cost_old)
                break;
            end
            alpha=alpha/2;
            [p_new,v_new] = retract(G,p_k,v_k,-alpha*gp,-alpha*gv);
            
            % don't let alpha shrink too much
            if (alpha < 1e-6)
                break;
            end
        end
        
        % keep the new estimates
        p_k = p_new;
        v_k = v_new; 
        
        if verbose
            p_iter{iter+1} = p_k;
            v_iter{iter+1} = v_k;
            C_iter = [C_iter; cost_new];
        end
            
        % compute termination checks
        sasaki_m = sasaki(gp,gv,gp,gv);
        fprintf('%d: sasaki=%.10f, cost (new)=%.10f, cost (old)=%.10f\n', ...
            iter, sasaki_m, cost_new, cost_old);
        iter = iter+1;
       
        % TODO: this can be done more elegantly, since it would be great 
        %if we should compare the relative change from iter -> iter+1
        if (sasaki_m<epsilon)
            break;
        end
        if (iter>max_iter)
            break;
        end
    end  
    p = p_k;
    v = v_k;
end


function [a,b] = retract(G,p,v,dp,dv)
    assert(norm(p'*dp,'fro')<1e-6, 'p*dp');
    assert(norm(p'*dv,'fro')<1e-6, 'p*dv');
    
    if norm(p'*(v+dv),'fro')>=1e-6
        pause
    end
    
    assert(norm(p'*(v+dv),'fro')<1e-6, 'p*(v+dv)');
    
    % first, move p by dp
    a = G.exp(p,dp);
    
    % then, compute the vector from p to a in T_pM
    z = G.log(p,a);
    assert(norm(p'*z,'fro')<1e-6);
    
    % finally, transport (v+dv) to the new position
    % of p, i.e., a (and this is done by moving it 
    % from p along z, i.e., the vector pointing from
    % p to a
    b = cdc_pt(p,z,v+dv,1);
    assert(norm(a'*b,'fro')<1e-6);
end


function [gp,gv] = gradf(G,q_i,t_i,p_k,v_k)

    N = size(q_i,1);
    d = size(q_i,2);

    % compute \dot(\beta)(1)
    % 
    % requires to compute -\log(\gamma(p,v,t_i),q_i)
    % (see definition below eq.(8))
    tmp = gam(G,p_k,v_k,t_i);
    bdot_1 = -G.log(tmp,q_i);
   
    % compute T = \Gamma_{\gamma(p,v,t_i)->p}(\dot{\beta}(1))
    %  
    % i.e., parallel transport of \dot{\beta}(1) from \gamma(p,v,t_i) to p!
    % (see, below eq.(10))
    base_pnt = G.exp(p_k,v_k*t_i);
    H = G.log(base_pnt,p_k);
    T = cdc_pt(base_pnt,H,bdot_1,1);
    %T = cdc_pt(p_k,v_k,bdot_1,t_i);
    
    % as noted in the paper, A = \gamma'(p,v,0) which is, by the def.
    % below eq.(4), A = v
    A = v_k;
    [U,S,W] = svd(A,'econ');
    s = diag(S);

    %i-th basis vec in R^d
    B1 = eye(d);
    %B2 = eye(N-2*d);
    
    lam1 = zeros(d,1);
    lam2 = zeros(d*(d-1)/2,1);
    lam3 = zeros(d*(d-1)/2,1);
    
    % handle special case of d=1
    if d == 1
        lam2 = [];
        lam3 = [];
        X2 = [];
        X3 = [];
    end

    for i=1:d
        e_i = B1(:,i);
        lam1(i) = 0;
        D_i = e_i*e_i';
        X1{i} = U*D_i*W';
    end
    
    cnt=1;
    for j=1:d
        for i=1:j-1
            e_i = B1(:,i);
            e_j = B1(:,j);
            X2{cnt} = U*1/sqrt(2)*(e_i*e_j' + e_j*e_i')*W';
            lam2(cnt) = (s(i)-s(j))^2;
            cnt=cnt+1;
        end
    end

    cnt=1; % reset
    for j=1:d
        for i=1:j-1
            e_i = B1(:,i);
            e_j = B1(:,j);
            X3{cnt} = U*1/sqrt(2)*(e_i*e_j' - e_j*e_i')*W';
            lam3(cnt) = (s(i)+s(j))^2;
            cnt=cnt+1;
        end
    end
    lam3 =  lam3(:);

    % this is the C defined in Eq. (13)
    C1 = diag(cos(sqrt(diag(S))*t_i)).^2;
    C2 = diag(sin(sqrt(diag(S))*t_i)./sqrt(diag(S)));
    % Description: tbd.
    E_f1 = (eye(N) - [p_k U]*[p_k U]')*T*(W*C1*W');
    E_f2 = (eye(N) - [p_k U]*[p_k U]')*T*(W*C2*W');
    
    cnt=1; % reset
    %U_T = null([p_k U]');
    
    lam4 = zeros(d*(N-2*d),1);
    if d == 1
        lam4 = [];
    end
    
    for i=1:d
        for j=1:(N-2*d)
            %e_i = B1(:,i);
            %e_tilde_j = B2(:,j);
            %X4{cnt} = U_T*(e_tilde_j*e_i')*W';
            lam4(cnt)=s(i)^2;
            cnt=cnt+1;
        end
    end
    lam4 = lam4';
    
    m_dim = d*(N-d); % manifold dimension
    
    % eigenvalue/eigenvector pair of diagonalized Jacobi operator
    lam = [lam1(:);lam2(:);lam3(:);lam4(:)];
    E = [X1 X2 X3];% X4];
    assert(m_dim == length(lam));
    assert(m_dim - (d*(N-2*d)) == length(E) );

    % comp. Eq. (10) in paper
    gp=0;
    gv=0;
    for k=1:m_dim - (d*(N-2*d))
        l_k = lam(k);
        if l_k==0
            c_k = 1;
            s_k = t_i;
        elseif l_k>0
            c_k = cos(sqrt(l_k)*t_i);
            s_k = sin(sqrt(l_k)*t_i)/sqrt(l_k);
        else
            assert(l_k<0);
            c_k = cosh(sqrt(-l_k)*t_i);
            s_k = sin(sqrt(-l_k)*t_i)/sqrt(-l_k);
        end
        gp = gp+c_k*trace(T'*E{k})*E{k};
        gv = gv+s_k*trace(T'*E{k})*E{k};
    end
    gp = gp + E_f1;
    gv = gv + E_f2;
end


function phat = gam(G,p,v,t)
% compute \gamma(p,v,t) which equals p in case of t=0
    if t==0
        phat=p;
        return;
    end
    phat = G.exp(p,t*v);
end


function r = sasaki(p1,v1,p2,v2)
% compute Sasaki metric
    r = g(p1,p2)+g(v1,v2);
end


function r = g(A,B)
% compute metric on Gr(d,n)
    r = trace(A'*B);
end