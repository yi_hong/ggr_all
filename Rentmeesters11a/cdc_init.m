function [p0_opt,v0_opt,p0_iter,v0_iter,opt_idx] = cdc_init(Q,t,n_iter)
    
    p0_iter = cell(n_iter,1);
    v0_iter = cell(n_iter,1);
    opt_idx = -1;

    % nr. of points
    m = length(Q);
    assert(m>1);
    
    % define G(d,N)
    N = size(Q{1},1);
    d = size(Q{1},2);
    
    G = grassmannfactory(N,d);
   
    p0_opt = Inf;
    v0_opt = Inf;
    
    % time of base point 
    t0 = t(1);
    t1 = t(end);
    
    E = Inf;
    for i=1:n_iter
        % randomly pick two points
        sel = randsample(1:m,2);
        s0 = sel(1);
        s1 = sel(2);
        
        if (abs(t(s0)-t(s1))<1e-6)
            continue;
        end
        
        p0 = Q{sel(1)};
        p1 = Q{sel(2)};
        
        % swap points if required
        if (t(s0) < t(s1))
            p2 = p0;
            p0 = p1;
            p1 = p2;
            
            s2 = s0;
            s0 = s1;
            s1 = s2;
        end
        
        v0 = G.log(p0,p1);
        
        abs_t = t(s1)-t(s0);
        abs_dt0 = t0 - t(s0);
        abs_dt1 = t1 - t(s1);
        
        dt0 = abs_dt0/abs_t;
        dt1 = abs_dt1/abs_t;
        
        p0hat = G.exp(p0,dt0*v0); % transport p0 to position t0
        T = cdc_pt(p0,v0,v0,1); % transport v0 to p1
        p1hat = G.exp(p1,T*dt1); % transport p1 to t1 (along T)
        v0hat = G.log(p0hat,p1hat);
        
        p0_iter{i} = p0hat;
        v0_iter{i} = v0hat;
        
        E_tmp = cdc_cost(G,Q,p0hat,v0hat,t);
        fprintf('Current. cost: %.10f\n', E_tmp);
        
        if (E_tmp < E)
            E = E_tmp;
            %fprintf('Min. cost: %.10f\n', E);
            p0_opt = p0hat;
            v0_opt = v0hat;
            opt_idx = i;
        end
    end
end