% clear G;
% clear Q;
% 
% rng(10);
% 
% m = 10;
% N = 10;
% d = 3;
% G = grassmannfactory(N,d);
% 
% Q = cell(m,1);
% for i=1:m
%    Q{i} = G.rand();
% end
% 
% rs = randsample(1:N,2);
% 
% p0 = Q{rs(1)};
% p1 = Q{rs(2)};
% v0 = G.log(p0,p1);
% 
% t = linspace(0,1,m);
% [p,v] = cdc_gd(Q,p0,v0,t,1,50,0.5);


clear cc
load ./data/cc.mat
delta = 1;

% load ./data/rat.mat
% cc.data = rat.data;
% cc.ages = rat.ages;
% delta = 8;  % parameter for leave-one-subject-out cross validation 
%             % for rat, one subject has 8 landmarks
%             % for cc, one subject has 1 landmark, set it to be one

xAge = cc.ages;
time = (xAge-min(xAge))./(max(xAge(:) - min(xAge(:))));
m = length(time);
 
Q = cell(m,1);
sigma = zeros(size(cc.data{1},2), size(cc.data{1},2));
for i=1:m
    ccTmp = cc.data{i};
    ccTmp = ccTmp - repmat(mean(ccTmp, 1), size(ccTmp, 1), 1);
    [U,sTmp,~] = svd(ccTmp,0);
    Q{i} = U;
    sigma = sigma + sTmp;
end
sigma = sigma ./ m;

tic 

% pick initial geodesic
[p0,v0] = cdc_init(Q,time,50);

% run GD algorithm
[p_opt,v_opt,~,~,C] = cdc_gd(Q,p0,v0,time,1e-6,100,0.5,1);

toc

N = size(Q{1},1); % pts
d = size(Q{1},2); % dim
G = grassmannfactory(N,d);

plotPoints = 50;
color = parula(plotPoints);%jet(plotPoints);
spac = linspace(0,1,plotPoints);

pointCDC = cell(plotPoints, 1);
figure;
hold on;
for i=1:plotPoints
    x = G.exp(p_opt,spac(i)*v_opt);
    pointCDC{i} = x;
    x = x * sigma;
    x(end+1,:) = x(1,:);
    plot(x(:,1),x(:,2), ...
        'color',color(i,:), ...
        'linewidth',1.5);
end

%caxis([min(time) max(time)]); 
caxis([min(cc.ages) max(cc.ages)]);
colorbar();
box off;
axis off;
title('CDC');
% ECCV compatible view
set(gca,'xdir','normal','ydir','reverse');
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 3])
print -depsc '/tmp/output.eps'

% compute R2
% need use a function in ggrbox, go to ggrbox, run setup
fitQ = cell(length(Q), 1);
for i = 1:length(fitQ)
    x = G.exp(p_opt, time(i)*v_opt);
    fitQ{i} = x;
end
R2 = 1 - computeVar(fitQ, Q, 3) / computeVar(Q, Q, 1);

% compute MSE, leave-one-out
errList = zeros(length(Q), 1);
for i = 1:delta:length(Q)
    idx = i:i+delta-1;
    QNew = Q;
    QTest = QNew(idx);
    QNew(idx) = [];
    
    TNew = time;
    TTest = TNew(idx);
    TNew(idx) = [];
   
    TTest = (TTest - min(TNew)) / (max(TNew) - min(TNew));
    TNew = (TNew - min(TNew)) / (max(TNew) - min(TNew));
    
    [p0, v0] = cdc_init(QNew, TNew, 50);
    [p_opt, v_opt] = cdc_gd(QNew, p0, v0, TNew, 1e-6, 100, 0.5, 1);
    
    for j = 1:length(TTest)
        x = G.exp(p_opt, TTest(j) * v_opt);
        errList(i+j-1) = cdc_grDistSq(x, QTest{j});
    end
end

mean(errList)

