function sqD = cdc_grDistSq(p,q)
    [~,S,~] = svd(p'*q);
    theta = acos(diag(S));
    sqD = norm(theta, 2).^2;
end