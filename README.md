## Grassmannian Geodesic Regression (GGR) ##

This is a Matlab library for geodesic regression on the Grassmann manifold. 
The algorithms implemented in this library were published in the following 
two papers:

```
@incollection{hong2014geodesic,
  title={Geodesic Regression on the Grassmannian},
  author={Y.~Hong and R.~Kwitt and N.~Singh and B.~Davis and N.~Vasconcelos and M.~Niethammer},
  booktitle={Computer Vision--ECCV 2014},
  pages={632--646},
  year={2014},
  publisher={Springer}
}
```
and

```
@incollection{hong2014time,
  title={Time-warped geodesic regression},
  author={Y.~Hong and N.~Singh and R.~Kwitt and M.~Niethammer},
  booktitle={MICCAI},
  pages={105--112},
  year={2014},
  publisher={Springer}
}
```

For information on how to use this library, see the [wiki](https://bitbucket.org/yi_hong/ggr_all/wiki/Home).