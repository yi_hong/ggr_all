function energy = adimat_compute_regression_energy( x, params )
% ADIMAT_COMPUTE_REGRESSION_ENERGY computes the regression energy.
%

    vi = -1; %#ok<NASGU>
    k = length(params.tau);
    n = params.n;
    p = params.p;
    T = length(x)/(n*p);
    dt = 1/(T-1);
    tau = params.tau;
    curve = reshape(x,[n,p,T]);
    P = params.P;
    
    %----------------------------------------------------------------------
    % Data term
    %----------------------------------------------------------------------    
    Ed = 0;
    v = zeros(n,p,k);
    Vtilde = zeros(n,p,T,k);
    tmp = 0; %#ok<NASGU>
    for i=1:k
        tmp = -adimat_gr_log(curve(:,:,tau(i)),P(:,:,i));
        
        if adimat_fro_norm_real(tmp)<1e-6
            v(:,:,i) = zeros(n,p);
        else
            v(:,:,i) = tmp;
        end
        Vtilde(:,:,:,i) = adimat_gr_parallel_transport_along_path(...
            curve,...
            v(:,:,i),...
            tau(i));
         
        vi = v(:,:,i);
        Ed = Ed + sqrt(sum(vi(:).^2));
    end
    energy = params.lambda1*Ed;
    
    %----------------------------------------------------------------------
    % Smoothness term
    %----------------------------------------------------------------------        
    tmp1 = -1; %#ok<NASGU>
    tmp2 = -1; %#ok<NASGU>    
    gammaDot = zeros(n,p,T);
    for t=1:T
        if t == 1
            gammaDot(:,:,t) = adimat_gr_log(curve(:,:,t),curve(:,:,t+1))/dt; 
        elseif t == T 
            gammaDot(:,:,t) = adimat_gr_parallel_transport(...
                curve(:,:,t-1), ...                             % src
                adimat_gr_log(curve(:,:,t-1),curve(:,:,t))/dt, ...     % tangent
                curve(:,:,t));                                  % dst
        elseif t == T-1
            tmp1 = adimat_gr_parallel_transport(...
                curve(:,:,t-1), ...
                adimat_gr_log(curve(:,:,t-1),curve(:,:,t))/dt, ...
                curve(:,:,t));
            gammaDot(:,:,t) = (tmp1 + ...
                3*adimat_gr_log(curve(:,:,t),curve(:,:,t+1))/dt)/4;
        else
            tmp1 = adimat_gr_parallel_transport(...
                curve(:,:,t-1), ...
                adimat_gr_log(curve(:,:,t-1),curve(:,:,t))/dt, ...
                curve(:,:,t));
            tmp2 = adimat_gr_parallel_transport(...
                curve(:,:,t+1), ...
                adimat_gr_log(curve(:,:,t+1),curve(:,:,t+2))/dt,...
                curve(:,:,t));
            gammaDot(:,:,t) = ...
                (tmp1 + ...
                2*adimat_gr_log(curve(:,:,t),curve(:,:,t+1))/dt + tmp2)/4;
        end
    end
    
    % Covariant derivatives
    B1 = zeros(n,p,T);
    B2 = zeros(n,p,T);
    D2gamma = zeros(n,p,T);
    for t=1:T-1
        B1(:,:,t) = adimat_gr_parallel_transport(...
            curve(:,:,t+1),...
            gammaDot(:,:,t+1),...
            curve(:,:,t));
        B2(:,:,t+1) = adimat_gr_parallel_transport(...
            curve(:,:,t),...
            gammaDot(:,:,t),...
            curve(:,:,t+1));
    end
    for t=1:T
        if t == 1
            D2gamma(:,:,t) = (B1(:,:,t) - gammaDot(:,:,t))/dt;
        elseif t == T
            D2gamma(:,:,t) = (gammaDot(:,:,t) - B2(:,:,t))/dt;
        else
            D2gamma(:,:,t) = (B1(:,:,t) - B2(:,:,t))/(2*dt);
        end
    end
    
    % Energy
    A = -1; %#ok<NASGU>
    Es2 = 0;
    for t=1:T
        A = D2gamma(:,:,t);
        Es2 = Es2 + sum(A(:).^2)/T; % Frobenius norm squared
    end
    energy = energy + params.lambda2*Es2;
end