function Y = adimat_gr_exp(X, U)
% ADIMAT_GR_EXP computes the exponential map on the Grassmannian
%
% Y = GR_EXP(X, U) computes Y = \exp_X(U)
%
% Adapted from the manopt toolbox.
%
% Modified by Roland Kwitt, 2015

     tU = U;
     Y = zeros(size(X)); %#ok<PREALL>

     [u, s, v] = svd(tU, 0);
     cos_s = diag(cos(diag(s)));
     sin_s = diag(sin(diag(s)));
     Y = X*v*cos_s*v' + u*sin_s*v';
     [q, unused] = qr(Y, 0); %#ok
     Y = q;
 end
 