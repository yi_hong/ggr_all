function y = adimat_fro_norm_real(A)
% ADIMAT_FRO_NORM_REAL computes the Frobenius norm (in adimat compatible format)
% 
% (Caution: only for matrices of reals)
% Author: Roland Kwitt, 2015
    y = sqrt(sum(A(:).^2));
end