function adimat_show_curve( YPnts, ts, pDim, lineStyle, color)
    [sz1, nSta] = size(YPnts(:,:,1));
    kObs = floor( sz1 / pDim );

    assert( kObs * pDim == sz1 );

    realvalue = zeros( size(YPnts,3), 1 );
    imagvalue = zeros( size(YPnts,3), 1 );
    for iI = 1:size(YPnts,3)
        [A, ~] = estimate_dynamic_matrix( YPnts(:,:,iI), pDim, nSta, kObs );
        eigvalue = eig(A);
        realvalue(iI) = real(eigvalue(1));
        imagvalue(iI) = imag(eigvalue(1));
    end
    
    plot( ts, imagvalue, lineStyle, 'LineWidth', 2, 'Color', color );    
    hold on;
    plot( ts, realvalue, lineStyle, 'LineWidth', 2, 'Color', color );    
end

