function W = adimat_gr_parallel_transport_along_path(gam,V,tt)
% ADIMAT_GR_PARALLEL_TRANSLATE_ALONG_PATH translates a tangent vector along path.
%
% (adjusted for use with adimat)
% Author: Jingyong Su & Roland Kwitt, 2015

    W = -1; %#ok<NASGU>
    T = size(gam,3);

    if adimat_fro_norm_real(V) < 0.0001
        W = zeros(size(gam));
    else
        W = zeros(size(gam));
        W(:,:,tt) = V;

        for t = tt-1:-1:1
            W(:,:,t) = adimat_gr_parallel_transport(...
                gam(:,:,t+1),...    % src
                W(:,:,t+1),...      % tangent
                gam(:,:,t));        % dst
        end   

        for t=tt+1:1:T
            W(:,:,t) = adimat_gr_parallel_transport(...
                gam(:,:,t-1),...    % src
                W(:,:,t-1),...      % tangent
                gam(:,:,t));        % dst
        end
    end
end