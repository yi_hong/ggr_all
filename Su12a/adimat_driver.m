function [f,g] = adimat_driver( x, opts_all )
    params = opts_all.params;
    opts = opts_all.optsAdimat;

    if ( nargout>1 )
      [g,f] = admDiffRev( @adimat_compute_regression_energy, ...
          1, x, params, opts );
    else
      f = adimat_compute_regression_energy( x, params );
    end 
end