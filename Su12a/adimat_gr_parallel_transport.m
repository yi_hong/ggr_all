function Bt = adimat_gr_parallel_transport(O1,B,O2)
% ADIMAT_GR_PARALLEL_TRANSPORT parallel transports a tangent along geodesic.
%
% (adjusted for use with adimat)
% Author: Jingyong Su & Roland Kwitt, 2015

    t=1;
    H = adimat_gr_log(O1,O2);
    U = -1; %#ok<NASGU>
    S = -1; %#ok<NASGU>
    V = -1; %#ok<NASGU>
    
    % ADIMAT does not support economy size SVD
    p = size(O1,2);
    [U,S,V] = svd(H);
    
    U = U(:,1:p);
    S = S(1:p,1:p);
    
    Sigmat = diag(S)*t;
    M = [diag(-sin(Sigmat)); diag(cos(Sigmat))];

    part0 =[O1*V U]*(M*(U'*B));

    part1 = B - U*(U'*B);
    Bt = part0 + part1;
end