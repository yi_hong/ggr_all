%
% given a span, compute the points along the geodesic
% Author: Yi Hong
% Email: yihong@cs.unc.edu
%

function [t, pointsSyn] = computePointsAlongGeodesic( initPnt, initVel, t0, tSpan )

% make sure tSpan is 1 by n
sz = size(tSpan);
if sz(1) > 1
    tSpan = tSpan';
end

% check the size of tSpan is larger than 2
assert( length(tSpan) >= 2 );

% check the tSpan is increasing
[~, indexSort] = sort(tSpan);
indexCompare = 1:length(tSpan);
assert( norm( indexSort - indexCompare ) == 0 );

if t0 > tSpan(end)
    % shoot back
    [t, pointsSyn] = integrateForwardWithODE45( initPnt, initVel, [t0 fliplr(tSpan)] );
    t(1) = [];
    pointsSyn(1) = [];
    indexReorder = length(t):-1:1;
    t = t(indexReorder);
    pointsSyn = pointsSyn(indexReorder);
elseif t0 == tSpan(end)
    % shoot back
    tSpan = fliplr(tSpan);
    insertFlag = false;
    if length(tSpan) == 2
        tSpan = [tSpan(1) (tSpan(1)+tSpan(2))/2.0 tSpan(2)];
        insertFlag = true;
    end
    [t, pointsSyn] = integrateForwardWithODE45( initPnt, initVel, tSpan );
    if insertFlag
        t(2) = [];
        pointsSyn(2) = [];
    end
    indexReorder = length(t):-1:1;
    t = t(indexReorder);
    pointsSyn = pointsSyn(indexReorder);
elseif t0 > tSpan(1) 
    indTmp = find( tSpan == t0 );
    addT0 = false;
    if isempty(indTmp)
        tSpan = sort( [tSpan t0] );
        indTmp = find( tSpan == t0 );
        addT0 = true;
    end

    tSpan1 = tSpan(indTmp:end);
    insertFlag = false;
    if length(tSpan1) == 2
        tSpan1 = [tSpan1(1) (tSpan1(1)+tSpan1(2))/2 tSpan1(2)];
        insertFlag = true;
    end
    [t1, pointsSyn1] = integrateForwardWithODE45( initPnt, initVel, tSpan1 );
    if insertFlag 
        t1(2) = [];
        pointsSyn1(2) = [];
    end

    tSpan2 = fliplr(tSpan(1:indTmp));
    insertFlag = false;
    if length(tSpan2) == 2
        tSpan2 = [tSpan2(1) (tSpan2(1)+tSpan2(2))/2 tSpan2(2)];
        insertFlag = true;
    end
    [t2, pointsSyn2] = integrateForwardWithODE45( initPnt, initVel, tSpan2 );
    if insertFlag 
        t2(2) = [];
        pointsSyn2(2) = [];
    end
    t2(1) = [];
    pointsSyn2(1) = [];
    indexReorder = length(t2):-1:1;
    t2 = t2(indexReorder);
    pointsSyn2 = pointsSyn2(indexReorder);

    t = [t2; t1];
    pointsSyn = [pointsSyn2; pointsSyn1];
    
    if addT0
        t(indTmp) = [];
        pointsSyn(indTmp) = [];
    end

elseif t0 == tSpan(1)
    insertFlag = false;
    if length(tSpan) == 2
        tSpan = [tSpan(1) (tSpan(1)+tSpan(2))/2.0 tSpan(2)];
        insertFlag = true;
    end
    [t, pointsSyn] = integrateForwardWithODE45( initPnt, initVel, tSpan );
    if insertFlag 
        t(2) = [];
        pointsSyn(2) = [];
    end
else
    [t, pointsSyn] = integrateForwardWithODE45( initPnt, initVel, [t0 tSpan] );
    t(1) = [];
    pointsSyn(1) = [];
end