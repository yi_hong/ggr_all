function lam = max_evs( curve, pDim )
% MAX_EVS returns the max. eigenvalue of the LDS dynamic matrix A, obtained
% from each observability matrix in the NxMxT observability matrix array.
        [sz1, nSta] = size(curve(:,:,1));
        kObs = floor( sz1 / pDim );
        assert( kObs * pDim == sz1 );
        lam = zeros(size(curve,3),1);
        for iI = 1:size(curve,3)
            [A, ~] = estimate_dynamic_matrix( curve(:,:,iI), pDim, nSta, kObs );
            evs = eig(A);
            lam(iI) = evs(1);
        end
    end
