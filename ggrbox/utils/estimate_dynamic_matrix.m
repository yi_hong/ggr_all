% estimate the C and A matrices of the dynamical system
% from the interplated orthogonalized observability matrix

function [A, C] = estimate_dynamic_matrix( oma, pDim, nSta, kObs )

X = eye(nSta);
nOut = size(X, 2);
Y = zeros(pDim, kObs*nOut);
for iI = 1:nOut
    x0 = X(:, iI);
    for iJ = 1:kObs
        Y(:, (iI-1)*kObs+iJ) = oma((iJ-1)*pDim+1:iJ*pDim, :) * x0;
    end
end

% Creating Mean Subtracted sequence
%C0         = mean(Y,2);
%Ybar       = Y - repmat(C0,1,size(Y, 2));

% Perform SVD for the paramters
[U,S,V]    = svd(Y,0);
C          = U(:,1:nSta);
Z          = S(1:nSta,1:nSta)*V(:,1:nSta)';

for iI = 1:nOut
    E(:, (iI-1)*(kObs-1)+1:iI*(kObs-1)) = Z(:, (iI-1)*kObs+2:iI*kObs);
    F(:, (iI-1)*(kObs-1)+1:iI*(kObs-1)) = Z(:, (iI-1)*kObs+1:iI*kObs-1);
end
A = E*pinv(F);
