% 
% plot the 2D points on the geodesic
% Author: Yi Hong
% Email: yihong@cs.unc.edu
%

function pointsSyn = plot2DShapeOnGeodesic( initPnt, initVel, tPos, timeWarpParams, tMinPlot, tMaxPlot, sigma, nNumShape, grParams, nTypeFlag )

assert( tMinPlot < tMaxPlot );
ages = linspace( tMinPlot, tMaxPlot, nNumShape );

% plot the points on the geodesic
figure(400);
for iPlot = 1:length(initPnt)
    
    % need to check the part for piecewise regression because I'm now
    % focusing on the full GGR and time warped regression
    if strcmp( grParams.regressMethod{iPlot}, 'PiecewiseRegression' ) || ...
       strcmp( grParams.regressMethod{iPlot}, 'ContinuousPiecewiseRegression' ) || ...
       strcmp( grParams.regressMethod{iPlot}, 'ContinuousPiecewiseRegressionWithOptimalBreakpoints' )
        error('Not supported yet');
        
    elseif strcmp( grParams.regressMethod{iPlot}, 'TimeWarpRegression' )
        % time points for shooting forward
        tSpan = sort(generalisedLogisticFunction( ages, timeWarpParams{iPlot} ));
        [t{iPlot}, pointsSyn] = computePointsAlongGeodesic( initPnt{iPlot}{1}, initVel{iPlot}{1}, tPos{iPlot}(1), tSpan );
        % go back to real time
        t{iPlot} = inverseLogisticFunction( t{iPlot}, timeWarpParams{iPlot} );
        
        % plot the optimized logistic function
        %subplot(length(initPnt)+1, 1, length(initPnt)+1), hold on;
        %plot( ages, generalisedLogisticFunction(ages, timeWarpParams{iPlot}), 'bo-.', 'LineWidth', 2 );
        % plot the M point
        %plot( timeWarpParams{iPlot}.M*[1 1], [0 generalisedLogisticFunction( timeWarpParams{iPlot}.M, ...
        %    timeWarpParams{iPlot} ) ], 'r--', 'LineWidth', 1 );
        hold off;
    elseif strcmp( grParams.regressMethod{iPlot}, 'CubicSplineRegression' )
        [t{iPlot}, pointsSyn] = computePointsAlongGeodesicSpline( initPnt{iPlot}, initVel{iPlot}, tPos{iPlot}, ages, grParams.nt );
    else
        [t{iPlot}, pointsSyn] = computePointsAlongGeodesic( initPnt{iPlot}{1}, initVel{iPlot}{1}, tPos{iPlot}(1), ages );
    end

    clr = jet(tMaxPlot - tMinPlot + 1);
    %{
    subplot(2, length(initPnt), iPlot), hold on;
    for iSyn = 1:length(pointsSyn)
        plot( [pointsSyn{iSyn}(:, 1); pointsSyn{iSyn}(1, 1)], -[pointsSyn{iSyn}(:, 2); pointsSyn{iSyn}(1, 2)], ...
              'Color', clr(round(t{iPlot}(iSyn)-tMinPlot+1), :), 'LineWidth', 1 );
    end
    axis equal
    colormap(clr);
    caxis( [minClrValue maxClrValue] );
    colorbar
    xlim( [-0.25 0.25] );
    ylim( [-0.25 0.25] );
    %}

    subplot(length(initPnt), 1, iPlot), hold on;
    pointsSynOrig = pointsSyn;
    
    if nTypeFlag == 1
        % connected all the points into a loop
        for iSyn = 1:length(pointsSyn)
            pointsSynOrig{iSyn} = pointsSynOrig{iSyn} * sigma;
            plot( [pointsSynOrig{iSyn}(:, 1); pointsSynOrig{iSyn}(1, 1)], -[pointsSynOrig{iSyn}(:, 2); ...
                pointsSynOrig{iSyn}(1, 2)], 'Color', clr(round(t{iPlot}(iSyn)-tMinPlot+1), :), 'LineWidth', 1.5 );
        end
    elseif nTypeFlag == 2
        % simply plot all the points
        for iSyn = 1:length(pointsSyn)
            pointsSynOrig{iSyn} = pointsSynOrig{iSyn} * sigma;
            plot( pointsSynOrig{iSyn}(:, 1), pointsSynOrig{iSyn}(:, 2), '.', ...
                'Color', clr(round(t{iPlot}(iSyn)-tMinPlot+1), :), 'LineWidth', 1 );
        end
    else
        error('Unknown plotting type');
    end
    
    hold off
    axis equal
    axis off 
    box off
    set(gca,'xdir','normal','ydir','normal');
    set(gcf,'PaperUnits','inches','PaperPosition',[0 0 8 3])
    colormap(clr);
    caxis( [tMinPlot tMaxPlot] );
    colorbar
end


