% 
% estimate indpedent value using nearest neighborhood 
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 10/16/2014
%

function [ageTest] = estimateIndependentValueNN(omaTrain, ageTrain, omaTest)

ageTest = zeros(length(omaTest), 1);

for iI = 1:length(omaTest)
    minDist = 0;
    for iJ = 1:length(omaTrain)
        curDist = grarc(omaTest{iI}, omaTrain{iJ});
        if iJ == 1 || curDist < minDist
            minDist = curDist;
            ageTest(iI) = ageTrain(iJ);
        end
    end
end