function [Y1] = generateGaussianSamples(Y0, sigma)
    [m, n] = size(Y0);
    Tan = normrnd(0, sigma/sqrt(m*n), [m, n]);
    Tan = (eye(size(Y0, 1)) - Y0*Y0')*Tan;
    %[~, Ytmp, ~] = integrateForwardWithODE45(Y0, Tan, [0, 1]);
    %Y1 = Ytmp{end};
    G = grassmannfactory(m, n);
    Y1 = G.exp(Y0, Tan, 1.0);
end