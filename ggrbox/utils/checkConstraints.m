% check constraints 
function checkConstraints(Ys, Y0, Y0dot, Y0ddot)

%disp( 'Check the integrated-forward points stay on the Grassmanian manifold ...' );
errY = zeros(length(Ys), 1);
for iI = 1:length(Ys)
    %strTmp = sprintf('%d -- f-norm distance to id: %f', ...
    %         iI, norm( (Yend{iI})'*Yend{iI} - eye(size(Yend{iI}, 2)), 'fro' ) );
    %disp(strTmp);
    errY(iI) = norm( (Ys{iI})'*Ys{iI} - eye(size(Ys{iI}, 2)), 'fro' );
end

figure, hold on
plot(1:length(errY), errY);

plot(length(errY)+1, norm( Y0' * Y0dot, 'fro' ), 'o');

if nargin > 3
    plot(length(errY)+2, norm( Y0' * Y0ddot, 'fro' ), 'p');
end

hold off

%disp( 'Check the initial velocity and initial point ...' );
%strTmp = sprintf( 'f-norm distance to zero: %f', norm( Y0' * Y0dot, 'fro' ) );
%disp(strTmp);

