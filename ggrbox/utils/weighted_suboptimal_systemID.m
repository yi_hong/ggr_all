function sysParam = weighted_suboptimal_systemID(dataMatrix,order,weights,params,useBIRSVD)
% Caclculates system parameters of LDS using weighted suboptimal sysid  
%
% This function calculates the system parameters of a LDS that represents a video 
% sequence. This is a weighted modification of the suboptimal approach proposed by Doretto et. al IJCV 2003.
%
% INPUTS
%   dataMatrix - p x F vector matrix or r x c x F sequence matrix
%   order      - [n nv] vector, [Default: nv=1]
%   weights    - weights for each frame, 1 X F, should sum up to one (if not normalized them)
%   params     - parameter structure containing the various parameters
%    .useBIRSVD (default false): uses BIRSVD to solve weighted SVD. Slow, but can use regularization; otherwise use svd(Y*diag( sqrt( weights ) ) ) 
%
% OUTPUTS
%   sysParam   - output structure containing all the system parameters
%
% EXAMPLE
% 
%
%% Unweighted version written by: Avinash Ravichandran, Rizwan Chaudhry
%% Weighted adaption: Marc Niethammer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initial Parameter Checks and Preprocessing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% Checking the Data Matrix
F = size(dataMatrix,3);

if (size(dataMatrix,3)~=1)
    I = double(reshape(dataMatrix,[],F));
else
    F = size(dataMatrix,2);
    I = double(dataMatrix);
end

% Checking if we have noise order 
if length(order)==1
    n    = order(1);
    nv   = 1;
else
    n    = order(1);
    nv   = order(2);
end

% Parsing the parameter structure
dParams.Rfull = 0;
dParams.class = 1;
dParams.Areg  = 1;
if nargin<4
    params = dParams
else
    params = test_param_validity(dParams,params);
end

if nargin<5
  useBIRSVD = false;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Basic Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Creating Mean Subtracted sequence
if ~isfield(params, 'subtractMean') || params.subtractMean
	C0         = mean(I,2);
	Y          = I - repmat(C0,1,F);
else
	C0         = zeros(size(I, 1), 1);
	Y          = I;
end

p = size( Y, 1 );

% creating the weight matrix for BIRSVD (the weighted SVD)
weights = reshape( weights, 1, length( weights ) );

sumWeights = sum( weights );
if ( abs( sumWeights-1 )>1e-7 )
  warning('Weights do not sum to 1. Auto-scaling within weightedSuboptimalSystemID.');
  weights = weights/sumWeights;
end

if ( useBIRSVD )

  WBir = repmat( weights, [p 1] )*length( weights );
  % perform the weighted SVD
  paramBir.niter            =  30;
  %paramBir.niter            =  10;
  paramBir.ini_method       = 'unweightedSVD';
  paramBir.regu_type_left   = 'Tikh'; % '2ndOrderDiff_acc8';
  paramBir.regu_type_right  = 'Tikh'; %2ndOrderDiff_acc8';
  paramBir.regu_left        = .001;
  paramBir.regu_right       = .001;
  paramBir.lsqr_niter       = 25;
  %paramBir.lsqr_niter       = 10;

  [U,S,V] = BIRSVD_mn(Y, WBir, n, paramBir );
  
else % just do straight-up SVD (faster)
  WBirDiagSqrt = diag( sqrt( weights ) )*sqrt( length( weights ) ); % to make sure we get the same reconstruction
  [U,S,V] = svds( Y*WBirDiagSqrt, n );
end

C          = U(:,1:n);
Z          = S(1:n,1:n)*V(:,1:n)';

% Do a weighted estimate of A
% weights for transition (as averages)
transWeights = weights( 1:end-1 ) + weights( 2:end );
transWeights = transWeights/sum(transWeights);
WSqrt = diag( sqrt( transWeights ) ); % need the square root diagonal matrix here
A          = Z(:,2:F)*WSqrt*pinv(Z(:,1:(F-1))*WSqrt);
Z0         = Z(:,1);

% Old SVD for comparison
%% Perform SVD for the paramters
%[U,S,V]    = svd(Y,0);
%C          = U(:,1:n);
%Z          = S(1:n,1:n)*V(:,1:n)';
%
%% Estimate A
%A          = Z(:,2:F)*pinv(Z(:,1:(F-1)));
%Z0         = Z(:,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assigning Output Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sysParam.Z0    = Z0;
sysParam.A     = A;
sysParam.C     = C;
sysParam.Z     = Z;
sysParam.C0    = C0;
sysParam.class = params.class;


