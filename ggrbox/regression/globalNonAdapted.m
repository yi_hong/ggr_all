% compute global non-adapted 

function [SNonAdapted, meanNonAdapted] = globalNonAdapted(dataAll, labelAll, iClass, iP, dim)

dataTrainAll = [];
for iK = 1:length(dataAll)
    if iK == iP 
        continue;
    end
    dataTmp = dataAll{iK};
    dataTrainAll = [dataTrainAll; dataTmp(find(labelAll{iK} == iClass), :)];
end
tmpSubSpace = compute_subspace(dataTrainAll, dim, -1, -1);
SNonAdapted = tmpSubSpace.P;
meanNonAdapted = tmpSubSpace.M;