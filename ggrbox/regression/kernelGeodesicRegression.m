function [errSqr, SAdapted] = kernelGeodesicRegression(subSpace, xYear, xTest, sigma, params)

grParams = params;

% train on data without target domain
[ind] = find(xYear == xTest);
grParams.nSampling = 1:length(xYear);
grParams.nSampling(ind) = [];

if isempty(ind)
    grParams.nTesting = grParams.nSampling;
    subSpaceTest = [];
else
    grParams.nTesting = ind;
    subSpaceTest = subSpace(ind);
end

% compute weight
wi = gaussmf(xYear(grParams.nSampling), [sigma xTest]);
wi = wi ./ sum(wi) * length(grParams.nSampling);
grParams.weight = wi;
grParams.project = 0;

% regression
grParams.regressMethod = {'FullRegression'};
[~, ~, tPos, initPnt, initVel, ~, timeWarpParams] = estimateOnFeature( subSpace, xYear, grParams);

% compute testing subspace and error
[errSqr, SAdaptedTmp] = computeGeodesicTestError( initPnt{1}, initVel{1}, tPos{1}, ...
    timeWarpParams{1}, subSpaceTest, xTest );
if length(xTest) == 1
    SAdapted = SAdaptedTmp{1};
else
    SAdapted = SAdaptedTmp;
end