% compute local non-adapted subspace, two time points
function [SLocalNonAdapted, meanLocalNonAdapted] = localTwoPointsNonAdapted(dataAll, labelAll, iClass, xYear, xTest, xYearBase, dim)

[~, indClosest] = sort( abs(xYear - xTest) );
assert( length(indClosest) >= 3 );
if xYear(indClosest(1)) == xTest
    indStart = indClosest(2);
    indEnd = indClosest(3);
else
    indStart = indClosest(1);
    indEnd = indClosest(2);
end
indStartBase = find(xYearBase == xYear(indStart));
indEndBase = find(xYearBase == xYear(indEnd));

dataTrainLocal = [];
dataTmp = dataAll{indStartBase};
dataTrainLocal = [dataTrainLocal; dataTmp(find(labelAll{indStartBase} == iClass), :)];
dataTmp = dataAll{indEndBase};
dataTrainLocal = [dataTrainLocal; dataTmp(find(labelAll{indEndBase} == iClass), :)];
tmpSubSpace = compute_subspace(dataTrainLocal, dim, -1, -1);
SLocalNonAdapted = tmpSubSpace.P;
meanLocalNonAdapted = tmpSubSpace.M;