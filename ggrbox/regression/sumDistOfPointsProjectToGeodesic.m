% compute the distances of measurements projected to the geodesic
% input:
% Y0: the initial point on the manifold
% Y0dot: the initial velocity in the tangent space
% t0: the associated value of the initial point
% oma: the measurements
% t: the sequence of the associated values
% tStep: the time step for integrating forward
% output:
% distSqrSum: the sum of square distances
% 
% Author: Yi Hong
% Date: 10/02/2013
%
function distSqrSum = sumDistOfPointsProjectToGeodesic(Y0, Y0dot, t0, oma, t, tStep )

% integrate forward to the maximum value and backward to the minimum value
% so that we only need to shoot once
maxT = max(t);
minT = min(t);

% integrate forward from t0 to maxT
[YendForward] = integrateForwardToGivenTime(Y0, Y0dot, maxT-t0, tStep);
% integrate backward from t0 to minT
[YendBackward] = integrateForwardToGivenTime(Y0, Y0dot, minT-t0, tStep);
        
% compute the sum of the square distances
distSqrSum = 0;
for iI = 1:length(oma)   % for each measurement, find its projection
    distMin = -1;
    for iJ = 1:length(YendForward)
        distTmp = grarc( oma{iI}, YendForward{iJ} );
        if iJ == 1 || distTmp < distMin
            distMin = distTmp;
        end
    end
    for iJ = 1:length(YendBackward)
        distTmp = grarc( oma{iI}, YendBackward{iJ} );
        if (distMin == -1 && iJ == 1) || distTmp < distMin
            distMin = distTmp;
        end
    end
    distSqrSum = distSqrSum + distMin;
end
