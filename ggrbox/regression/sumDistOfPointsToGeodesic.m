% compute the distances between geodesic and measurements
% Input:
% Y0: the initial point on the manifold
% Y0dot: the initial velocity in the tangent space
% t0: the associated value of the initial point
% oma: the measurements
% t: the sequence of the associated values
% tStep: the time step for integrating forward
%
% Output:
% distSqrSum: the sum of square distances
% 
% Author: Yi Hong
% Date: 10/02/2013
%
function distSqrSum = sumDistOfPointsToGeodesic(Y0, Y0dot, t0, oma, t, nStep )

maxT = max(t);
minT = min(t);

% integrate to the boundary of time range
[YendForward] = integrateForwardToGivenTime(Y0, Y0dot, maxT-t0, nStep);
[YendBackward] = integrateForwardToGivenTime(Y0, Y0dot, minT-t0, nStep);
        
% compute the sum of the square distances
distSqrSum = 0;
for iK = 1:length(t)
    if t(iK) >= t0
        if t(iK) == maxT
            oma_t = YendForward{end};
        else
            pos = min( round( (t(iK) - t0) / nStep ) + 1, length(YendForward) );
            oma_t = YendForward{pos};
        end
    else
        if t(iK) == minT
            oma_t = YendBackward{end};
        else
            pos = min( round( (t0 - t(iK)) / nStep ) + 1, length(YendBackward) );
            oma_t = YendBackward{pos};
        end
    end
    distSqrSum = distSqrSum + grarc( oma{iK}, oma_t );
end
