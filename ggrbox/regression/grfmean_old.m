% geodesic regression based on Frechet mean
% input: observability matrix sequences (oma)
%        regression variable, like time (t)
% output: initial point (Y0)
%         initial velocity (Y0dot)
%         variable value at initial point (t0)

function [Y0, Y0dot, t0, id0] = grfmean( oma, t )

strTmp = sprintf('Compute geodesic of %d objects using within-sample Frechet mean', length(t) );
disp( strTmp );

% find the within-sample Frechet mean
dist = zeros(length(t), length(t));
for iI = 1:length(t)
    for iJ = 1:length(t)
        dist(iI, iJ) = grarc( oma{iI}, oma{iJ} );
    end
end
[~, idx] = min(sum(dist, 2));
strTmp = sprintf( 'Frechet mean -- id: %d, time: %f', idx, t(idx) );
disp(strTmp);

% average the initial velocity
tMean = t(idx);
vel = {};
for iI = 1:length(t)
    %[~, vel{iI}] = grgeo_direct( oma{idx}, oma{iI}, 1 );
    [~, ~, ~, vel{iI}] = grgeo( oma{idx}, oma{iI}, 1, 'v3', 'v2' );
end
avgVel = averageVelocity( vel, t, tMean );

Y0 = oma{idx};
Y0dot = avgVel;
t0 = tMean;
id0 = idx;
