% local adapted subspace
function [SLocalAdapted, dataMeanAdapted] = localTwoPointsGeodesicRegression(subSpace, dataMean, xYear, xTest) 

[~, indClosest] = sort( abs(xYear - xTest) );
assert( length(indClosest) >= 3 );
if xYear(indClosest(1)) == xTest
    indStart = indClosest(2);
    indEnd = indClosest(3);
else
    indStart = indClosest(1);
    indEnd = indClosest(2);
end
xYearStart = xYear(indStart);
omaStart = subSpace{indStart};
xYearEnd = xYear(indEnd);
omaEnd = subSpace{indEnd};
SLocalAdapted = extendFromTwoPoints( omaStart, omaEnd, xYearStart, xYearEnd, xTest );

dataMeanStart = dataMean{indStart};
dataMeanEnd = dataMean{indEnd};
if xYearStart == xYearEnd
    dataMeanAdapted = dataMeanStart;
else
    dataMeanAdapted = (dataMeanEnd - dataMeanStart) / (xYearEnd - xYearStart) * (xTest - xYearStart) + dataMeanStart;
end

% test the distance among start / end points and the estimated points
fprintf('Time points -- start: %d, end: %d, estimate: %d', xYearStart, xYearEnd, xTest);
fprintf('Distance -- start to end: %.3f, start to estimate: %.3f, end to estimate: %.3f', ...
    sqrt(grarc(omaStart, omaEnd)), sqrt(grarc(omaStart, SLocalAdapted)), sqrt(grarc(omaEnd, SLocalAdapted)));
