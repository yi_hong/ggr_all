% compute the angle between subspaces
nType = 1;  % 1: people, 2: traffic

switch nType
    case 1
        inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714/people_3Ms/results_nt200';   
        namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep100_state10_wsid1_sigma100_alpha0.00_sigmasqr1.00_linesearch_weightedData1_dir1_Init0_ds0.25_Part';
        nPartition = 4;
        filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix, 2, nPartition);
        tmp = load(filename);
        minT = min(tmp.cntAvg);
        maxT = max(tmp.cntAvg);
    case 2
        inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/traffic_3Ms_200_300';
        namePrefix1 = 'traffic_RAW_state10_train202_ds1.00_wsi0_linesearch_weightData0_Part';
        namePrefix2 = 'traffic_RAW_state10_train203_ds1.00_wsi0_linesearch_weightData0_Part'; 
        nPartition = 5;
        filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix2, 5, nPartition);
        tmp = load(filename);
        minT = min(tmp.speed);
        maxT = max(tmp.speed);
    otherwise
        error('Unknown types');
end

% get the initial conditions of full regression
indFullGGR = 2;
Y0 = tmp.initPnt{indFullGGR}{1};
Y0dot = tmp.initVel{indFullGGR}{1};
t0 = tmp.tPos{2}(1);

% integrate forward and backward
if t0 == minT
    Ybegin = Y0;
else
    [tTmp, pntYs] = integrateForwardWithODE45(Y0, Y0dot, [t0 minT]);
    Ybegin = pntYs{end};
end

if t0 == maxT
    Yend = Y0;
else
    [tTmp, pntYs] = integrateForwardWithODE45(Y0, Y0dot, [t0 maxT]);
    Yend = pntYs{end};
end

% compute the angles
[~,S,~] = svd(Ybegin'*Yend);
theta = acos(diag(S));
    