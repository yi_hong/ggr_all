How to generate testing shape data
----------------------------------

1) generateShapesMain.m : generate shapes along geodesic using srivastava's dataset. The generated shape will be stored in "shapes.mat", and used in ../shapeDyn/nlds_test_yi.m .

Note: There is a variable, nTotal, to control the number of frames generated for each subject, it is not used. If you want to generate the exact number of frames, uncomment the last line in generatePeriodicShapes.m .

2) generateSasakiTestDAta.m : generate the input data using shapes.mat for testing sasaki metric.