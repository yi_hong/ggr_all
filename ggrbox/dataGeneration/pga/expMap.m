function [yx yy] = expMap( inputVx,inputVy,baseX, baseY,stepsize, eps)
    norm_x2 = sum(baseX.^2 + baseY.^2);
    
    theta_expmap = stepsize*sqrt(sum(inputVx.^2+inputVy.^2));
    
    
    factor = ((sqrt(norm_x2)*sin(theta_expmap))/theta_expmap);
    factor(abs(theta_expmap)<eps)=0;
    
    yx= cos(theta_expmap)*baseX + factor*stepsize*inputVx;
    yy = cos(theta_expmap)*baseY + factor*stepsize*inputVy;
