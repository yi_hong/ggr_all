%loadhands;
%generatetriangles;
loadCCshapes;
[SxPreshape SyPreshape] = projecttopreshape (Sx, Sy);

targetX = SxPreshape(1,:); targetY = SyPreshape(1,:);

%movingX = SxPreshape(20,:); movingY = SyPreshape(20,:);
theta=pi/3;
rotationmat = [cos(theta) sin(theta);-sin(theta) cos(theta)];
rotatedx= rotationmat*[targetX ;targetY];
movingX = rotatedx(1,:);movingY = rotatedx(2,:);

[alignedX alignedY thetafound] = orginaryprocrustesalign(targetX, targetY, movingX,movingY);

figure; hold on; plot(targetX,targetY);plot(movingX,movingY,'g'); plot(alignedX,alignedY,'r'); axis equal;hold off;
legend('target','moving','aligned');