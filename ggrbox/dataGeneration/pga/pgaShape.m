function [U S V compXs compYs] = pgaShape(InputX, InputY, meanShapeX, meanShapeY,pltflg)
    [n d] = size(InputX);
    eps=0.0001;
    InputX = InputX - repmat(mean(InputX,2),[ 1 d]);
    InputY = InputY - repmat(mean(InputY,2),[ 1 d]);
    
    % get vi's using log map
    [Vx Vy] = logMap( InputX,InputY,meanShapeX, meanShapeY,eps);
    
    % pga: pca in tangent space
    shapeData = [Vx Vy];
    covarShape = cov(shapeData);
    [U S V] = svd(covarShape);
    eigVals = diag(S);
    normeigVals = eigVals;
    normeigVals(eigVals==0)=[];
    normeigVals = cumsum(normeigVals)./sum(normeigVals);
    figure; plot(normeigVals);title('eigen values');
    
       
    figure; title('PGA');
    for i=1:4
        comp = V(:,i)';
        [compX compY] = expMap( comp(1:d),comp(d+1:2*d),meanShapeX, meanShapeY,1,eps);
        subplot(2,2,i);plot(compX,compY); title(['principal geodesic: ' num2str(i)]);
    end;

    figure;
    maxEv=3;
    i=1;    
    for ev=1:maxEv    
        for s=-2:2        
            subplot(maxEv,5,i);i=i+1;
            PCshape=V(:,ev)';
            alpha = s*sqrt(eigVals(ev));
            [compX compY] = expMap( PCshape(1:d),PCshape(d+1:2*d), meanShapeX, meanShapeY, alpha, eps);
            plot(compX,compY,'-*'); title(['shooting along geodesic: ' num2str(ev)]);
            
            if pltflg == 1
               plot(compX,compY,'-*'); title(['shooting along geodesic: ' num2str(ev)]); axis equal;axis([2*(min(meanShapeX)) 2*(max(meanShapeX)) 2*(min(meanShapeY)) 2*(max(meanShapeY))]);
            elseif pltflg==2
               line([compX compX(1)] ,[compY compY(1)]); title(['shooting along geodesic: ' num2str(ev)]); axis equal;axis([2*(min(meanShapeX)) 2*(max(meanShapeX)) 2*(min(meanShapeY)) 2*(max(meanShapeY))]);
            else
               plot(compX,compY,'x'); title(['shooting along geodesic: ' num2str(ev)]); axis equal;axis([2*(min(meanShapeX)) 2*(max(meanShapeX)) 2*(min(meanShapeY)) 2*(max(meanShapeY))]);
            end
        
            
            if s==1
                compXs(ev,:) = compX;compYs(ev,:) = compY;
            end;
        end    
    end    

    
ev=1;
h = figure; 
s=-2;
PCshape=V(:,ev)';
alpha = s*sqrt(eigVals(ev));
[compX compY] = expMap( PCshape(1:d),PCshape(d+1:2*d), meanShapeX, meanShapeY, alpha, eps);
%p=plot(compX ,compY, '-*'); axis equal;axis([2*(min(meanShapeX)) 2*(max(meanShapeX)) 2*(min(meanShapeY)) 2*(max(meanShapeY))]);

            if pltflg == 1
               p=plot(compX,compY,'-*');  axis equal;axis([2*(min(meanShapeX)) 2*(max(meanShapeX)) 2*(min(meanShapeY)) 2*(max(meanShapeY))]);
            elseif pltflg==2
               p=line([compX compX(1)] ,[compY compY(1)]); axis equal;axis([2*(min(meanShapeX)) 2*(max(meanShapeX)) 2*(min(meanShapeY)) 2*(max(meanShapeY))]);
            else
               p=plot(compX,compY,'x'); axis equal;axis([2*(min(meanShapeX)) 2*(max(meanShapeX)) 2*(min(meanShapeY)) 2*(max(meanShapeY))]);
            end


title(['evolving along PC: ' num2str(ev)]); 
while true
    for s=-2:0.2:inf            
        PCshape=V(:,ev)';
        alpha = s*sqrt(eigVals(ev));
        [compX compY] = expMap( PCshape(1:d),PCshape(d+1:2*d), meanShapeX, meanShapeY, alpha, eps);
        if pltflg == 2
            set(p,'xdata',[compX compX(1)]);set(p,'ydata',[compY compY(1)]);
        else
            set(p,'xdata',compX);set(p,'ydata',compY);
        end;        
        drawnow;            
        pause(0.2);
    end     
    %{
    for s=2:-0.2:-2            
        PCshape=V(:,ev)';
        alpha = s*sqrt(eigVals(ev));
        [compX compY] = expMap( PCshape(1:d),PCshape(d+1:2*d), meanShapeX, meanShapeY, alpha, eps);
        set(p,'xdata',compX);set(p,'ydata',compY);
        drawnow;            
        pause(0.2);
    end         
    %}
end