close all;
%generatetriangles; pltflg=2;
loadCCshapes; pltflg=3;
%loadhands; pltflg=1
[SxPreshape SyPreshape] = projecttopreshape (Sx, Sy, pltflg);
maxIter = 5;
[templateSx templateSy Sxaligned Syaligned theta] = generalizedprocrustesalign(SxPreshape, SyPreshape, maxIter,pltflg);

%[U S V meanShape compXs compYs] = pcaShape( Sxaligned, Syaligned, pltflg);

% Ideally should be frechet mean
[U S V compXs compYs] = pgaShape(Sx, Sy, templateSx, templateSy, pltflg);