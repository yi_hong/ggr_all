function [SxPreshape SyPreshape] = projecttopreshape (Sx, Sy, pltflg)

[n d] = size(Sx);
%%
% STEP 1: transaltion
% computing centroids
centroidsX = mean(Sx,2); centroidsY = mean(Sy,2);
% mod translations i.e., make zero centered
transSx = Sx - repmat(centroidsX,[1 d]);
transSy = Sy - repmat(centroidsY,[1 d]);

% STEP 2: scale
% make scale=1
normS = sqrt(sum((transSx.^2+transSy.^2),2));
SxPreshape = diag(1./normS)*transSx;
SyPreshape = diag(1./normS)*transSy;


figure; h1=subplot(1,2,1);title('raw shapes'); axis equal;
h2=subplot(1,2,2);title('objects in pre-shape space');axis equal; 

if pltflg==1
        subplot(h1);plot(Sx',Sy');axis equal; subplot(h2);plot(SxPreshape',SyPreshape');axis equal; 
elseif pltflg==2
        subplot(h1);line([Sx Sx(:,1)]',[Sy Sy(:,1)]');axis equal; subplot(h2);line([SxPreshape SxPreshape(:,1)]',[SyPreshape SyPreshape(:,1)]');axis equal;
else
        subplot(h1);plot(Sx,Sy, 'x');axis equal;subplot(h2);plot(SxPreshape,SyPreshape, 'x');axis equal; 
end