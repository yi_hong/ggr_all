%%
[n d] = size(Sx);
figure; subplot(2,2,1);hold on;plot(Sx(1,:),Sy(1,:),'x');plot(Sx(20,:),Sy(20,:),'rx'); axis equal;hold off;
title('original shapes');
%%
% STEP 1: transaltion
% computing centroids
centroidsX = mean(Sx,2); centroidsY = mean(Sy,2);

% mod translations i.e., make zero centered
transSx = Sx - repmat(centroidsX,[1 d]);
transSy = Sy - repmat(centroidsY,[1 d]);
subplot(2,2,2);hold on;plot(transSx(1,:),transSy(1,:),'x');plot(transSx(2,:),transSy(2,:),'rx');axis equal; hold off;
title('translated shapes');

%%
% STEP 2: scale
% least square metric

% choose a template
templateSx = transSx(1,:); templateSy = transSy(1,:);

% rescale all shapes to be "equal to size" to this template. 
scales = (transSx*templateSx' + transSy*templateSy')./ (sum(transSx.^2,2)+sum(transSy.^2,2));
scaledSx = diag(scales)*transSx;
scaledSy = diag(scales)*transSy;
subplot(2,2,3);hold on;plot(scaledSx(1,:),scaledSy(1,:),'x');plot(scaledSx(20,:),scaledSy(20,:),'rx'); axis equal;hold off;
title('scaled shapes');
%% 
% STEP 3: rotation

% calculate optimal rotation required to align each shape to the template
xtemplate = [templateSx' templateSy'];
rotatedSx = zeros(n,d);rotatedSy = rotatedSx;

for j=1:n
    xj = [scaledSx(j,:)' scaledSy(j,:)'];
    [U S V]= svd(xj'*xtemplate);
    rotationj = V*U';
    % apply rotation;
    rotatedxj= rotationj*xj';
    rotatedSx(j,:) = rotatedxj(1,:); rotatedSy(j,:) = rotatedxj(2,:);
end;

subplot(2,2,4);hold on;plot(rotatedSx(1,:),rotatedSy(1,:),'x');plot(rotatedSx(20,:),rotatedSy(20,:),'rx'); axis equal;hold off;
title('rotated shapes');

figure; plot(Sx',Sy','x'); title('All shapes plotted raw');
figure; plot(rotatedSx',rotatedSy','x'); title('All shapes plotted after procrustes alignment');