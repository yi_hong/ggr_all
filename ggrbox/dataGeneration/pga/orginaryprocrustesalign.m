function [alignedX alignedY theta] = orginaryprocrustesalign(targetX, targetY, movingX,movingY)

% compute rotation
xtarget = [targetX' targetY'];
xmoving = [movingX' movingY'];
[U S V]= svd(xmoving'*xtarget);
rotationmat = V*U';
theta = acos(rotationmat(1,1));

% apply rotation;
rotatedx= rotationmat*xmoving';
alignedX = rotatedx(1,:); alignedY = rotatedx(2,:);