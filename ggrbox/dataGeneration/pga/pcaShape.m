function [U S V meanShape compXs compYs] = pcaShape( finalSx, finalSy ,pltflg)
[n d] = size(finalSx);
shapeData = [finalSx finalSy];
meanShape = mean(shapeData);

covarShape = cov(shapeData); % or use covarShape = (shapeData-repmat(meanShape,[n 1]))'*(shapeData - repmat(meanShape,[n 1]))./(n-1);
[U S V] = svd(covarShape, 0);
eigVals = diag(S);
normeigVals = eigVals;
normeigVals(eigVals==0)=[];
normeigVals = cumsum(normeigVals)./sum(normeigVals);
figure; plot(normeigVals);title('eigen values');

figure; title('Linear PCA');
for i=1:4
    comp(i,:) = V(:,i)';
    subplot(2,2,i);plot(comp(i,1:d),comp(i,d+1:2*d)); title(['principal component: ' num2str(i)]);
end;

figure;

i=1;    
maxEv=3;
for ev=1:maxEv    
    for s=-2:2        
        subplot(maxEv,5,i);i=i+1;
        PCshape=meanShape + s*sqrt(eigVals(ev))*V(:,ev)';
        if pltflg == 1
            plot(PCshape(1:d),PCshape(d+1:2*d),'-*'); title(['evolving along PC: ' num2str(ev)]); axis equal;axis([2*(min(meanShape(1:d))) 2*(max(meanShape(1:d))) 2*(min(meanShape(d+1:2*d))) 2*(max(meanShape(d+1:2*d)))]);
        elseif pltflg==2
            line([PCshape(1:d) PCshape(1)],[PCshape(d+1:2*d) PCshape(d+1)]); title(['evolving along PC: ' num2str(ev)]); axis equal;axis([2*(min(meanShape(1:d))) 2*(max(meanShape(1:d))) 2*(min(meanShape(d+1:2*d))) 2*(max(meanShape(d+1:2*d)))]);
        else
            plot(PCshape(1:d),PCshape(d+1:2*d),'x'); title(['evolving along PC: ' num2str(ev)]); axis equal;axis([2*(min(meanShape(1:d))) 2*(max(meanShape(1:d))) 2*(min(meanShape(d+1:2*d))) 2*(max(meanShape(d+1:2*d)))]);
        end
            if s==1
                compXs(ev,:) = PCshape(1:d);compYs(ev,:) = PCshape(d+1:2*d);
            end;
    end    
end  


%animation
ev=1;
h = figure; 
s=-2;
PCshape=meanShape + s*sqrt(eigVals(ev))*V(:,ev)';        


        if pltflg == 1
            p=plot(PCshape(1:d),PCshape(d+1:2*d),'-*');            
        elseif pltflg==2            
            p=line([PCshape(1:d) PCshape(1)],[PCshape(d+1:2*d) PCshape(d+1)]); 
        else
            p=plot(PCshape(1:d),PCshape(d+1:2*d),'x'); 
        end
        


axis equal;axis([2*(min(meanShape(1:d))) 2*(max(meanShape(1:d))) 2*(min(meanShape(d+1:2*d))) 2*(max(meanShape(d+1:2*d)))]);
title(['evolving along PC: ' num2str(ev)]); 
while true
    for s=-2:0.2:2
        PCshape=meanShape + s*sqrt(eigVals(ev))*V(:,ev)';                
        if pltflg == 2
            set(p,'xdata',[PCshape(1:d) PCshape(1)]);set(p,'ydata',[PCshape(d+1:2*d) PCshape(d+1)]);
        else
            set(p,'xdata',PCshape(1:d));set(p,'ydata',PCshape(d+1:2*d));
        end;
        drawnow;            
        pause(0.2);
    end            
    
    for s=2:-0.2:-2            
        PCshape=meanShape + s*sqrt(eigVals(ev))*V(:,ev)';                
        if pltflg == 2
            set(p,'xdata',[PCshape(1:d) PCshape(1)]);set(p,'ydata',[PCshape(d+1:2*d) PCshape(d+1)]);
        else
            set(p,'xdata',PCshape(1:d));set(p,'ydata',PCshape(d+1:2*d));
        end;
        drawnow;            
        pause(0.2);
    end         
    
end

