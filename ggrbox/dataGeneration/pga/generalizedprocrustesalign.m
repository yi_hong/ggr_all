function [templateSx templateSy Sxaligned Syaligned theta] = generalizedprocrustesalign(SxPreshape, SyPreshape, maxIter,pltflg)
[n d] = size(SxPreshape);

% initialize the template
templateSx = SxPreshape(1,:); templateSy = SyPreshape(1,:);
figure; h1=subplot(1,2,1);title('aligned shapes'); axis equal;
h2=subplot(1,2,2);title('mean shape');axis equal; 


Sxaligned=SxPreshape;
Syaligned=SyPreshape;

theta=[];
for i=1:maxIter
    %align all shapes to template
    for j=1:n
        [Sxaligned(j,:) Syaligned(j,:) theta(i,j)] = orginaryprocrustesalign(templateSx, templateSy, Sxaligned(j,:), Syaligned(j,:));
    end;
    
    %recompute mean from aligned Shapes and update the template
    templateSx = mean(Sxaligned); templateSy = mean(Syaligned);    
    if pltflg==1
        subplot(h1);plot(Sxaligned',Syaligned');axis equal; subplot(h2);plot(templateSx,templateSy);axis equal;
    elseif pltflg==2
        subplot(h1);line([Sxaligned Sxaligned(:,1)]',[Syaligned Syaligned(:,1)]');axis equal; subplot(h2);line([templateSx templateSx(:,1)],[templateSy templateSy(:,1)]);axis equal;
    else
        subplot(h1);plot(Sxaligned,Syaligned, 'x');axis equal; subplot(h2);plot(templateSx,templateSy,'x');axis equal;    
    end
    
end;



