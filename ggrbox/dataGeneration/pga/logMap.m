function [Vx Vy] = logMap( inputX,inputY,baseX, baseY,eps)
    [n d] = size(inputX);
    x = [baseX' + baseY'*1i];
    norm_x = norm(x);
    for j=1:n
        y = [inputX(j,:)' + inputY(j,:)'*1i];
        inner_y_x = x'*y;
        pi_x_y = x*inner_y_x./(norm_x^2);
        
        theta  = acos( abs(inner_y_x) / (norm_x*norm(y)));
        missingpart = inner_y_x'/abs(inner_y_x);
        
        temp = (y - pi_x_y);
        v  = theta * (temp/norm(temp))*missingpart ;
        
        vx = real(v);
        vy = imag(v);
        if abs(theta)<eps
            vx = zeros(1,56);            
            vy = zeros(1,56);            
        end;
        Vx(j,:) = vx';Vy(j,:) = vy';        
    end;