clear all
close all

addpath(genpath('.././'));

grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/data/srivastava';
grParams.name = 'Srivastava.mat';
grParams.id = [254 498 187];
grParams.nTotal = 300;

iK = 0;
for iI = 10:10:30
    iK = iK + 1;
    grParams.nPt = iI;
    grParams.filename = sprintf('shapes%d.gif', grParams.nPt);
    [shapes{iK}] = generatePeriodicShapes(grParams);
end
save('shapes.mat', 'shapes');
