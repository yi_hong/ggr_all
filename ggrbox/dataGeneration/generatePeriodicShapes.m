% toy data generation

function [newShapes] = generatePeriodicShapes(grParams)

% load data
inputName = sprintf('%s/%s', grParams.inputPrefix, grParams.name);
tmp = load(inputName);
data = tmp.C;

id = grParams.id;
if length(id) <= 1
    error('At least two sample shapes');
end
if size(id, 1) > size(id, 2)
    id = id';
end
idTmp = fliplr(id);
idTmp = [id idTmp(2:end-1)];
nNum = ceil(((grParams.nTotal - 1) / (grParams.nPt-1) + 1)/length(idTmp));
id = repmat(idTmp, [1 nNum]);
%nNum = ceil((grParams.nTotal - 1) / (grParams.nPt-1) + 1);
%id = id(1:nNum);

nNum = find(id == id(1));
id = id(1:nNum(end));

subSpace = cell(length(id), 1);
scale = zeros(length(id), 2);

figure;
for iI = 1:length(id)
    if iI <= length(grParams.id)
        subplot(1, length(grParams.id), iI), plot(data(1, :, id(iI)), data(2, :, id(iI)));
    end
    tmpData = data(:, :, id(iI))';
    meanData = mean(tmpData);
    tmpData = tmpData - repmat(meanData, [size(tmpData, 1), 1]);
    [U, S, ~] = svd(tmpData, 0);
    subSpace{iI} = U(:, 1:2);
    tmpS = diag(S);
    scale(iI, :) = tmpS(1:2);
end
scaleMean = mean(scale);

% geodesic between two shapes
nData = grParams.nPt;
nShapes = (length(id)-1)*(nData-2) + length(id);
newShapes = cell(nShapes, 1);
newSpaces = cell(nShapes, 1);
timepoints = linspace(0, 1, nData);

figure(100), 
iK = 0;
for iI = 1:length(id)-1
    if iI == 1
        startSpace = subSpace{iI};
    else
        startSpace = newSpaces{iK};
    end
    for iJ = 1:length(timepoints)
        if iI > 1 && iJ == 1 
            % skip the starting point at the middle
            continue;
        end
        iK = iK + 1;
        newSpaces{iK} = extendFromTwoPoints(startSpace, subSpace{iI+1}, ...
            timepoints(1), timepoints(end), timepoints(iJ));
        newShapes{iK} = newSpaces{iK} * diag(scaleMean);
        plot(newShapes{iK}(:, 1), newShapes{iK}(:, 2));
        axis equal
        xlim([-250 250]);
        ylim([-250 250]);
        drawnow 
        frame = getframe(100);
        im = frame2im(frame);
        [imind, cm] = rgb2ind(im, 256);
        if iK == 1
            imwrite(imind, cm, grParams.filename, 'gif', 'Loopcount', inf);
        else
            imwrite(imind, cm, grParams.filename, 'gif', 'WriteMode', 'append');
        end
    end
end

%newShapes = newShapes(1:grParams.nTotal);