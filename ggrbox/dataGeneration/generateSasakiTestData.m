% save data for sasaki test
clear all
close all

load('shapes.mat');
dataName = 'shapeDataInput.mfd';
timeName = 'shapeTimeInput.mfd';

N = length(shapes);
figure;
M = 10;
k = 0;
dataX = [];
dataY = [];
age = [];
id = [];
for i=1:N
    data = shapes{i};
    nCount = 0;
    for j=1:length(data)
        %nCount = nCount + 1;
        %if nCount > M
        %    break;
        %end
        dataMat = data{j}';
        dataX = [dataX; dataMat(1, :)];
        dataY = [dataY; dataMat(2, :)];
        age = [age 0.1*(j-1)];
        id = [id i];
        %k = k+1;
        %subplot(N, M, k), plot(dataMat(1, :), dataMat(2, :));
    end
end
age = age ./ max(age);

pltflg = 1;
[SxPreshape, SyPreshape] = projecttopreshape(dataX, dataY, pltflg);
maxIter = 1;
[~, ~, SxAligned, SyAligned, ~] = generalizedprocrustesalign(SxPreshape, SyPreshape, maxIter, pltflg);

saveMFD(SxAligned, SyAligned, age, id, dataName, timeName)