function data = gen_spline_synthetic_data( param )

    % Example:
    % param.seed = 1234;
    % param.nTotalNum = 20;
    % param.fNoise = 0.3;
    % param.sNoise = 0.1;

    rng(param.seed);
    step=0.05; 
    x = 0:step:10*pi;

    % Frequencies within (0, 10)
    data.minBoundary = 0.2;
    data.maxBoundary = 9.8;
    data.tt = linspace(data.minBoundary, data.maxBoundary, param.nTotalNum);
    data.ft = 3*(sin(data.tt/6)+1.5);

    data.pDim = 24;
    data.nSta = 2;
    data.kObs = 2;

    % Linear projector into 24-D
    [CC, ~, ~] = svd(randn(data.pDim, data.nSta), 0);

    data.tru = cell(length(data.ft),1); % 24-d signals (truth)
    data.sig = cell(length(data.ft),1); % 24-d signals
    data.sys = cell(length(data.ft),1); % LDS
    data.Y_c = cell(length(data.ft),1); % Finite observ. matrices (cell array)
    data.Z_c = cell(length(data.ft),1); % Finite observ. matrices (cell array)    
    data.org = cell(length(data.ft),1); % Original signal

    % System identification
    data.fn = zeros(length(data.ft),1);
    useWeightedSystemIdentification = false;
    for i=1:length(data.ft)
        data.fn(i) = data.ft(i) + param.fNoise*randn();     
        s = [sin(data.fn(i)*x); cos(data.fn(i)*x)]; % using noisy frequency  
        o = [sin(data.ft(i)*x); cos(data.ft(i)*x)]; % using clean frequency
        y = CC*s; % in 24-D space
        z = CC*o; % in 24-D space
        tmp = rand(size(y));
        y = y + param.sNoise*tmp; % noise in 24-D space
        z = z + param.sNoise*tmp; % noise in 24-D space
        data.sig{i} = y;
        data.org{i} = z;
        sysID_params.class = 2;

        if ( ~useWeightedSystemIdentification )
            weights = ones( 1, size( y, 2 ) );
            weights = weights/sum(weights); % normalize
        else
            gVal = linspace( -3, 3, size( y,2 ) );
            weights = exp( -gVal.^2 );
            weights = weights/sum(weights); % normalize
        end
        data.tru{i} = weighted_suboptimal_systemID(data.org{i},data.nSta,weights,sysID_params);
        data.sys{i} = weighted_suboptimal_systemID(data.sig{i},data.nSta,weights,sysID_params);
        data.Y_c{i} = omat(data.sys{i}, data.kObs);
        data.Z_c{i} = omat(data.tru{i}, data.kObs);
    end
end