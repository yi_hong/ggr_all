function readMFD()

filename = '/Users/yihong/Dropbox/geodesicanalysis/Applications/test.mfd';
outputname = 'sasakiEsimatedShapes.gif';
fid = fopen(filename, 'r');
tline = fgets(fid);
dFlag = 0;
figure(100);
while 1
    if tline == -1
        break;
    end
    if strcmp(tline(1:3), 'dim')
        dim = sscanf(tline, '%*s %*s %d %d');
    elseif strcmp(tline(1:3), 'num')
        [num] = sscanf(tline, '%*s %*s %d');
    elseif strcmp(tline(1:4), 'data')
        dFlag = 1;
    elseif dFlag >= 1
        data = reshape(str2num(tline), dim(1), dim(2));
        plot(data(1, :), data(2, :));
        axis equal
        pause(0.2);
        frame = getframe(100);
        im = frame2im(frame);
        [imind, cm] = rgb2ind(im, 256);
        if dFlag == 1
            imwrite(imind, cm, outputname, 'gif', 'Loopcount', inf);
        else
            imwrite(imind, cm, outputname, 'gif', 'WriteMode', 'append');
        end
        dFlag = dFlag + 1;
    end
    tline = fgets(fid);
end
fclose(fid);