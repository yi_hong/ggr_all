function saveMFD(dataX, dataY, age, id, dataName, timeName)

fid = fopen(dataName, 'wt');

fprintf(fid, 'type=KendallShapeSpace\n');
fprintf(fid, 'dimParameters=%d %d\n', 2, size(dataX, 2));
fprintf(fid, 'rep=Matrix\n');
fprintf(fid, 'numPoints=%d\n', length(id));
fprintf(fid, 'format=text\n');
fprintf(fid, 'data:\n');

for iI = 1:size(dataX, 1)
    for iJ = 1:size(dataX, 2)
        fprintf(fid, '%f %f ', dataX(iI, iJ), dataY(iI, iJ)); 
        if mod(iJ, 4) == 0
            fprintf(fid, '\n');
        end
    end
    if mod(size(dataX, 2), 4) == 0
        fprintf(fid, '\n');
    else
        fprintf(fid, '\n\n');
    end
end

fclose(fid);

fid = fopen(timeName, 'wt');
for iI = 1:length(id)
    fprintf(fid, '%d %f\n', id(iI), age(iI));
end
fclose(fid);