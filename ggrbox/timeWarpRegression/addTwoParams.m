% add two groups of parameters
% paramsSum = params1 + ratio * params2

function [paramsSum] = addTwoParams( params1, params2, ratio )

paramsSum.A = params1.A + ratio * params2.A;
paramsSum.K = params1.K + ratio * params2.K;
paramsSum.beta = params1.beta + ratio * params2.beta;
paramsSum.k = params1.k + ratio * params2.k;
paramsSum.M = params1.M + ratio * params2.M;
paramsSum.m = params1.m + ratio * params2.m;