% derivative of the logistic function with respect to its parameters
% at time point ti

function [devParams] = computeDerivativeLogisticFunction( params, ti )

% do not change the asymptotes 
devParams.A = 0;
devParams.K = 0;

% compute for other parameters
tmp0 = exp(-params.k * (ti - params.M) );
tmp1 = params.beta * tmp0;
tmp2 = (1+tmp1)^(-1.0/params.m);
tmp3 = (params.K - params.A) * (-1.0/params.m) * (1+tmp1)^(-1.0/params.m-1);

devParams.beta = tmp3 * tmp0;
devParams.k = tmp3 * tmp1 * (params.M - ti);
devParams.M = tmp3 * tmp1 * params.k;
devParams.m = (params.K - params.A) * tmp2 * log(tmp1) / (params.m^2);

% since we fix beta and m
devParams.beta = 0;
devParams.m = 0;