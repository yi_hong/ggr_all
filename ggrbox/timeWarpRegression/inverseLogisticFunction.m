% the inverse of the generalised logistic function

function [t] = inverseLogisticFunction( yt, params )

tmp1 = (params.K - params.A) ./ (yt - params.A);
tmp2 = log( (1.0 / params.beta) .* ( tmp1.^params.m - 1) );
t = -1.0 ./ params.k .* tmp2 + params.M;