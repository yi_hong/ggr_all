%
% time warped regression
% Author: Yi Hong
% Email: yihong@cs.unc.edu
% 

function [Y0, Y0dot, currentEnergy, tsWarped, timeWarpParams] = timeWarpedRegressionOnGrassmannManifold( params )

% initialize the time-warp parameters
[timeWarpParamsInit] = initializeTimeWarpParams( params );
figure(params.figIdLogisticFunc), plot( sort(params.ts), generalisedLogisticFunction(sort(params.ts), timeWarpParamsInit), 'b*-', 'LineWidth', 2);
hold on
plot( timeWarpParamsInit.M*[1 1], [0 generalisedLogisticFunction( timeWarpParamsInit.M, timeWarpParamsInit ) ], 'b--', 'LineWidth', 2 );
hold off
title('Generalised Logistical Funciton');

if strcmp( params.twMinFunc, 'linesearch' )
    [Y0, Y0dot, currentEnergy, tsWarped, timeWarpParams] = lineSearchTimeWarpRegression( params, timeWarpParamsInit );

elseif strcmp( params.twMinFunc, 'fmincon' )
    
    % set global values
    global pntY0_preStep
    global tanY0_preStep
    pntY0_preStep = params.Ys{1};
    tanY0_preStep = zeros(size(params.Ys{1}));
    
    theta0 = param2theta( timeWarpParamsInit );
    
    lb = [-Inf -Inf -Inf 0 0 -Inf];
    ub = [Inf Inf Inf Inf Inf Inf];
    optOpts = loadTimeWarpRegressionOptimizerOptions(params);
    [theta] = fmincon( @(theta)energyAndGradientTimeWarpRegression( theta, params ), [theta0], ...
        [], [], [], [], lb, ub, [], optOpts.fmincon );
    
    timeWarpParams = theta2param( theta );

    % 1) compute new timepoints 
    tsWarped = generalisedLogisticFunction(params.ts, timeWarpParams);

    % 2) compute standard geodesic regression
    paramsFullGGR = params;
    paramsFullGGR.ts = tsWarped;
    paramsFullGGR.pntY0 = params.Ys{1};
    paramsFullGGR.tanY0 = zeros(size(params.Ys{1}));
    paramsFullGGR.pntT0 = min(paramsFullGGR.ts);
    [Y0, Y0dot, currentEnergy] = fullRegressionOnGrassmannManifold( paramsFullGGR );

else
    error( 'Not supported optimization method' );
end

figure(params.figIdLogisticFunc), hold on;
plot( sort(params.ts), generalisedLogisticFunction(sort(params.ts), timeWarpParams), 'r*-', 'LineWidth', 2);
plot( timeWarpParams.M*[1 1], [0 generalisedLogisticFunction( timeWarpParams.M, timeWarpParams ) ], 'r--', 'LineWidth', 2 );
hold off
