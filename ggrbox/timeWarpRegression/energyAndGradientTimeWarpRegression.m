function [energy, gradient] = energyAndGradientTimeWarpRegression( theta, params )

global pntY0_preStep
global tanY0_preStep

timeWarpParams = theta2param( theta );

% 1) compute new timepoints 
tsWarped = generalisedLogisticFunction(params.ts, timeWarpParams);
    
% 2) compute standard geodesic regression
paramsFullGGR = params;
paramsFullGGR.ts = tsWarped;
paramsFullGGR.timeWarpRegression = true;

% initialize with zero velocity
%paramsFullGGR.pntY0 = params.Ys{1};
%paramsFullGGR.tanY0 = zeros(size(params.Ys{1}));
%paramsFullGGR.pntT0 = min(paramsFullGGR.ts);

% or initialize with results of previous steps
paramsFullGGR.pntY0 = pntY0_preStep;
paramsFullGGR.tanY0 = tanY0_preStep;
paramsFullGGR.pntT0 = min(paramsFullGGR.ts);

[Y0, Y0dot, energy] = fullRegressionOnGrassmannManifold( paramsFullGGR );

% save the results of previous steps
pntY0_preStep = Y0;
tanY0_preStep = Y0dot;

% 3) compute the gradient for time warp parameters
[tsWarpedSort, indSort] = sort(tsWarped);
YsSort = params.Ys(indSort);
tsSort = params.ts(indSort);
inpFlag = false;
tSpan = unique(tsWarpedSort);
assert( length(tSpan) >= 2 );
if length(tSpan) == 2 
    tSpan = [tSpan(1) (tSpan(1)+tSpan(2))/2.0 tSpan(2)];
    inpFlag = true;
end
[tsGeo, YsGeo, YDotsGeo] = integrateForwardWithODE45( Y0, Y0dot/(max(tsWarped) - min(tsWarped)), tSpan );
if inpFlag
    tsGeo(2) = [];
    YsGeo(2) = [];
    YDotsGeo(2) = [];
end

% plot the points and geodesic
if isfield( params, 'pDim' )
    figure(params.figIdFitting), plotGeodesic( params.Ys, params.ts, params.pDim, 'o' );
    hold on
    plotGeodesic( YsGeo, inverseLogisticFunction(tsGeo, timeWarpParams), params.pDim, '--' );
    if isfield(params, 't_truth_org') && isfield(params, 'Y_truth')
        plotGeodesic( params.Y_truth, params.t_truth_org, params.pDim, '-' );
    end
    hold off
end

gradTimeWarpParams = resetTimeWarpParams();
for iTs = 1:length(YsSort)
    indGeo = find( tsGeo == tsWarpedSort(iTs) );
    [~, ~, ~, vTmp] = grgeo( YsGeo{indGeo}, YsSort{iTs}, 1, 'v3', 'v2' );
    trTmp = 2 * trace( vTmp' * YDotsGeo{indGeo} ) / (-params.sigmaSqr);
    [devParams] = computeDerivativeLogisticFunction( timeWarpParams, tsSort(iTs) );
    gradTimeWarpParams = addTwoParams( gradTimeWarpParams, devParams, trTmp );
end

gradient = param2theta( gradTimeWarpParams );