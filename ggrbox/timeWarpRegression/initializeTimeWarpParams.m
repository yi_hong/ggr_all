% initialize the time warp parameters based on input time

function [params] = initializeTimeWarpParams ( inputParams )

tsMin = min(inputParams.ts);
tsMax = max(inputParams.ts);

% the fixed number
params.A = 0.0;
params.K = 1.0;
params.beta = 1.0;
params.m = 1.0;

% the two adjustable parameters
tbar1 = 0.1;
tbar2 = 0.9;
b1 = log( 1.0 / tbar1 - 1 );
b2 = log( 1.0 / tbar2 - 1 );

params.k = (b1 - b2) / (tsMax - tsMin);
params.M = (b1 + params.k * tsMin) / params.k;

if isfield( inputParams, 'k' )
    params.k = inputParams.k;
end

if isfield( inputParams, 'M' )
    params.M = inputParams.M;
end