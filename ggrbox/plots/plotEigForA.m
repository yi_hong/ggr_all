% plot the eigenvalue of the original and synthetic A matrix

function plotEigForA( sys, A, num )

figure;
hold on
cmap = cool(num);
for i = 1:num
    plotEig( sys{i}.A, A{i}, cmap(i, :) );
end
tCircle = 0:0.1:2*pi;
plot(sin(tCircle), cos(tCircle), 'g-');
hold off
axis equal
colormap(cmap);
colorbar
xlim([-1, 1]);
ylim([-1, 1]);

    function plotEig( A, B, clrA )

    AEig = eig(A);
    BEig = eig(B);

    hold on 
    plot( real(AEig), imag(AEig), 'o', 'Color', clrA );
    plot( real(BEig), imag(BEig), 'b*' );

    legend('Original', 'Synthetic');
    end

end