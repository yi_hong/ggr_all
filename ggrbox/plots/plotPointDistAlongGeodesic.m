% for each measurement to find it's projection
function plotPointDistAlongGeodesic( num, tInit, avgVelInit, Geo, omac, t )

dStep = 0.1;
tEst = min(t)-10*dStep:dStep:max(t)+10*dStep;
dist = zeros(num, length(tEst));
for i = 1:num
    for j = 1:length(tEst)
        oma_j = Geo{i}.Q * expm((tEst(j)-tInit)*avgVelInit) * Geo{i}.J;
        dist(i, j) = grarc( oma_j, omac{i} );
    end
end

cmap = cool(num);
figure;
hold on
for i = 1:num
    plot( dist(i, :), 'Color', cmap(i, :) );
    [minValue, minId] = min( dist(i, :) );
    plot( minId, minValue, 'ro' );
    strTmp = sprintf('(%d, %.1f, %.1f, %.3f)', i, t(i), tEst(minId), minValue );
    text( minId, minValue-0.005, strTmp );
    strTmp = sprintf('Time Diff: %f, Geodesic dist: %f at minimum', abs(t(i)-tEst(minId)), minValue);
    disp(strTmp);
end
ylim([-0.01, max(max(dist))]);
hold off
colormap(cmap);
colorbar