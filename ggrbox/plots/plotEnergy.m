% plot the energy 

inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_042214/car_3Ms2';
nPartition = 7;
namePrefix = 'car_decaf_train6_linesearch_weightData0_Part';

energy = [];
for iI = 1:nPartition
    filename = sprintf('%s/%s%dOf%d.mat', inputPrefix, namePrefix, iI, nPartition');
    tmp = load(filename);
    energy = [energy; tmp.totEnergy];
end

figure, boxplot(energy);
set(gca, 'Xtick', 1:3, 'XTickLabel', {'Pointwise', 'Full GGR', 'Piecewise'});