%--------------------------------------------------------------------------
% Plot the geodesic on the Grassmann manifold using the independent value 
% and the eigenvalue of the A matrix.
%--------------------------------------------------------------------------
function plotGeodesic( YPnts, ts, pDim, lineStyle )

    [sz1, nSta] = size(YPnts{1});
    kObs = floor( sz1 / pDim );

    assert( kObs * pDim == sz1 );

    realvalue = zeros( length(YPnts), 1 );
    imagvalue = zeros( length(YPnts), 1 );
    for iI = 1:length(YPnts)
        [A, ~] = estimate_dynamic_matrix( YPnts{iI}, pDim, nSta, kObs );
        eigvalue = eig(A);
        realvalue(iI) = real(eigvalue(1));
        imagvalue(iI) = imag(eigvalue(1));
    end

    plot( ts, realvalue, lineStyle, 'LineWidth', 2, 'Color', 'r' );
    hold on
    plot( ts, imagvalue, lineStyle, 'LineWidth', 2, 'Color', 'b' );
    legend( 'Real part', 'Imaginary part' );
end


