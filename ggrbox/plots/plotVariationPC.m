% plot the eigenvalues
function plotVariationPC(A)

eigvalue = diag(A);
eigvalue = eigvalue ./ sum(eigvalue);
eigvalue = cumsum(eigvalue);
plot(eigvalue, 'b*-', 'LineWidth', 2 );

grid on