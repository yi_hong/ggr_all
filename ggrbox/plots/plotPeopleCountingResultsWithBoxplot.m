% plot the results
%close all
clear all

%inputPrefix = '../results/people_final/raw_2_fold';
%namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep200_state10_wsid0_alpha0.00_sigmasqr1.00_linesearch_weightedData0_ds0.10_dir1_Part';

inputPrefix = '../results/people_final/raw';
%namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep200_state10_wsid0_alpha0.00_sigmasqr1.00_linesearch_weightedData0_ds0.10_dir1_Part';
namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep100_state10_wsid0_alpha0.00_sigmasqr1.00_linesearch_weightedData0_ds0.25_dir1_Part';

minValue = 15;
maxValue = 40;
nPartition = 4;

regressMethod = {'Pairwise Search', 'Full Regression', 'Piecewise Regression'};

nRows = nPartition + 1;
nCols = 3;
maxEstErr = 0;
figure;
for iI = 1:nPartition
    filename = sprintf('%s/%s%d.mat', inputPrefix, namePrefix, iI);
    tmp = load(filename);
    cntAvg = tmp.cntAvg;
    cntEst = tmp.cntEst;
    nTraining = tmp.grParams.nSampling;
    nTesting = tmp.grParams.nTesting;
    
    clear absErrTrain absErrTest
    for iJ = 1:length(cntEst)
        subplot( nRows, nCols, (iI-1)*length(cntEst)+iJ ), hold on;
        plot(cntAvg(nTraining), cntEst{iJ}(nTraining), 'r*', 'MarkerSize', 5);
        plot(cntAvg(nTesting), cntEst{iJ}(nTesting), 'bo', 'Markersize', 5);
        plot(minValue:maxValue, minValue:maxValue, 'm--', 'LineWidth', 1);
        hold off
        axis equal
        xlim([minValue maxValue]);
        ylim([minValue maxValue]);
        grid on
        xlabel('Truth');
        ylabel('Prediction');
        %legend('Training', 'Testing');
        if iI == 1
            title( regressMethod{iJ} );
        end
        set(gca, 'FontSize', 24);
        absErrTrain(iJ, :) = abs( cntAvg(nTraining) - cntEst{iJ}(nTraining) ); %./ cntAvg(nTraining);
        absErrTest(iJ, :) = abs( cntAvg(nTesting) - cntEst{iJ}(nTesting) ); %./ cntAvg(nTesting);
        absErr{iI}.absErrTrain = absErrTrain;
        absErr{iI}.absErrTest = absErrTest;
        absErr{iI}.nTrain = length(nTraining);
        absErr{iI}.nTest = length(nTesting);
        maxEstErr = max( maxEstErr, max( max(absErrTrain(iJ, :)), max(absErrTest(iJ, :)) ) );
    end
end

for iJ = 1:length(cntEst)
    subplot(nRows, nCols, (nRows-1)*nCols+iJ), hold on;
    nTrain = 0;
    nTest = 0;
    for iI = 1:nPartition
         nTrain = nTrain + absErr{iI}.nTrain;
         nTest = nTest + absErr{iI}.nTest;
    end
    err = repmat(NaN, max(nTrain, nTest), (nPartition+1)*2);
    startTrainId = 0;
    startTestId = 0;
    meanErr = zeros( (nPartition+1)*2, 1 );
    for iI = 1:nPartition
        err(1:absErr{iI}.nTrain, (iI-1)*2+1) = absErr{iI}.absErrTrain(iJ, :);
        err(1:absErr{iI}.nTest, (iI-1)*2+2) = absErr{iI}.absErrTest(iJ, :);
        err(startTrainId+1:startTrainId+absErr{iI}.nTrain, nPartition*2+1) = absErr{iI}.absErrTrain(iJ, :);
        err(startTestId+1:startTestId+absErr{iI}.nTest, nPartition*2+2) = absErr{iI}.absErrTest(iJ, :);
        startTrainId = startTrainId + absErr{iI}.nTrain;
        startTestId = startTestId + absErr{iI}.nTest;
        meanErr((iI-1)*2+1) = mean(absErr{iI}.absErrTrain(iJ, :));
        meanErr((iI-1)*2+2) = mean(absErr{iI}.absErrTest(iJ, :));
    end
    meanErr(end-1) = mean(err(1:startTrainId, end-1));
    meanErr(end) = mean(err(1:startTestId, end));
    
    hold on
    strTmp = {};
    for iI = 1:nPartition
        yAxis = 0:maxEstErr;
        xAxis = ones(size(yAxis)) * iI * 2 + 0.5;
        plot( xAxis, yAxis, 'Color', [0.5 0.5 0.5], 'LineWidth', 1 );
        strTmp{(iI-1)*2+1} = sprintf('  Fold %d', iI);
        strTmp{(iI-1)*2+2} = '';
        
    end
    strTmp{nPartition*2+1} = 'Total';
    strTmp{nPartition*2+2} = '';
    
    boxplot(err, 'colors', repmat('rb', 1, (nPartition+1)*2), 'labels', strTmp);
    ylim([0 maxEstErr]);
    %set(gca, 'XTickLabel', {'1', '', '2', '', '3', '4'});
    
    for iI = 1:length(meanErr)
        str = sprintf('%.2f', meanErr(iI));
        text( iI-0.5, maxEstErr*(0.8+mod(iI,2)*0.1), str );
    end
    box off
    grid on
    set(gca, 'FontSize', 24);
end
