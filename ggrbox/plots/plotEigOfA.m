% plot the eigenvalue of A matrix

function plotEigOfA( A )

assert(length(size(A)) == 2 && size(A, 1) == size(A, 2));
eigenvalue = eig(A);
hold on;
plot( real(eigenvalue), imag(eigenvalue), 'bo', 'MarkerSize', 10 );
tCircle = 0:0.1:2*pi;
plot(sin(tCircle), cos(tCircle), 'g-');
hold off
grid on
axis equal
