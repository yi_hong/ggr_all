% plot the results
close all
clear all
nType = 2;              % 1: people, 2: traffic, 3: amy, 4: corpus callosum
saveResults = false;

% font size
fz = 20;

if nType == 1
    
    % some parameters need to set before running
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/people_3Ms_100_300';
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714/people_dict_3Ms';
    inputDataName = '/Users/yihong/Desktop/work/Project/GGR_data_results/data/ucsdpeds_feats/features/Peds1_feats.mat';
    
    %inputResultsPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_011714/people_3Ms';
    %inputDataName = '/playpen/Project/dynamicalSystem/GGR_data_results/data/ucsdpeds_feats/features/Peds1_feats.mat';
    
    %namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep100_state10_wsid1_sigma100_alpha0.00_sigmasqr1.00_linesearch_weightedData1_dir1_Init0_ds0.25_Part';
    %namePrefix = 'counting_DICT_start1_end4000_winsize400_winstep100_state10_wsid1_sigma100_alpha0.00_sigmasqr1.00_linesearch_weightedData1_dir1_Init0_max_row_code_s_Part';
    
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/people_3Ms_200_300';
    %namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep100_state10_wsid1_sigma100_alpha0.00_sigmasqr1.00_linesearch_weightedData0_dir1_Init0_ds0.25_Part';
    
    % GGR: crowd, CS-GGR: crowd2, GGR-PW: crowd5
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/crowd2';
    %namePrefix = 'counting_RAW_start1_end4000_winsize400_winstep100_state10_wsid1_sigma100_alpha0.00_sigmasqr1.00_linesearch_weightedData0_dir1_ds0.25_Part';
    
    % baseline
    inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/CrowdBaseline';
    namePrefix = 'crowd_baseline_nn_Part';
    
    % load Antoni's prediction for comparison
    tmpAntoni = load('peds1_results.mat');
   
    minValue = 15;
    maxValue = 40;
    nPartition = 4;
    
    % read parameters setting from the file
    filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix, 1, nPartition);
    tmp = load(filename);
    params = tmp.grParams;
    
    nTotalNum = params.nSys;
    nWinStep = params.nWinStep;
    nFrameStart = params.nFrameStart;
    nWinSize = params.nWinSize;
    useWeightedSysid = params.useWeightedSysid;
    
	% read groud-truth from the file
    tmp =  load(inputDataName);
	counts = tmp.cnt{end};                % the total number in each frame
    
    % plot the antoni's results
    
    figure, hold on
    plot( counts, 'r-' );
    plot( 1:1400, tmpAntoni.count_1to1400_pred, 'm--' );
    plot( 2601:4000, tmpAntoni.count_2600to4000_pred, 'm--' );
    plot( 1:1400, tmpAntoni.count_1to1400_true, 'b-.' );
    plot( 2601:4000, tmpAntoni.count_2600to4000_true, 'b-.' );
    hold off
    AntoniPrediction = [tmpAntoni.count_1to1400_pred; counts(1401:2600)'; tmpAntoni.count_2600to4000_pred];
    AntoniPredForComp = zeros( nTotalNum, 1 );
    
    % compute the ground truth for plot
    groundTruth = zeros( nTotalNum, 2 );   % 1st column: true values, 2st column: standard deviation
    nFrameStart = 1;
    for iI = 1:nTotalNum
        idStart = (iI-1)*nWinStep + nFrameStart;
        idEnd = (iI-1)*nWinStep + nFrameStart + nWinSize - 1;
        
        if ~useWeightedSysid
            groundTruth(iI, 1) = mean(counts(idStart:idEnd));
            groundTruth(iI, 2) = std(counts(idStart:idEnd));
            AntoniPredForComp(iI) = mean(AntoniPrediction(idStart:idEnd));
        else
            weights = gaussmf( 1:nWinSize, [params.sigmaWeightedSysid round(nWinSize/2)] );
            weights = weights ./ sum(weights);
            nNum = length( find( weights ~= 0 ) );
            groundTruth(iI, 1) = sum( counts(idStart:idEnd) .* weights );
            groundTruth(iI, 2) = sqrt( sum( weights .* ( counts(idStart:idEnd) - groundTruth(iI, 1) ).^2 ) * nNum / (nNum-1) );
            AntoniPredForComp(iI) = sum( AntoniPrediction(idStart:idEnd)' .* weights );
        end
    end

elseif nType == 2
        
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714_duke/traffic_3Ms_100_300';
    % full GGR
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/traffic';
    % CS-GGR
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/traffic8';
    % piecewise
    %inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/traffic7';
    
    
    namePrefix1 = 'traffic_RAW_state10_train202_ds1.00_wsi0_linesearch_weightData0_Part';
    namePrefix2 = 'traffic_RAW_state10_train203_ds1.00_wsi0_linesearch_weightData0_Part'; 
    
    % baseline
    inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/TrafficBaseline';
    namePrefix1 = 'traffic_baseline_nn_Part';
    namePrefix2 = namePrefix1;

    minValue = 10;
    maxValue = 70;
    nPartition = 5;
    
    % read parameters
    filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix1, 1, nPartition);
    tmp = load(filename);
    params = tmp.grParams;
    nTotalNum = params.nSys;
    
elseif nType == 3
    inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714/amy_AR';
    namePrefix = 'amy_wd0_Part';
    minValue = 0;
    maxValue = 300000;
    nPartition = 12;
    
    % read parameters
    filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix, 1, nPartition);
    tmp = load(filename);
    params = tmp.grParams;
    nTotalNum = params.nSys;
    
elseif nType == 4
    inputResultsPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714/corpus_callosum';
    namePrefix = 'corpus_callosum_Part';
    minValue = 0;
    maxValue = 100;
    nPartition = 32;
    
    % read parameters
    filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix, 1, nPartition);
    tmp = load(filename);
    params = tmp.grParams;
    nTotalNum = params.nSys;
end

% regression
regressMethod = params.regressMethod;

% last dimension: 1-3 (training), 4-6 (testing)
absErr2 = zeros( nPartition, length(regressMethod), 6 );
predictions = zeros( nTotalNum, length(regressMethod));    

% energy for each training
energy = zeros( nPartition, length(regressMethod) );

for iI = 1:nPartition
    % people or amy 
    if nType == 1 || nType == 3 || nType == 4
        filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix, iI, nPartition);
    else
        % traffic
        if iI <= 3
            filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix1, iI, nPartition);
        else
            filename = sprintf('%s/%s%dOf%d.mat', inputResultsPrefix, namePrefix2, iI, nPartition);
        end
    end
    tmp = load(filename);
    %tmp.totEnergy
    %tmp.tPos{2}
    
    if nType == 1
        cntAvg = tmp.cntAvg;
        cntEst = tmp.cntEst;
        assert( norm(cntAvg - groundTruth(:, 1)) == 0 );
    elseif nType == 2
        cntAvg = tmp.speed;
        cntEst = tmp.speedEst;
    elseif nType == 3
        cntAvg = tmp.fibro;
        cntEst = tmp.fibroEst;
    elseif nType == 4
        cntAvg = tmp.ages;
        cntEst = tmp.ageEst;
    end
    nTraining = tmp.grParams.nSampling;
    nTesting = tmp.grParams.nTesting;
    
    if nType ~= 1
        groundTruth(:, 1) = cntAvg;
    end
    %assert( sum(cntAvg == groundTruth(:, 1)) == length(cntAvg) );

    % energy
    if isfield(tmp, 'totEnergy')
        energy(iI, :) = tmp.totEnergy;
    end
    
    % for each regression method
    for iJ = 1:length(cntEst)
        
        % each testing group
        predictions(nTesting, iJ) = cntEst{iJ}(nTesting);
             
        % training
        absErr2(iI, iJ, 1) = sum( abs( cntEst{iJ}(nTraining) - cntAvg(nTraining) ) );  % sum of the absolute error
        absErr2(iI, iJ, 2) = length(nTraining);                                        % the number of training data
        absErr2(iI, iJ, 3) = absErr2(iI, iJ, 1) ./ absErr2(iI, iJ, 2);                 % mean absolute error of a folder
        
        % testing
        absErr2(iI, iJ, 4) = sum( abs( cntEst{iJ}(nTesting) - cntAvg(nTesting) ) );    % sum of the absolute error
        absErr2(iI, iJ, 5) = length(nTesting);                                         % the number of testing data
        absErr2(iI, iJ, 6) = absErr2(iI, iJ, 4) ./ absErr2(iI, iJ, 5);                 % mean absolute error of a folder
    end
end

% compute absolute mean error over folders
absMeanErr = zeros( size(absErr2, 2), 2 );     % last dimension: training and testing
% standard deviations
stdErr = zeros( size(absErr2, 2), 2 );         % last dimension: training and testing
for iJ = 1:size(absMeanErr, 1)
    % training
    absMeanErr(iJ, 1) = sum( absErr2(:, iJ, 1) ) ./ sum( absErr2(:, iJ, 2) );   % sum over folders / total number
    stdErr(iJ, 1) = std( absErr2(:, iJ, 3) );
    % testing
    absMeanErr(iJ, 2) = sum( absErr2(:, iJ, 4) ) ./ sum( absErr2(:, iJ, 5) );   % sum over folders / total number
    stdErr(iJ, 2) = std( absErr2(:, iJ, 6) );
end

% plot the error bar
figure, barwitherr( stdErr, absMeanErr, 'BarWidth', 0.8);
barmap = [0.3 0.3 1.0; 1.0 0.3 0.3];
colormap(barmap);
legend('Train', 'Test');
%xticklabel_rotate(1:length(regressMethod), 45, regressMethod);
box off
grid on
set(gca, 'FontSize', fz, 'xTickLabel', regressMethod);
if nType == 1
    titlename = sprintf('Antoni mean absolute error: %f', mean( abs(AntoniPredForComp - groundTruth(:, 1)) ) );
    title(titlename);
end

style = {'k+', 'b+', 'g+', 'k.', 'cp'};

% plot correlation, only testing
figure, hold on;
for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( predictions(:, iJ), groundTruth(:, 1), style{iJ} );
    plot( minValue:maxValue, minValue:maxValue, 'k--', 'LineWidth', 2 );
    % least sqaure fit
    %pValue = polyfit( groundTruth(:, 1), predictions(:, iJ), 1);
    %xTmp = 0:max(groundTruth(:, 1));
    %plot( xTmp, pValue(1)*xTmp+pValue(2), 'k--' );
    axis equal
    xlim([minValue maxValue]);
    ylim([minValue maxValue])
    grid on
    xlabel('Predictions');
    ylabel('Ground truth');
end

% plot groud-truth and prediction
figure, hold on;
switch nType
    case 1
        for iJ = 1:size(predictions, 2)
            subplot(1, size(predictions, 2), iJ), hold on;
            h=fill( [1:size(groundTruth, 1), fliplr(1:size(groundTruth,1))], ...
                    [(groundTruth(:, 1)-groundTruth(:, 2))', fliplr((groundTruth(:, 1)+groundTruth(:, 2))')], [0.85 0.85 0.85]);
            set(h, 'EdgeColor', 'None');
            numberInside = sum( ( predictions(:, iJ) >= groundTruth(:, 1) - groundTruth(:, 2) ) & ...
                                ( predictions(:, iJ) <= groundTruth(:, 1) + groundTruth(:, 2) ) );
            titleName = sprintf('%s energy: %f \n %d / %d inside: %f', regressMethod{iJ}, mean(energy(:, iJ)), ...
                                numberInside, size(predictions, 1), numberInside / size(predictions, 1));
            title(titleName);
        end
    case 2
        [groundTruth(:, 1), idSort] = sort(groundTruth(:, 1));
        for iJ = 1:size(predictions, 2)
            subplot(1, size(predictions, 2), iJ), hold on;
            predictions(:, iJ) = predictions(idSort, iJ);
            titleName = sprintf('%s energy: %f', regressMethod{iJ}, mean(energy(:, iJ)));
            title(titleName);
        end
    case 3
        [groundTruth(:, 1), idSort] = sort(groundTruth(:, 1));
        for iJ = 1:size(predictions, 2)
            subplot(1, size(predictions, 2), iJ), hold on;
            predictions(:, iJ) = predictions(idSort, iJ);
            titleName = sprintf('%s energy: %f', regressMethod{iJ}, mean(energy(:, iJ)));
            title(titleName);
        end
    case 4
        [groundTruth(:, 1), idSort] = sort(groundTruth(:, 1));
        for iJ = 1:size(predictions, 2)
            subplot(1, size(predictions, 2), iJ), hold on;
            predictions(:, iJ) = predictions(idSort, iJ);
            titleName = sprintf('%s energy: %f', regressMethod{iJ}, mean(energy(:, iJ)));
            title(titleName);
        end
    otherwise
        error('Not supported');
end

for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( predictions(:, iJ), style{iJ}, 'LineWidth', 2);
    plot( groundTruth(:, 1), 'r-', 'LineWidth', 2 );
end
hold off

if saveResults
    switch nType
        case 1
            filename = sprintf('%s/crowd_counting_prediction_w%d.mat', inputResultsPrefix, params.useWeightedData);
            save(filename, 'groundTruth', 'predictions', 'absMeanErr', 'stdErr');
        case 2
            filename = sprintf('%s/traffic_speed_prediction.mat', inputResultsPrefix);
            save(filename, 'groundTruth', 'predictions', 'absMeanErr', 'stdErr');
        case 3
            filename = sprintf('%s/amy_fibrob_prediction.mat', inputResultsPrefix);
        otherwise
            error('Not supported.');
    end
end