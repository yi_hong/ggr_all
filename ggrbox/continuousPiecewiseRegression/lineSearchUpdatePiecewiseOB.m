% line search update for continuous piecewise regression with optimal breakpoints
function [pntY, tanYs, bkPnts, currentEnergy] = lineSearchUpdatePiecewiseOB( pntY0, tanYs0, bkPnts0, params )

pntY = pntY0;
tanYs = tanYs0;
bkPnts = bkPnts0;

% save the input parameters
paramsIn = params;

params.bks = bkPnts;
params = updateGroupInfo(params);

% integrate forward
[X1, X2, Y_RK] = integrateForwardPiecewise( pntY, tanYs, params );

% compute energy
[currentEnergy, X1s] = computeEnergyPiecewise( X1, X2, params );
fprintf( 'Continuous Piecewise GGR with Optimal Breakpoints (CPGGROB)\n' );
fprintf( 'CPGGROB Intial energy = %f\n', currentEnergy );

% plot the energy
figure(600), hold on;
clr = rand(1, 3);
eng_pre = currentEnergy;
figure(600), plot( 0, eng_pre, 'p', 'LineWidth', 4, 'Color', clr );
title('Continuous Piecewise Regression with Optimal Breakpoints Energy');
xlabel('Iteration');
ylabel('Energy');

% plot geodesic
if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
    h = figure(100);
    clf(h);
    hold on
    plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
    plotGeodesic( X1s, params.ts_pre, params.pDim, '--' );
    drawnow
    hold off
end

% plot the optimal breakpoints
figure(1000), hold on;
bks_pre = bkPnts0;
eng_pre = currentEnergy;
clr1 = rand(1, 3);
% plot the starting point
for iTmp = 1:length(bks_pre)
    bks_pre(iTmp) = bks_pre(iTmp) * (max(paramsIn.ts) - min(paramsIn.ts)) + min(paramsIn.ts);
    figure(1000), plot( bks_pre(iTmp), eng_pre, 'p', 'LineWidth', 4, 'Color', clr1 );
end
drawnow
title( 'Breakpoints Optimization' );
xlabel('Breakpoints');
ylabel('Energy');

stopIteration = false;
for iI = 1:params.nIterMax
    % integrate backward
    [gradientX10, gradientX20s, lambdas] = integrateBackwardAndUpdateGradientPiecewise(X1, X2, X1s, Y_RK, params);
    
    % compute the gradient for the breakpoints
    for iJ = 1:length(lambdas)
        gradientBks(iJ) = trace( lambdas{iJ}' * (X2{iJ}{end} - X2{iJ+1}{1}) );
    end
    
    % time step size
    dt = params.deltaT;
    energyReduced = false;
    
    % store the current status
    curPntY0 = pntY;
	curTanYs = tanYs;
    curBkPnts = bkPnts;
    
    % change the positive gradient to negative
    gradientX10 = -gradientX10;
    % make the gradientX10 to be tangent to the point
    gradientX10 = gradientX10 - curPntY0 * (curPntY0' * gradientX10);
    for iJ = 1:params.maxReductionSteps
		
        % update breakpoints
        bValid = true;
        for iK = 1:length(bkPnts)
            bkPnts = curBkPnts(iK) - dt * gradientBks(iK);
            if bkPnts(iK) < 0 || bkPnts(iK) > 1
                bValid = false;
                break;
            end
        end
        if ~bValid 
            fprintf('#');
            dt = dt * params.rho;
            continue; 
        end
        
		% update the initial point 
        % integrate forward
        if params.useODE45
            [~, Ytmp] = integrateForwardWithODE45( curPntY0, gradientX10, [0 dt/2 dt] );
        else
            [Ytmp] = integrateForwardToGivenTime( curPntY0, gradientX10, dt, params.h );
        end
        pntY = Ytmp{end};  % updated initial point
        
        if sum(sum(isnan(pntY))) > 0 
            fprintf('#');
            dt = dt * params.rho;
            continue; 
        end
        
		% update the initial velocity at the initial point 
		tanYs{1} = curTanYs{1} - dt * gradientX20s{1};
        % make sure the velocity is tangent to the point
        tanYs{1} = tanYs{1} - curPntY0 * (curPntY0' * tanYs{1});
        % parallel transport 
		tanYs{1} = ptEdelman(curPntY0, gradientX10, tanYs{1}, dt);
        % project to the new point
		tanYs{1} = tanYs{1} - pntY * (pntY' * tanYs{1});	

		% update the initial velocity at the breakpoints
        Y0_tmp = pntY;
        tmpNewPntatBk = cell(length(curTanYs)-1, 1);
		for iK = 2:length(curTanYs)
            % integrate forward 
            [X1_tmp, ~] = integrateForwardMP( Y0_tmp, tanYs{iK-1}, params.Gns(iK-1), params.h );
            % the initial point at the breakpoint
            Y0_tmp = X1_tmp{end};
            
            % update the initial velocity
			tanYs{iK} = curTanYs{iK} - dt * gradientX20s{iK};
            % make sure the velocity is tangent to the point
            tanYs{iK} = tanYs{iK} - X1{iK}{1} * ((X1{iK}{1})' * tanYs{iK});
            % paralle transport
            [~, ~, ~, vTmp] = grgeo(X1{iK}{1}, Y0_tmp, 1, 'v3', 'v2');
            tanYs{iK} = ptEdelman(X1{iK}{1}, vTmp, tanYs{iK}, 1);
            %tanYs{iK} = parallelTransport( tanYs{iK}, X1{iK}{1}, Y0_tmp, dt, params.useODE45 );
            % project to the breakpoint
			tanYs{iK} = tanYs{iK} - Y0_tmp * (Y0_tmp' * tanYs{iK});
            
            tmpNewPntatBk{iK-1} = Y0_tmp;
        end
        
        params = paramsIn;
        params.bks = bkPnts;
        params = updateGroupInfo(params);
        
        % transport the updated the velocity because the breakpoints
        % changed
        
        % update the initial velocity at the breakpoints
        Y0_tmp = pntY;
		for iK = 2:length(tanYs)
            % integrate forward 
            [X1_tmp, ~] = integrateForwardMP( Y0_tmp, tanYs{iK-1}, params.Gns(iK-1), params.h );
            % the initial point at the breakpoint
            Y0_tmp = X1_tmp{end};
           
            % paralle transport
            [~, ~, ~, vTmp] = grgeo(tmpNewPntatBk{iK-1}, Y0_tmp, 1, 'v3', 'v2');
            tanYs{iK} = ptEdelman(tmpNewPntatBk{iK-1}, vTmp, tanYs{iK}, 1);
            %tanYs{iK} = parallelTransport( tanYs{iK}, X1{iK}{1}, Y0_tmp, dt, params.useODE45 );
            % project to the breakpoint
			tanYs{iK} = tanYs{iK} - Y0_tmp * (Y0_tmp' * tanYs{iK});
        end
        
        % integrate forward in time
        [X1, X2, Y_RK] = integrateForwardPiecewise( pntY, tanYs, params );
        
        if sum(sum(isnan(X1{end}{end}))) + sum(sum(isnan(X2{end}{end}))) > 0 
            fprintf('#');
            dt = dt * params.rho;
            continue; 
        end

        % compute energy
        [energy, X1s] = computeEnergyPiecewise( X1, X2, params );
        
        if energy >= currentEnergy
            fprintf('#');
        else
            % energy decreased
            currentEnergy = energy;
            energyReduced = true;
            fprintf( 'CPGGROB Iter %04d: Energy = %f\n', iI, currentEnergy );
            figure(600), plot( [iI-1 iI], [eng_pre currentEnergy], '-o', 'LineWidth', 2, 'Color', clr );
            drawnow
            for iTmp = 1:length(bkPnts)
                bks_cur(iTmp) = bkPnts(iTmp) * (max(paramsIn.ts)-min(paramsIn.ts)) + min(paramsIn.ts);
                figure(1000), plot( [bks_pre(iTmp) bks_cur(iTmp)], [eng_pre currentEnergy], '-s', 'LineWidth', 2, 'Color', clr1 );
            end
            drawnow
            
            if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
                h = figure(100);
                clf(h);
                hold on
                plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
                plotGeodesic( X1s, params.ts_pre, params.pDim, '--' );
                drawnow
                hold off
            end
            
            if eng_pre - currentEnergy < params.stopThreshold
                stopIteration = true;
            end
            
            bks_pre = bks_cur;
            eng_pre = currentEnergy;
            
            break;
        end
        dt = dt * params.rho;
    end
    
    if (~energyReduced)
        fprintf( 'CPGGROB Iteration %d: Could not reduce energy further. Quitting.\n', iI );
        pntY = curPntY0;
		tanYs = curTanYs;
        bkPnts = curBkPnts;
        break;
    end
    
    if stopIteration
        fprintf( 'Quitting because the descreased energy is smaller than the threshold.\n' );
        break;
    end
end

end

function [paramsNew] = updateGroupInfo( params )
% update when breakpoints change

% convert value to id
params.ts_pre = params.ts;
%params.bks = params.bks - params.ts(1);      % breakpoint is in [0, 1]
params.ts = params.ts - params.ts(1);
%params.bks = params.bks ./ params.ts(end);
params.ts = params.ts ./ params.ts(end);
params.ts = min( max( round(params.ts / params.h) + 1, 1 ), params.nt+1 );
params.bks = min( max( round(params.bks / params.h) + 1, 1 ), params.nt+1 );
params.nPieces = length(params.bks) + 1;

% divide the data into groups 
% for each point, find its belonging piece
params.YgIds = zeros(length(params.Ys), 1);
for iI = 1:length(params.Ys)
	iJ = 1;
	while iJ <= length(params.bks)
		if params.ts(iI) <= params.bks(iJ)
			break;
		end
		iJ = iJ + 1;
	end
	params.YgIds(iI) = iJ;
end

% for each piece, find its points
params.GyIds = cell(params.nPieces, 1);
for iJ = 1:length(params.GyIds)
	params.GyIds{iJ} = find( params.YgIds == iJ );
end
% for each piece, find its length
tmp = [1 params.bks params.nt+1];
params.Gns = tmp(2:end) - tmp(1:end-1);

paramsNew = params;

end