% 
% continuous piecewise regression with optimal breakpoints
% 
% Inputs: params (parameters for piecewise regression)
%         .Ys (measurements)
%         .ts (time points for each point)
%         .nBks (number of breakpoints)
% 
% Results: pntY  -- the initial point
%          tanYs -- a sequence of initial velocity at the 
%                   initial point and the breakpoints
%          bkPnts -- breakpoints
% 
% Author: yihong
% yihong@cs.unc.edu
% 

function [pntY, tanYs, bkPnts, currentEnergy] = continuousPiecewiseRegressionWithOptimalBreakpointsIterative( params )

% adjust the time points with the initial point
[params.ts, idSort] = sort( params.ts );
params.Ys = params.Ys(idSort);
params.wi = params.wi(idSort);

% initialize the breakpoints
if ~isfield(params, 'bks') || isempty(params.bks)
    tmp = linspace( params.ts(1), params.ts(end), params.nBks+2 );
    params.bks = tmp(2:end-1);
end

% initialize the starting point and the velocity with piecewise regression
if ~( isfield(params, 'Y0Init') && isfield(params, 'Y0dotInit') || ...
      isfield(params, 'pntY0s') && isfield(params, 'tanY0s') && isfield(params, 'pntT0s') || ...
      isfield(params, 'pntY0') && isfield(params, 'tanY0s') && isfield(params, 'pntT0s') )
    disp('Compute the piecewise regression for initialization');
    [params.pntY0s, params.tanY0s, params.pntT0s] = piecewiseRegression(params);
end
[pntY, tanYs, currentEnergy, gradientBks, pntYsGeo] = continuousPiecewiseRegression(params);
params.optimalBreakpoint = true;
fprintf( 'Continuous Piecewise GGR with Optimal Breakpoints (CPGGROB)\n' );
fprintf( 'CPGGROB Initial energy = %f\n', currentEnergy );

% plot geodesic
if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
    h = figure(100);
    clf(h);
    hold on
    plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
    plotGeodesic( pntYsGeo, params.ts, params.pDim, '--' );
    drawnow
    hold off
end

% plot the optimal breakpoints
figure(1000), hold on;
bks_pre = params.bks;
eng_pre = currentEnergy;
clr1 = rand(1, 3);
% plot the starting point
for iTmp = 1:length(bks_pre)
    figure(1000), plot( bks_pre(iTmp), eng_pre, 'p', 'LineWidth', 4, 'Color', clr1 );
end
drawnow
title( 'Breakpoints Optimization' );
xlabel('Breakpoints');
ylabel('Energy');

% plot the energy
figure(10000), hold on;
clr2 = rand(1, 3);
figure(10000), plot( 0, eng_pre, 'p', 'LineWidth', 4, 'Color', clr2 );
drawnow
title( 'Optimal Breakpoint Energy Optimization');
xlabel('Iteration');
ylabel('Energy');

% linesearch for breakpoint based on its gradient
stopIteration = false;
for iter = 1:params.nIterMaxBk
    
    % initial step for updating breakpoints
    dt = params.deltaTBk;
    energyReduced = false;
    
    curBks = params.bks;
    curGradient = gradientBks;
    curPntY = pntY;
    curTanYs = tanYs;
    for iJ = 1:params.maxReductionSteps
        % simplified version, update all the breakpoints with same step size
        bValid = true;
        for iI = 1:length(params.bks)
            params.bks(iI) = curBks(iI) - dt * curGradient(iI) * (max(params.ts) - min(params.ts));
            % breakpoint should be inside of the range
            if params.bks(iI) < min(params.ts) || params.bks(iI) > max(params.ts)
                bValid = false;
                break;
            end
        end
        
        % initialize the starting point and the velocity with piecewise regression
        if bValid
            %[params.pntY0s, params.tanY0s, params.pntT0s] = piecewiseRegression(params);
            
            % choose to initialize with the previsou initial conditions
            % instead of piecewise regression because of compution cost
            
            % this initialization is important, will have a look at it
            % again
            params.pntY0 = curPntY;
            params.tanY0 = curTanYs{1} ./ (max(params.ts) - min(params.ts));
            params.pntT0 = min(params.ts);
            %{
            params.pntY0 = curPntY;
            params.pntT0s = [min(params.ts) params.bks];
            params.tanY0s = cell(length(params.pntT0s), 1);
            params.tanY0s{1} = curTanYs{1} ./ (max(params.ts) - min(params.ts));
            for iTanTmp = 2:length(params.tanY0s)
                params.tanY0s{iTanTmp} = zeros(size(params.tanY0s{1}));
            end
            %}
            
            %{
            params.pntY0s{1} = pntY;
            params.pntT0s = [min(params.ts) curBks]; 
            for iTanYs = 1:length(tanYs)
                params.tanY0s{iTanYs} = tanYs{iTanYs} ./ (max(params.ts) - min(params.ts));
                if iTanYs < length(tanYs)
                    if params.useODE45
                        [~, Ytmp, ~] = integrateForwardWithODE45( params.pntY0s{iTanYs}, params.tanY0s{iTanYs}, ...
                            [params.pntT0s(iTanYs) (params.pntT0s(iTanYs)+params.pntT0s(iTanYs+1))*0.5 params.pntT0s(iTanYs+1)] );
                    else
                        [Ytmp, ~] = integrateForwardToGivenTime( params.pntY0s{iTanYs}, params.tanY0s{iTanYs}, ...
                            params.pntT0s(iTanYs+1)-params.pntT0s(iTanYs), (params.pntT0s(iTanYs+1)-params.pntT0s(iTanYs))/50.0 );
                    end
                    params.pntY0s{iTanYs+1} = Ytmp{end};
                end
            end
            %}
            [pntY, tanYs, energy, gradientBks, pntYsGeo] = continuousPiecewiseRegression(params);
        end
        
        if ~bValid || energy >= currentEnergy
            fprintf('#');
        else
            % energy decreased
            currentEnergy = energy;
            energyReduced = true;
            bkPnts = params.bks;
            fprintf( 'CPGGROB Iter %04d: Energy = %f\n', iter, currentEnergy );
            %disp(bkPnts);
            for iTmp = 1:length(bkPnts)
                figure(1000), plot( [bks_pre(iTmp) bkPnts(iTmp)], [eng_pre currentEnergy], '-s', 'LineWidth', 2, 'Color', clr1 );
            end
            drawnow
            figure(10000), plot( [iter-1 iter], [eng_pre currentEnergy], '-o', 'LineWidth', 2, 'Color', clr2 );
            drawnow
            
            if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
                h = figure(100);
                clf(h);
                hold on
                plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
                plotGeodesic( pntYsGeo, params.ts, params.pDim, '--' );
                drawnow
                hold off
            end
            
            if eng_pre - currentEnergy < params.stopThreshold
                stopIteration = true;
            end
            
            bks_pre = bkPnts;
            eng_pre = currentEnergy;
            
            break;
        end
        dt = dt * params.rho;
    end
   
    if (~energyReduced) 
        bkPnts = curBks;
        pntY = curPntY;
        tanYs = curTanYs;
        fprintf( 'CPGGROB Iteration %d: Could not reduce energy further. Quitting.\n', iter );
        break;
    end
    
    if stopIteration
        fprintf( 'CPGGROB Quitting because the decreased energy is smaller than the threshold.\n' );
        break;
    end
end

end


