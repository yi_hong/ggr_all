%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% compute full Hessian matrix for fullGGR

function [ Hessian ] = computeHessian( Y0, params )

%compute the first order forward and backward propagation
Y = Y0;
len = floor(size(Y, 1)/2);

% integrate forward in time
[X1, X2, Y_RK] = integrateForwardMP( Y(1:len, :), Y(len+1:end, :), params.nt, params.h );
X1s = cell(length(params.ts), 1);
for iI = 1:length(params.ts)
    X1s{iI} = X1{params.ts(iI)};
end

% integrate backward in time
lam1_end = zeros( size(X1{end}) );
lam2_end = zeros( size(lam1_end) );
[lam1, lam2, lam_RK] = integrateBackwardMP( lam1_end, lam2_end, Y_RK, X1s, params );


full_parameter_length = length(Y(:));
Hessian = zeros(full_parameter_length, full_parameter_length);
[m, p] = size(Y);
n = floor(m/2);
assert(n*2 == m);
sizeNP = n*p;
for i = 1:full_parameter_length
     
    curPos = i;
    iBase = 0;
    if i > sizeNP
        curPos = curPos - sizeNP;
        iBase = iBase + n;
    end
    iPos = mod(curPos-1, n) + 1 + iBase;
    jPos = floor((curPos-1)/n) + 1;
    
    delta_y = zeros(size(Y));
    delta_y(iPos, jPos) = 1;
    Hessian(:, i) = computeHessianVectorProduct(X1, X2, Y_RK, lam1, lam2, lam_RK, delta_y, X1s, params);
end

