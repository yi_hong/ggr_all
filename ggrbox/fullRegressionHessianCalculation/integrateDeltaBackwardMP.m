function [delta_lam1, delta_lam2] = integrateDeltaBackwardMP( delta_lam1_end, delta_lam2_end, lam_RK, delta_Y_RK, Y_RK, X1s, delta_X1s, params )
nt = params.nt;
h = -params.h;

delta_lam1_k = delta_lam1_end;
delta_lam2_k = delta_lam2_end;
delta_lam_k = [delta_lam1_k; delta_lam2_k];
delta_lam1 = {};
delta_lam2 = {};
nSize = size(delta_lam1_end, 1);

% 4th Runge-Kutta
for it = 1:nt
    id = find(params.ts == nt-it+2);
    for iI = 1:length(id)
        % compute jump for second order
        [hessDistSq] = computeJumpsNumerically(X1s{id(iI)}, delta_X1s{id(iI)}, params.Ys{id(iI)});
        if isfield(params, 'diagSigmaSqs')
%             matSigma = diag(1.0 ./ params.diagSigmaSqs);
%             delta_lam_k(1:nSize, :) = delta_lam_k(1:nSize, :) ...
%                 - reshape(matSigma * hessDistSq(:) / 2.0, size(hessDistSq)) * params.wi(id(iI)); % energy/2sigma^2
            delta_lam_k(1:nSize, :) = delta_lam_k(1:nSize, :) - computeJumpWithSigma(params.diagSigmaSqs{id(iI)}, hessDistSq);
        else
            delta_lam_k(1:nSize, :) = delta_lam_k(1:nSize, :) - hessDistSq / params.sigmaSqr * params.wi(id(iI));
        end
    end
    delta_lam1{it} = delta_lam_k(1:nSize, :);
    delta_lam2{it} = delta_lam_k(nSize+1:end, :);
    
    delta_Y4_k = delta_lam_k;
    Y4_k = lam_RK{it, 4};
    delta_f4 = delta_f(delta_Y_RK{nt-it+1, 4}, Y_RK{nt-it+1, 4}, delta_Y4_k, Y4_k);
    
    delta_Y3_k = delta_lam_k + h/2.0 * delta_f4;
    Y3_k = lam_RK{it, 3};
    delta_f3 = delta_f(delta_Y_RK{nt-it+1, 3}, Y_RK{nt-it+1, 3}, delta_Y3_k, Y3_k);
    
    delta_Y2_k = delta_lam_k + h/2.0 * delta_f3;
    Y2_k = lam_RK{it, 2};
    delta_f2 = delta_f(delta_Y_RK{nt-it+1, 2}, Y_RK{nt-it+1, 2}, delta_Y2_k, Y2_k);
    
    delta_Y1_k = delta_lam_k + h * delta_f2;
    Y1_k = lam_RK{it, 1};
    delta_f1 = delta_f(delta_Y_RK{nt-it+1, 1}, Y_RK{nt-it+1, 1}, delta_Y1_k, Y1_k);
    
    delta_lam_k = delta_lam_k + h * ( delta_f1/6.0 + delta_f2/3.0 + delta_f3/3.0 + delta_f4/6.0 );
end
id = find(params.ts == 1);
for iI = 1:length(id)
    %compute the jump
    [hessDistSq] = computeJumpsNumerically(X1s{id(iI)}, delta_X1s{id(iI)}, params.Ys{id(iI)});
    if isfield(params, 'diagSigmaSqs')
%         matSigma = diag(1.0 ./ params.diagSigmaSqs);
%         delta_lam_k(1:nSize, :) = delta_lam_k(1:nSize, :) ...
%             - reshape(matSigma * hessDistSq(:) / 2.0, size(hessDistSq)) * params.wi(id(iI)); % energy/2sigma^2
        delta_lam_k(1:nSize, :) = delta_lam_k(1:nSize, :) - computeJumpWithSigma(params.diagSigmaSqs{id(iI)}, hessDistSq);
    else
        delta_lam_k(1:nSize, :) = delta_lam_k(1:nSize, :) - hessDistSq / params.sigmaSqr * params.wi(id(iI));
    end
end
delta_lam1{nt+1} = delta_lam_k(1:nSize, :);
delta_lam2{nt+1} = delta_lam_k(nSize+1:end, :);
end

function delta_Y = delta_f(delta_X, X, delta_lam, lam)

nSizeX = floor(size(X, 1)/2);
X1 = X(1:nSizeX, :);
X2 = X(nSizeX+1:end, :);

delta_X1 = delta_X(1:nSizeX, :);
delta_X2 = delta_X(nSizeX+1:end, :);

nSizeL = floor(size(lam, 1)/2);
lam1 = lam(1:nSizeL, :);
lam2 = lam(nSizeL+1:end, :);

delta_lam1 = delta_lam(1:nSizeL, :);
delta_lam2 = delta_lam(nSizeL+1:end, :);

delta_Y1 = delta_lam2 * ( X2' * X2 ) + lam2 * ( delta_X2' * X2 ) + lam2 * ( X2' * delta_X2 );
delta_Y2 = -delta_lam1 + delta_X2 * ( lam2'*X1 + X1'*lam2 ) + X2 * ( delta_lam2'*X1 + lam2'*delta_X1 + delta_X1'*lam2 + X1'*delta_lam2 );
delta_Y = [delta_Y1; delta_Y2];
end