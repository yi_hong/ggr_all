% 
% compute jumps numerically, i.e., compute the Hessian of the Grassmannian
% squared distance function
% 

function [hessDistSq] = computeJumpsNumerically(Xi, delta_Xi, Yi)

epsilon = 1e-6;

[~, ~, ~, tanTmp] = grgeo(Xi, Yi, 1, 'v3', 'v2');
grad = -2 * tanTmp;

[~, ~, ~, tanTmp] = grgeo(Xi + epsilon * delta_Xi, Yi, 1, 'v3', 'v2');
gradNew = -2 * tanTmp;

hessDistSq = (gradNew - grad) / epsilon;

end