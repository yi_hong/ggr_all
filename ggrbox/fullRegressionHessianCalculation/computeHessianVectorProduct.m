function product = computeHessianVectorProduct(X1, X2, Y_RK, lam1, lam2, lam_RK, delta_y, X1s, params)

len = floor(size(Y_RK{1, 1}, 1)/2);


delta_y1 = delta_y(1:len, :);
delta_y2 = delta_y(len+1:end, :);

[delta_X1, delta_X2, delta_Y_RK] = integrateDeltaForwardMP(delta_y1, delta_y2, Y_RK, params.nt, params.h);
delta_X1s = cell(length(params.ts), 1);
for iI = 1:length(params.ts)
    delta_X1s{iI} = delta_X1{params.ts(iI)};
end

delta_lam1_end = zeros( size(delta_X1{end}) );
delta_lam2_end = zeros( size(delta_lam1_end) );
[delta_lam1, delta_lam2] = integrateDeltaBackwardMP( delta_lam1_end, delta_lam2_end, lam_RK, delta_Y_RK, Y_RK, X1s, delta_X1s, params );

delta_gradientX10 = -delta_lam1{end} ...
                    + delta_X1{1} * ( (X1{1})' * lam1{end} ) + X1{1} * ( (delta_X1{1})' * lam1{end} ) + X1{1} * ( (X1{1})' * delta_lam1{end} ) + ...
                    + delta_X2{1} * ( (lam2{end})' * X1{1} ) + X2{1} * ( (delta_lam2{end})' * X1{1} ) + X2{1} * ( (lam2{end})' * delta_X1{1} );
                
delta_gradientX20 = 2*params.alpha*delta_X2{1} - delta_lam2{end}...
                    + delta_X1{1} * ( (X1{1})' * lam2{end} ) + X1{1} * ( (delta_X1{1})' * lam2{end} ) + X1{1} * ( (X1{1})' * delta_lam2{end} );

product = [delta_gradientX10(:); delta_gradientX20(:)];
end