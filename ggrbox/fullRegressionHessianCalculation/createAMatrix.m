% create the matrix A for forward propagating Covariance matrix
function A = createAMatrix(X)
    len = floor(size(X, 1)/2);
    X1 = X(1:len, :);
    X2 = X(len+1:end, :);
    sz = size(X1);

    % computing propagation function for \dot{\sigma X1}
    A_zeros = zeros(length(X1(:)));
    A_ones = eye(length(X1(:)));

    % computing propagation function for \dot{\sigma X2}
    A_weight_delta_X1 =  kron(-X2'*X2, eye(sz(1)));
    A_weight_delta_X2 = - kron(X2', X1) * TvecMat(sz(1), sz(2)) - kron(eye(sz(2)), X1*X2');
    A = [A_zeros, A_ones; A_weight_delta_X1, A_weight_delta_X2];
end