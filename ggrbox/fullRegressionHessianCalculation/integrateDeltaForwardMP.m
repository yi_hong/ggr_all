function [delta_X1, delta_X2, delta_Y_RK] = integrateDeltaForwardMP( delta_X10, delta_X20, Y_RK, nt, h )

delta_X1_k = delta_X10;
delta_X2_k = delta_X20;

delta_X_k = [delta_X1_k; delta_X2_k];

delta_X1 = {};
delta_X2 = {};

nSize = size(delta_X10, 1);

delta_Y_RK = cell( nt, 4 );

% 4-th Runge-Kutta
for it = 1:nt
	delta_X1{it} = delta_X_k(1:nSize, :);
    delta_X2{it} = delta_X_k(nSize+1:end, :);
	delta_Y1_k = delta_X_k;
    Y1_k = Y_RK{it, 1};    
    delta_f1 = delta_f(delta_Y1_k, Y1_k);
    
    delta_Y2_k = delta_X_k + h/2.0 * delta_f1; 
    Y2_k = Y_RK{it, 2};
    delta_f2 = delta_f(delta_Y2_k, Y2_k);

    delta_Y3_k = delta_X_k + h/2.0 * delta_f2; 
    Y3_k = Y_RK{it, 3};
    delta_f3 = delta_f(delta_Y3_k, Y3_k);
    
    delta_Y4_k = delta_X_k + h * delta_f3; 
    Y4_k = Y_RK{it, 4};
    delta_f4 = delta_f(delta_Y4_k, Y4_k);
    
    delta_X_k =delta_X_k + h * ( delta_f1/6.0 + delta_f2/3.0 + delta_f3/3.0 + delta_f4/6.0 );
    
    delta_Y_RK{it, 1} = delta_Y1_k;
    delta_Y_RK{it, 2} = delta_Y2_k;
    delta_Y_RK{it, 3} = delta_Y3_k;
    delta_Y_RK{it, 4} = delta_Y4_k;
end
delta_X1{nt+1} = delta_X_k(1:nSize, :);
delta_X2{nt+1} = delta_X_k(nSize+1:end, :);
end


function delta_Y = delta_f(delta_X, X)
nSize = floor(size(X, 1)/2);
X1 = X(1:nSize, :);
X2 = X(nSize+1:end, :);
delta_X1 = delta_X(1:nSize, :);
delta_X2 = delta_X(nSize+1:end, :);

delta_Y1 = delta_X2;
delta_Y2 = -delta_X1*(X2'*X2) - X1*(delta_X2'*X2) - X1*(X2'*delta_X2);

delta_Y = [delta_Y1;delta_Y2];
end