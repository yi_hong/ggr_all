% 
% plot the covariance matrix with 95% confidence interval and 95%
% prediction interval
% 

function plotCovMatrixGrassmann(X1s, covs, Ys, scaleMean, fid, filename, conf, plotRange, outputPrefix)

assert(length(covs) == length(X1s));
[n, p] = size(X1s{1});
sizeNP = n * p;
%delta = 1.96;

% [minDetPos, maxDetPos, minDetVel, maxDetVel] = computeDeterminateRange(covs, scaleMean, n, p);
cmap = colormap;

marginal = zeros(length(X1s), 4);

% fHandle = figure(fid);
% set(fHandle, 'Position', [100 100 1300 700]);
iFig = 0;
for iI = 1:length(X1s)
    
    scaleMat = diag(scaleMean);
    curShape = X1s{iI} * scaleMat;
    iFig = iFig + 1;
    
    iPnt = 1;
    marginal(iI, 1:2) = sqrt(diag(covs{iI}([iPnt n+iPnt], [iPnt n+iPnt])));
    marginal(iI, 3:4) = sqrt(diag(covs{iI}([sizeNP+iPnt sizeNP+n+iPnt], [sizeNP+iPnt sizeNP+n+iPnt])));
    
    if p == 2
        
        [minDetPos, maxDetPos, minDetVel, maxDetVel] = computeDeterminateRange(covs(iI), scaleMean, n, p);
        
%         subplot(1, 2, 1); title('Shape');
        figure(fid), clf, title('Shape');
        hold on
        plotEllipseBand(curShape, covs{iI}(1:sizeNP, 1:sizeNP), scaleMean, ...
            n, conf, minDetPos, maxDetPos, cmap);
        hold on
        plot(curShape(:, 1), curShape(:, 2), '-m', 'LineWidth', 2);
        if(~isempty(Ys))
            [tmpU, ~, tmpV] = svd(Ys{iI}' * X1s{iI});
            tmpYs = Ys{iI} * (tmpU * tmpV');
            plot(tmpYs(:, 1)*scaleMean(1), tmpYs(:, 2)*scaleMean(2), '--k', 'LineWidth', 2); 
        end
        hold off
        xlim(plotRange * scaleMean(1));
        ylim(plotRange * scaleMean(2));
        axis equal
        h = colorbar;
        ylabel(h, 'Determinant of sub-covariaces');
        caxis([minDetPos maxDetPos]);
        set(gca, 'FontSize', 24);
        box on
        if ~isempty(outputPrefix)
            figFilename = sprintf('%s/shape_%d.fig', outputPrefix, iI);
            saveas(gca, figFilename);
        end
        
%         subplot(1, 2, 2); title('Tangent vector');
        figure(fid+1), clf, title('Tangent vector');
        hold on
        plotEllipseBand(curShape, covs{iI}(1+sizeNP:end, 1+sizeNP:end), scaleMean, ...
            n, conf, minDetVel, maxDetVel, cmap);
        plot(curShape(:, 1), curShape(:, 2), '-m', 'LineWidth', 2);
        hold off
        xlim(plotRange * scaleMean(1));
        ylim(plotRange * scaleMean(2));
        axis equal
        h = colorbar;
        ylabel(h, 'Determinant of sub-covariaces');
        caxis([minDetVel maxDetVel]);
        set(gca, 'FontSize', 24);
        box on
        if ~isempty(outputPrefix)
            figFilename = sprintf('%s/vector_%d.fig', outputPrefix, iI);
            saveas(gca, figFilename);
        end
    else
        disp('Currently we only deal with 2D plot');
    end
    
    drawnow
    
    frame = getframe(fid);
    im = frame2im(frame);
    [imind, cm] = rgb2ind(im, 256);
    if ~isempty(outputPrefix)
        figFilename = sprintf('%s/%s', outputPrefix, filename);
    else
        figFilename = filename;
    end
    if iFig == 1
        imwrite(imind, cm, figFilename, 'gif', 'Loopcount', inf);
    else
        imwrite(imind, cm, figFilename, 'gif', 'WriteMode', 'append');
    end

end

figure, subplot(1, 2, 1), plot(linspace(0, 1, size(marginal, 1))', marginal(:, 1)*scaleMean(1), '-r');
hold on
subplot(1, 2, 1), plot(linspace(0, 1, size(marginal, 1))', marginal(:, 2)*scaleMean(2), '-b');
hold off
title('Marginal for position state of the first point');
legend('x', 'y'); xlabel('time'); ylabel('sigma');

subplot(1, 2, 2), plot(linspace(0, 1, size(marginal, 1))', marginal(:, 3)*scaleMean(1), '-r');
hold on
subplot(1, 2, 2), plot(linspace(0, 1, size(marginal, 1))', marginal(:, 4)*scaleMean(2), '-b');
hold off
title('Marginal for tangent vector state of the first point');
legend('x', 'y'); xlabel('time'); ylabel('sigma');

    function plotEllipseBand(shape, cov, scale, n, conf, minDet, maxDet, cmap)
        
        for k = 1:size(shape, 1)
            curCov = cov([k n+k], [k n+k]);
            curCov(1, 1) = curCov(1, 1) * (scale(1).^2);
            curCov(2, 2) = curCov(2, 2) * (scale(2).^2);
            curCov(1, 2) = curCov(1, 2) * scale(1) * scale(2);
            curCov(2, 1) = curCov(2, 1) * scale(1) * scale(2);
            
            tmpDet = det(curCov);
            nClrPos = floor((tmpDet - minDet) / (maxDet - minDet) * (size(cmap, 1)-1)) + 1; 
            error_ellipse(curCov, shape(k, :), conf, cmap(nClrPos, :));
        end
        
    end

end


% compute pca on covs
% n = grParams.nsize(1);
% p = grParams.nsize(2);
% np = n * p;
% eigValues = zeros(2, length(covs));
% for iI = 1:length(covs)
%     [eigVec, eigVal, ~] = pcacov(covs{iI});
%     eigValues(:, iI) = eigVal(1:2);
%     v1 = 0.0001 * sqrt(eigVal(1)) * eigVec(:, 1);
%     v2 = -v1;
%     
%     X1v1 = X1s{iI} + reshape(v1(1:np), n, p);
%     X1v2 = X1s{iI} + reshape(v2(1:np), n, p);
%     
%     X2v1 = X2s{iI} + reshape(v1(np+1:end), n, p);
%     X2v2 = X2s{iI} + reshape(v2(np+1:end), n, p);
% end
% figure, subplot(1, 2, 1), plot(eigValues(1, :)); title('First eigenvalue');
% subplot(1, 2, 2), plot(eigValues(2, :)); title('Second eigenvalue');