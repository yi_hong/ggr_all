% 
% generate some synthetic shapes
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/13/2016
%

function [shapeSequence, scaleMean] = generateShapeSequenceFromDataset(params)

% load data
inputName = sprintf('%s/%s', params.inputPrefix, params.name);
tmp = load(inputName);
data = tmp.C;

id = params.id;
if length(id) <= 1
    error('At least two sample shapes');
end

% compute subspaces from the given shapes
shapeSequence = cell(1, length(id));
scales = zeros(2, 1);
figure;
for iI = 1:length(id)
    tmpData = data(:, :, id(iI))';
    meanData = mean(tmpData);
    tmpData = tmpData - repmat(meanData, [size(tmpData, 1), 1]);
    [U, S, ~] = svd(tmpData, 0);
    shapeSequence{iI} = U;
    subplot(1, length(id), iI), plot(U(:, 1), U(:, 2));
    axis equal
    axis off
    tmpS = diag(S);
    scales = scales + tmpS;
end
scaleMean = scales / length(id);


end