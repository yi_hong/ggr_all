%
% visualize covs matrix on the Grassmannian using eigendeomposition 
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/14/2016
%

function plotCovMatrixGrassmannPCA(X1s, X2s, covs, std)

assert(length(X1s) == length(covs) && length(X2s) == length(covs));

[n, p] = size(X1s{1});
sizeNP = n*p;

error = zeros(length(covs), 6);
maxEigs = zeros(length(covs), 1);

for iI = 1:length(covs)
    
    [eigVec, eigVal] = eig(covs{iI});
    eigVal = diag(eigVal);
    
    [maxVal, maxId] = max(eigVal);
    %delta = std * sqrt(maxVal(1)) * eigVec(:, maxId(1));
    delta = eigVec(:, maxId(1));
    
%     curX1Pos = X1s{iI} + reshape(delta(1:sizeNP), n, p);
%     curX1Neg = X1s{iI} - reshape(delta(1:sizeNP), n, p);
%     error(iI, 1) = norm(curX1Pos' * curX1Pos - eye(p));
%     error(iI, 2) = norm(curX1Neg' * curX1Neg - eye(p));

    error(iI, 1) = norm(X1s{iI}' * reshape(delta(1:sizeNP), n, p));
    error(iI, 2) = norm(X1s{iI}' * reshape(delta(sizeNP+1:end), n, p));
    error(iI, 3) = norm(X1s{iI}' * X1s{iI} - eye(p));
    error(iI, 4) = norm(X1s{iI}' * X2s{iI});
    error(1, 1:4)
    
%     curX2Pos = X2s{iI} + reshape(delta(sizeNP+1:end), n, p);
%     curX2Neg = X2s{iI} - reshape(delta(sizeNP+1:end), n, p);
%     error(iI, 4) = norm(X1s{iI}' * curX2Pos);
%     error(iI, 5) = norm(X1s{iI}' * curX2Neg);
%     error(iI, 6) = norm(X1s{iI}' * X2s{iI});
    
    maxEigs(iI) = maxVal(1);
    
end

figure, hold on;
plot(error(:, 1), '-r'); 
plot(error(:, 2), '-.b'); 
plot(error(:, 3), '--g');
plot(error(:, 4), '.m'); 
% plot(error(:, 5), '-b'); 
% plot(error(:, 6), '-g');
hold off
legend('X1^T * X1PC1', 'X1^T * X2PC1', 'X1^T * X1 - I', 'X1^T * X2');

% legend('Constraint for X1+std (X1^T X1 - I)', 'Constraint for X1-std (X1^T X1 - I)', ...
%     'Constraint for X1 (X1^T X1 - I)', 'Constraint for X2+std (X1^T X2)', ...
%     'Constraint for X2-std (X1^T X2)', 'Constraint for X2 (X1^T X2)');

figure, plot(maxEigs); title('Max Eigvalue');

end