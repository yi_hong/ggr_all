% 
% compute jumps with sigma matrix
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/18/2016
%

function [jump] = computeJumpWithSigma(sigmaSqs, variation)

[n, p] = size(variation);
assert(size(sigmaSqs, 1) == n && size(sigmaSqs, 2) == p^2);

jump = zeros(size(variation));
for iI = 1:n
    jump(iI, :) = inv(reshape(sigmaSqs(iI, :), p, p)) * (variation(iI, :))';
end

jump = jump ./ 2;

end