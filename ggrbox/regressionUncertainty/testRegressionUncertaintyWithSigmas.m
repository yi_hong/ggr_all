%
% test regression uncertainty, with estimting sigmas 
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/18/2016
%

clear all
close all

% generate shape sequences
dataParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/data/srivastava';
dataParams.name = 'Srivastava.mat';
dataParams.id = [498 200]; % 498 --> 200, 504 
dataParams.nPnts = 11;
dataParams.sigma = 0.05;
dataParams.figId = 500;
dataParams.idNoise = [227 590 267 218 279 523 429 227 152 966 905]; %randi(1000, [1 dataParams.nPnts]);

testCase = 1;
if testCase == 1  % synthetic data
    [shapeSequence, shapeSequenceNoise, shapeSequenceNoise2, shapeGT, velGT, scaleMean] = ...
        generateShapeSequenceFromTwoShapes(dataParams);
    ts = linspace(0, 1, dataParams.nPnts);
    shapes = shapeSequenceNoise2; %(randperm(length(shapeSequence)));
    % for synthetic data, don't do scaling
    scaleMean = [1 1];
    plotRange = [-0.22 0.22];
    outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results/regressionUncertainty/flowerBirdShape';
    
elseif testCase == 2
    dataParams.id = [300 350 375 410 435 440];
    [shapeSequence, scaleMean] = generateShapeSequenceFromDataset(dataParams);
    ts = linspace(0, 1, length(shapeSequence));
    shapes = shapeSequence;
    % for synthetic data, don't do scaling
    scaleMean = [1 1];
    
elseif testCase == 3
    % load cc
    dataParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/';
    [shapeSequence, ages, scaleMean] = loadCorpusCallosumFeatureAndSVD( dataParams );
    [ages, index] = sort(ages);
    shapeSequence = shapeSequence(index);
    scaleMean = diag(scaleMean);
    ts = (ages - min(ages))/(max(ages)-min(ages));
    shapes = shapeSequence;
    
    % rotate the shape by 180
    for iS = 1:length(shapes)
        shapes{iS} = shapes{iS} * [-1 0; 0 -1];
    end
    
    plotRange = [-0.3 0.3];
    outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results/regressionUncertainty/corpusCallosum';
    
elseif testCase == 4
    [shape1, scale1, shape2, scale2] = generateShapes(dataParams.nPnts);
    shapes = shape2;
    scaleMean = scale2;
    ts = linspace(0, 1, length(shape2));
    % for synthetic data, don't do scaling
    scaleMean = [1 1];
    plotRange = [-0.2 0.2];
    
    outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results/regressionUncertainty/syntheticTshape';
end

% fit GGR
grParams = setRegressParams();
grParams.Ys = shapes;
grParams.ts = ts; 
grParams.wi = ones(length(grParams.ts), 1);
grParams.pntY0 = shapes{1};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(grParams.ts);
grParams.nsize = size(shapes{1});
grParams.deltaT = 0.01;
grParams.nIterMax = 100;
grParams.stopThreshold = 1e-14;
grParams.minFunc = 'both';
% grParams.diagSigmaSqs = ones(length(shapes{1}(:)), 1);  % each entry of Y has its own sigma, will be estimated
grParams.diagSigmaSqs = cell(length(shapes), 1);
sigmaTmp = zeros(size(shapes{1}, 1), size(shapes{1}, 2).^2);
matEye = eye(size(shapes{1}, 2));
for k = 1:size(sigmaTmp, 1)
    sigmaTmp(k, :) = matEye(:);
end
for k = 1:length(grParams.diagSigmaSqs)
    grParams.diagSigmaSqs{k} = sigmaTmp;
end
grParams.nSigmaEstimate = 5;
if testCase == 3
    grParams.nt = max(ages) - min(ages); % for cc
end

[X10, X20, energy, grParamsUsed] = fullRegressionWithNoiseSigmaEstimation(grParams);
if isfield(grParamsUsed, 'diagSigmaSqs')
    figure, hold on;
    for iI = 1:length(grParamsUsed.diagSigmaSqs)
    	plot(grParamsUsed.diagSigmaSqs{iI}(:), 'color', rand(1, 3));
    end
    hold off
end

[~, X1s, X2s] = integrateForwardWithODE45(X10, X20, (0:grParamsUsed.nt)*grParamsUsed.h);
figure;
for iI = 1:length(X1s)
    clf
    plot(X1s{iI}(:, 1)*scaleMean(1), X1s{iI}(:, 2)*scaleMean(2));
    axis equal
    axis off
    drawnow
end

% std = 3;

% compute hessian
[covs] = computeCovAtOptimalSolution(X10, X20, X1s, X2s, grParamsUsed, energy);
marginals = diag(covs{1});
n = size(X10, 1);
marginals([1:n 2*n+1:3*n]) = marginals([1:n 2*n+1:3*n]) * (scaleMean(1)^2);
marginals([n+1:2*n 3*n+1:4*n]) = marginals([n+1:2*n 3*n+1:4*n]) * (scaleMean(2)^2);
figure, subplot(1, 2, 1), plot(marginals);
subplot(1, 2, 2), plot(marginals(1:floor(length(marginals)/2)));
% plotCovMatrixGrassmannPCA(X1s, X2s, covs, std);
 
% plot the covariance matrix
filename = 'syntheticShapeRegression.gif';
conf = 0.95;
% plotCovMatrixGrassmann(X1s(grParamsUsed.ts), covs(grParamsUsed.ts), shapes, ...
%     scaleMean, 100, filename, conf, plotRange, outputPrefix);
plotCovMatrixGrassmann(X1s, covs, [], scaleMean, 100, filename, conf, plotRange, '');
% sampleCovAndShootForward(X10, X20, covs{1}, scaleMean, std, grParamsUsed, 1000, filename);