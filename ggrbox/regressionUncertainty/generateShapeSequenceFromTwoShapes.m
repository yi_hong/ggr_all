% 
% generate some synthetic shapes
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/07/2016
%

function [shapeSequence, shapeSequenceNoise, shapeSequenceNoise2, shapeGT, velGT, scaleMean] = generateShapeSequenceFromTwoShapes(params)

% load data
inputName = sprintf('%s/%s', params.inputPrefix, params.name);
tmp = load(inputName);
data = tmp.C;

id = params.id;
if length(id) <= 1
    error('At least two sample shapes');
end

% compute subspaces from the two given shapes
subspaces = cell(1, 2);
scales = zeros(2, 2);
figure;
for iI = 1:2
    tmpData = data(:, :, id(iI))';
    meanData = mean(tmpData);
    tmpData = tmpData - repmat(meanData, [size(tmpData, 1), 1]);
    [U, S, ~] = svd(tmpData, 0);
    subspaces{iI} = U;
    subplot(1, 2, iI), plot(U(:, 1), U(:, 2));
    axis equal
    axis off
    tmpS = diag(S);
    scales(iI, :) = tmpS;
end
scaleMean = mean(scales);


% compute geodesic between the two subspaces
[~, ~, ~, velInit] = grgeo( subspaces{1}, subspaces{2}, 1, 'v3', 'v2' );
nTimes = linspace(0, 1, params.nPnts);
[~, shapeSequence] = integrateForwardWithODE45( subspaces{1}, velInit, nTimes );
shapeSequenceNoise = cell(length(shapeSequence), 1);
figure(params.figId);
for iI = 1:length(shapeSequence)
    subplot(length(shapeSequence), 3, 3*iI-2);
    plot(shapeSequence{iI}(:, 1), shapeSequence{iI}(:, 2));
    axis equal
    axis off
    
    subplot(length(shapeSequence), 3, 3*iI-1);
    shapeSequenceNoise{iI} = generateGaussianSamples(shapeSequence{iI}, params.sigma);
    plot(shapeSequenceNoise{iI}(:, 1), shapeSequenceNoise{iI}(:, 2));
    axis equal
    axis off
end

shapeGT = subspaces{1};
velGT = velInit;

% use add noise to shape by using other shapes
shapeSequenceNoise2 = cell(length(shapeSequence), 1);
for iI = 1:length(shapeSequence)
    tmpData = data(:, :, params.idNoise(iI))';
    meanData = mean(tmpData);
    tmpData = tmpData - repmat(meanData, [size(tmpData, 1), 1]);
    [U, ~, ~] = svd(tmpData, 0);
    [~, ~, ~, velInit] = grgeo( shapeSequence{iI}, U, 1, 'v3', 'v2' );
    [~, tmpShape] = integrateForwardWithODE45( shapeSequence{iI}, velInit, [0 0.1 0.2] );
    shapeSequenceNoise2{iI} = tmpShape{end};
    
    subplot(length(shapeSequence), 3, 3*iI);
    plot(shapeSequenceNoise2{iI}(:, 1), shapeSequenceNoise2{iI}(:, 2));
    axis equal
    axis off
end

end