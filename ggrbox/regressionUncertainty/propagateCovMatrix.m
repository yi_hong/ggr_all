% 
% propagate covariance matrix
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/06/2016
%

function covs = propagateCovMatrix(X1s, X2s, cov0, dt)

covs = cell(length(X1s), 1);
covs{1} = cov0;
lastCov = cov0;

for iI = 1:length(X1s)-1
    curX1 = X1s{iI};
    curX2 = X2s{iI};
    
    curA = createAMatrix([curX1; curX2]);
    curA = curA * dt + eye(size(curA));
    
    curCov = curA * lastCov * curA';
    
%     % remove numerical errors
%     [V, D] = eig(curCov);
%    	vecD = diag(D);
%     [~, index] = sort(vecD);
%     vecD(index(1:8)) = 0;
%     curCov = V * diag(vecD) * V';

    curCov = 1.0/2 * (curCov + curCov');
    covs{iI+1} = curCov;
    lastCov = curCov;
end

end