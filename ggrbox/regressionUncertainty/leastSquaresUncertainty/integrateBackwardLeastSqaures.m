% 
% integrate backward for least squares
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function [lam1, lam2] = integrateBackwardLeastSqaures(lam1End, lam2End, x1sForYs, params)

lam1 = zeros(params.nt+1, 1);
lam2 = zeros(params.nt+1, 1);

lam1(1) = lam1End;
lam2(1) = lam2End;
for iI = 2:length(lam2)
    id = find(params.tPos == params.nt - iI + 3);
    for iJ = 1:length(id)
        lam1(iI-1) = lam1(iI-1) - 2 * (x1sForYs(id(iJ)) - params.ys(id(iJ)));
    end
    lam1(iI) = lam1(iI-1);
    lam2(iI) = -lam1(iI-1) * (-params.h) + lam2(iI-1);
end
id = find(params.tPos == 1);
for iJ = 1:length(id)
    lam1(end) = lam1(end) - 2 * (x1sForYs(id(iJ)) - params.ys(id(iJ)));
end

end