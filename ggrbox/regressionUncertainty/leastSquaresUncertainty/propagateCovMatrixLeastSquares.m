% 
% Propagate the covariance matrix 
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function [covs] = propagateCovMatrixLeastSquares(cov0, A, N)

covs = cell(N, 1);
covs{1} = cov0;
for iI = 2:N
    covs{iI} = A * covs{iI-1} * A';
end

end