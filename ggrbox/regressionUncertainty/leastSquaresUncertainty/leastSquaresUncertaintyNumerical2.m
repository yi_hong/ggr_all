%
% compute hessian using derivative of the gradient
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function [hess0] = leastSquaresUncertaintyNumerical2(x0, params)

[~, grad] = energyAndGradientLeastSquares(x0, params);

delta = 1e-8;
hess0 = zeros(length(x0));
for iI = 1:length(x0)
    dx = x0;
    dx(iI) = dx(iI) + delta;
    [~, gradNew] = energyAndGradientLeastSquares(dx, params);
    hess0(:, iI) = (gradNew - grad)/delta;
end

end