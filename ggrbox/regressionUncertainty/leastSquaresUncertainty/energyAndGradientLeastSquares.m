% 
% compute energy and gradient for least squares
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
% 

function [energy, grad] = energyAndGradientLeastSquares(x0, params)

x10 = x0(1);
x20 = x0(2);
[x1s, ~] = integrateForwardLeastSquares(x10, x20, params);

[energy, x1sForYs] = computeEnergy(x1s, params);

[lam1, lam2] = integrateBackwardLeastSqaures(0, 0, x1sForYs, params);

grad(1) = -lam1(end);
grad(2) = -lam2(end);

figure(params.fid); clf;
hold on
plot(params.ts, params.ys, 'bo');
plot((0:params.nt)*params.h*(max(params.ts)-min(params.ts))+min(params.ts), x1s);
hold off

end

function [energy, x1sForYs] = computeEnergy(x1s, params)

x1sForYs = zeros(length(params.ys), 1);
energy = 0;
for iI = 1:length(params.ys)
    x1sForYs(iI) = x1s(params.tPos(iI));
    energy = energy + (x1sForYs(iI) - params.ys(iI)).^2;
end

end