% 
% function test geodesic shooting for least squares
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

% clear all
% close all
rng default;

nNum = 51;
params.ys = (1:nNum)/5 + 5*rand(nNum, 1)';
params.ts = linspace(0, 1, length(params.ys));  % ts should be in [0, 1]
params.nt = 100;
params.h = 1.0/params.nt;
params.fid = 1000;

[intercept, slope, grParams, energy] = geodesicShootingForLeastSquares(params);
[x1s, x2s] = integrateForwardLeastSquares(intercept, slope, grParams);
slope = slope ./ (max(params.ts) - min(params.ts));

res = polyfit(params.ts, params.ys, 1);

disp(intercept - res(2))
disp(slope - res(1))

twoSigmaSq = 2 * sum((res(1) * params.ts + res(2) - params.ys).^2) / (length(params.ys)-2);

hess0GT = [2*length(params.ys), 2*sum(params.ts); 2*sum(params.ts) 2*sum(params.ts.^2)] / twoSigmaSq;

twoSigmaSq = 2 * energy / (length(params.ys)-2);
[hess0Numerical] = leastSquaresUncertaintyNumerical2([intercept; slope], grParams);
hess0Numerical = hess0Numerical / twoSigmaSq;
hess0Numerical - hess0GT
cov0Numerical = inv(hess0Numerical);

[hess0Estimate] = leastSquaresUncertainty(grParams);
hess0Estimate = hess0Estimate / twoSigmaSq;
hess0Estimate - hess0GT
cov0Estimate = inv(hess0Estimate);

[covGT] = leastSquareUncertaintyNumerical(energy, params.ts, length(params.ts));
cov0Numerical - covGT
cov0Estimate - covGT

matA = [1, params.h; 0 1];
[covs] = propagateCovMatrixLeastSquares(cov0Estimate, matA, params.nt+1);
plotCovMatricesLeastSquares(params.ts, params.ys, ...
    (0:params.nt)*params.h*(max(params.ts)-min(params.ts))+min(params.ts), ...
    x1s, x2s, covs, twoSigmaSq/2);