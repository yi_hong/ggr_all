% 
% intergrate forward for least squares
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function [x1s, x2s] = integrateForwardLeastSquares(x10, x20, params)

x1s = zeros(params.nt+1, 1);
x2s = zeros(params.nt+1, 1);

for iI = 1:length(x2s)
    x2s(iI) = x20;
end

x1s(1) = x10;
for iI = 2:length(x1s)
    x1s(iI) = x2s(iI-1) * params.h + x1s(iI-1);
end

end