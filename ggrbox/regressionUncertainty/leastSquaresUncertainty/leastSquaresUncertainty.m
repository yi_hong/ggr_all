% 
% regression uncertainty for least squares
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function [hess0] = leastSquaresUncertainty(params)

eyeMat = eye(2);
hess0 = zeros(2);
for iI = 1:size(eyeMat, 2)
    dX10 = eyeMat(1, iI);
    dX20 = eyeMat(2, iI);
    [dX1s, ~] = integrateForwardLeastSquares(dX10, dX20, params);
    [dLam1, dLam2] = integrateBackwardDeltaLam(0, 0, dX1s, params);
    hess0(1, iI) = -dLam1(end);
    hess0(2, iI) = -dLam2(end);
end

end