% 
% integrate backward for deltaX
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function [dLam1, dLam2] = integrateBackwardDeltaLam(dLam1End, dLam2End, dX1s, params)

dLam1 = zeros(params.nt+1, 1);
dLam2 = zeros(params.nt+1, 1);

dLam1(1) = dLam1End;
dLam2(1) = dLam2End;
for iI = 2:length(dLam1)
    id = find(params.tPos == params.nt - iI + 3);
    dLam1(iI-1) = dLam1(iI-1) - 2 * dX1s(params.nt - iI + 3) * length(id);
    dLam1(iI) = dLam1(iI-1);
    dLam2(iI) = -dLam1(iI-1) * (-params.h) + dLam2(iI-1);
end
id = find(params.tPos == 1);
dLam1(end) = dLam1(end) - 2 * dX1s(1) * length(id);

end