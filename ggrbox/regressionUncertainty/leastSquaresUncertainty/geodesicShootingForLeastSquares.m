% 
% geodesic shooting for least squares
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
% 

function [intercept, slope, grParams, energy] = geodesicShootingForLeastSquares(params)

grParams = params;
grParams.tPos = params.ts;
grParams.tPos = grParams.tPos - min(grParams.tPos);
grParams.tPos = grParams.tPos / max(grParams.tPos);
grParams.tPos = round(grParams.tPos / params.h) + 1;

opt = loadFullRegressionOptimizerOptions(params);
[x0] = fmincon(@(x0)energyAndGradientLeastSquares(x0, grParams), [params.ys(1); 0], ...
    [], [], [], [], [1e-10; 1e-10], [1e10; 1e10], [], opt.fmincon);

[energy, ~] = energyAndGradientLeastSquares(x0, grParams);

intercept = x0(1);
slope = x0(2);

end