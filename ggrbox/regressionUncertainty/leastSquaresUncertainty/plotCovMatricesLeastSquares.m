% 
% plot the covariance matrices for least squares
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function plotCovMatricesLeastSquares(ts, ys, tPos, x1s, x2s, covs, sigmaSq)

assert(length(x1s) == length(covs) && length(x2s) == length(covs));

N = length(covs);

% plot the covariance matrix
figure, hold on;
plot(tPos, x1s);
hold on
for iI = 1:10:N
    drawEllipseForCovMatrix([tPos(iI) x1s(iI)], covs{iI});
end
title('Covariance matrix');

sigmaSqs = zeros(2, N);
delta = 1.96;
CI = zeros(2, N);
PI = zeros(2, N);

dets = zeros(1, N);

for iI = 1:N
    sigmaSqs(1, iI) = covs{iI}(1, 1);
    sigmaSqs(2, iI) = covs{iI}(2, 2);
    
    eigVal = eig(covs{iI});
    dets(iI) = eigVal(1) * eigVal(2);
    
    deltaCI = delta * sqrt(sigmaSqs(1, iI)); 
    deltaPI = delta * sqrt(sigmaSqs(1, iI) + sigmaSq);

    CI(1, iI) = x1s(iI) + deltaCI;
    CI(2, iI) = x1s(iI) - deltaCI;

    PI(1, iI) = x1s(iI) + deltaPI;
    PI(2, iI) = x1s(iI) - deltaPI;
end

figure;
subplot(1, 2, 1), plot(sqrt(sigmaSqs(1, :)), 'LineWidth', 2, 'Color', 'r'); 
title('Sigma of Intercept');
subplot(1, 2, 2), plot(sqrt(sigmaSqs(2, :)), 'LineWidth', 2, 'Color', 'b');
title('Sigma of Slope');

figure, hold on;
plot(ts, ys, 'o');
plot(tPos, x1s, '-b');
plot(tPos, CI(1, :), '-r');
plot(tPos, PI(1, :), '-k');
plot(tPos, CI(2, :), '-r');
plot(tPos, PI(2, :), '-k');
hold off
legend('LS', 'CI', 'PI');
title('Intercept intervals');

% subplot(1, 2, 2), hold on;
% plot(ts, ys, 'o');
% plot(tPos, x2s, '-b');
% plot(tPos, x2sDelta(1, :), '-r');
% plot(tPos, x2sDelta(2, :), '-g');
% hold off
% title('Slope intervals');

figure, plot(dets);
title('Determinant of covariance matrix');

end
