% 
% compute uncertainty for least square, numerically
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/09/2016
%

function [cov] = leastSquareUncertaintyNumerical(energy, ts, N)

sigYSq = energy / (N-2);

delta = N*sum(ts.^2) - sum(ts)^2;
cov(2, 2) = N * sigYSq / delta;
cov(1, 1) = sigYSq * sum(ts.^2) / delta;
cov(1, 2) = - sigYSq * sum(ts) / delta;
cov(2, 1) = cov(1, 2);

end