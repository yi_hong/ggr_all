rng default;

% 51 observations
nNum = 51;

% error
yerr = 5*rand(nNum, 1)';

% estimate sigma
sigma = std(yerr);

% construct (x,y) 
y = (1:nNum)/5 + yerr;
y = y - mean(y);
x = linspace(0, 1, length(y));

% define the forward model, i.e., the line
forwardmodel=@(m)m(1)*x + m(2);

% define the loglikelihood
logLike=@(m)sum(lognormpdf(y,forwardmodel(m)));

% MLE estimate
m_maxlike=fminsearch(@(m)-logLike(m),polyfit(x,y,1)');

% estimate sigma from the fit
sigma = std((m_maxlike(1)*x+m_maxlike(2)-y).^2);

% define Gaussian error model
lognormpdf=@(x,mu)-0.5*((x-mu)./sigma).^2  -log(sqrt(2*pi).*sigma);

% init = MLE + noise
minit=bsxfun(@plus,m_maxlike,randn(2,100)*0.01);

% run the MCMC hammer
m=gwmcmc(minit,{logLike},100000,'ThinChain',5,'burnin',.2);
t = m(:,:)';
t = [t(:,2) t(:,1)];
estC = cov(t);
disp(estC); % should be same/similar to ours