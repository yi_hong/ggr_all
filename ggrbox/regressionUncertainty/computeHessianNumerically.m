% 
% compute Hessian numerically
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/06/2016
%

function [hess] = computeHessianNumerically(Y, params, grad, deltaT)

[m, p] = size(Y);
n = floor(m/2);
assert(n*2 == m);

sizeNP = n*p;
hess = zeros(m*p);

for iter = 1:size(hess, 1)  % compute each column of the Hessian
    
%     i = floor((iter-1)/p) + 1;
%     j = mod(iter-1, p) + 1;

    curPos = iter;
    iBase = 0;
    if iter > sizeNP
        curPos = curPos - sizeNP;
        iBase = iBase + n;
    end

    i = mod(curPos-1, n) + 1 + iBase;
    j = floor((curPos-1)/n) + 1;
    
    YNew = Y;
    YNew(i, j) = Y(i, j) + deltaT;
    
    [~, gradNew] = energyAndGradientFullRegression(YNew, params);
    gradDelta = (gradNew - grad) ./ deltaT;
    
    gradDeltaX1 = gradDelta(1:n, :);
    gradDeltaX2 = gradDelta(1+n:end, :);
    hess(:, iter) = [gradDeltaX1(:); gradDeltaX2(:)];
    
end

end

% iBase = 0;
% if iter > sizeNP
%     curPos = curPos - sizeNP;
%     iBase = iBase + n;
% end
% 
% i = mod(curPos-1, n) + 1 + iBase;
% j = floor((curPos-1)/n) + 1;