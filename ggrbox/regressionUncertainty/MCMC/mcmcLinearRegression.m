%
% MCMC for linear regression
% 
% Author: Yi Hong, yihong@cs.uga.edu
% Date: 06/01/2017
% 

clear all
close all

% generate test data: y = a*x + b 
aGT = 5;
bGT = 0;
nPoints = 31; 

xObs = linspace(0, 1, nPoints);
yObs = aGT * xObs + bGT + normrnd(0, 0.5, [1, nPoints]);
figure, plot(xObs, yObs, 'bo');

% Metropolis algorithm
initParams = [1 4];
obs.x = xObs;
obs.y = yObs;
[chain, acceptance] = metropolisMCMC(initParams, 500000, obs);

figure, hold on;
subplot(1, 2, 1), hist(chain(:, 1), 100);
subplot(1, 2, 2), hist(chain(:, 2), 100);
hold off

fprintf('Mean and Standard deviation: a (%f, %f), b (%f, %f)\n', ...
    mean(chain(:, 1)), std(chain(:, 1)), mean(chain(:, 2)), std(chain(:, 2)));

res = polyfit(xObs, yObs, 1);
energy = sum((res(1) * xObs + res(2) - yObs).^2);
sigmaSq = energy / (length(yObs)-2);
hessGT = [length(yObs), sum(xObs); sum(xObs) sum(xObs.^2)] / sigmaSq;
covGT = inv(hessGT);
fprintf('Ground truth: a(%f, %f), b (%f, %f)\n', ...
    res(1), sqrt(covGT(2, 2)), res(2), sqrt(covGT(1, 1)));

fprintf('Diff: a(%f, %f), b(%f, %f)\n', abs(mean(chain(:, 1)) - res(1))/abs(res(1)), ...
    abs(std(chain(:, 1)) - sqrt(covGT(2, 2)))/sqrt(covGT(2, 2)), ...
    abs(mean(chain(:, 2)) - res(2))/abs(res(2)), ...
    abs(std(chain(:, 2)) - sqrt(covGT(1, 1)))/sqrt(covGT(1, 1)));

function prior = computePrior(params)
    aprior = log(normpdf(params(1), 0, 10)); 
    bprior = log(normpdf(params(2), 4, 5));
    prior = aprior + bprior;
end

function likelihood = computeLikelihood(params, obs)
yhat = params(1) * obs.x + params(2);
sigmaSq = sum((yhat - obs.y).^2)/(length(obs.y)-2);
llh = log(normpdf(obs.y, yhat, sqrt(sigmaSq)));
likelihood = sum(llh);
end

function posterior = computePosterior(params, obs)
posterior = computePrior(params) + computeLikelihood(params, obs);
end

function proposal = proposalFunction(params)
proposal = normrnd(params, [0.5, 0.5]);
end

function plotParams(params, level)
subplot(1, 2, 1), plot(params(1), level, 'ro'); hold on
subplot(1, 2, 2), plot(params(2), level, 'bo'); hold on
end

function [chain, acceptance] = metropolisMCMC(initParams, nIters, obs)
chain = zeros(nIters, 2);
chain(1, :) = initParams;
nCount = 0;
index = [];
% figure, hold on;
% plotParams(chain(1, :), 1);
% drawnow
for iI = 1:nIters
    proposal = proposalFunction(chain(iI, :));
    prob = exp(computePosterior(proposal, obs) - computePosterior(chain(iI, :), obs));
    if(unifrnd(0, 1) < prob)
        chain(iI+1, :) = proposal;
        nCount = nCount + 1;
    else
        chain(iI+1, :) = chain(iI, :);
        index = [index, iI];
    end
%     plotParams(chain(iI+1, :), iI+1);
%     drawnow
end
% hold off
chain(index, :) = [];
acceptance = nCount / nIters;
end