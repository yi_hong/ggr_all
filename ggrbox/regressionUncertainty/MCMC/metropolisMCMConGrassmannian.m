%
% metropolis MCMC
% 
% Author: Yi Hong, yihong@cs.uga.edu
% Date: 06/02/2017
% 

function [chain, acceptance] = metropolisMCMConGrassmannian(nIters, obs)

chain = {};
chain{1} = startPointOnGrassmannian(obs.shapes);
propOld = computePosterior(chain{1}, obs);
index = 1;
for iI = 1:nIters
    fprintf('Iteration %d\n', iI);
    proposal = proposalFunctionOnGrassmannian(chain{index});
    propNew = computePosterior(proposal, obs);
    prob = exp(propNew - propOld);
    if(unifrnd(0, 1) < prob)
        index = index + 1;
        chain{index} = proposal;
        propOld = propNew;
    end
end

acceptance = (length(chain)-1) / nIters;

end

function startValue = startPointOnGrassmannian(shapes)

shapeMean = grfmean(shapes, 1e-5);
nSize = size(shapeMean);
zrnd = normrnd(shapeMean, 0.005, nSize);
startValue.X1 = zrnd * (zrnd' * zrnd)^(-0.5);
zrnd = normrnd(0, 0.01, nSize);
startValue.X2 = (eye(nSize(1)) - startValue.X1 * startValue.X1') * zrnd;

end

function proposal = proposalFunctionOnGrassmannian(params)

shape = params.X1;
vel = params.X2;

% generate a sample on manifold
[n, p] = size(shape);
tan = normrnd(0, 0.01, [n, p]);
tan = (eye(n) - shape * shape') * tan;
[~, Ytmp, ~] = integrateForwardWithODE45(shape, tan, [0, 1]);
newShape = Ytmp{end};
proposal.X1 = newShape;

[n, p] = size(vel);
newVel = normrnd(vel, 0.01, [n, p]);
newVel = (eye(n) - shape * shape') * newVel;
[~, ~, ~, tan] = grgeo(shape, newShape, 1, 'v3', 'v2');
newVel = ptEdelman(shape, tan, newVel, 1); 

proposal.X1 = newShape;
proposal.X2 = newVel;

end

function posterior = computePosterior(params, obs)

posterior = computePrior(params) + computeLikelihood(params, obs);

end

function prior = computePrior(params)

shape = params.X1;
vel = params.X2;

shapePrior = log(normpdf(shape, 0, 0.01));
velPrior = log(normpdf(vel, 0, 0.01));

prior = sum(shapePrior) + sum(velPrior);

end

function likelihood = computeLikelihood(params, obs)

grParams = setRegressParams();
grParams.Ys = obs.shapes;
grParams.ts = obs.ts;
grParams.wi = ones(length(grParams.ts), 1);
[grParams.ts, idSort] = sort(grParams.ts);
grParams.Ys = grParams.Ys(idSort);
grParams.wi = grParams.wi(idSort);
grParams.ts_pre = grParams.ts;   
grParams.ts = grParams.ts - grParams.ts(1);
grParams.ts = grParams.ts ./ grParams.ts(end);
grParams.ts = min( max( round(grParams.ts / grParams.h) + 1, 1 ), grParams.nt+1 );

[~, X1s, X2s] = integrateForwardWithODE45(params.X1, params.X2, (0:grParams.nt)*grParams.h);
sigmaSq = estimateSigmaOfNoiseModel(X1s(grParams.ts), grParams.Ys);

energyV = grParams.alpha * trace( (X2s{1})' * X2s{1} );
energyS = 0;
for iI = 1:length(grParams.ts)
    [~, ~, ~, vTmp] = grgeo(X1s{grParams.ts(iI)}, grParams.Ys{iI}, 1, 'v3', 'v2');
    tmpDistSq = computeGeodesciDistanceWithSigma(sigmaSq{iI}, vTmp);
    energyS = energyS + tmpDistSq * grParams.wi(iI);
end
energyS = energyS / 2.0;  % here we compute energy / 2sigma^2
likelihood = energyV + energyS;

end