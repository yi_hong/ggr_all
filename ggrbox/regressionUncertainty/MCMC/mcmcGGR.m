% 
% MCMC for geodesic regression on the Grassmannian
% 
% Author: Yi Hong, yihong@cs.uga.edu
% Date: 06/02/2017
% 

clear all 
close all

[~, ~, shapes, ~] = generateShapes(11);
ts = linspace(0, 1, length(shapes));
% for synthetic data, don't do scaling
scaleMean = [1 1];
plotRange = [-0.2 0.2];

obs.shapes = shapes;
obs.ts = ts;
[chain, acceptance] = metropolisMCMConGrassmannian(100, obs);

sampledShapes = cell(length(chain), 1);
for iI = 1:length(chain)
    sampledShapes{iI} = chain{iI}.X1;
end
[meanShape, covMatrix] = estimateCovOnGrassmannian(sampledShapes);

minDet = 0; 
maxDet = 0;
n = size(meanShape, 1);
for k = 1:size(meanShape, 1)
    curCov = covMatrix([k k+n], [k k+n]);
    tmpDet = det(curCov);
    if k == 1
        minDet = tmpDet;
        maxDet = tmpDet;
    else
        if tmpDet < minDet 
           minDet = tmpDet;
        end
        if tmpDet > maxDet
            maxDet = tmpDet;
        end
    end
end

cmap = colormap;
conf = 0.95;
figure, hold on
for k = 1:size(meanShape, 1)
    curCov = covMatrix([k k+n], [k k+n]);
    curCov(1, 1) = curCov(1, 1) * (scaleMean(1).^2);
    curCov(2, 2) = curCov(2, 2) * (scaleMean(2).^2);
    curCov(1, 2) = curCov(1, 2) * scaleMean(1) * scaleMean(2);
    curCov(2, 1) = curCov(2, 1) * scaleMean(1) * scaleMean(2);
    
    tmpDet = det(curCov);
    nClrPos = floor((tmpDet - minDet) / (maxDet - minDet) * (size(cmap, 1)-1)) + 1;
    error_ellipse(curCov, meanShape(k, :), conf, cmap(nClrPos, :));
end
plot(meanShape(:, 1), meanShape(:, 2), 'r-', 'LineWidth', 2);
hold off