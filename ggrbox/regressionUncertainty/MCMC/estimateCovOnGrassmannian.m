% 
% Estimate the mean and covariance matrix given a sequence of shapes 
%
% Author: Yi Hong, yihong@cs.uga.edu
% Date: 06/05/2017
%

function [meanShape, covMatrix] = estimateCovOnGrassmannian(shapes)

meanShape = grfmean(shapes, 1e-05);

covMatrix = zeros(length(meanShape(:)));
for iI = 1:length(shapes)
    [~, ~, ~, vTmp] = grgeo(meanShape, shapes{iI}, 1, 'v3', 'v2');
    covMatrix = covMatrix + vTmp(:) * vTmp(:)';
end
covMatrix = covMatrix ./ (length(shapes));

end