% 
% sample at the initial time point and shoot forward along the first
% eigen direction
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/14/2016
%

function sampleCovAndShootForward(X10, X20, cov0, scaleMean, std, params, fid, filename)

[n, p] = size(X10);
sizeNP = n*p;

[eigVec, eigVal] = eig(cov0);
eigVal = diag(eigVal);

[maxVal, maxId] = max(eigVal);
% find the second largest one
% tmp = eigVal;
% tmp(maxId) = -tmp(maxId);
% [maxVal, maxId] = max(tmp);
maxVec = std * sqrt(maxVal(1)) * eigVec(:, maxId(1));
deltaX10 = reshape(maxVec(1:sizeNP), n, p);
deltaX20 = reshape(maxVec(sizeNP+1:end), n, p);

% remove the numerical error
deltaX10 = deltaX10 - X10 * (X10' * deltaX10);
deltaX20 = deltaX20 - X10 * (X10' * deltaX20);

% generate new shapes along the first principle direction
[~, X1s, ~] = integrateForwardWithODE45(X10, deltaX10, [0 0.5 1]);
newX10Pos = X1s{end};
newX20Pos = X20 + deltaX20;
newX20Pos = ptEdelman(X10, deltaX10, newX20Pos, 1);
newX20Pos = newX20Pos - newX10Pos * (newX10Pos' * newX20Pos); % remove error
[~, X1sPos, ~] = integrateForwardWithODE45(newX10Pos, newX20Pos, (0:params.nt)*params.h);

deltaX10 = -deltaX10;
deltaX20 = -deltaX20;
[~, X1s, ~] = integrateForwardWithODE45(X10, deltaX10, [0 0.5 1]);
newX10Neg = X1s{end};
newX20Neg = X20 + deltaX20;
newX20Neg = ptEdelman(X10, deltaX10, newX20Neg, 1);
newX20Neg = newX20Neg - newX10Neg * (newX10Neg' * newX20Neg); % remove error
[~, X1sNeg, ~] = integrateForwardWithODE45(newX10Neg, newX20Neg, (0:params.nt)*params.h);

[~, X1sMean, ~] = integrateForwardWithODE45(X10, X20, (0:params.nt)*params.h);

plotShapeBandOverTime(X1sMean, X1sPos, X1sNeg, scaleMean, fid, filename);

end