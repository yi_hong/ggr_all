% 
% continuous kelman filter for Least Squares
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/11/2016
%

function [xt, pt] = continousKalmanFilterForLeastSquares(ts, zs, x0, h)

tPos = round((ts - min(ts))/(max(ts) - min(ts)) / h) + 1;

ft = [0 1; 0 0];
ht = [1 0];
sigma = 0.01;

xt = x0;
pt = eye(length(x0));
rt = sigma;

for iI = 1:max(tPos)
    id = find(tPos == iI);
    yt = 0;
    for iJ = 1:length(id)
        yt = zs(id(iJ)) -  ht * xt; 
    end
    
    % gain
    %kt = pt * ht' / rt;
    kt = pt * ht' * inv(ht * pt * ht');
    
    % update
    xt = xt + h * (ft * xt + kt * yt);
    pt = pt + h * (ft * pt + pt * ft' - kt * rt * kt');
    
end

end