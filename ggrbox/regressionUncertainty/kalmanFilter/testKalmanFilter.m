% 
% test kalman filter
% 
% Author: Yi Hong, yihong@cs.unc.edu
% 05/11/2016
%

clear all
close all

N = 11;
ts = linspace(0, 1, N);
zs = (1:N) + 0.1*randn(1, N);
x0 = [1; 0];
h = 0.01;
[xt, pt] = continousKalmanFilterForLeastSquares(ts, zs, x0, h);

res = polyfit(ts, zs, 1);
energy = sum((zs - res(1) * ts - res(2)).^2);
twoSigmaSq = 2 * energy / (length(zs)-2);
hess = [2*N, 2*sum(ts); 2*sum(ts) 2*sum(ts.^2)] / twoSigmaSq;
cov = inv(hess);