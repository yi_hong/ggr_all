%
%  compute Covariance matrices at the optimal solution
%
%  Author: Yi Hong, yihong@cs.unc.edu
%  Date: 05/06/2016
%

function [covs] = computeCovAtOptimalSolution(X10, X20, X1s, X2s, params, energy)

% compute gradient at optimial solution
% [energy, grad, energyS] = energyAndGradientFullRegression( [X10; X20], params );

% compute hessian numerically
% deltaT = 1e-6;
% [hess0] = computeHessianNumerically([X10; X20], params, grad, deltaT);
[hess0] = computeHessian( [X10; X20], params );
hess0 = 1/2 * (hess0 + hess0');
% [L, D] = mchol(hess0);
% hess0 = L * D * L';
figure, subplot(1, 3, 1), plot(1:size(hess0,1), sort(eig(hess0))); title('Eig of Hessian (init)');

% Tikhonov regularization 
% [V, D] = eig(hess0);
% hess0 = V * (D + eye(size(D))) * V';
[V, D] = eig(hess0);
vecD = diag(D);
[~, index] = sort(vecD);
p = size(X10, 2);
vecD(index(1:2*p^2)) = 0;
% id = find(vecD <= 0);
% vecD(id) = 0;
hess0 = V * diag(vecD) * V';
hess0 = 1/2 * (hess0 + hess0');
subplot(1, 3, 2), plot(1:size(hess0,1), sort(eig(hess0))); title('Eig of Hessian (remove <0)');

if ~isfield(params, 'diagSigmaSqs')
    twoSigmaSq = 2 * energy / (length(params.Ys)-2);
    hess0 = hess0 ./ twoSigmaSq;
end
subplot(1, 3, 3), plot(1:size(hess0,1), sort(eig(hess0))); title('Eig of Hessian (/2sigma^2)');

% prepagate covariance matrix
%[L, D] = mchol(inv(hess0));
%cov0 = L * D * L';
cov0 = pinv(hess0);
cov0 = 1/2 * (cov0 + cov0');
figure, plot(1:size(cov0,1), sort(eig(cov0))); title('Eig of Covariance');
figure, imagesc(cov0); title('Covaraince matrix');
covs = propagateCovMatrix(X1s, X2s, cov0, params.h);

end