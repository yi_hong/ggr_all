% 
% compute the determinate range for position and velocity respectively
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/17/2016
%

function [minDetPos, maxDetPos, minDetVel, maxDetVel] = computeDeterminateRange(covs, scale, n, p)

    minDetPos = 0;
    maxDetPos = 0;
    minDetVel = 0;
    maxDetVel = 0;
    
    sizeNP = n * p;
    for iI = 1:length(covs)
        [minTmp, maxTmp] = computeMinMaxDet(covs{iI}(1:sizeNP, 1:sizeNP), scale, n);
        if iI == 1
            minDetPos = minTmp;
            maxDetPos = maxTmp;
        else
            if minTmp < minDetPos
                minDetPos = minTmp;
            end
            if maxTmp > maxDetPos
                maxDetPos = maxTmp;
            end
        end
        
        [minTmp, maxTmp] = computeMinMaxDet(covs{iI}(sizeNP+1:end, sizeNP+1:end), scale, n);
        if iI == 1
            minDetVel = minTmp;
            maxDetVel = maxTmp;
        else
            if minTmp < minDetVel
                minDetVel = minTmp;
            end
            if maxTmp > maxDetVel
                maxDetVel = maxTmp;
            end
        end
    end
    

    function [minDet, maxDet] = computeMinMaxDet(cov, scale, n)
        minDet = 0;
        maxDet = 0;
        for k = 1:n
            curCov = cov([k n+k], [k n+k]);
            curCov(1, 1) = curCov(1, 1) * (scale(1).^2);
            curCov(2, 2) = curCov(2, 2) * (scale(2).^2);
            curCov(1, 2) = curCov(1, 2) * scale(1) * scale(2);
            curCov(2, 1) = curCov(2, 1) * scale(1) * scale(2);
            if k == 1
                minDet = det(curCov);
                maxDet = minDet;
            else
                tmpDet = det(curCov);
                if(tmpDet < minDet)
                    minDet = tmpDet;
                end
                if(tmpDet > maxDet)
                    maxDet = tmpDet;
                end
            end
        end
    end

end