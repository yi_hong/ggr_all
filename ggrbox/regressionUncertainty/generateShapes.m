% 
% generate shapes
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/17/2016
%

function [shapes, scale, shapesNoise, scaleNoise] = generateShapes(nShapes)

basicShape = [3 3; 2 3; 2 2; 1 2; 1 1; 2 1; 3 1; 4 1; 4 2; 3 2; 3 3];

shapesOriginal = cell(nShapes, 1);
shapes = cell(nShapes, 1);
shapesNoise = cell(nShapes, 1);

delta = 0.2;
scale = zeros(2, 1);
scaleNoise = zeros(2, 1);
idChanged1 = find(basicShape(:, 2) == 3);
idChanged2 = find(basicShape(:, 1) == 4);
idChanged3 = [idChanged1; idChanged1(end-1)+1; idChanged1(end)-1];
for iI = 1:nShapes
    if iI == 1
        tmpShape = basicShape;
        tmpShapeNoise = tmpShape;
    else
        tmpShape = shapesOriginal{iI-1};
        tmpShape(idChanged1, 2) = tmpShape(idChanged1, 2) + ones(length(idChanged1), 1)*delta;
%         tmpShape(idChanged2, 1) = tmpShape(idChanged2, 1) + ones(length(idChanged2), 1)*delta;
        tmpShapeNoise = tmpShape;
        tmpShapeNoise(idChanged3, 1) = tmpShapeNoise(idChanged3, 1) + (-1)^iI * 0.4;
    end
    tmpShapeNoise = tmpShapeNoise + 0.1*rand(size(tmpShapeNoise));
    shapesOriginal{iI} = tmpShape;
    
    [U, S] = computeSVD(tmpShape, iI, shapes);
    shapes{iI} = U;
    scale = scale + diag(S);
    
    [U, S] = computeSVD(tmpShapeNoise, iI, shapesNoise);
    shapesNoise{iI} = U;
    scaleNoise = scaleNoise + diag(S);
end
scale = scale / nShapes;
scaleNoise = scaleNoise / nShapes;

figure;
for iI = 1:length(shapes)
    subplot(nShapes, 3, iI*3-2), plot(shapesOriginal{iI}(:, 1), shapesOriginal{iI}(:, 2), '-bo');
    axis equal
    subplot(nShapes, 3, iI*3-1), plot(shapes{iI}(:, 1), shapes{iI}(:, 2), '-r.');
    axis equal
    subplot(nShapes, 3, iI*3), plot(shapesNoise{iI}(:, 1), shapesNoise{iI}(:, 2), '-g.');
end

    function shapeInp = interpolateShape(shape)
        % interpolate between points
        shapeInp = [];
        nIncPnts = 15;
        for k = 1:size(shape, 1)-1
            pnt1 = shape(k, :);
            pnt2 = shape(k+1, :);
            tmp1 = linspace(pnt1(1), pnt2(1), nIncPnts+1)';
            tmp2 = linspace(pnt1(2), pnt2(2), nIncPnts+1)';
            shapeInp = [shapeInp; [tmp1(1:end-1) tmp2(1:end-1)]];
        end
        shapeInp = [shapeInp; shape(end, :)];

    end

    function [U, S] = computeSVD(shapeInput, iPos, shapeSequence)
        shapeDense = interpolateShape(shapeInput);
        % add some noise
%         shapeDense = shapeDense + 0.05 * rand(size(shapeDense));
        meanData = mean(shapeDense);
        shapeDense = shapeDense - repmat(meanData, [size(shapeDense, 1), 1]);   
        [U, S, ~] = svd(shapeDense, 0);
        
        if iPos > 1
            [tmpU, ~, tmpV] = svd(U'*shapeSequence{1});
            tmpR = tmpU * tmpV';
            U = U * tmpR;
            if(acos(tmpR(1,1)) > pi*3.0/8.0)
                S = diag(flipud(diag(S)));
            end
        end
    end

end