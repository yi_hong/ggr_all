% 
% plot shape band over time
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 05/14/2016
%

function plotShapeBandOverTime(X1sMean, X1sPos, X1sNeg, scaleMean, fid, filename)

assert(length(X1sMean) == length(X1sPos) && length(X1sMean) == length(X1sNeg));

if size(X1sMean{1}, 2) ~= 2
    error('Currently we only deal with 2D shapes');
end

figure(fid), hold on;
for iI = 1:length(X1sMean)
    
    clf
    plotShapeBand(X1sPos{iI}*diag(scaleMean), X1sNeg{iI}*diag(scaleMean));
    hold on
    plot(X1sMean{iI}(:, 1)*scaleMean(1), X1sMean{iI}(:, 2)*scaleMean(2), '-m');
    hold off
    axis equal
    xlim([-0.3 0.3]*scaleMean(1));
    ylim([-0.3 0.3]*scaleMean(2));
    drawnow
    
    frame = getframe(fid);
    im = frame2im(frame);
    [imind, cm] = rgb2ind(im, 256);
    if iI == 1
        imwrite(imind, cm, filename, 'gif', 'Loopcount', inf);
    else
        imwrite(imind, cm, filename, 'gif', 'WriteMode', 'append');
    end
    
end

    function plotShapeBand(shape1, shape2)
        for k = 1:size(shape1, 1)-1
            block = [shape1(k, :); shape1(k+1, :); shape2(k+1, :); shape2(k, :); shape1(k, :)];
            patch(block(:, 1), block(:, 2), [0.5 0.5 0.5]);
            hold on
        end
        plot(shape1(:, 1), shape1(:, 2), '-k');
        plot(shape2(:, 1), shape2(:, 2), '-g');
    end

end