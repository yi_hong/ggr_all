% 
% compute points along the cubic spline curve
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 10/06/2014
%

function [ts, pnts, vels] = computePointsAlongGeodesicSpline(initPnt, initVel, tPos, ages, nt)

X10 = initPnt{1};
X20 = initVel{1};
X30 = initVel{2};
X4s = initVel(3:end);

cps = tPos(2:end-1);

cps = (cps - tPos(1)) / (tPos(end) - tPos(1));


%[pointsSyn, ~, ~, ~, ts] = integrateForwardEachIntervalWithODE45Spline( X10, X20, X30, X4s, cps, tSpan );
paramsTmp.h = 1.0 / nt;
cps = min( max( round(cps / paramsTmp.h) + 1, 1), nt+1 );
cps = [1 cps nt+1];
paramsTmp.Gns = cps(2:end) - cps(1:end-1);
[pointsTmp, velTmp] = integrateForwardEachIntervalSpline( X10, X20, X30, X4s, paramsTmp );
pointsSyn = [];
velSyn = [];
for iITmp = 1:length(pointsTmp)
    if iITmp == 1
        pointsSyn = [pointsSyn, pointsTmp{iITmp}];
        velSyn = [velSyn, velTmp{iITmp}];
    else
        pointsSyn = [pointsSyn, pointsTmp{iITmp}(2:end)];
        velSyn = [velSyn, velTmp{iITmp}(2:end)];
    end
end
tTmp = 0:paramsTmp.h:1;
assert( length(tTmp) == length(pointsSyn) );
assert( length(tTmp) == length(velSyn) );

pnts = cell(length(ages), 1);
vels = cell(length(ages), 1);
for iI = 1:length(ages)
    ageTmp = (ages(iI) - tPos(1)) / (tPos(end) - tPos(1));
    [~, idx] = min( abs(tTmp - ageTmp) );
    pnts{iI} = pointsSyn{idx(1)};
    vels{iI} = velSyn{idx(1)};
end
ts = ages;