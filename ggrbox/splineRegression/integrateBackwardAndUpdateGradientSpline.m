%
% integrate backward in each interval and compute gradient for spline regression
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 08/24/2014
%

function [gradX10, gradX20, gradX30, gradX4s] = integrateBackwardAndUpdateGradientSpline(X1, X2, X3, X1s, Y_RK, params)

% backward in time
lam1_end = zeros( size(X1{1}{1}) );
lam2_end = zeros( size(lam1_end) );
lam3_end = zeros( size(lam1_end) );
lam4_end = zeros( size(lam1_end) );

% gradient for X4, the first one is the initial X4, the rest is the X4 at
% control points
gradX4s = cell(params.nPieces, 1);

for iJ = params.nPieces:-1:1
    paramsTmp.nt = params.Gns(iJ);
    paramsTmp.h = params.h;
    paramsTmp.Ys = params.Ys(params.GyIds{iJ});
    paramsTmp.ts = params.ts(params.GyIds{iJ});
    if iJ > 1
        paramsTmp.ts = paramsTmp.ts - params.cps(iJ-1) + 1;
    end
    paramsTmp.sigmaSqr = params.sigmaSqr;
    paramsTmp.alpha = params.alpha;
    paramsTmp.wi = params.wi(params.GyIds{iJ});
    [lam1, lam2, lam3, lam4] = integrateBackwardSpline( lam1_end, lam2_end, lam3_end, lam4_end, ...
                                  Y_RK{iJ}, X1s(params.GyIds{iJ}), paramsTmp );
    gradX4s{iJ} = -lam4{end};
    if iJ > 1
        lam1_end = lam1{end};
        lam2_end = lam2{end};
        lam3_end = lam3{end};
        lam4_end = zeros( size(lam1_end) );
    end
end

gradX10 = -lam1{end} + X1{1}{1}*(X1{1}{1}'*lam1{end}) ...
          + X2{1}{1}*(lam2{end}'*X1{1}{1}) + X3{1}{1}*(lam3{end}'*X1{1}{1});
gradX20 = -lam2{end} + X1{1}{1}*(X1{1}{1}'*lam2{end});
gradX30 = -lam3{end} + X1{1}{1}*(X1{1}{1}'*lam3{end});
