%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Line search update for spline regression
%
%  Author: Yi Hong
%  yihong@cs.unc.edu
%  
%  Date: 08/24/2014
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [X1Opt, X2Opt, X3Opt, X4sOpt, currentEnergy] = lineSearchUpdateSpline(X10, X20, X30, X4s, params)


% integrate forward
[X1, X2, X3, X4, Y_RK] = integrateForwardEachIntervalSpline( X10, X20, X30, X4s, params );

% compute energy
[currentEnergy, X1s] = computeEnergySpline( X1, X3, params );
if ~isfield(params, 'cubicSpline') || ~params.cubicSpline
    fprintf( 'Cubic spline regression on the Grassmannian (CS-GGR)\n' );
    fprintf( 'CS-GGR Intial energy = %f\n', currentEnergy );
end

% plot the energy
figure(params.fidSplineEnergy), hold on;
clr = rand(1, 3);
eng_pre = currentEnergy;
figure(params.fidSplineEnergy), plot( 0, eng_pre, 'p', 'LineWidth', 4, 'Color', clr );
title('Cubic Spline Regression Energy');
xlabel('Iteration');
ylabel('Energy');

% gradient decent
X1Opt = X10;
X2Opt = X20;
X3Opt = X30;
X4sOpt = X4s;
stopIteration = false;
for iI = 1:params.nIterMax
    % integrate backward
    [gradX10, gradX20, gradX30, gradX4s] = integrateBackwardAndUpdateGradientSpline(X1, X2, X3, X1s, Y_RK, params);
    
    % time step size
    dt = params.deltaT;
    energyReduced = false;
    
    % store the current status
    curX10 = X1Opt;
	curX20 = X2Opt;
    curX30 = X3Opt;
    curX4s = X4sOpt;
    
    % change the positive gradient to negative
    gradX10 = -gradX10;
    % make the gradientX10 to be tangent to the point, remove introduced
    % numerical error
    gradX10 = gradX10 - curX10 * (curX10' * gradX10);
    for iJ = 1:params.maxReductionSteps
		
		% update the initial point by integrating forward
        if params.useODE45
            [~, Ytmp] = integrateForwardWithODE45( curX10, gradX10, [0 dt/2 dt] );
        else
            [Ytmp] = integrateForwardToGivenTime( curX10, gradX10, dt, params.h );
        end
        X1Opt = Ytmp{end};  % updated initial point
        
        if sum(sum(isnan(X1Opt))) > 0 
            if ~isfield(params, 'cubicSpline') || ~params.cubicSpline 
                fprintf('#');
            end
            dt = dt * params.rho;
            continue; 
        end
        
		% update the initial velocity  
		X2Opt = curX20 - dt * gradX20;
        % make sure the velocity is tangent to the point
        X2Opt = X2Opt - curX10 * (curX10' * X2Opt);
        % parallel transport 
		X2Opt = ptEdelman(curX10, gradX10, X2Opt, dt);
        % project to the new point
		X2Opt = X2Opt - X1Opt * (X1Opt' * X2Opt);	
        
        % update the initial acceleration, need checking
        X3Opt = curX30 - dt * gradX30;
        X3Opt = X3Opt - curX10 * (curX10' * X3Opt);
        X3Opt = ptEdelman(curX10, gradX10, X3Opt, dt);
        X3Opt = X3Opt - X1Opt * (X1Opt' * X3Opt);
        
        % update X4s, because it is not tangent to X1, no parallel
        % transport, need checking
        for iK = 1:length(curX4s)
            X4sOpt{iK} = curX4s{iK} - dt * gradX4s{iK};
        end
        
        % integrate forward in time
        [X1, X2, X3, X4, Y_RK] = integrateForwardEachIntervalSpline( X1Opt, X2Opt, X3Opt, X4sOpt, params );
        
        if sum(sum(isnan(X1{end}{end}))) + sum(sum(isnan(X2{end}{end}))) ...
         + sum(sum(isnan(X3{end}{end}))) + sum(sum(isnan(X4{end}{end}))) > 0 
            if ~isfield(params, 'cubicSpline') || ~params.cubicSpline 
                fprintf('#');
            end
            dt = dt * params.rho;
            continue; 
        end

        % compute energy
        [energy, X1s] = computeEnergySpline( X1, X3, params );
        %wolfeDecrement = 0; % params.c1 * dt * dot(pk, gk);
        
        if energy >= currentEnergy
            if ~isfield(params, 'cubicSpline') || ~params.cubicSpline 
                fprintf('#');
            end
        else
            % energy decreased
            currentEnergy = energy;
            energyReduced = true;
            if ~isfield(params, 'cubicSpline') || ~params.cubicSpline 
                fprintf( 'CS-GGR Iter %04d: Energy = %f\n', iI, currentEnergy );
            end
            figure(params.fidSplineEnergy), plot( [iI-1 iI], [eng_pre currentEnergy], ...
                '-o', 'LineWidth', 2, 'Color', clr );
            drawnow
            
            if ~isfield(params, 'cubicSpline') || ~params.cubicSpline 
                if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
                    h = figure(params.fidSplineFitting);
                    clf(h);
                    hold on
                    plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
                    plotGeodesic( X1s, params.ts_pre, params.pDim, '--' );
                    if ~isempty(params.cps_pre)
                        Ycps = cell(length(params.cps_pre), 1);
                        for iTmp = 1:length(Ycps)
                            Ycps{iTmp} = X1{iTmp}{end};
                        end
                        plotGeodesic( Ycps, params.cps_pre, params.pDim, '*' );
                    end
                    if isfield(params, 'Y_noise')
                        plotGeodesic( params.Y_noise, params.t_truth, params.pDim, 'o' );
                    end
                    drawnow
                    hold off
                end
            end
            
            if eng_pre - currentEnergy < params.stopThreshold
                stopIteration = true;
            end
            
            eng_pre = currentEnergy;
            
            break;
        end
        dt = dt * params.rho;
    end
    
    if (~energyReduced)
        if ~isfield(params, 'cubicSpline') || ~params.cubicSpline 
            fprintf( 'CS-GGR Iteration %d: Could not reduce energy further. Quitting.\n', iI );
        end
        X1Opt = curX10;
		X2Opt = curX20;
        X3Opt = curX30;
        X4sOpt = curX4s;
        break;
    end
    
    if stopIteration
        if ~isfield(params, 'cubicSpline') || ~params.cubicSpline
            fprintf( 'CS-GGR Qutting because the decreased energy is smaller than the threshold.\n' );
        end
        break;
    end
end

    


