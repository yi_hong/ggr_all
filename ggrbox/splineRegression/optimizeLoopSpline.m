% 
% spline optimiation loop
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/25/2014
%

function [X1Opt, X2Opt, X3Opt, X4sOpt, energy] = optimizeLoopSpline( X10, X20, X30, X4s, params )

if strcmp( params.minFunc, 'linesearch' )
    [X1Opt, X2Opt, X3Opt, X4sOpt, energy] = lineSearchUpdateSpline( X10, X20, X30, X4s, params );
    
elseif strcmp( params.minFunc, 'fmincon' )
    % doesn't work, the states will leave the space
    optOpts = loadFullRegressionOptimizerOptions(params);
    
    % initial values
    Y0 = [X10; X20; X30];
    for iI = 1:length(X4s)
        Y0 = [Y0; X4s{iI}];
    end
    global nSizeX;
    nSizeX = size(X10);
    lb = ones(size(Y0)) * (-1e10);
    ub = ones(size(Y0)) * 1e10;
    [Y, energy] = fmincon( @(Y)energyAndGradientSplineRegression(Y, params), Y0, ...
        [], [], [], [], lb, ub, [], optOpts.fmincon );
    X1Opt = Y(1:nSizeX(1), :);
    X2Opt = Y(nSizeX(1)+1:2*nSizeX(1), :);
    X3Opt = Y(2*nSizeX(1)+1:3*nSizeX(1), :);
    for iI = 1:length(X4s)
        X4sOpt{iI} = Y((iI+2)*nSizeX(1)+1:(iI+3)*nSizeX(1), :);
    end
elseif strcmp( params.minFunc, 'both')
    % use linsearch and fmincon to iteratively update states
    optOpts = loadFullRegressionOptimizerOptions(params);
    global nSizeX;
    nSizeX = size(X10);
    
    for iIter = 1:params.nIterBoth 
        % fmincon
        Y0 = [X10; X20; X30];
        for iI = 1:length(X4s)
            Y0 = [Y0; X4s{iI}];
        end
        lb = ones(size(Y0)) * (-1e10);
        ub = ones(size(Y0)) * 1e10;
        [Y, energy] = fmincon( @(Y)energyAndGradientSplineRegression(Y, params), Y0, ...
            [], [], [], [], lb, ub, [], optOpts.fmincon );
        X1Opt = Y(1:nSizeX(1), :);
        X2Opt = Y(nSizeX(1)+1:2*nSizeX(1), :);
        X3Opt = Y(2*nSizeX(1)+1:3*nSizeX(1), :);
        for iI = 1:length(X4s)
            X4sOpt{iI} = Y((iI+2)*nSizeX(1)+1:(iI+3)*nSizeX(1), :);
        end
        % linesearch
        [X10, X20, X30, X4s, energy] = lineSearchUpdateSpline( X1Opt, X2Opt, X3Opt, X4sOpt, params );
    end
    X1Opt = X10;
    X2Opt = X20;
    X3Opt = X30;
    X4sOpt = X4s;
else
    if ~isfield(params, 'cubicSpline') || ~params.cubicSpline
        error('Not supported optimization method');
    end
end