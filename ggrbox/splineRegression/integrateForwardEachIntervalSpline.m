% 
% integrate forward for spline regression in each interval 
% 
% Author: Yi Hong, yihong@cs.unc.edu
%
% Date: 08/24/2014
%

function [X1, X2, X3, X4, Y_RK] = integrateForwardEachIntervalSpline( X10, X20, X30, X4s, params )

X10_tmp = X10;
X20_tmp = X20;
X30_tmp = X30;
for iI = 1:length(X4s)
    [X1{iI}, X2{iI}, X3{iI}, X4{iI}, Y_RK{iI}] = integrateForwardSpline( X10_tmp, X20_tmp, ...
        X30_tmp, X4s{iI}, params.Gns(iI), params.h );
    X10_tmp = X1{iI}{end};
    X20_tmp = X2{iI}{end};
    X30_tmp = X3{iI}{end};
end
