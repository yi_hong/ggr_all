% 
% compute the intial values of initial conditions for spline regression
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/25/2014
%

function [X10Init, X20Init, X30Init, X4sInit] = computeInitialConditionsForSplineRegression( params )

[X10Init, X20Init, X30Init, X4sInit] = setInitialValueSpline( params );
X1Tmp = X10Init;
X2Tmp = X20Init;
X3Tmp = X30Init;
X4Tmp{1} = X4sInit{1};
% need computing initial values and more than one piece
if isfield(params, 'estimateInitialValueSpline') && params.estimateInitialValueSpline && ~isempty(params.cps)
    for iI = 1:params.nPieces
        paramsTmp = params;
        paramsTmp.nt = params.Gns(iI);
        paramsTmp.h = params.h;
        paramsTmp.Ys = params.Ys(params.GyIds{iI});
        paramsTmp.ts = params.ts(params.GyIds{iI});
        if iI > 1
            paramsTmp.ts = paramsTmp.ts - params.cps(iI-1) + 1;
        end
        paramsTmp.sigmaSqr = params.sigmaSqr;
        paramsTmp.wi = params.wi(params.GyIds{iI});
        paramsTmp.cps = [];
        paramsTmp.YgIds = ones(length(paramsTmp.Ys), 1);
        paramsTmp.GyIds = [];
        paramsTmp.GyIds{1} = 1:length(paramsTmp.Ys);
        paramsTmp.Gns = paramsTmp.nt;
        paramsTmp.nPieces = 1;
        paramsTmp.ts_pre = params.ts_pre(params.GyIds{iI});
        paramsTmp.cps_pre = paramsTmp.cps;
        
        X4Tmp{1} = X4sInit{iI};
        paramsTmp.X10GT = X1Tmp;
        if iI > 1
            paramsTmp.X20GT = X2Tmp;
            paramsTmp.X30GT = X3Tmp;
        end
        [X1Opt, X2Opt, X3Opt, X4sOpt, ~] = optimizeLoopSpline( X1Tmp, X2Tmp, X3Tmp, X4Tmp, paramsTmp );
        [X1, X2, X3, X4, ~] = integrateForwardSpline( X1Opt, X2Opt, X3Opt, X4sOpt{1}, paramsTmp.nt, paramsTmp.h );
        X1Tmp = X1{end};
        X2Tmp = X2{end};
        X3Tmp = X3{end};
        %X4Tmp{1} = X4{end};
        if iI == 1
            X10Init = X1Opt;
            X20Init = X2Opt;
            X30Init = X3Opt;
        end
        X4sInit{iI} = X4sOpt{1};
    end
end


%{
[X10, X20, X30, X4s] = setInitialValueSpline( params );

% estimate the initial values by piecewise cubic, divided by control points
if isfield(params, 'estimateInitialValueSpline') && params.estimateInitialValueSpline && ~isempty(params.cps)
    disp('estimate initial values ...');
    % first test only initialize the first piece
    grParams = params;
    ind = find( params.ts <= min(params.cps) );
    grParams.ts = params.ts(ind);
    grParams.Ys = params.Ys(ind);
    grParams.wi = params.wi(ind);
    grParams.cps = [];
    [X10, X20, X30, X40] = cubicSplineRegression( grParams );
    X4s{1} = X40{1};
    ratio = (max(grParams.ts)-min(grParams.ts)) / (max(params.ts)-min(params.ts));
    X20 = X20 * ratio;
    X30 = X30 * ratio;
    %X4s{1} = X4s{1} * ratio;
end
%}
