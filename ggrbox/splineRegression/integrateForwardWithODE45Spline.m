function [X1, X2, X3, X4, Tend] = integrateForwardWithODE45Spline( X10, X20, X30, X40, tSpan )

[s1, s2] = size(X10);
len = s1*s2;
YInit = zeros( 4*len, 1 );
YInit(1:len) = reshape(X10, [], 1);
YInit(len+1:2*len) = reshape(X20, [], 1);
YInit(2*len+1:3*len) = reshape(X30, [], 1);
YInit(3*len+1:end) = reshape(X40, [], 1);

options = odeset('RelTol', 1e-7, 'AbsTol', 1e-7);
[Tend, YTmp] = ode45( @(t, Y)vdp(t, Y, s1, s2), tSpan, YInit, options );

X1 = cell(length(Tend), 1);
X2 = cell(length(Tend), 1);
X3 = cell(length(Tend), 1);
X4 = cell(length(Tend), 1);
for iI = 1:length(Tend)
    X1{iI} = reshape(YTmp(iI, 1:len), s1, s2);
    X2{iI} = reshape(YTmp(iI, len+1:2*len), s1, s2);
    X3{iI} = reshape(YTmp(iI, 2*len+1:3*len), s1, s2);
    X4{iI} = reshape(YTmp(iI, 3*len+1:end), s1, s2);
end



    function dY = vdp( t, Y, s1, s2 )
        len = s1*s2;
        X1 = reshape( Y(1:len), s1, s2 );
        X2 = reshape( Y(len+1:2*len), s1, s2 );
        X3 = reshape( Y(2*len+1:3*len), s1, s2 );
        X4 = reshape( Y(3*len+1:end), s1, s2 );
        dY = zeros( 4*len, 1 );
        dY(1:len) = reshape( X2, [], 1 );
        dY(len+1:2*len) = reshape( X3-X1*(X2'*X2), [], 1 );
        dY(2*len+1:3*len) = reshape( -X4+X1*(X1'*X4 - X2'*X3), [], 1 );
        dY(3*len+1:end) = reshape( X3*(X2'*X2) + X2*(X4'*X1 - X3'*X2), [], 1);
    end
end

