%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  integrate backward in time for spline regression
%  
%  Author: Yi Hong
%  Date: 08/24/2014
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [lam1, lam2, lam3, lam4] = integrateBackwardSpline(lam1_end, lam2_end, lam3_end, lam4_end, Y_RK, X1s, params)

nt = params.nt;
h = -params.h;

lam1_k = lam1_end;
lam2_k = lam2_end;
lam3_k = lam3_end;
lam4_k = lam4_end;
lam_k = [lam1_k; lam2_k; lam3_k; lam4_k];
lam1 = {};
lam2 = {};
lam3 = {};
lam4 = {};
nSize = size(lam1_end, 1);

% 4th Runge-Kutta
for it = 1:nt
    id = find(params.ts == nt-it+2);
    for iI = 1:length(id)
        [~, ~, ~, tanTmp] = grgeo(X1s{id(iI)}, params.Ys{id(iI)}, 1, 'v3', 'v2');
        lam_k(1:nSize, :) = lam_k(1:nSize, :) + tanTmp * 2.0 / params.sigmaSqr * params.wi(id(iI));   % jump
    end
    lam1{it} = lam_k(1:nSize, :);
    lam2{it} = lam_k(nSize+1:2*nSize, :);
    lam3{it} = lam_k(2*nSize+1:3*nSize, :);
    lam4{it} = lam_k(3*nSize+1:end, :);
    Y4_k = lam_k;
    f4 = f(Y_RK{nt-it+1, 4}, Y4_k, params.alpha);
    Y3_k = lam_k + h/2.0 * f4;       %f(Y_RK{nt-it+1, 4}, Y4_k);
    f3 = f(Y_RK{nt-it+1, 3}, Y3_k, params.alpha);
    Y2_k = lam_k + h/2.0 * f3;       %f(Y_RK{nt-it+1, 3}, Y3_k);
    f2 = f(Y_RK{nt-it+1, 2}, Y2_k, params.alpha);
    Y1_k = lam_k + h * f2;           %f(Y_RK{nt-it+1, 2}, Y2_k);
    lam_k = lam_k + h * ( f(Y_RK{nt-it+1, 1}, Y1_k, params.alpha)/6.0 + f2/3.0 + f3/3.0 + f4/6.0 );
    %lam_k = lam_k + h * ( f(Y_RK{nt-it+1, 1}, Y1_k)/6.0 + f(Y_RK{nt-it+1, 2}, Y2_k)/3.0 + ...
    %                      f(Y_RK{nt-it+1, 3}, Y3_k)/3.0 + f(Y_RK{nt-it+1, 4}, Y4_k)/6.0 );
end
id = find(params.ts == 1);
for iI = 1:length(id)
    [~, ~, ~, tanTmp] = grgeo(X1s{id(iI)}, params.Ys{id(iI)}, 1, 'v3', 'v2');
    lam_k(1:nSize, :) = lam_k(1:nSize, :) + tanTmp * 2.0 / params.sigmaSqr * params.wi(id(iI));   % jump
end
lam1{nt+1} = lam_k(1:nSize, :);
lam2{nt+1} = lam_k(nSize+1:2*nSize, :);
lam3{nt+1} = lam_k(2*nSize+1:3*nSize, :);
lam4{nt+1} = lam_k(3*nSize+1:end, :);
end


% function: f(X, lam)  
%         = ( lam2*X2'*X2 - lam3*(X4'*X1-X3'*X2) - X4*(lam3'*X1 + X2'*lam4);
%             -lam1 + X2*(lam2'*X1 + X1'*lam2 - lam4'*X3 - X3'*lam4) +
%             X3*(lam3'*X1 + X2'* lam4) + lam4*(-X1'*X4 + X2'*X3);
%             -lam2 + X2*(X1'*lam3 + lam4'*X2) - lam4*(X2'*X2);
%             lam3 - X1*(X1'*lam3 + lam4'*X2) )
function Y = f(X, lam, alpha)

nSizeX = floor(size(X, 1)/4);
X1 = X(1:nSizeX, :);
X2 = X(nSizeX+1:2*nSizeX, :);
X3 = X(2*nSizeX+1:3*nSizeX, :);
X4 = X(3*nSizeX+1:end, :);

nSizeL = floor(size(lam, 1)/4);
lam1 = lam(1:nSizeL, :);
lam2 = lam(nSizeL+1:2*nSizeL, :);
lam3 = lam(2*nSizeL+1:3*nSizeL, :);
lam4 = lam(3*nSizeL+1:end, :);

Y1 = lam2*(X2'*X2) - lam3*(X4'*X1-X3'*X2) - X4*(lam3'*X1 + X2'*lam4);
Y2 = -lam1 + X2*(lam2'*X1 + X1'*lam2 - lam4'*X3 - X3'*lam4) ...
     + X3*(lam3'*X1 + X2'* lam4) + lam4*(-X1'*X4 + X2'*X3);
Y3 = -lam2 + X2*(X1'*lam3 + lam4'*X2) - lam4*(X2'*X2) + 2*alpha*X3;
Y4 = lam3 - X1*(X1'*lam3 + lam4'*X2);
Y = [Y1; Y2; Y3; Y4];
end
