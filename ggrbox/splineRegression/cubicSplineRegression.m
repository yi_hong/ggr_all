% 
% cublic spline regression
% 
% Inputs: params (parameters for spline regression)
%         .Ys (measurements)
%         .ts (time points for each point)
%         .wi (weight for each point)
%         .cls (control points)
% 
% Results: X10, X2, X3  -- the initial conditions
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 08/25/2014
% 

function [X1Opt, X2Opt, X3Opt, X4sOpt, energy, params] = cubicSplineRegression( params )

% adjust the time points with the initial point
[params.ts, idSort] = sort( params.ts );
params.Ys = params.Ys(idSort);
params.wi = params.wi(idSort);
params.cps = sort(params.cps);

% convert value to id
params.ts_pre = params.ts;
params.cps_pre = params.cps;
params.cps = params.cps - params.ts(1);
params.ts = params.ts - params.ts(1);
params.cps = params.cps ./ params.ts(end);
params.ts = params.ts ./ params.ts(end);
params.ts = min( max( round(params.ts / params.h) + 1, 1 ), params.nt+1 );
params.cps = min( max( round(params.cps / params.h) + 1, 1 ), params.nt+1 );
params.nPieces = length(params.cps) + 1;

% divide the data into groups 
% for each point, find its belonging piece
params.YgIds = zeros(length(params.Ys), 1);
for iI = 1:length(params.Ys)
	iJ = 1;
	while iJ <= length(params.cps)
		if params.ts(iI) <= params.cps(iJ)
			break;
		end
		iJ = iJ + 1;
	end
	params.YgIds(iI) = iJ;
end

% for each piece, find its points
params.GyIds = cell(params.nPieces, 1);
for iJ = 1:length(params.GyIds)
	params.GyIds{iJ} = find( params.YgIds == iJ );
end
% for each piece, find its length
tmp = [1 params.cps params.nt+1];
params.Gns = tmp(2:end) - tmp(1:end-1);

[X10, X20, X30, X4s] = computeInitialConditionsForSplineRegression( params );
[X1Opt, X2Opt, X3Opt, X4sOpt, energy] = optimizeLoopSpline( X10, X20, X30, X4s, params );

end

   