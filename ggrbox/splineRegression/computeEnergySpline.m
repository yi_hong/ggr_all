%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Compute the energy for spline regression
%
%  Author: Yi Hong
%  Date: 10/13/2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [energyTotal, X1s] = computeEnergySpline( X1, X3, params )


% compute the energy of the initial acceleration, need further checking
energyV = 0;
for iI = 1:length(X3)
    if iI == 1
        nStart = 1;
    else
        nStart = 2;   % avoid counting the control points twice
    end
    for iJ = nStart:length(X3{iI})
        energyV = energyV + trace( (X3{iI}{iJ})' * X3{iI}{iJ} ) * params.h;
    end
end
energyV = energyV * params.alpha;

% compute the energy of the similarity term
energyS = 0;
for iI = 1:length(params.ts)
	% find the corresponding piece for the point iI	
	iJ = params.YgIds(iI);
	if iJ == 1
		X1s{iI} = X1{iJ}{params.ts(iI)};
	else
		X1s{iI} = X1{iJ}{params.ts(iI)-params.cps(iJ-1)+1};
    end
    
	energyS = energyS + grarc(X1s{iI}, params.Ys{iI}) * params.wi(iI);
end

energyS = energyS / params.sigmaSqr;

% compute the total energy
energyTotal = energyV + energyS;


