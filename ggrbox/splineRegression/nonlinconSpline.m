% 
% nonlinear constraint funciton for spline regression using fmincon
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/24/2014
%

function [c, ceq] = nonlinconSpline(x)

global nSizeX 

X1 = x(1:nSizeX(1), :);
X2 = x(nSizeX(1)+1:2*nSizeX(1), :);
X3 = x(2*nSizeX(1)+1:3*nSizeX(1), :);

c = [];
ceq1 = X1' * X1 - eye(size(x, 2));
ceq2 = X1' * X2;
ceq3 = X1' * X3;
ceq = [ceq1; ceq2; ceq3];