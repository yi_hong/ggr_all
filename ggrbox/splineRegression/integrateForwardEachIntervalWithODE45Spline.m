% 
% integrate forward using ODE45
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 10/03/14
%
% Note: tSpan is normalized to (0, 1), assume it is sorted
%       the resulting values will be sorted, even the input tSpan is not sorted 

function [X1, X2, X3, X4, Ts] = integrateForwardEachIntervalWithODE45Spline( X10, X20, X30, X4s, cps, tSpan )

assert( length(X4s)-1 == length(cps) );

% assume control point is within (0, 1)
if ~isempty(cps)
    assert( min(cps) > 0 && max(cps) < 1 );
end
cps = reshape(cps, 1, []);
cps = [0 cps 1];

X1 = [];
X2 = [];
X3 = [];
X4 = [];
Ts = [];

tSpan = reshape(tSpan, 1, []);
[tSpan, idxSort] = sort(tSpan);
idxTmp = 1:length(tSpan);
if sum(idxSort - idxTmp) > 0
    disp('Warning: tSpan is not sorted, will be sorted during the computation');
end

X10_tmp = X10;
X20_tmp = X20;
X30_tmp = X30;
for iI = 1:length(X4s)
    idx = find( tSpan >= cps(iI) & tSpan <= cps(iI+1) );
    tSpanTmp = tSpan(idx);
    flagAddLeft = 0;
    flagAddRight = 0;
    if isempty(tSpanTmp)
        tSpanTmp = [cps(iI) cps(iI+1)];
        flagAddLeft = 1;
        flagAddRight = 1;
    end
    if tSpanTmp(1) ~= cps(iI)
        tSpanTmp = [cps(iI) tSpanTmp];
        flagAddLeft = 1;
    end
    if tSpanTmp(end) ~= cps(iI+1)
        tSpanTmp = [tSpanTmp cps(iI+1)];
        flagAddRight = 1;
    end
    flagAddMiddle = 0;
    if length(tSpanTmp) == 2
        tSpanTmp = [tSpanTmp(1) (tSpanTmp(1)+tSpanTmp(2))/2 tSpanTmp(2)];
        flagAddMiddle = 1;
    end
    [X1_tmp, X2_tmp, X3_tmp, X4_tmp, Ts_tmp] = integrateForwardWithODE45Spline(X10_tmp, X20_tmp, ...
        X30_tmp, X4s{iI}, tSpanTmp);
    X10_tmp = X1_tmp{end};
    X20_tmp = X2_tmp{end};
    X30_tmp = X3_tmp{end};
    X40_tmp = X4_tmp{end};
    
    % remove added points
    if flagAddMiddle == 1
        X1_tmp(2) = [];
        X2_tmp(2) = [];
        X3_tmp(2) = [];
        X4_tmp(2) = [];
        Ts_tmp(2) = [];
    end
    if flagAddRight == 1
        X1_tmp(end) = [];
        X2_tmp(end) = [];
        X3_tmp(end) = [];
        X4_tmp(end) = [];
        Ts_tmp(end) = [];
    end
    if flagAddLeft == 1
        X1_tmp(1) = [];
        X2_tmp(1) = [];
        X3_tmp(1) = [];
        X4_tmp(1) = [];
        Ts_tmp(1) = [];
    end
    
    X1 = [X1; X1_tmp];
    X2 = [X2; X2_tmp];
    X3 = [X3; X3_tmp];
    X4 = [X4; X4_tmp];
    Ts = [Ts; Ts_tmp];
end

% deal with t outside of [0, 1]
idx = find( tSpan < 0 );
if ~isempty(idx)
    tSpanTmp = [0 fliplr(tSpan(idx))];
    [X1_tmp, X2_tmp, X3_tmp, X4_tmp, Ts_tmp] = integrateForwardWithODE45Spline(X10, X20, ...
        X30, X4s{1}, tSpanTmp);
    X1_tmp(1) = [];
    X2_tmp(1) = [];
    X3_tmp(1) = [];
    X4_tmp(1) = [];
    Ts_tmp(1) = [];
    [~, idxSort] = sort(Ts_tmp);
    X1 = [X1_tmp(idxSort); X1];
    X2 = [X2_tmp(idxSort); X2];
    X3 = [X3_tmp(idxSort); X3];
    X4 = [X4_tmp(idxSort); X4];
    Ts = [Ts_tmp(idxSort); Ts];
end

idx = find( tSpan > 1 );
if ~isempty(idx)
    tSpanTmp = [1 tSpan(idx)];
    [X1_tmp, X2_tmp, X3_tmp, X4_tmp, Ts_tmp] = integrateForwardWithODE45Spline(X10_tmp, X20_tmp, ...
        X30_tmp, X40_tmp, tSpanTmp);
    X1_tmp(1) = [];
    X2_tmp(1) = [];
    X3_tmp(1) = [];
    X4_tmp(1) = [];
    Ts_tmp(1) = [];
    X1 = [X1; X1_tmp];
    X2 = [X2; X2_tmp];
    X3 = [X3; X3_tmp];
    X4 = [X4; X4_tmp];
    Ts = [Ts; Ts_tmp];
end

