% 
% set initial values for spline
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/25/2014
%

function [X10, X20, X30, X4s] = setInitialValueSpline( params )

if isfield(params, 'X10GT') 
    X10 = params.X10GT;
else
    X10 = params.Ys{1};
end

if isfield(params, 'X20GT')
    X20 = params.X20GT;
else
    X20 = zeros(size(X10));
end
%X20 = X20 - X10 * (X10' * X20);

if isfield(params, 'X30GT')
    X30 = params.X30GT;
else
    X30 = zeros(size(X10));
end
%X30 = X30 - X10 * (X10' * X20);

if isfield(params, 'X4sGT')
    X4s = params.X4sGT;
else
    X4s = cell(length(params.cps)+1, 1);
    for iI = 1:length(X4s)
        X4s{iI} = zeros(size(X10));
    end
end