% 
% estimate independent value using cubic spline
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 10/11/2014
%

function cntEst = projectPointsToGeodesicODE45Spline(X10, X20, X30, X4s, tPos, omaTest, lb, ub, nt)
        
assert( lb < ub );
        
% normalize the upper and lower boundaries
lbNormalized = (lb - tPos(1)) / (tPos(end) - tPos(1));
ubNormalized = (ub - tPos(1)) / (tPos(end) - tPos(1));

cps = tPos(2:end-1);
cps = (cps - tPos(1)) / (tPos(end) - tPos(1));

params.h = 1.0/nt;
cps = min( max( round(cps / params.h) + 1, 1), nt+1 );

lbPos = round( lbNormalized / params.h ) + 1;
ubPos = round( ubNormalized / params.h ) + 1;

if lb >= tPos(1)

    if ubPos > nt+1
        keyPos = [1 cps ubPos];
        tTmp = 0:params.h:(ubPos-1)*params.h;
    else
        keyPos = [1 cps nt+1];
        tTmp = 0:params.h:1;
    end
    params.Gns = keyPos(2:end) - keyPos(1:end-1);
    [pointsTmp] = integrateForwardEachIntervalSpline( X10, X20, X30, X4s, params );
    pointsSyn = [];
    for iITmp = 1:length(pointsTmp)
        if iITmp == 1
            pointsSyn = [pointsSyn, pointsTmp{iITmp}];
        else
            pointsSyn = [pointsSyn, pointsTmp{iITmp}(2:end)];
        end
    end
    

elseif ub > tPos(1)
    
    % first part, 0 - ubPos
    if ubPos > nt+1
        keyPos = [1 cps ubPos];
        tTmp = 0:params.h:(ubPos-1)*params.h;
    else
        keyPos = [1 cps nt+1];
        tTmp = 0:params.h:1;
    end
    params.Gns = keyPos(2:end) - keyPos(1:end-1);
    [pointsTmp] = integrateForwardEachIntervalSpline( X10, X20, X30, X4s, params );
    pointsSyn = [];
    for iITmp = 1:length(pointsTmp)
        if iITmp == 1
            pointsSyn = [pointsSyn, pointsTmp{iITmp}];
        else
            pointsSyn = [pointsSyn, pointsTmp{iITmp}(2:end)];
        end
    end
    
    % second part, lbPos - 0
    assert(lbPos <= 0 );
    keyPos = [1 2-lbPos];
    params.Gns = keyPos(2:end) - keyPos(1:end-1);
    X4sNew{1} = -X4s{1};
    [pointsTmp] = integrateForwardEachIntervalSpline( X10, -X20, X30, X4sNew, params );
    pointsSyn = [pointsTmp{1}(2:end), pointsSyn];
    tTmpSecond = 0:params.Gns;
    tTmpSecond = tTmpSecond * (-params.h);
    tTmp = [tTmpSecond(2:end) tTmp];
else
    assert(lbPos <= 0 );
    keyPos = [1 2-lbPos];
    params.Gns = keyPos(2:end) - keyPos(1:end-1);
    X4sNew{1} = -X4s{1};
    [pointsSyn] = integrateForwardEachIntervalSpline( X10, -X20, X30, X4sNew, params );
    tTmp = 1:params.Gns+1;
    tTmp = tTmp * (-params.h);
end
    
assert( length(tTmp) == length(pointsSyn) );
tTmp = tTmp * (tPos(end) - tPos(1)) + tPos(1);

cntEst = zeros(length(omaTest), 1);
for iI = 1:length(omaTest)
    minValue = -1;
    minId = 0;
    for iJ = 1:length(pointsSyn)
        dist = grarc(omaTest{iI}, pointsSyn{iJ});
        if iJ == 1 || dist < minValue
            minValue = dist;
            minId = iJ;
        end
    end
    cntEst(iI) = tTmp(minId);
end