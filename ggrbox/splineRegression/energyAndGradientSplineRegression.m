% 
% compute energy and gradient for spline regression using fmincon
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/24/2014
%

function [energy, gradient] = energyAndGradientSplineRegression( Y, params )

% get the size for X1...
nLen = size(params.Ys{1}, 1);
X10 = Y(1:nLen, :);
X20 = Y(nLen+1:2*nLen, :);
X30 = Y(2*nLen+1:3*nLen, :);
X4s = cell(params.nPieces, 1);
for iI = 1:length(X4s)
    X4s{iI} = Y((2+iI)*nLen+1:(3+iI)*nLen, :);
end

% integrate forward
[X1, X2, X3, X4, Y_RK] = integrateForwardEachIntervalSpline( X10, X20, X30, X4s, params );

% compute energy
[energy, X1s] = computeEnergySpline( X1, X3, params );

if ~isfield(params, 'cubicSpline') || ~params.cubicSpline 
    if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
        h = figure(params.fidSplineFitting);
        clf(h);
        hold on
        plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
        plotGeodesic( X1s, params.ts_pre, params.pDim, '--' );
        if ~isempty(params.cps_pre)
            Ycps = cell(length(params.cps_pre), 1);
            for iTmp = 1:length(Ycps)
                Ycps{iTmp} = X1{iTmp}{end};
            end
            plotGeodesic( Ycps, params.cps_pre, params.pDim, '*' );
        end
        if isfield(params, 'Y_noise')
            plotGeodesic( params.Y_noise, params.t_truth, params.pDim, 'o' );
        end
        drawnow
        hold off
    end
end

% integrate backward
[gradX10, gradX20, gradX30, gradX4s] = integrateBackwardAndUpdateGradientSpline(X1, X2, X3, X1s, Y_RK, params);

% do not change the first state
gradX10 = zeros(size(gradX10));

gradient = [gradX10; gradX20; gradX30];
for iI = 1:length(gradX4s)
    gradient = [gradient; gradX4s{iI}];
end

%{

if isfield(params, 'X10GT')
    %disp('X10 is set to be ground truth, it will not change during the optimization');
    gradX10 = zeros(size(gradX10));
else
    %gradX10 = gradX10 - X10 * (X10' * gradX10);
end

if isfield(params, 'X20GT')
    %disp('X20 is set to be ground truth, it will not change during the optimization');
    gradX20 = zeros(size(gradX20));
else
    %gradX20 = gradX20 - X10 * (X10' * gradX20);
end

if isfield(params, 'X30GT')
    %disp('X30 is set to be ground truth, it will not change during the optimization');
    gradX30 = zeros(size(gradX30));
else
    %gradX30 = gradX30 - X10 * (X10' * gradX30);
end

gradient = [gradX10; gradX20; gradX30];
if isfield(params, 'X4sGT')
    %disp('X40 is set to be ground truth, it will not change during the optimization');
    for iI = 1:length(gradX4s)
        gradient = [gradient; zeros(size(gradX4s{iI}))];
    end
else
    for iI = 1:length(gradX4s)
        gradient = [gradient; gradX4s{iI}];
    end
end
%}