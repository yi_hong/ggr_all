function setup()
    disp('Set up ...');
    addpath(genpath(pwd));
    
    % Import 3rd party code
%     cwd = pwd;
%     cd '../3rdparty/manopt/';
%     importmanopt
%     cd(cwd);
%     addpath('../3rdparty/export_fig');
    
    % Competitors
    addpath('../Rentmeesters11a/');
    addpath('../Su12a');

    % Setup environment for automatic differentiation
%     setenv('ADIMAT_HOME', '/Users/rkwitt/Software/adimat-0.6.0-4971-GNU_Linux-x86_64' );
%     run('/Users/rkwitt/Software/adimat-0.6.0-4971-GNU_Linux-x86_64/ADiMat_startup.m');
    %run('../3rdparty/adimatMacOS/ADiMat_startup');
end