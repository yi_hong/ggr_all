%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
%  compute energy and gradient for shooting
%
%  Author: Yi Hong
%  Date: 08-30-2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [energyTotal, gradientX0, energyS] = energyAndGradientFullRegression( Y, params ) 

len = params.nsize(1);

% linesearch
%Y(len+1:end, :) = parallelTransport( Y(len+1:end, :), params.X10_pre, Y(1:len, :), 1.0 ); 
%Y(len+1:end, :) = (eye(len) - Y(1:len, :)*Y(1:len, :)') * Y(len+1:end, :); 
%params.X10_pre = Y(1:len, :);

% forward in time
[X1, X2, Y_RK] = integrateForwardMP( Y(1:len, :), Y(len+1:end, :), params.nt, params.h );

% compute the energy of the initial velocity
energyV = params.alpha * trace( (X2{1})' * X2{1} );

if isfield(params, 'diagSigmaSqs') 
    % modified on 05/18/16, energy computed with estimated sigma
    energyS = 0;
%     matSigma = diag(1.0 ./ params.diagSigmaSqs);
    params.X1s = cell(length(params.ts), 1);
    for iI = 1:length(params.ts)
        params.X1s{iI} = X1{params.ts(iI)};
        [~, ~, ~, vTmp] = grgeo(params.X1s{iI}, params.Ys{iI}, 1, 'v3', 'v2');
%         energyS = energyS + norm(sqrt(matSigma)*vTmp(:)).^2 * params.wi(iI);
        tmpDistSq = computeGeodesciDistanceWithSigma(params.diagSigmaSqs{iI}, vTmp);
        energyS = energyS + tmpDistSq * params.wi(iI);
    end
    energyS = energyS / 2.0;  % here we compute energy / 2sigma^2
else
    % compute the energy of the similarity term
    energyS = 0;
    params.X1s = cell(length(params.ts), 1);
    for iI = 1:length(params.ts)
        params.X1s{iI} = X1{params.ts(iI)};
        energyS = energyS + grarc(params.X1s{iI}, params.Ys{iI}) * params.wi(iI);
    end
    energyS = energyS / params.sigmaSqr;
end

% compute the total energy
energyTotal = energyV + energyS;

% backward in time
lam1_end = zeros( size(X1{end}) );
lam2_end = zeros( size(lam1_end) );
[lam1, lam2] = integrateBackwardMP( lam1_end, lam2_end, Y_RK, params.X1s, params );

% compute the gradient to update
% update on 05/12/2016, from negative gradient to positive 
tmp1 = eye(size(X1{1}, 1)) - X1{1} * (X1{1})';
gradientX10 = -tmp1 * lam1{end} + X2{1} * (lam2{end})' * X1{1};
gradientX20 = 2*X2{1}*params.alpha - tmp1 * lam2{end};

gradientX0 = [gradientX10; gradientX20];

% update on 05/12/2016 for some need of no x1 update
if isfield(params, 'noX1Update') == 1 && params.noX1Update == 1
    gradientX0 = [zeros(size(gradientX10)); gradientX20];
end
