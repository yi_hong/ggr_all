%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Compute the energy
%
%  Author: Yi Hong
%  Date: 10/13/2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [energyTotal, X1s] = computeEnergy( X1, X2, params )


% compute the energy of the initial velocity
energyV = trace( (X2{1})' * X2{1} ) * params.alpha;

if isfield(params, 'diagSigmaSqs') 
    % modified on 05/18/16, energy computed with estimated sigma
    energyS = 0;
%     matSigma = diag(1.0 ./ params.diagSigmaSqs);
    X1s = cell(length(params.ts), 1);
    for iI = 1:length(params.ts)
        X1s{iI} = X1{params.ts(iI)};
        [~, ~, ~, vTmp] = grgeo(X1s{iI}, params.Ys{iI}, 1, 'v3', 'v2');
%         energyS = energyS + norm(sqrt(matSigma)*vTmp(:)).^2 * params.wi(iI);
        tmpDistSq = computeGeodesciDistanceWithSigma(params.diagSigmaSqs{iI}, vTmp);
        energyS = energyS + tmpDistSq * params.wi(iI);
    end
    energyS = energyS / 2.0;  % here we compute energy / 2sigma^2
else
    % compute the energy of the similarity term
    energyS = 0;
    X1s = cell(length(params.ts), 1);
    for iI = 1:length(params.ts)
        X1s{iI} = X1{params.ts(iI)};
        energyS = energyS + grarc(X1s{iI}, params.Ys{iI}) * params.wi(iI);
    end

    %energyS = sumDistOfPointsToGeodesicODE45(X1{1}, X2{1}/(max(params.ts_pre)-min(params.ts_pre)), ...
    %    params.ts_pre(1), params.Ys, params.ts_pre, -1, params.useODE45);

    energyS = energyS / params.sigmaSqr;
end

% compute the total energy
energyTotal = energyV + energyS;


