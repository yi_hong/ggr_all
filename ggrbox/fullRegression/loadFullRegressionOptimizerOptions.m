function opt = loadFullRegressionOptimizerOptions(params)

opt = [];

% optimizer options minfunc
optMinFunc.MaxIter = 10000;
optMinFunc.Display = 'iter';
optMinFunc.Method = 'lbfgs';
optMinFunc.Corr = 5;
optMinFunc.LS_type = 0;
optMinFunc.progTol = 1e-4;
opt.minfunc = optMinFunc;

% optimizer options fmincon
options_fmincon = optimset( 'fmincon' );
options_fmincon = optimset( options_fmincon, 'Display', 'iter' );
options_fmincon = optimset( options_fmincon, 'FunValCheck', 'off' );
options_fmincon = optimset( options_fmincon, 'GradObj', 'on' );
options_fmincon = optimset( options_fmincon, 'algorithm', 'interior-point' );
options_fmincon = optimset( options_fmincon, 'Hessian', 'lbfgs' );
options_fmincon = optimset( options_fmincon, 'MaxFunEvals', 10000 );
%options_fmincon = optimset( options_fmincon, 'InitBarrierParam', 1e-3);
%options_fmincon = optimset( options_fmincon, 'TolX', 1e-14 );
%options_fmincon = optimset( options_fmincon, 'TolFun', 1e-14 );
%options_fmincon = optimset( options_fmincon, 'OutputFcn', 'augmentedfminconoutput' );

if nargin >= 1
    if isfield( params, 'maxIterFmincon')
        options_fmincon = optimset( options_fmincon, 'MaxIter', params.maxIterFmincon );
    end
end

opt.fmincon = options_fmincon;