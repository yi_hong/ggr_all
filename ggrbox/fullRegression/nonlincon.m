% Nonlinear constraints function
function [c, ceq] = nonlincon(x)

len = floor( size(x, 1) /2 );
Y = x(1:len, :);
Ydot = x(len+1:end, :);

c = [];
ceq1 = Y' * Y - eye(size(Y, 2));
ceq2 = Y' * Ydot;
ceq = [ceq1; ceq2];