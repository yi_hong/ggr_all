%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Line search
%
%  Author: Yi Hong
%  Date: 10/13/2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Y, currentEnergy] = lineSearchUpdate(Y0, params)

Y = Y0;
len = floor(size(Y, 1)/2);

% integrate forward in time
[X1, X2, Y_RK] = integrateForwardMP( Y(1:len, :), Y(len+1:end, :), params.nt, params.h );

% compute energy
[currentEnergy, X1s] = computeEnergy( X1, X2, params );
if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
&& ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
    fprintf( 'Full GGR (FGGR) Intial energy = %f\n', currentEnergy );
end

% plot the energy
figure(200), hold on;
clr = rand(1, 3);
eng_pre = currentEnergy;
figure(200), plot( 0, eng_pre, 'p', 'LineWidth', 4, 'Color', clr );
title('Regression Energy');
xlabel('Iteration');
ylabel('Energy');

stopIteration = false;

for iI = 1:params.nIterMax
    
    % integrate backward in time
    lam1_end = zeros( size(X1{end}) );
    lam2_end = zeros( size(lam1_end) );
    [lam1, lam2] = integrateBackwardMP( lam1_end, lam2_end, Y_RK, X1s, params );

    % compute the gradient for updating
    % this is negative gradient
    gradientX10 = lam1{end} - X1{1} * ( (X1{1})' * lam1{end} ) - X2{1} * ( (lam2{end})' * X1{1} );
    % this is positive gradient, will be negative when using it
    gradientX20 = 2*params.alpha*X2{1} - lam2{end} + X1{1} * ( (X1{1})' * lam2{end} );
    
    % update 
    dt = params.deltaT;
    energyReduced = false;
    
    %pk = [reshape(gradientX10, [], 1); reshape(-gradientX20, [], 1) ];
    %gk = -pk;
    
    curY = Y;
    % make the gradient to be tangent to the point
    gradientX10 = gradientX10 - curY(1:len, :) * (curY(1:len, :)' * gradientX10);
    for iJ = 1:params.maxReductionSteps
        
        % integrate forward to update the initial point
        if params.useODE45
            [~, Ytmp] = integrateForwardWithODE45( curY(1:len, :), gradientX10, [0 dt/2 dt] );
        else
            [Ytmp] = integrateForwardToGivenTime( curY(1:len, :), gradientX10, dt, params.h );
        end
        Y(1:len, :) = Ytmp{end};
       
        % because the gradient is positive, use '-'
        Y(len+1:end, :) = curY(len+1:end, :) - dt * gradientX20;
        % make the velocity to be tangent to the current point
        Y(len+1:end, :) = Y(len+1:end, :) - curY(1:len, :) * (curY(1:len, :)' * Y(len+1:end, :));
        % parallel transport
        %Y(len+1:end, :) = parallelTransport( Y(len+1:end, :), curY(1:len, :), Y(1:len, :), dt, params.useODE45 );
        Y(len+1:end, :) = ptEdelman(curY(1:len, :), gradientX10, Y(len+1:end, :), dt);
        % project to the new point
        Y(len+1:end, :) = Y(len+1:end, :) - Y(1:len, :) * (Y(1:len, :)' * Y(len+1:end, :));
        
        if sum(sum(isnan(Y))) > 0 
            if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
            && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )        
                fprintf('#');
            end
            dt = dt * params.rho;
            continue; 
        end
        
        % integrate forward in time
        [X1, X2, Y_RK] = integrateForwardMP( Y(1:len, :), Y(len+1:end, :), params.nt, params.h );
        
        if sum(sum(isnan(X1{end}))) + sum(sum(isnan(X2{end}))) > 0 
            if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
            && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
                fprintf('#');
            end
            dt = dt * params.rho;
            continue; 
        end

        % compute energy
        [energy, X1s] = computeEnergy( X1, X2, params );
        %wolfeDecrement = params.c1 * dt * dot(pk, gk);
        
        if energy >= currentEnergy
            if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
            && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
                fprintf('#');
            end
        else
            % energy decreased
            currentEnergy = energy;
            energyReduced = true;
            if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
            && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
                fprintf( 'FGGR Iter %04d: Energy = %f\n', iI, currentEnergy );
            end
            figure(200), plot( [iI-1 iI], [eng_pre currentEnergy], '-o', 'LineWidth', 2, 'Color', clr );
            drawnow
            
            % comparison with the ground-truth
            % this is not shown during doing the optimization of the breakpoint
            if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
            && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
                if isfield(params, 't_truth') && isfield(params, 'Y_truth') && isfield(params, 'pDim')
                    h = figure(100);
                    clf(h);
                    hold on
                    plotGeodesic( params.Y_truth, params.t_truth, params.pDim, '-' );
                    plotGeodesic( X1s, params.ts_pre, params.pDim, '--' );
                    if isfield(params, 'Y_noise')
                        plotGeodesic( params.Y_noise, params.t_truth, params.pDim, 'o' );
                    end
                    drawnow
                    hold off
                end
            end
            
            if eng_pre - currentEnergy < params.stopThreshold
                stopIteration = true;
            end
            eng_pre = currentEnergy;
            
            break;
        end
        dt = dt * params.rho;
    end
    
    if ~energyReduced
        if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
        && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
            fprintf( 'FGGR Iteration %04d: Could not reduce energy further. Quitting.\n', iI );
        end
        Y = curY;
        break;
    end
    
    if stopIteration
        if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
        && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
            fprintf( 'FGGR Quitting because the decreased energy is smaller than the threshold.\n' );
        end
        break;
    end
end
    


