%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Implements Algorithm 3, geodesic regression algorithm for multiple points, 
%  which is in the paper, "Geodesic regression on the grassmann manifold"
% 
%  Data: params (parameters for full regression)
%          .Ys       (measurements)
%          .ts       (time points for each point)
%          .t0       (the time point for the initial point)
%          .sigmaSqr (weight for similarity term)
%          .nt       (number of desired time-steps)
%          .h        (time step)
%  Result: pntY, tanY (initial point and velocity, from 0 to 1, scale it if needed)
%
%  Author: Yi Hong, yihong@cs.unc.edu
%  Date: 08-30-2013
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [pntY, tanY, energy, pntY0, tanY0, params] = fullRegressionOnGrassmannManifold( params )

% adjust the time points with the initial point
[params.ts, idSort] = sort( params.ts );
params.Ys = params.Ys(idSort);
params.wi = params.wi(idSort);
% already sorted during the estimation
% if isfield(params, 'diagSigmaSqs')
%     params.diagSigmaSqs = params.diagSigmaSqs(idSort);  
% end

% here, pntY0 and tanY0 are at t0
if isfield(params, 'Y0Init') && isfield(params, 'Y0dotInit') 
    % Initialize with the inputs
    if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
    && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
        disp('Initialize with given values');
    end
    pntY0 = params.Y0Init;
    tanY0 = params.Y0dotInit{1};
    tanY0 = tanY0 * (max(params.ts) - min(params.ts));  % scale to 1
    params.Y0dotInit{1} = tanY0;                        % this for comparison

elseif isfield( params, 'pntY0' ) && isfield( params, 'tanY0' ) && isfield( params, 'pntT0' )
    % Initialize with the input the initialization, usually from other methods
    if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
    && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
        disp('Initialize using results from given values or another method');
    end
    minT = min(params.ts);
    maxT = max(params.ts);
    pntY0 = params.pntY0;
    tanY0 = params.tanY0;
    if params.pntT0 > minT
        if params.useODE45
            [~, Ytmp, Ydottmp] = integrateForwardWithODE45( pntY0, -tanY0, [0 (params.pntT0-minT)/2.0 params.pntT0-minT] );
        else
            [Ytmp, Ydottmp] = integrateForwardToGivenTime( pntY0, -tanY0, params.pntT0-minT, (params.pntT0-minT)/50.0 );
        end
        pntY0 = Ytmp{end};
        tanY0 = -Ydottmp{end};
    elseif params.pntT0 < minT
        if params.useODE45
            [~, Ytmp, Ydottmp] = integrateForwardWithODE45( pntY0, tanY0, [0 (minT-params.pntT0)/2.0 minT-params.pntT0] );
        else
            [Ytmp, Ydottmp] = integrateForwardToGivenTime( pntY0, tanY0, minT-params.pntT0, (minT-params.pntT0)/50.0 );
        end
        pntY0 = Ytmp{end};
        tanY0 = Ydottmp{end};
    end
    tanY0 = tanY0 * (maxT-minT);    % scale to 1
else
    % initialize with the pairwise searching
    if ( ~isfield(params, 'optimalBreakpoint') || ~params.optimalBreakpoint ) ...
    && ( ~isfield(params, 'timeWarpRegression') || ~params.timeWarpRegression )
        disp('Initialize using results from pairwise-searching computation');
    end
    [pntY0, tanY0] = initializeRegression( params.Ys, params.ts, params.alpha, params.sigmaSqr, params.useODE45, params.useRansac, params.wi );
end

% convert independent values into the discretized ids
params.ts_pre = params.ts;   
params.ts = params.ts - params.ts(1);
params.ts = params.ts ./ params.ts(end);
params.ts = min( max( round(params.ts / params.h) + 1, 1 ), params.nt+1 );

if strcmp( params.minFunc, 'linesearch' )
    [Y, energy] = lineSearchUpdate( [pntY0; tanY0], params );
    
elseif strcmp( params.minFunc, 'both' )
    % updated on 05/12/2016
    optOpts = loadFullRegressionOptimizerOptions();
    
    pntY = pntY0;
    tanY = tanY0;
    params.noX1Update = 1;
    for iter = 1:5
        [Y] = fmincon( @(Y)energyAndGradientFullRegression(Y, params), [pntY; tanY], ...
                    [], [], [], [], [], [], @nonlincon, optOpts.fmincon ); 
        pntY = Y(1:params.nsize(1), :);
        tanY = Y(params.nsize(1)+1:end, :); 
        
        [Y, energy] = lineSearchUpdate( [pntY; tanY], params );
        pntY = Y(1:params.nsize(1), :);
        tanY = Y(params.nsize(1)+1:end, :);  
     
    end
    params = rmfield(params, 'noX1Update');
 
else
    error('Not supported optimization method');
end

pntY = Y(1:params.nsize(1), :);
tanY = Y(params.nsize(1)+1:end, :);  
