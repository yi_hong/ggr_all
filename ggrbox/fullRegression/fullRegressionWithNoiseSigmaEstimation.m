% 
% regression and estimate sigmas of noise model
%

function [X10, X20, energy, grParamsUsed] = fullRegressionWithNoiseSigmaEstimation(grParams)

sigmaSq = grParams.diagSigmaSqs;
for iter = 1:grParams.nSigmaEstimate

    if iter == 1
        constant = 1;
    else
        constant = mean(abs(sigmaSq{1}(:)));
    end
    for iI = 1:length(grParams.diagSigmaSqs)     
        grParams.diagSigmaSqs{iI} = sigmaSq{iI} ./ constant;
    end
    
    if iter > 1
        grParams.pntY0 = X10;
        grParams.tanY0 = X20;
    end
    [X10, X20, energy, ~, ~, grParamsUsed] = fullRegressionOnGrassmannManifold(grParams);
    grParamsUsed.diagSigmaSqs = sigmaSq;

    [~, X1s, ~] = integrateForwardWithODE45(X10, X20, (0:grParamsUsed.nt)*grParamsUsed.h);

    sigmaSq = estimateSigmaOfNoiseModel(X1s(grParamsUsed.ts), grParamsUsed.Ys);

end

end