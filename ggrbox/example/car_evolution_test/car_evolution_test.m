% traffic data

close all
clear all

profile -memory on

grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_042214/car_3Ms';

%grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';
%outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_042214/car_3Ms2';

fprintf(outputPrefix);

setup();

grParams.featureName = 'decaf';  % fish4K, decaf6, decaf, phow
grParams.dim = 10;
grParams.lam = 0.3;  % percentage of sample size for testing
grParams.manufacturer = -2;  % -2: all brands but seperate, -1: all brands, 0: Mercedes, 1: BMW, 2: VW
%grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression', 'ContinuousPiecewiseRegression', 'ContinuousPiecewiseRegressionWithOptimalBreakpoints'}; 
grParams.regressMethod = {'FullRegression', 'ContinuousPiecewiseRegression'}; 
%grParams.regressMethod = {'PiecewiseRegression'};
grParams.minValue = 0;
grParams.maxValue = 40;

grParams.sigmaSqr = 1;
grParams.alpha = 0.0;
grParams.nt = 100;
grParams.deltaT = 0.2;
grParams.deltaTBk = 0.05;
grParams.maxReductionSteps = 10;
grParams.rho = 0.5;
grParams.nIterMax = 300;
grParams.nIterMaxBk = 50;
grParams.stopThreshold = 1e-6;
grParams.useODE45 = true;
grParams.minFunc = 'linesearch';
grParams.estErr = 0.05;
grParams.bks = [23];
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue];
grParams.useWeightedData = false;
grParams.useRansac = true;
grParams.useFullInitializePiecewise = true;
grParams.useFullInitializeContinuous = false;
grParams.usePiecewiseInitializeContinuous = true;
grParams.useContinuousInitializeContinuousOB = true; % use CPGGR to initialize the CPGGR with optimal breakpoint

grParams.optimizeBreakpoint = 2; % 1: update initial conditions and breakpoints simultaneously, 2: iteratively 

% load feature
[subSpaceAll, xYearAll, dataMeanAll, dataAll, labelAll, classes, xYearBase, ...
    range, accBaseline] = loadCarFeaturesAndPCA( grParams );

% compute the distance between points for each class
figure(2000);
distSqrMap = cell(length(dataAll), 1);
brandName = {'Mercedes', 'BMW', 'VolksWagen'};
for iS = 1:length(subSpaceAll)
    subSpaceTmp = subSpaceAll{iS};
    distSqrMap = zeros(length(subSpaceTmp), length(subSpaceTmp));
    for iI = 1:length(subSpaceTmp)
        for iJ = 1:length(subSpaceTmp)
            distSqrMap(iI, iJ) = sqrt(grarc(subSpaceTmp{iI}, subSpaceTmp{iJ}));
        end
        distSqrMap(iI, iI) = NaN;
    end
    subplot(1, length(subSpaceAll), iS), imagesc(distSqrMap);
    set(gca, 'xTickLabel', num2str(xYearAll{iS}), 'yTickLabel', num2str(xYearAll{iS}));
    %colormap(cool);
    title(brandName{iS});
end

errSqr = zeros(length(subSpaceAll), length(dataAll), length(grParams.regressMethod));
SAdapted = cell(length(subSpaceAll), length(grParams.regressMethod));
SNonAdapted = cell(length(subSpaceAll), 1);
SLocalAdapted = cell(length(subSpaceAll), 1);
SLocalNonAdapted = cell(length(subSpaceAll), 1);
SKernelAdapted = cell(length(subSpaceAll), 1);

meanLocalAdapted = cell(length(subSpaceAll), 1);
meanLocalNonAdapted = cell(length(subSpaceAll), 1);
meanNonAdapted = cell(length(subSpaceAll), 1);

accuracyAdapted = zeros(length(dataAll), length(grParams.regressMethod));
accuracyNonAdapted = zeros(length(dataAll), 1);
accuracyLocalAdapted = zeros(length(dataAll), 1);
accuracyLocalNonAdapted = zeros(length(dataAll), 1);
accuracyKernelAdapted = zeros(length(dataAll), 1);

assert( length(dataAll) == length(labelAll) && length(labelAll) == length(xYearBase) );
xticklabel = cell(1, length(dataAll));

for iP = 1:length(dataAll)
    
    % regress for each type of subspace
    for iType = 1:length(subSpaceAll)
        subSpace = subSpaceAll{iType};
        xYear = xYearAll{iType};
        indTest = find(xYear == xYearBase(iP));
        grParams.nSys = length(subSpace);
        grParams.nTesting = indTest;
        grParams.nSampling = 1:grParams.nSys;
        grParams.nSampling(grParams.nTesting) = [];
        if isempty(indTest)
            grParams.nTesting = grParams.nSampling;
        end
        
        % Regression
        %{
        [yearEst, errEst, tPos, initPnt, initVel, totEnergy, timeWarpParams] = ...
            estimateOnFeature( subSpace, xYear, grParams, iP );
        
        % compute the test error
        for iJ = 1:length(initPnt)
            if isempty(indTest)
                subSpaceTest = [];
                xYearTest = xYearBase(iP);
            else
                subSpaceTest = subSpace(grParams.nTesting);
                xYearTest = xYear(grParams.nTesting);
            end
            [errSqr(iType, iP, iJ), SAdapted{iType, iJ}] = computeGeodesicTestError( initPnt{iJ}, initVel{iJ}, ...
                tPos{iJ}, timeWarpParams{iJ}, subSpaceTest, xYearTest );
        end
        %}
        
        % compute kernal adapted subspace for each class
        %[~, SKernelAdapted{iType}] = kernelGeodesicRegression(subSpace, xYear, xYearBase(iP), 10, grParams);
        
        % compute local adapted subspace for each class
        [SLocalAdapted{iType}, meanLocalAdapted{iType}] = localTwoPointsGeodesicRegression(subSpace, dataMeanAll{iType}, xYear, xYearBase(iP));
          
        % compute local non-adapted subspace for each class
        [SLocalNonAdapted{iType}, meanLocalNonAdapted{iType}] = localTwoPointsNonAdapted(dataAll, ...
            labelAll, classes(iType), xYear, xYearBase(iP), xYearBase, grParams.dim);
        
        % compute non-adapted subspace for each class
        [SNonAdapted{iType}, meanNonAdapted{iType}] = globalNonAdapted(dataAll, labelAll, classes(iType), iP, grParams.dim);
    
        % compute the distance
        [posTmp] = find(xYear == xYearBase(iP));
        if ~isempty(posTmp)
            fprintf('Distance -- Local adapted to test data: %.3f\n', sqrt(grarc(SLocalAdapted{iType}, subSpace{posTmp})));
            fprintf('Distance -- Local non-adapted to test data: %.3f\n', sqrt(grarc(SLocalNonAdapted{iType}, subSpace{posTmp})));
            fprintf('Distance -- Non-adapted to test data: %.3f\n', sqrt(grarc(SNonAdapted{iType}, subSpace{posTmp})));
            fprintf('Distance -- Local adapted to Local non-adapted: %.3f\n', sqrt(grarc(SLocalAdapted{iType}, SLocalNonAdapted{iType})));
        end
    end
    
    % compute classification accuracy
    %{
    for iJ = 1:length(initPnt)
        SAdaptedTmp = cell(size(SAdapted, 1), 1);
        for iK = 1:size(SAdapted, 1)
            tmp = SAdapted(iK, iJ);
            SAdaptedTmp{iK} = tmp{1}{1}; 
        end
        accuracyAdapted(iP, iJ) = computeClassifyAccuracy( SAdaptedTmp, dataAll{iP}, labelAll{iP}, classes ); 
    end
    %}
    %accuracyKernelAdapted(iP) = computeClassifyAccuracy( SKernelAdapted, dataAll{iP}, labelAll{iP}, classes );
    accuracyLocalAdapted(iP) = computeClassifyAccuracy( SLocalAdapted, dataAll{iP}, labelAll{iP}, classes, meanLocalAdapted );
    accuracyLocalNonAdapted(iP) = computeClassifyAccuracy( SLocalNonAdapted, dataAll{iP}, labelAll{iP}, classes, meanLocalNonAdapted );
    accuracyNonAdapted(iP) = computeClassifyAccuracy( SNonAdapted, dataAll{iP}, labelAll{iP}, classes, meanNonAdapted );
    
    xticklabel{iP} = sprintf('%d -- %d', range(iP, 1), range(iP, 2));
end

accuracy = [accBaseline accuracyLocalNonAdapted accuracyLocalAdapted accuracyNonAdapted ...
            accuracyAdapted accuracyKernelAdapted];
legendNames = {'Baseline' 'Local non-Adapted', 'Local-Adapted', 'Non-Adapted', ...
         'Adapted (Full GGR)', 'Adapted (Piecewise)', 'Adapted (kernel)'};

figure(1002), bar( accuracy );
set(gca, 'XTickLabel', xticklabel );
legend( legendNames );
meanAccuracy = mean( accuracy );
titlename = {};
for iI = 1:length(meanAccuracy)
    tmpStr = sprintf('%s: %.3f ', legendNames{iI}, meanAccuracy(iI));
    titlename = [titlename tmpStr];
end
title(titlename);

figure(1001);
filename = sprintf('%s/PCA_verifyi_6M_500_kernel4_tmp.fig', outputPrefix);
saveas(gca, filename);
filename = sprintf('%s/PCA_verify_6M_500_kernel4_tmp.png', outputPrefix);
saveas(gca, filename);
figure(1002);
filename = sprintf('%s/predict_verify_6M_500_kernel4_tmp.fig', outputPrefix);
saveas(gca, filename);
filename = sprintf('%s/predict_verify_6M_500_kernel4_tmp.png', outputPrefix);
saveas(gca, filename);

%{
for iType = 1:length(subSpaceAll)
    
    subSpace = subSpaceAll{iType};
    xYear = xYearAll{iType};
    
    grParams.nSys = length(subSpace);

    R2 = zeros( 1, length(grParams.regressMethod) );
    errSqr = zeros( grParams.nSys, length(grParams.regressMethod) );

    nPartition = grParams.nSys;
    %nPartition = 1;
    for iP = 4:nPartition  % start with the fourth one
        grParams.nTesting = iP:nPartition:grParams.nSys;
        grParams.nSampling = 1:grParams.nSys;
        if nPartition > 1
            grParams.nSampling(grParams.nTesting) = [];
        end

        % training and testing
        [yearEst, errEst, tPos, initPnt, initVel, totEnergy, timeWarpParams] = ...
            estimateOnFeature( subSpace, xYear, grParams, iP );

        % save the plot
        filename = sprintf('%s/car_%s_train%d_%s_Type%d_Part%dOf%d.fig', ...
            outputPrefix, grParams.featureName, length(grParams.nSampling), ...
            grParams.minFunc, iType, iP, nPartition);
        saveas( gca, filename );
        filename = sprintf('%s/car_%s_train%d_%s_Type%d_Part%dOf%d.png', ...
            outputPrefix, grParams.featureName, length(grParams.nSampling), ...
            grParams.minFunc, iType, iP, nPartition);
        saveas( gca, filename );

        % save results
        filename = sprintf('%s/car_%s_train%d_%s_Type%d_Part%dOf%d.mat', ...
            outputPrefix, grParams.featureName, length(grParams.nSampling), ...
            grParams.minFunc, iType, iP, nPartition);
        save(filename, 'xYear', 'yearEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy');

        % compute test error
        for iJ = 1:length(initPnt)
            [errSqr(grParams.nTesting, iJ), omaEst] = computeGeodesicTestError( initPnt{iJ}{1}, initVel{iJ}{1}, tPos{iJ}(1), ...
                timeWarpParams{iJ}, subSpace(grParams.nTesting), xYear(grParams.nTesting) );
        end
        
        

        % compute the R2 statistic
        if nPartition == 1
            R2 = computeR2Statistic( initPnt, initVel, tPos, timeWarpParams, subSpace, xYear, grParams );
        end
    end
end

filename = sprintf('%s/errSqr_%d_parts.mat', outputPrefix, nPartition);
save(filename, 'errSqr');

%h = figure(1000);
%filename = sprintf('%s/car_optimal_breakpoints.fig', outputPrefix);
%saveas(h, filename);
%}

%p = profile('info');
%filename = sprintf('%s/car_speed_profile', outputPrefix);
%profsave(p, filename);
