function [subSpace, xYear, dataMean, dataTest, labelTest, classes, xYearBase, ...
    range, accBaseline] = loadCarFeaturesAndPCA( grParams )

fprintf('lam=%.3f\n', grParams.lam);
fprintf('dim=%.4d\n', grParams.dim);

% read the features
data_root = sprintf('%s/data/car/CEDv2-features/', grParams.inputPrefix);
info_file = fullfile(data_root, 'CEDv2-annotation.txt');
if strcmp(grParams.featureName, 'fish4K')
    data_file = fullfile(data_root, 'CEDv2.fish4K');
elseif strcmp(grParams.featureName, 'decaf6')
    data_file = fullfile(data_root, 'CEDv2.decaf6');
elseif strcmp(grParams.featureName, 'decaf')
    data_file = fullfile(data_root, 'CEDv2.decaf');
elseif strcmp(grParams.featureName, 'phow')
    data_file = fullfile(data_root, 'CEDv2.phow128');
end

if (exist('info') ~= 1)
    info = load(info_file);
end
if (exist('data') ~= 1)
    data = load(data_file);
end

%[pca_acc,svm_acc] = pca_clf(data,info,[1970 2000],1,5,10);

% group cars every five years
clear range;
%{
range = [...
    1970 1979; ...
    1980 1989; ...
    1990 1999; ...
    2000 2010];
xYearBase = [0, 10, 20, 30]';
%}


range = [...
    1970 1974; ...
    1975 1979; ...
    1980 1984; ...
    1985 1989; ...
    1990 1994; ...
    1995 1999; ...
    2000 2004; ...
    2005 2009];
xYearBase = (0:5:35)';


N =  size(range,1);

% types of subspace, based on the manufacturers
classes = unique(info(:,1));         

if grParams.manufacturer == -2
    M = length(classes);  
else
    M = 1;             % one subspace
end
    
D.S = cell(N,M);       % subspace
D.X = cell(N,M);       % data
D.p = cell(N,M);       % index 
indNull = cell(1, M);  % index for empty groups
nNumCounter = zeros(N, M);
accBaseline = zeros(N, 1);

for j = 1:M
    indNullTmp = [];
    for i=1:N
        if grParams.manufacturer == -1          % one subpsace with all manufacturers
            D.p{i, j} = find(info(:,3)>=range(i,1) & info(:,3)<=range(i,2));
        else
            if grParams.manufacturer == -2      % M subpsaces, one for each manufacturer
                brand = classes(j);
            else
                brand = grParams.manufacturer;  % one subspace, for the input manufacturer
            end
            D.p{i, j} = find(info(:,3)>=range(i,1) & info(:,3)<=range(i,2) & ...
                             info(:,1) == brand);
        end
        if isempty(D.p{i, j})                   % find empty group
            indNullTmp = [indNullTmp i];
        end
        D.X{i, j} = data(D.p{i, j},:);          % obtain data
        D.S{i, j} = compute_subspace(D.X{i, j},grParams.dim,-1,-1);  % compute subspace
        nNumCounter(i, j) = length(D.p{i, j});
    end
    indNull{j} = indNullTmp;
end

nNumCounter
accBaseline = max(nNumCounter, [], 2) ./ sum(nNumCounter, 2);

% clear the empty groups
subSpace = cell(M, 1);
xYear = cell(M, 1);
dataMean = cell(M, 1);
for j = 1:M
    tmp = D.S(:, j);
    tmp(indNull{j}) = [];
    subSpaceTmp = cell(length(tmp), 1);
    meanTmp = cell(length(tmp), 1);
    for i = 1:length(tmp)
        subSpaceTmp{i} = tmp{i}.P;
        meanTmp{i} = tmp{i}.M;
    end
    subSpace{j} = subSpaceTmp;
    dataMean{j} = meanTmp;
    tmpYear = xYearBase;
    tmpYear(indNull{j}) = [];
    xYear{j} = tmpYear;
end

% collect data and label for each time points
dataTest = cell(N, 1);
labelTest = cell(N, 1);
for i = 1:N
    dataTmp = [];
    labelTmp = [];
    for j = 1:M
        tmpMatrix = D.X(i,j);
        dataTmp = [dataTmp; tmpMatrix{1}];
        labelTmp = [labelTmp; ones(size(tmpMatrix{1}, 1),1)*classes(j)];
    end
    dataTest{i} = dataTmp;
    labelTest{i} = labelTmp;
end

% test PCA, only for time points with data for each manufacturer
testPoint = xYearBase;
indNullAll = [];
for j = 1:M
    indNullAll = [indNullAll indNull{j}];
end
testPoint(indNullAll) = [];

% Comparison
nTimes = 1;
nCount = 0;
nCountEach = zeros(length(testPoint), 1);
for nt = 1:nTimes
accuracyAdapted = zeros(length(testPoint), 1);
accuracyNonAdapted = zeros(length(testPoint), 1);
xticklabel = {};
for i = 1:length(testPoint)
    indCur = find(xYearBase == testPoint(i));
    
    % get the rest data for train
    restDataTrain = cell(M, 1);
    for j = 1:M
        restDataTrainTmp = [];
        for k = 1:length(xYearBase)
            if indCur == k
                continue;
            end
            tmpMatrix = D.X(k, j);
            restDataTrainTmp = [restDataTrainTmp; tmpMatrix{1}];
        end
        restDataTrain{j} = restDataTrainTmp;
    end
    
    % part for train, rest for test
    dataTmp = D.X(indCur, :);
    SAdapted = cell(M, 1);
    MAdapted = cell(M, 1);
    SNonAdapted = cell(M, 1);
    MNonAdapted = cell(M, 1);
    curDataTest = [];
    curLabelTest = [];
    for j = 1:M
        nNum = size(dataTmp{j}, 1);
        nTest = round(grParams.lam * nNum);
        rng('shuffle');
        indTmp = randperm(nNum);
        curDataTest = [curDataTest; dataTmp{j}(indTmp(1:nTest), :)];
        curLabelTest = [curLabelTest; ones(nTest,1) * classes(j)];
        curDataTrain{j} = dataTmp{j}(indTmp(nTest+1:end), :);
        
        restDataTrain{j} = [restDataTrain{j}; curDataTrain{j}];
        
        % compute subspace for each class, at this time point
        tmpS = compute_subspace(curDataTrain{j}, grParams.dim, -1, -1);
        SAdapted{j} = tmpS.P;
        MAdapted{j} = tmpS.M;
        tmpS = compute_subspace(restDataTrain{j}, grParams.dim, -1, -1);
        SNonAdapted{j} = tmpS.P;
        MNonAdapted{j} = tmpS.M;
    end
    
    accuracyAdapted(i) = computeClassifyAccuracy( SAdapted, curDataTest, curLabelTest, classes, MAdapted );
    accuracyNonAdapted(i) = computeClassifyAccuracy( SNonAdapted, curDataTest, curLabelTest, classes, MNonAdapted );
    
    fprintf('Testing time point: %d\n', testPoint(i));
    fprintf('Adapted: %.3f\n', accuracyAdapted(i));
    fprintf('NonAdapted: %.3f\n\n', accuracyNonAdapted(i));
    
    tmpStr = sprintf('%d -- %d', range(indCur, 1), range(indCur, 2));
    xticklabel = [xticklabel tmpStr];
end

tmpCount = accuracyAdapted > accuracyNonAdapted;
nCountEach = nCountEach + tmpCount;
if(mean(accuracyAdapted) > mean(accuracyNonAdapted))
    nCount = nCount+1;
end
end

fprintf('%d out of %d has higher accuracy for adapted method: %f\n', nCount, nTimes, nCount/nTimes);
disp(nCountEach./nTimes);

fprintf('Mean adapted accuracy: %.3f\n', mean(accuracyAdapted));
fprintf('Mean non-adapted accuracy: %.3f\n', mean(accuracyNonAdapted));

figure(1001), bar( [accuracyNonAdapted accuracyAdapted] );
set(gca, 'XTickLabel', xticklabel );
legend('non-Adapted', 'Adapted');
titlename = sprintf('non-adapted: %.3f, adapted: %.3f', mean(accuracyNonAdapted), mean(accuracyAdapted));
title(titlename);

