% use different methods for regression and testing
function [cntEst, err, tPos, initPnt, initVel, totEnergy, timeWarpParams] = estimateOnFeature( oma, cntAvg, grParams, nFigure )

% the figure id, create a new figure if no input for nFigure
if nargin < 4
    nFigure = 1100;
end

% training data
cntTrain = cntAvg(grParams.nSampling);
omaTrain = oma(grParams.nSampling); 

% add weight for each testing data
if ~isfield(grParams, 'weight')
    grParams.weight = estimateWeights( cntTrain, grParams.useWeightedData );
end
if sum(grParams.weight) ~= length(cntTrain)
    grParams.weight = grParams.weight ./ sum(grParams.weight) * length(cntTrain);
end

% store the results from the previous method for initialization
% so that the next method does not need to recompute it 
grParams.method_pre = [];
grParams.Y0_pre = [];
grParams.Y0dot_pre = [];
grParams.t0_pre = [];

if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
    figure(nFigure), hold on;
end
cntEst = [];
err = [];

nMethods = length(grParams.regressMethod);
for iFlag = 1:nMethods
    % regression and prediction
    [cntEst{iFlag}, Y0, Y0dot, t0, energy, twParams]  = regressAndTest( omaTrain, cntTrain, oma, cntAvg, grParams, iFlag );
    % store the results for next method
    grParams.method_pre = grParams.regressMethod{iFlag};
    grParams.Y0_pre = Y0;
    grParams.Y0dot_pre = Y0dot;
    grParams.t0_pre = t0;
    initPnt{iFlag} = Y0;
    initVel{iFlag} = Y0dot;
    totEnergy(iFlag) = energy;
    tPos{iFlag} = t0;
    timeWarpParams{iFlag} = twParams;
    %pntForInit{iFlag} = initPntUsed;
    %velForInit{iFlag} = initVelUsed;
    
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % get prediction results for training and testing
        cntTrainEst = cntEst{iFlag}(grParams.nSampling);
        cntTestEst  = cntEst{iFlag}(grParams.nTesting);

        % plot the results, m: training, b: testing, r: ground-truth
        figure(nFigure), subplot(2, nMethods, iFlag), hold on;
        plot( 1:length(cntAvg), cntAvg, 'r-', 'LineWidth', 1 );
        plot( grParams.nSampling, cntTrainEst, 'm*', 'MarkerSize', 5 );
        plot( grParams.nTesting, cntTestEst, 'bo', 'MarkerSize', 5 ); 
        ylim([grParams.minValue, grParams.maxValue]);

        % compute mean square error of the testing results
        mseTest = mean( (cntTestEst - cntAvg(grParams.nTesting)).^2 );
        errTest = mean( abs(cntTestEst - cntAvg(grParams.nTesting)) );

        mseTrain = mean( (cntTrainEst - cntTrain).^2 );
        errTrain = mean( abs(cntTrainEst - cntTrain) );

        err{iFlag}.mseTest = mseTest;
        err{iFlag}.errTest = errTest;
        err{iFlag}.mseTrain = mseTrain;
        err{iFlag}.errTrain = errTrain;

        titlename = sprintf( '%s \n Train--MSE: %.5f, Err: %.5f \n Test--MSE: %.5f, Err: %.5f', ...
            grParams.regressMethod{iFlag}, mseTrain, errTrain, mseTest, errTest );
        title(titlename);
        xlabel('Subject Id');
        ylabel('Predicted value');
        hold off

        figure(nFigure), subplot(2, nMethods, iFlag+nMethods), hold on;
        plot(cntAvg(grParams.nSampling), cntTrainEst, 'm*', 'MarkerSize', 5);
        plot(cntAvg(grParams.nTesting), cntTestEst, 'bo', 'MarkerSize', 5);
        plot(min(cntAvg):max(cntAvg), min(cntAvg):max(cntAvg), 'r-', 'LineWidth', 1);
        xlabel('Truth');
        ylabel('Prediction');
        xlim([grParams.minValue, grParams.maxValue]);
        ylim([grParams.minValue, grParams.maxValue]);
        hold off
    end
end
