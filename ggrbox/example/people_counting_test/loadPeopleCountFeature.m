% load features

function tmpFeature = loadPeopleCountFeature( params )

if strcmp( params.featureName, 'RAW' )
    nFrame = 200;
    for iV = 1:20
        for iF = 1:nFrame
            filename = sprintf( '%s/data/ucsdpeds/vidf/vidf1_33_%03d.y/vidf1_33_%03d_f%03d.png', params.inputPrefix, iV-1, iV-1, iF );
            imageFrame(:, (iV-1)*nFrame+iF) = double(reshape(imresize( imread(filename), params.downsampling ), [], 1)); 
        end
    end
    tmpFilename = sprintf( '%s/data/ucsdpeds_feats/features/Peds1_feats.mat', params.inputPrefix );
	tmp = load( tmpFilename );  % 1-4000 frames
	% using raw imaging, can only count the total number of people
	tmpFeature.dirs{1} = tmp.dirs{end};
	tmpFeature.cnt{1} = tmp.cnt{end};
	tmpFeature.fv{1} = imageFrame;
    
elseif strcmp( params.featureName, 'FEAT' )
	%tmpFeature = load( './data/ucsdpeds_feats/features/cvpr_feats.mat' );  % 1-2000 frames
    tmpFilename = sprintf( '%s/data/ucsdpeds_feats/features/Peds1_feats.mat', params.inputPrefix );
	tmpFeature = load( tmpFilename );  % 1-4000 frames
    if params.allFEATS == 0
        for iDir = 1:length(tmpFeature.fv)
            feats = tmpFeature.fv{iDir};
            %feats = feats( [1:2, 9:10, 17:size(feats, 1)], : );
            feats = feats( [1:2, 5:10, 11:17], : );
            tmpFeature.fv{iDir} = feats;
        end
    end
    
elseif strcmp( params.featureName, 'HOOF' )
    nFrame = 200;
    for iV = 1:20
		for iF = 1:nFrame
		    % read the hoof feature
		    filename = sprintf( '%s/data/hoof/people_hoof_%03d_%03d.mat', params.inputPrefix, iV-1, iF);
		    tmp = load(filename);
		    hoofFeature(:, (iV-1)*nFrame+iF) = log( tmp.hoofFeature + 1 );
		end
    end
    tmpFilename = sprintf( '%s/data/ucsdpeds_feats/features/Peds1_feats.mat', params.inputPrefix );
    tmp = load( tmpFilename );  % 1-4000 frames
	tmpFeature.dirs{1} = tmp.dirs{end};
	tmpFeature.cnt{1} = tmp.cnt{end};
	tmpFeature.fv{1} = hoofFeature;
    
elseif strcmp( params.featureName, 'DICT' )
    
    if ~isempty(params.sparseCode)
        filename = sprintf('%s/data/people_dict/%s', params.inputPrefix, params.sparseCode);
        tmp = load(filename);
        alpha = tmp.alpha;
        pCount = tmp.pCount;
    else
        filename = sprintf('%s/data/people_dict/config.mat', params.inputPrefix);
        tmp = load(filename);
        config = tmp.config;
        %config.iVid = 0:19;
        config.iDir = sprintf( '%s/data/gt/vidf', params.inputPrefix );
        config.vDir = sprintf( '%s/data/ucsdpeds/vidf', params.inputPrefix );
        %config.imsc = 0.5;
        %config.base = 'vidf1_33';
        %config.roiM = 'vidf1_33_roi_mainwalkway.mat';
        %config.nImg = 200;
        data = vidf(config);

        nVideos = length(data.videos);

        % Training/Testing indices
        indTr = randperm(nVideos, round(0.8*nVideos));
        indTe = setdiff(1:nVideos, indTr);

        % Patch extraction settings
        patchParams.win = [10,10,5]; % 10x10x5 ST-patch
        patchParams.step = [3,3,3];  % Step size of 3x3x3
        
        
        % compute the average people number 
		pCount = zeros( size(data.pCount, 1), round((size(data.pCount, 2) - patchParams.win(end))/patchParams.step(end))+1 );
		for iI = 1:size(data.pCount, 1)
			nTmp = 0;
			for iJ = 1:patchParams.step(end):size(data.pCount, 2)
				nTmp = nTmp + 1;
                nEndTmp = iJ+patchParams.win(end)-1;
                if nEndTmp > size(data.pCount, 2)
                    break;
                end
				pCount(iI, nTmp) = mean( data.pCount(iI, iJ:nEndTmp) );
			end
		end

        D = [];
        for i=1:length(indTr)
            P = getPatches(data.videos{indTr(i)}, patchParams);
            tmpD = patchToMat(P,0.1,1);   
            D = [D tmpD];
        end

        % Dictionary learning 
        codeParams.iternum = 50;
        codeParams.data = D;
        codeParams.Tdata = 64;
        codeParams.dictsize = 256;

        [Dict, Gamma] = ksvd(codeParams);
        codeParams.Dict = Dict; % save

        alpha = cell(nVideos,1);
        for i=1:nVideos
            alpha{i} = codeVideo(data.videos{i}, patchParams, codeParams, 1);
        end
	
        %save( 'code_5_3.mat', 'D', 'Dict', 'Gamma', 'alpha', 'nVideos', 'patchParams', 'pCount' );
    end
    
    feature = [];
    % pooling to get the feature
    %for iI = 1:length(alpha)
    %    strTmp = sprintf('Deal with %dth alpha', iI);
    %    disp(strTmp);
    %    for iJ = 1:length(alpha{iI})
    %        feature(:, end+1) = blockprocSparse( alpha{iI}{iJ}, [size(alpha{iI}{iJ}, 1) 1], 'max' );
    %    end
    %end
    for iI = 1:length(alpha)
        feature = [ feature; cell2mat(poolingSparseCode(alpha{iI}, params.poolStyle, params.poolDirection))] ;
    end
    clear alpha
    figure, imagesc(feature'); colormap cool
    strTmp = sprintf('video_after_%s_pool_%s.png', params.poolStyle, params.poolDirection);
    saveas(gca, strTmp);
    size(feature')
    tmpFilename = sprintf( '%s/data/ucsdpeds_feats/features/Peds1_feats.mat', params.inputPrefix );
	tmp = load( tmpFilename );  % 1-4000 frames
	% using raw imaging, can only count the total number of people
	tmpFeature.dirs{1} = tmp.dirs{end};
	%tmpFeature.cnt{1} = tmp.cnt{end};
    tmpFeature.cnt{1} = reshape(pCount', 1, []);
    tmpFeature.fv{1} = feature';
    
else
    error('Unknown feature');
end

% plot the grouth-truth
lineStyle = {'r-.', 'b--', 'g-'};
figure, hold on;
for iDir = 1:length(tmpFeature.dirs)
    plot( tmpFeature.cnt{iDir}, lineStyle{iDir}, 'LineWidth', 2 );
end
hold off
