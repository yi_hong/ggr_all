% people counting test example

close all
clear all

%profile -memory on

%grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
%outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/crowd';

grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';         % data for regression
outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_093014/crowd5';  % regression results


%grParams.inputPrefix = '/usr/project/xtmp/qiangcao/GGR_101014/data_results';
%grParams.outputPrefix = '/usr/project/xtmp/qiangcao/GGR_101014/data_results/crowd';

grParams.sysid = 'dt';                % n4sid or dt
grParams.useWeightedSysid = true;     % use weighted system identification
grParams.sigmaWeightedSysid = 100;    % the bandwidth for weighted system identification
grParams.featureName = 'RAW';         % featureName = {'RAW', 'FEAT', 'HOOF', 'DICT'};
grParams.regressMethod = {'PiecewiseRegression'};  
grParams.minValue = 0;                % the minimal range of the independent value, for plots                
grParams.maxValue = 50;               % the maximal range of the independent value, for plots

% parameters for compute the cost function
grParams.sigmaSqr = 1;                             % the coefficient for matching term in the cost function
grParams.alpha = 0;                                % the coefficient for prior knowledge
grParams.nt = 100;
grParams.deltaT = 0.05;
grParams.deltaTWarp = 0.01;
grParams.deltaTBk = 0.5;
grParams.maxReductionSteps = 10;
grParams.rho = 0.5;
grParams.nIterMax = 1000;
grParams.stopThreshold = 1e-6;
grParams.minFunc = 'linesearch';                    % linesearch, fmincon
grParams.csMinFunc = 'both';
grParams.nIterBoth = 10;
grParams.useODE45 = true;                           % more stable
grParams.estErr = 0.05;                             % discretizing size for prediction
grParams.useWeightedData = false;                    % weight data using its density
grParams.useRansac = false;                         % randomly choose pairs for pair-wise searching
grParams.fullGGRInitZero = true;                    % use zero to initialize the initial conditions   
grParams.fidIdLogisticFunc = 600;
grParams.fidSplineEnergy = 500;
grParams.maxIterFmincon = 50;
grParams.cps = [23];
grParams.bks = [23];                                % breakpoints
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue];  % breakpoint with two boundaries


if strcmp( grParams.featureName, 'FEAT' )
    grParams.allFEATS = 1;  % this parameter is to control how many FEATs used, no effect on RAW and HOOF
    if grParams.allFEATS == 0
        disp( 'Use part of the features and subtract mean in the sid' );
    else
        disp( 'Use all of the features and subtract mean in the sid' );
    end
end

grParams.downsampling = 0.25;        % downsample the raw image to reduce the dimension of the matrix
grParams.nFrameStart = 1;            % starting frame
grParams.nFrameEnd = 4000;           % ending frame
grParams.nWinSize = 400;             % video clip, including 400 frames
grParams.nWinStep = 100;             % moving step, 100 frames
grParams.nSta = 10;                  % the number of the state
grParams.nObs = grParams.nSta;       % the number k of the observation matrix
grParams.nSys = floor( (grParams.nFrameEnd - grParams.nFrameStart + 1 - ...
    grParams.nWinSize) / grParams.nWinStep ) + 1;    % the number of the observations

if ~strcmp(grParams.featureName, 'DICT')
    tmpFeature = loadPeopleCountFeature( grParams );     % load features from file   
else
    tmpFeature.dirs{1} = 't';
end

nPartition = 4;                                      % number of the partitions, n-fold cv

% do regression for all three directions
for iDir = 1:length(tmpFeature.dirs)
    
    % do system identification on the features to get the observations Y (oma)
    if ~strcmp(grParams.featureName, 'DICT')
        [oma, cntAvg] = sysIdOnFeature( tmpFeature.fv{iDir}, tmpFeature.cnt{iDir}, grParams );
    end
    
    for iP = 1:nPartition
        iP
        if strcmp(grParams.featureName, 'DICT')
            scParams.nWinSize = grParams.nWinSize;
            scParams.nWinStep = grParams.nWinStep;
            scParams.iP = iP;
            scParams.nPartition = nPartition;
            scParams.sparseCode = grParams.sparseCode;
            scParams.winTemporal = 5;
            scParams.stepTemporal = 1;
            % sparse coding
            [alphaDict, countDict] = sparseCodingCrowd(scParams);
            % pooling
            for iAlpha = 1:length(alphaDict)
                alphaPooled{iAlpha} = (cell2mat(poolingSparseCode(alphaDict{iAlpha}, grParams.poolStyle, grParams.poolDirection)))';
            end
            % system identification
            siParams.sysid = grParams.sysid;
            siParams.useWeightedSysid = grParams.useWeightedSysid;
            siParams.nSta = grParams.nSta;
            siParams.nObs = grParams.nObs;
            siParams.subtractMean = true;
            siParams.useWeightedSysid = grParams.useWeightedSysid;
            siParams.sigmaWeightedSysid = grParams.sigmaWeightedSysid;
            siParams.nWinSize = grParams.nWinSize;
            siParams.winTemporal = scParams.winTemporal;
            siParams.stepTemporal = scParams.stepTemporal;
            [oma, cntAvg] = sysIdOnVideoAndComputeIndependentValue(alphaPooled, countDict, siParams);
        end
        
        grParams.nTesting = iP:nPartition:grParams.nSys;              % the id for testing
        grParams.nSampling = 1:grParams.nSys;  
        if nPartition > 1
            grParams.nSampling(grParams.nTesting) = [];                   % the id for training
        end
        
        % compute baseline using nearest neigborhood
        if strcmp( grParams.regressMethod, 'None-NN' ) == 1
            cntEst{1} = estimateIndependentValueNN(oma(grParams.nSampling), cntAvg(grParams.nSampling), oma);
            filename = sprintf('%s/crowd_baseline_nn_Part%dOf%d.mat', outputPrefix, iP, nPartition);
            save(filename, 'cntAvg', 'cntEst', 'grParams');
            continue;
        end
        
        [cntEst, errEst, tPos, initPnt, initVel, totEnergy] = estimateOnFeature( oma, cntAvg, grParams, iP ); % regression and prediction

        % save the plot and data
        if strcmp( grParams.featureName, 'FEAT' ) || strcmp( grParams.featureName, 'HOOF' )
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Part%dOf%d.fig', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Part%dOf%d.png', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, iP, nPartition);
            saveas( gca, filename );
            % save the results
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_Part%dOf%d.mat', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, iP, nPartition);
            save( filename, 'cntAvg', 'cntEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy' );
            
        elseif strcmp( grParams.featureName, 'DICT' )
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_%s_%s_%s_Part%dOf%d.fig', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.poolStyle, grParams.poolDirection, grParams.sparseCode(1:end-4), iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_%s_%s_%s_Part%dOf%d.png', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.poolStyle, grParams.poolDirection, grParams.sparseCode(1:end-4), iP, nPartition);
            saveas( gca, filename );
            % save the results
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_%s_%s_%s_Part%dOf%d.mat', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.poolStyle, grParams.poolDirection, grParams.sparseCode(1:end-4), iP, nPartition);
            save( filename, 'cntAvg', 'cntEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy' );

        elseif strcmp( grParams.featureName, 'RAW' )
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_ds%.2f_Part%dOf%d.fig', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.downsampling, iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_ds%.2f_Part%dOf%d.png', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.downsampling, iP, nPartition);
            saveas( gca, filename );
            filename = sprintf('%s/counting_%s_start%d_end%d_winsize%d_winstep%d_state%d_wsid%d_sigma%d_alpha%.2f_sigmasqr%.2f_%s_weightedData%d_dir%d_ds%.2f_Part%dOf%d.mat', ...
                outputPrefix, grParams.featureName, grParams.nFrameStart, grParams.nFrameEnd, grParams.nWinSize, grParams.nWinStep, grParams.nSta, ...
                grParams.useWeightedSysid, grParams.sigmaWeightedSysid, grParams.alpha, grParams.sigmaSqr, grParams.minFunc, grParams.useWeightedData, ...
                iDir, grParams.downsampling, iP, nPartition);
            save( filename, 'cntAvg', 'cntEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy' );

        else
            error('Unknown feature name');
        end
    end
end


%p = profile('info');
%filename = sprintf('%s/people_counting_profile', outputPrefix);
%profsave(p, filename);
