% system the point between two points
% Y1 & Y2: the two points, p*n, 
% p is the dimension of signal, n is the number of time points
% tSpan: [0, 1], the position of the interploated points
% nSta: the number of the states used for system identification
% nInitail: 1: use the initial state of the first point
%           2: use the initial state of the second point
%           3: use the interpolation of above two initial states
%
% Author: Yi Hong, yihong@cs.unc.edu

function Ys = interpolatePoints( Y1, Y2, tSpan, nSta, nInitial )

% system identification
params.class = 2;
% centered with the mean
C0 = mean([Y1 Y2], 2);
Y1 = Y1 - repmat(C0, 1, size(Y1, 2));
Y2 = Y2 - repmat(C0, 1, size(Y2, 2));
params.subtractMean = false;
weights = ones( 1, size(Y1, 2) );
weights = weights/sum(weights);
sys1 = weighted_suboptimal_systemID( Y1, nSta, weights, params );
sys2 = weighted_suboptimal_systemID( Y2, nSta, weights, params );

% compute the points on the Grassmannian
kObs = nSta;
oma1 = omat(sys1, kObs);
oma2 = omat(sys2, kObs);

% compute the geodesic between two points
[~, ~, ~, intialVel] = grgeo( oma1, oma2, 1, 'v3', 'v2' );

% compute the inerploated points on Grassmannian
tSpan = sort(tSpan);   % make sure the position is increasing

if tSpan(1) == 0
    if length(tSpan) == 1
        omas = oma1;
    else
        [~, omas] = integrateForwardWithODE45( oma1, intialVel, [tSpan 1] );
        omas(end) = [];
    end
elseif tSpan(end) == 1
    if length(tSpan) == 1
        omas = oma2;
    else
        [~, omas] = integrateForwardWithODE45( oma1, intialVel, [0 tSpan] );
        omas(1) = [];
    end
else
    [~, omas] = integrateForwardWithODE45( oma1, intialVel, [0 tSpan 1] );
    omas(1) = []; 
    omas(end) = [];
end

% go back, compute the original points
Ys = cell(length(omas), 1);
for iI = 1:length(Ys)
    [A, C] = estimate_dynamic_matrix( omas{iI}, size(Y1, 1), nSta, kObs );
    switch nInitial
        case 1
            x0 = sys1.Z0;
            if params.subtractMean
                C0 = sys1.C0;
            end
        case 2
            x0 = sys2.Z0;
            if params.subtractMean 
                C0 = sys2.C0;
            end
        case 3
            x0 = (1-tSpan(iI))*sys1.Z0 + tSpan(iI)*sys2.Z0;
        otherwise
            error('Unsupported initial state');
    end
    Y = zeros(size(Y1));
    xi = x0;
    for iJ = 1:size(Y, 2)
        Y(:, iJ) = C * xi;
        xi = A * xi;
        Y(:, iJ) = Y(:, iJ) + C0;
    end
    Ys{iI} = Y;
end

