function [pca_acc,svm_acc] = pca_clf(data,info,range,target,cv,dim)
% Evaluate how an affine-space classifier (e.g., used by Mallat in
% the 'scattering transform' CVPR 2011 paper using PCA compares to 
% a linear SVM.

    % the target specifies if we want a brand(1) or model(2) clf.
    classes = unique(info(:,target));

    D = []; % data
    L = []; % labels

    % build-up the data
    for i=1:length(classes)
        tmp = data(...
            info(:,3)>=range(1) & ...
            info(:,3)<range(2) & ...
            info(:,1) == classes(i),:);
        D = [D; tmp];
        L = [L; ones(size(tmp,1),1)*classes(i)];
    end

    assert(length(L)==size(D,1));
    
    N = size(D,1);
    idx = crossvalind('KFold',N,cv); % indices for CV

    pca_acc = zeros(cv,1); % CV accuracies of PCA-clf.
    svm_acc = zeros(cv,1); % CV accuracies of SVM-clf.
    
    for j=1:cv
        % training portion of the data
        p_cv_trn = find(idx~=j);
        D_cv_trn = D(p_cv_trn,:);
        L_cv_trn = L(p_cv_trn,:);
        
        % testing portion of the data
        p_cv_tst = find(idx==j);
        D_cv_tst = D(p_cv_tst,:);
        L_cv_tst = L(p_cv_tst,:);
        
        fprintf('%d/%d training/testing samples\n', ...
            length(L_cv_trn),length(L_cv_tst));
       
        % compute class-conditional subspaces
        S = cell(length(classes),1);
        error = zeros(size(D_cv_tst,1),length(classes));
        for c=1:length(classes)
            % compute one subspace of dimensionality 'dim'
            % for each class
            S{c} = compute_subspace(...
                D_cv_trn(L_cv_trn==classes(c),:),dim,-1,-1);
            
            % now project the testing data
            from = S{c}.P'*D_cv_tst';
            % and reproject to compute the reprojection diff.
            back = D_cv_tst - from'*S{c}.P';
            
            % get the reprojection error (norm of each vec.)
            for k=1:size(back,1)
                error(k,c) = norm(back(k,:));
            end
        end
        
        % assign the label of the subspace that gives the 
        % smallest reprojection error
        [~,res] = min(error,[],2);
        cp{j} = classperf(L_cv_tst,classes(res));
        
        % Linear SVM classifier - just for comparison, built a 
        % 'dim'-dimensional subspace from all the training data,
        % project the training/testing data and learn a linear
        % SVM in that space
        SVM_SS = compute_subspace(D_cv_trn,dim,-1,-1);
        SVM_data_trn = (SVM_SS.P'*D_cv_trn')';
        SVM_data_tst = (SVM_SS.P'*D_cv_tst')';
        
        model = train(L_cv_trn,sparse(SVM_data_trn),'-c 1 -s 2');
        [~, svm_acc_cv, ~] = predict(L_cv_tst,sparse(SVM_data_tst),model);
        fprintf('SVM: %.3f\n',svm_acc_cv(1)/100);
        fprintf('PCA: %.3f\n', cp{j}.CorrectRate);
        
        pca_acc(j) = cp{j}.CorrectRate;
        svm_acc(j) = svm_acc_cv(1)/100;
    end
end