% load features

function [oma, speed] = loadTrafficFeatureAndSysId( params )

if strcmp( params.featureName, 'RAW' )
    tmpFilename = sprintf('%s/data/trafficdb_cluster/traffic_patches_reg.mat', params.inputPrefix);
    tmp = load( tmpFilename );  
    imgdb = tmp.imgdb;
    
    oma = cell( length(imgdb), 1 );
	if strcmp( params.sysid, 'dt' )
        sysParams.subtractMean = false;
        sysParams.class = 2;
        for iI = 1:length(imgdb)
            if params.downsampling < 1.0
                for iF = 1:size(imgdb{iI}, 3)
                    imgTmp(:, :, iF) = imresize( imgdb{iI}(:, :, iF), params.downsampling );
                end
                imgdb{iI} = imgTmp;
            end
            sysTmp = suboptimalSystemID(imgdb{iI}, params.nSta, sysParams);
            oma{iI} = omat(sysTmp, params.nObs);
        end
    elseif strcmp( params.sysid, 'n4sid' )
        for iI = 1:length(imgdb)
            dataTmp = iddata( (reshape(imgdb{iI}, [], size(imgdb{iI}, 3)))', [], 0.1);
            opt = n4sidOptions('Focus', 'prediction', 'Display', 'off');
            sysn4 = n4sid(dataTmp, params.nSta, opt);
            sysTmp.C = sysn4.c;
            sysTmp.A = sysn4.a;
            oma{iI} = omat(sysTmp, params.nObs);
        end
    else
        error( 'Not supported system identification' );
    end

elseif strcmp( params.featureName, 'RAW_ORG' )
    tmpFilename = sprintf('%s/data/trafficdb_cluster/ImageMaster.mat', params.inputPrefix);
    tmp = load( tmpFilename );
    imagemaster = tmp.imagemaster;
    oma = cell( length(imagemaster), 1 );
    for iI = 1:length(imagemaster)
        filename = sprintf( '%s/data/trafficdb_cluster/video/%s.avi', params.inputPrefix, imagemaster{iI}.root );
        videoObj = VideoReader( filename );
        nFrames = videoObj.NumberOfFrames;
        vidHeight = videoObj.Height;
        vidWidth = videoObj.Width;
        mov(1:nFrames) = struct('cdata', zeros(vidHeight, vidWidth, 3, 'uint8'), 'colormap', []);
	clear image
	for k=2:nFrames
            mov(k).cdata = read(videoObj, k);
	    image(:, :, k-1) = rgb2gray( mov(2).cdata );
        end
        if strcmp( params.sysid, 'dt' )
            sysParams.class = 2;
            if params.downsampling < 1.0
                for iF = 1:size(image, 3)
                    imgTmp(:, :, iF) = imresize( image(:, :, iF), params.downsampling );
                end
                image = imgTmp;
            end
            sysTmp = suboptimalSystemID(image, params.nSta, sysParams);
            oma{iI} = omat(sysTmp, params.nObs);
        elseif strcmp( params.sysid, 'n4sid' )
            dataTmp = iddata( (reshape(image, [], size(image, 3)))', [], 0.1);
            opt = n4sidOptions('Focus', 'prediction', 'Display', 'off');
            sysn4 = n4sid(dataTmp, params.nSta, opt);
            sysTmp.C = sysn4.c;
            sysTmp.A = sysn4.a;
            oma{iI} = omat(sysTmp, params.nObs);
   	else
            error( 'Not supported system identification' );
        end
    end
elseif strcmp( params.featureName, 'HOOF' )
    for iI = 1:253
        filename = sprintf( '%s/data/hoof_traffic/traffic_hoof_%03d.mat', params.inputPrefix, iI );
        tmp = load(filename);
	for iJ = 1:size(tmp.hoofFeature, 2)
		tmp.hoofFeature(:, iJ) = tmp.hoofFeature(:, iJ) ./ sum(tmp.hoofFeature(:, iJ));
	end
        imgdb{iI} = log(tmp.hoofFeature+1);
        if strcmp( params.sysid, 'dt' )
            sysParams.class = 2;
            sysTmp = suboptimalSystemID(imgdb{iI}, params.nSta, sysParams);
            oma{iI} = omat(sysTmp, params.nObs);
        elseif strcmp( params.sysid, 'n4sid' )
            dataTmp = iddata( (imgdb{iI})', [], 0.1);
            opt = n4sidOptions('Focus', 'prediction', 'Display', 'off');
            sysn4 = n4sid(dataTmp, params.nSta, opt);
            sysTmp.C = sysn4.c;
            sysTmp.A = sysn4.a;
            oma{iI} = omat(sysTmp, params.nObs);
        else
            error( 'Not supported system identification' );
        end
    end
else
    error('Unknown feature');
end

tmpFilename = sprintf('%s/data/trafficdb_cluster/sensordata.mat', params.inputPrefix);
tmp = load( tmpFilename );
sensor_speed = tmp.sensor_speed;
sensor_timestamp = tmp.sensor_timestamp;
video_class = tmp.video_class;
video_timestamp = tmp.video_timestamp;

speed = zeros( length(video_class), 1 );
for iI = 1:length(video_class)
    idSpeed = find( sensor_timestamp <= video_timestamp(iI) + 0.01 & ...
                    sensor_timestamp >= video_timestamp(iI) - 0.01 );
    speed(iI) = mean( sensor_speed( idSpeed ) );
end

% plot the grouth-truth
color = {'r', 'g', 'b'};
figure, hold on;
for iI = 1:length(speed)
    plot( iI, speed(iI), '.', 'Color', color{video_class(iI)}, 'MarkerSize', 10 );
end

hold off

str = sprintf('Total of the videos: %d', length(oma));
disp(str); 



