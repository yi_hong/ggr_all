% traffic data

close all
clear all

%profile -memory on

%grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
%outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/traffic';

grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';
outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_093014/traffic_2Ms';

%grParams.inputPrefix = '/usr/project/xtmp/qiangcao/GGR_101014/data_results';
%outputPrefix = '/usr/project/xtmp/qiangcao/GGR_101014/data_results/traffic';

grParams.sysid = 'dt';         % dynamical texture
grParams.useWeightedSysid = false;
grParams.featureName = 'RAW';  % RAW, RAW_ORG, HOOF
grParams.regressMethod = {'None-NN'}; % 'FullRegression', 
grParams.minValue = 0;
grParams.maxValue = 80;

grParams.sigmaSqr = 1;
grParams.alpha = 0.0;
grParams.nt = 100;
grParams.deltaT = 0.005;
grParams.deltaTWarp = 0.01;
grParams.maxReductionSteps = 10;
grParams.rho = 0.5;
grParams.nIterMax = 1000;
grParams.nIterMaxTimeWarp = 50;
grParams.stopThreshold = 1e-6;
grParams.useODE45 = true;
grParams.minFunc = 'linesearch';
grParams.twMinFunc = 'fmincon';
grParams.csMinFunc = 'both';
grParams.nIterBoth = 10;
grParams.estErr = 0.05;
grParams.useWeightedData = false;
grParams.useRansac = true;
grParams.maxIterFminconTW = 10;

grParams.downsampling = 1.0;
grParams.nSta = 10;
grParams.nObs = grParams.nSta;

grParams.fullGGRInitZero = true; % initialize X10 with the first point, X20 / X30 ... with zeros
grParams.figIdLogisticFunc = 600;
grParams.fidSplineEnergy = 500;
grParams.maxIterFmincon = 100;

grParams.M = 45;
grParams.k = 0.5;

% load feature and do sid
[oma, speed] = loadTrafficFeatureAndSysId( grParams );
grParams.nSys = length(oma);
grParams.cps = [45];

%oma = oma(1:5:end);
%speed = speed(1:5:end);
%grParams.nSys = length(oma);

nPartition = 5;
for iP = 1:nPartition
    grParams.nTesting = iP:nPartition:grParams.nSys;
    grParams.nSampling = 1:grParams.nSys;
    if nPartition > 1
        grParams.nSampling(grParams.nTesting) = [];
    end

    % compute baseline using nearest neigborhood
    if strcmp( grParams.regressMethod, 'None-NN' ) == 1
        speedEst{1} = estimateIndependentValueNN(oma(grParams.nSampling), speed(grParams.nSampling), oma);
        filename = sprintf('%s/traffic_baseline_nn_Part%dOf%d.mat', outputPrefix, iP, nPartition);
        save(filename, 'speed', 'speedEst', 'grParams');
        continue;
    end
    
    % training and testing
    [speedEst, errEst, tPos, initPnt, initVel, totEnergy, timeWarpParams] = estimateOnFeature( oma, speed, grParams, iP );
    
    % save the plot
    filename = sprintf('%s/traffic_%s_state%d_train%d_ds%.2f_wsi%d_%s_weightData%d_Part%dOf%d.fig', ...
        outputPrefix, grParams.featureName, grParams.nSta, length(grParams.nSampling), ...
        grParams.downsampling, grParams.useWeightedSysid, grParams.minFunc, grParams.useWeightedData, iP, nPartition);
    saveas( gca, filename );
    filename = sprintf('%s/traffic_%s_state%d_train%d_ds%.2f_wsi%d_%s_weightData%d_Part%dOf%d.png', ...
        outputPrefix, grParams.featureName, grParams.nSta, length(grParams.nSampling), ...
        grParams.downsampling, grParams.useWeightedSysid, grParams.minFunc, grParams.useWeightedData, iP, nPartition);
    saveas( gca, filename );

    % save results
    filename = sprintf('%s/traffic_%s_state%d_train%d_ds%.2f_wsi%d_%s_weightData%d_Part%dOf%d.mat', ...
        outputPrefix, grParams.featureName, grParams.nSta, length(grParams.nSampling), ...
        grParams.downsampling, grParams.useWeightedSysid, grParams.minFunc, grParams.useWeightedData, iP, nPartition);
    save(filename, 'speed', 'speedEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy', 'timeWarpParams');
end


%p = profile('info');
%filename = sprintf('%s/traffic_speed_profile', outputPrefix);
%profsave(p, filename);
