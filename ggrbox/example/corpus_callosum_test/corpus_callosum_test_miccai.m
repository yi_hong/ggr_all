% corpus callosum test

close all
clear all

profile -memory on

setup();

grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714/corpus_callosum';
%grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';         % data for regression
%outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_011714/corpus_callosum';  % regression results

grParams.regressMethod = {'FullRegression', 'TimeWarpRegression'};
grParams.minValue = 0;                 % the minimal range of the independent value, for plots                
grParams.maxValue = 100;               % the maximal range of the independent value, for plots

% parameters for compute the cost function
grParams.sigmaSqr = 1;                             % the coefficient for matching term in the cost function
grParams.alpha = 0;                                % the coefficient for prior knowledge
grParams.nt = 100;
grParams.deltaT = 0.03;
grParams.deltaTBk = 0.5;
grParams.deltaTWarp = 0.01;
grParams.maxReductionSteps = 10;
grParams.rho = 0.5;
grParams.nIterMax = 300;
grParams.nIterMaxBk = 50;
grParams.nIterMaxTimeWarp = 50;
grParams.stopThreshold = 1e-14;
grParams.minFunc = 'linesearch';                    % linesearch, fmincon
grParams.twMinFunc = 'fmincon';
grParams.useODE45 = true;                           % more stable
grParams.estErr = 0.05;                             % discretizing size for prediction
grParams.bks = [30 50 70];                                % breakpoints
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue];  % breakpoint with two boundaries
grParams.useWeightedData = false;                    % weight data using its density
grParams.useRansac = false;                         % randomly choose pairs for pair-wise searching
grParams.useFullInitializePiecewise = false;        % use full GGR results to initialize the piecewise GGR
grParams.useFullInitializeContinuous = false;       % use full GGR results to initialize the continuous piecewise GGR
grParams.usePiecewiseInitializeContinuous = true;   % use piecewise GGR results to initialize the continuous piecewise GGR
grParams.useContinuousInitializeContinuousOB = true; % use CPGGR to initialize the CPGGR with optimal breakpoint
grParams.optimizeBreakpoint = 2;                     % 1: update initial conditions and breakpoints simultaneously, 2: iteratively

grParams.M = 65;
grParams.k = 0.5;

tMinPlot = 19;
tMaxPlot = 90;
nNumShape = 30;

[omas, ages, sigma] = loadCorpusCallosumFeatureAndSVD( grParams );
[ages, index] = sort(ages);
omas = omas(index);
grParams.nSys = length(omas);

% permulation test
nPermutation = 0;
rng('default');

R2 = zeros( nPermutation+1, length(grParams.regressMethod) );
errSqr = zeros( grParams.nSys, length(grParams.regressMethod) );

for iPer = 1:nPermutation+1
    
    if iPer > 1
        % reorder the ages
        %rng('default');
        ages = ages( randperm(length(ages)) );
    end

    nPartition = 1; %grParams.nSys;
    for iP = 1:nPartition
        grParams.nTesting = iP:nPartition:grParams.nSys;
        grParams.nSampling = 1:grParams.nSys;
        %grParams.nSampling(grParams.nTesting) = [];
        [ageEst, errEst, tPos, initPnt, initVel, totEnergy, timeWarpParams] = estimateOnFeature( omas, ages, grParams, iP );

        % save the plot and data
        filename = sprintf('%s/corpus_callosum_Part%dOf%d.fig', outputPrefix, iP, nPartition);
        saveas( gca, filename );
        filename = sprintf('%s/corpus_callosum_Part%dOf%d.png', outputPrefix, iP, nPartition);
        saveas( gca, filename );
        filename = sprintf('%s/corpus_callosum_Part%dOf%d.mat', outputPrefix, iP, nPartition);
        save( filename, 'ages', 'ageEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy', 'timeWarpParams' );

        % compute the R2 statistic
        %R2(iPer, :) = computeR2Statistic( initPnt, initVel, tPos, timeWarpParams, omas, ages, grParams );
        %for iJ = 1:size(R2, 2);
        %    pValue = length( find(R2(2:end, iJ) >= R2(1, iJ)) ) / ( size(R2, 1) - 1 );
        %    fprintf('p-value: %f\n', pValue);
        %end
        %filename = sprintf('%s/corpus_callosum_pValue_bk%d_nPermute%d_Part%d.txt', outputPrefix, length(grParams.bks), nPermutation, iP);
        %save(filename, 'R2', 'pValue', '-ascii');
        
        % compute test error
        for iJ = 1:length(initPnt)
            errSqr(grParams.nTesting, iJ) = computeGeodesicTestError( initPnt{iJ}{1}, initVel{iJ}{1}, tPos{iJ}(1), ...
                timeWarpParams{iJ}, omas(grParams.nTesting), ages(grParams.nTesting) );
        end
        
        % plot shape on geodesic
        %plot2DShapeOnGeodesic( initPnt, initVel, tPos, timeWarpParams, tMinPlot, tMaxPlot, sigma, nNumShape, grParams, 1 );
    end

end

filename = sprintf('%s/errSqr_part%d.mat', outputPrefix, nPartition);
save(filename, 'errSqr');

mean(errSqr, 1)

%p = profile('info');
%filename = sprintf('%s/corpus_callosum_profile', outputPrefix);
%profsave(p, filename);
