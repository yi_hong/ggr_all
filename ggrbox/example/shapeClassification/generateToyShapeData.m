% toy data generation

function generateToyShapeData(grParams)

% load data
inputName = sprintf('%s/srivastava/Srivastava.mat', grParams.inputPrefix);
tmp = load(inputName);
data = tmp.C;

% get two extreme cases for bone and heart
%id = [1 9 61 78];
id = [254 498 498 187];
subSpace = cell(length(id), 1);
scale = zeros(length(id), 2);

figure;
for iI = 1:length(id)
    subplot(2, 2, iI), plot(data(1, :, id(iI)), data(2, :, id(iI)));
    tmpData = data(:, :, id(iI))';
    meanData = mean(tmpData);
    tmpData = tmpData - repmat(meanData, [size(tmpData, 1), 1]);
    [U, S, ~] = svd(tmpData, 0);
    subSpace{iI} = U(:, 1:2);
    tmpS = diag(S);
    scale(iI, :) = tmpS(1:2);
end

% geodesic between two shapes
nData = 10;
timepoints = linspace(0, 1, nData);
newSpaceBone = cell(nData, 1);
scaleBone = mean(scale(1:2, :));
newSpaceHeart = cell(nData, 1);
scaleHeart = mean(scale(3:4, :));
figure;
for iI = 1:length(timepoints)
    newSpaceBone{iI} = extendFromTwoPoints(subSpace{1}, subSpace{2}, ...
        timepoints(1), timepoints(end), timepoints(iI));
    newShape = newSpaceBone{iI} * diag(scaleBone);
    subplot(length(timepoints), 1, iI), plot(newShape(:, 1), newShape(:, 2));
    axis equal
end

figure;
for iI = 1:length(timepoints)
    newSpaceHeart{iI} = extendFromTwoPoints(subSpace{3}, subSpace{4}, ...
        timepoints(1), timepoints(end), timepoints(iI));
    newShape = newSpaceHeart{iI} * diag(scaleHeart);
    subplot(length(timepoints), 1, iI), plot(newShape(:, 1), newShape(:, 2));
    axis equal
end
