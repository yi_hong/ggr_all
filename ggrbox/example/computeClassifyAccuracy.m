function accuracy = computeClassifyAccuracy(subSpaceP, dataTest, labelTest, classes, dataMean)

% compute the classification error using subspace from PCA

meanFlag = 1;
if nargin <= 4
    meanFlag = 0;
end

% different subSpace for different classes
dataOrig = dataTest;
for iP = 1:length(subSpaceP)
    % subtract mean
    if meanFlag == 0
        dataTest = dataOrig - repmat(mean(dataOrig), [size(dataOrig,1), 1]);
    else
        dataTest = dataOrig - repmat(dataMean{iP}, [size(dataOrig,1), 1]);
    end
    % project the testing data
    from = subSpaceP{iP}' * dataTest';
    % reproject back and compute diff
    back = dataTest - from' * subSpaceP{iP}';
    % get error
    for k = 1:size(back, 1)
        error(k, iP) = norm(back(k, :));
    end
end

% assign label
[~, res] = min(error, [], 2);
cp = classperf(labelTest, classes(res));

accuracy = cp.CorrectRate;