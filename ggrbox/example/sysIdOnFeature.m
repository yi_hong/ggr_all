% generate LDS based on featuers

function [oma, cntAvg] = sysIdOnFeature( feats, cnts, params )

cntAvg = zeros( params.nSys, 1 );   % independent value
oma = cell( params.nSys, 1 );       % the Y matrix

% data
for iI = 1:params.nSys
    % compute the starting and ending frames for a video clip
    idStart = (iI-1)*params.nWinStep + params.nFrameStart;
    idEnd = (iI-1)*params.nWinStep + params.nFrameStart + params.nWinSize - 1;
    if (~params.useWeightedSysid)
        cntAvg(iI) = mean(cnts(idStart:idEnd));
    else
        weight = gaussmf(1:params.nWinSize, [params.sigmaWeightedSysid params.nWinSize/2]);
		weight = weight ./ sum(weight);
		cntAvg(iI) = sum(cnts(idStart:idEnd) .* weight);
    end
    imgdbTmp = feats(:, idStart:idEnd);
    
    % system identification
    if strcmp( params.sysid, 'dt' )
        sysParams.subtractMean = true;
        sysParams.class = 2;
        if (~params.useWeightedSysid)
            sysTmp = suboptimalSystemID(imgdbTmp, params.nSta, sysParams);
        else
            sysTmp = weighted_suboptimal_systemID(imgdbTmp, params.nSta, weight, sysParams);
        end
    elseif strcmp( params.sysid, 'n4sid' )
        dataTmp = iddata(imgdbTmp', [], 0.1);
        opt = n4sidOptions('Focus', 'prediction', 'Display', 'off');
        sysn4 = n4sid(dataTmp, params.nSta, opt);
        sysTmp.C = sysn4.c;
        sysTmp.A = sysn4.a;
    else
        error( 'Not supported system identification' );
    end
    oma{iI} = omat(sysTmp, params.nObs);

    %figure, hold on;
    %synTmp = generateFromLDS(sysTmp, size(imgdbTmp));
    %for iJ = 1:size(imgdbTmp, 1);
    %    plot( imgdbTmp(iJ, :), 'r-' );
    %    plot( synTmp(iJ, :), 'b-.' );
    %end
end
