% regression and testing

function [cntTestEst, Y0, Y0dot, t0, energy, timeWarpParams] = regressAndTest( omaTrain, cntTrain, omaTest, cntTruth, grParams, nFlag)

timeWarpParams = [];
cntTestEst = [];

% geodesic regression based on pair-searching
if strcmp( grParams.regressMethod{nFlag}, 'PairSearch' )
    disp('****** Pair-wise searching GGR ******');
    [Y0{1}, Y0dot{1}, t0(1), energy] = grpairSearchODE45( omaTrain, cntTrain, 1, grParams.alpha, grParams.sigmaSqr, grParams.useODE45, grParams.useRansac, grParams.weight );
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        searchRange = [min(cntTruth), max(cntTruth)];
        cntTestEst = projectPointsToGeodesicODE45(Y0{1}, Y0dot{1}, t0(1), omaTest, searchRange(1), searchRange(2), grParams.estErr);
    end
    disp(' ');
    
% geodesic regression based on pair-searching with projected distance    
elseif strcmp( grParams.regressMethod{nFlag}, 'PairSearchProj' )
    disp('****** Pair-wise searching with projection distances ******');
    [Y0{1}, Y0dot{1}, t0(1), energy] = grpairSearchODE45( omaTrain, cntTrain, 2, grParams.alpha, grParams.sigmaSqr, grParams.useODE45, grParams.useRansac, grParams.weight );
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        searchRange = [min(cntTruth), max(cntTruth)];
        cntTestEst = projectPointsToGeodesicODE45(Y0{1}, Y0dot{1}, t0(1), omaTest, searchRange(1), searchRange(2), grParams.estErr);
    end
    disp(' ');  
  
% geodesic regression using Frechet mean    
elseif strcmp( grParams.regressMethod{nFlag}, 'FrechetMean' )
    disp('****** Regression using Frechet mean ******');
    [Y0{1}, Y0dot{1}, t0(1), ~] = grfmean( omaTrain, cntTrain );
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        searchRange = [min(cntTruth), max(cntTruth)];
        cntTestEst = projectPointsToGeodesicODE45(Y0{1}, Y0dot{1}, t0(1), omaTest, searchRange(1), searchRange(2), grParams.estErr);
    end
    disp(' ');
   
% full grassmann geodesic regression    
elseif strcmp( grParams.regressMethod{nFlag}, 'FullRegression' )
    disp('****** Full geodesic regression (Full GGR) ******');
    paramsRegress.Ys = omaTrain;
    paramsRegress.ts = cntTrain;
    paramsRegress.wi = grParams.weight;
    paramsRegress.nt = grParams.nt;
    paramsRegress.h = 1./paramsRegress.nt;
    paramsRegress.sigmaSqr = grParams.sigmaSqr;
    paramsRegress.alpha = grParams.alpha;
    paramsRegress.stopThreshold = grParams.stopThreshold;
    paramsRegress.deltaT = grParams.deltaT;
    paramsRegress.maxReductionSteps = grParams.maxReductionSteps;
    paramsRegress.rho = grParams.rho;
    paramsRegress.nIterMax = grParams.nIterMax;
    paramsRegress.minFunc = grParams.minFunc;   
    paramsRegress.useODE45 = grParams.useODE45;
    paramsRegress.useRansac = grParams.useRansac;
    paramsRegress.nsize = size(paramsRegress.Ys{1});
    
    % the ground-truth for comparison
    if isfield( grParams, 't_truth' )
        paramsRegress.t_truth = grParams.t_truth;
    end
    if isfield( grParams, 'Y_truth' )
        paramsRegress.Y_truth = grParams.Y_truth;
    end
    if isfield( grParams, 'pDim' )
        paramsRegress.pDim = grParams.pDim;
    end
    
    % given an initialization, usually for debugging
    if isfield( grParams, 'Y0Init' )
        paramsRegress.Y0Init = grParams.Y0Init;
    end
    if isfield( grParams, 'Y0dotInit' )
        paramsRegress.Y0dotInit = grParams.Y0dotInit;
    end
    
    % use the previous one for initialization
    if isfield( grParams, 'fullGGRInitZero') && grParams.fullGGRInitZero
        paramsRegress.pntY0 = paramsRegress.Ys{1};
        paramsRegress.tanY0 = zeros(size(paramsRegress.Ys{1}));
        paramsRegress.pntT0 = min(paramsRegress.ts);
    elseif strcmp( grParams.method_pre, 'PairSearch' )
        paramsRegress.pntY0 = grParams.Y0_pre{1};
        paramsRegress.tanY0 = grParams.Y0dot_pre{1};
        paramsRegress.pntT0 = grParams.t0_pre(1);
    end
    
    % full GGR
    [Y0{1}, Y0dot{1}, energy, ~, ~] = fullRegressionOnGrassmannManifold( paramsRegress );
    % scale the velocity
    Y0dot{1} = Y0dot{1} ./ ( max(paramsRegress.ts) - min(paramsRegress.ts) );
    %initVelUsed = initVelUsed ./ ( max(paramsRegress.ts) - min(paramsRegress.ts) );
    t0(1) = min(paramsRegress.ts);
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        searchRange = [min(cntTruth), max(cntTruth)];
        cntTestEst = projectPointsToGeodesicODE45(Y0{1}, Y0dot{1}, t0(1), omaTest, searchRange(1), searchRange(2), grParams.estErr);
    end
    disp(' ');

% piecewise full GGR
elseif strcmp( grParams.regressMethod{nFlag}, 'PiecewiseRegression' )
    disp('****** Piece-wise full geodesic regression (piecewise full GGR) ******');
    % the breakpoint here includes the boundaries
    searchRange = grParams.breakpoint;
    searchRange(1) = min(cntTruth);
    searchRange(end) = max(cntTruth);
    nPieces = length(grParams.breakpoint) - 1;
    energyPiece = zeros(nPieces, 1);
    % for each piece do the full GGR
    iI = 1;
    while iI <= nPieces
        % find the point with the ith piece, the breakpoint may be included
        % in two pieces, while it is fine
        idPoints = find( cntTrain  >= grParams.breakpoint(iI) & cntTrain <= grParams.breakpoint(iI+1) );
        % do not need the piece with less than 2 points
        if length(idPoints) < 2
            grParams.breakpoint(iI+1) = [];
            searchRange(iI+1) = [];
            nPieces = nPieces - 1;
            continue;
        end
        paramsRegress.Ys = omaTrain(idPoints);
        paramsRegress.ts = cntTrain(idPoints);
        paramsRegress.wi = grParams.weight(idPoints);
        paramsRegress.wi = paramsRegress.wi ./ sum(paramsRegress.wi) * length(paramsRegress.wi);
        paramsRegress.nt = grParams.nt;
        paramsRegress.h = 1./paramsRegress.nt;
        paramsRegress.sigmaSqr = grParams.sigmaSqr;
        paramsRegress.alpha = grParams.alpha;
        paramsRegress.stopThreshold = grParams.stopThreshold;
        paramsRegress.deltaT = grParams.deltaT;
        paramsRegress.maxReductionSteps = grParams.maxReductionSteps;
        paramsRegress.rho = grParams.rho;
        paramsRegress.nIterMax = grParams.nIterMax;
        paramsRegress.minFunc = grParams.minFunc;   % linesearch, minFunc, fmincon
        paramsRegress.useODE45 = grParams.useODE45;
        paramsRegress.useRansac = grParams.useRansac;
        paramsRegress.nsize = size(paramsRegress.Ys{1});
        
        % use the previous one for initialization
        if isfield( grParams, 'fullGGRInitZero') && grParams.fullGGRInitZero
            paramsRegress.pntY0 = paramsRegress.Ys{1};
            paramsRegress.tanY0 = zeros(size(paramsRegress.Ys{1}));
            paramsRegress.pntT0 = min(paramsRegress.ts);
        elseif grParams.useFullInitializePiecewise && strcmp( grParams.method_pre, 'FullRegression' )
            paramsRegress.pntY0 = grParams.Y0_pre{1};
            paramsRegress.tanY0 = grParams.Y0dot_pre{1};
            paramsRegress.pntT0 = grParams.t0_pre(1);
        end
        
        % each piece do a full regression
        [Y0{iI}, Y0dot{iI}, energyPiece(iI)] = fullRegressionOnGrassmannManifold( paramsRegress );
        % scale the velocity
        Y0dot{iI} = Y0dot{iI} ./ ( max(paramsRegress.ts) - min(paramsRegress.ts) );
        % prediction for each piece
        t0(iI) = min(paramsRegress.ts);
        if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
            [cntTest(:, iI), estDist(:, iI)] = projectPointsToGeodesicODE45(Y0{iI}, Y0dot{iI}, t0(iI), omaTest, searchRange(iI), searchRange(iI+1), grParams.estErr);
        end
        iI = iI + 1;
    end
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % pick the estimation with minimal distance
        cntTestEst = zeros(size(cntTest, 1), 1);
        for iJ = 1:length(cntTestEst)
            [~, id] = min(estDist(iJ, :));
            cntTestEst(iJ) = cntTest(iJ, id);
        end
    end
    energy = sum(energyPiece);
    disp(' ');
    
% continuous piecewise full GGR    
elseif strcmp( grParams.regressMethod{nFlag}, 'ContinuousPiecewiseRegression' )
    disp('****** Continuous Piece-wise full geodesic regression (Continuous piecewise full GGR) ******');
    paramsRegress.Ys = omaTrain;
    paramsRegress.ts = cntTrain;
    paramsRegress.wi = grParams.weight;
    paramsRegress.nt = grParams.nt;
    paramsRegress.h = 1./paramsRegress.nt;
    paramsRegress.sigmaSqr = grParams.sigmaSqr;
    paramsRegress.alpha = grParams.alpha;
    paramsRegress.deltaT = grParams.deltaT;
    paramsRegress.stopThreshold = grParams.stopThreshold;
    paramsRegress.maxReductionSteps = grParams.maxReductionSteps;
    paramsRegress.rho = grParams.rho;
    paramsRegress.nIterMax = grParams.nIterMax;
    paramsRegress.minFunc = grParams.minFunc;   % linesearch, minFunc, fmincon
    paramsRegress.useODE45 = grParams.useODE45;
    paramsRegress.useRansac = grParams.useRansac;
    paramsRegress.nsize = size(paramsRegress.Ys{1});
    paramsRegress.bks = grParams.bks;           % the breakpoints with no boundary
    
    % the ground-truth for comparison
    if isfield( grParams, 't_truth' )
        paramsRegress.t_truth = grParams.t_truth;
    end
    if isfield( grParams, 'Y_truth' )
        paramsRegress.Y_truth = grParams.Y_truth;
    end
    if isfield( grParams, 'pDim' )
        paramsRegress.pDim = grParams.pDim;
    end
    
    % given initialization, usually for debugging
    if isfield( grParams, 'Y0Init' )
        paramsRegress.Y0Init = grParams.Y0Init;
    end
    if isfield( grParams, 'Y0dotInit' )
        paramsRegress.Y0dotInit = grParams.Y0dotInit;
    end
    
    % use the previous one for initialization
    if isfield( grParams, 'fullGGRInitZero') && grParams.fullGGRInitZero
        paramsRegress.pntY0 = paramsRegress.Ys{1};
        paramsRegress.tanY0 = zeros(size(paramsRegress.Ys{1}));
        paramsRegress.pntT0 = min(paramsRegress.ts);
    elseif strcmp( grParams.method_pre, 'PairSearch' ) || ...
       grParams.useFullInitializeContinuous && strcmp( grParams.method_pre, 'FullRegression' )
        paramsRegress.pntY0 = grParams.Y0_pre{1};
        paramsRegress.tanY0 = grParams.Y0dot_pre{1};
        paramsRegress.pntT0 = grParams.t0_pre{1};
    elseif grParams.usePiecewiseInitializeContinuous && strcmp( grParams.method_pre, 'PiecewiseRegression' )
        paramsRegress.pntY0s = grParams.Y0_pre;
        paramsRegress.tanY0s = grParams.Y0dot_pre;
        paramsRegress.pntT0s = grParams.t0_pre;
    end
    
    % continuous piecewise regression with fixed breakpoint
    [Y0{1}, Y0dots, energy] = continuousPiecewiseRegression( paramsRegress );
    % scale the initial velocity
    for iI = 1:length(Y0dots)
        Y0dots{iI} = Y0dots{iI} ./ ( max(paramsRegress.ts) - min(paramsRegress.ts) );
    end
    Y0dot = Y0dots;
    t0(1) = min(paramsRegress.ts);
    % the breakpoint should be bigger than the first point
    if ~isempty(paramsRegress.bks)
        assert( min(paramsRegress.bks) > t0(1) );
    end
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        searchRange = [min(cntTruth), max(cntTruth)];
        cntTestEst = projectPointsToGeodesicODE45Piecewise(Y0{1}, Y0dots, [t0(1) paramsRegress.bks], omaTest, searchRange(1), searchRange(2), grParams.estErr);
    end
    t0 = [t0(1) paramsRegress.bks];
    disp(' ');
    
% Continuous piecewise full GGR with optimal breakpoints    
elseif strcmp( grParams.regressMethod{nFlag}, 'ContinuousPiecewiseRegressionWithOptimalBreakpoints' )
    disp('****** Continuous Piece-wise full geodesic regression with optimal breakpoints ******');
    paramsRegress.Ys = omaTrain;
    paramsRegress.ts = cntTrain;
    paramsRegress.wi = grParams.weight;
    paramsRegress.nt = grParams.nt;
    paramsRegress.h = 1./paramsRegress.nt;
    paramsRegress.sigmaSqr = grParams.sigmaSqr;
    paramsRegress.alpha = grParams.alpha;
    paramsRegress.deltaT = grParams.deltaT;
    paramsRegress.stopThreshold = grParams.stopThreshold;
    if grParams.optimizeBreakpoint == 2
        paramsRegress.deltaTBk = grParams.deltaTBk;               % step size for breakpoint, a little large
    end
    paramsRegress.maxReductionSteps = grParams.maxReductionSteps;
    paramsRegress.rho = grParams.rho;
    paramsRegress.nIterMax = grParams.nIterMax;
    paramsRegress.nIterMaxBk = grParams.nIterMaxBk;
    paramsRegress.minFunc = grParams.minFunc;   % linesearch, minFunc, fmincon
    paramsRegress.useODE45 = grParams.useODE45;
    paramsRegress.useRansac = grParams.useRansac;
    paramsRegress.nsize = size(paramsRegress.Ys{1});
    paramsRegress.bks = grParams.bks;
    paramsRegress.nBks = length(grParams.bks);  % breakpoints, here it does not include the boundary
    
    % the ground-truth for comparison
    if isfield( grParams, 't_truth' )
        paramsRegress.t_truth = grParams.t_truth;
    end
    if isfield( grParams, 'Y_truth' )
        paramsRegress.Y_truth = grParams.Y_truth;
    end
    if isfield( grParams, 'pDim' )
        paramsRegress.pDim = grParams.pDim;
    end
    
    % the given initialization, usually for debugging
    if isfield( grParams, 'Y0Init' )
        paramsRegress.Y0Init = grParams.Y0Init;
    end
    if isfield( grParams, 'Y0dotInit' )
        paramsRegress.Y0dotInit = grParams.Y0dotInit;
    end
    
    % use the previous one for initialization
    if isfield( grParams, 'fullGGRInitZero') && grParams.fullGGRInitZero
        paramsRegress.pntY0 = paramsRegress.Ys{1};
        paramsRegress.pntT0s = [min(paramsRegress.ts) sort(paramsRegress.bks)];
        paramsRegress.tanY0s = cell(length(paramsRegress.pntT0s), 1);
        for iTmp = 1:length(paramsRegress.tanY0s)
            paramsRegress.tanY0s{iTmp} = zeros(size(paramsRegress.Ys{1}));
        end
    elseif grParams.useContinuousInitializeContinuousOB && strcmp( grParams.method_pre, 'ContinuousPiecewiseRegression' )
        paramsRegress.pntY0 = grParams.Y0_pre{1};
        paramsRegress.tanY0s = grParams.Y0dot_pre;
        paramsRegress.pntT0s = grParams.t0_pre;
    end
    
    % continuous piecewise regression with optimal breakpoints
    switch grParams.optimizeBreakpoint
        case 1
            [Y0{1}, Y0dots, bkPnts, energy] = continuousPiecewiseRegressionWithOptimalBreakpointsSimultaneous( paramsRegress );
        case 2
            [Y0{1}, Y0dots, bkPnts, energy] = continuousPiecewiseRegressionWithOptimalBreakpointsIterative( paramsRegress );
        otherwise
            error('Not supported optimization for breakpoints');
    end
    % scale the initial velocity
    for iI = 1:length(Y0dots)
        Y0dots{iI} = Y0dots{iI} ./ ( max(paramsRegress.ts) - min(paramsRegress.ts) );
    end
    Y0dot = Y0dots;
    t0(1) = min(paramsRegress.ts);
        
    % the minimal breakpoint should be bigger than the initial point
    if ~isempty(bkPnts)
        assert( min(bkPnts) > t0(1) );
    end
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        searchRange = [min(cntTruth), max(cntTruth)];
        cntTestEst = projectPointsToGeodesicODE45Piecewise(Y0{1}, Y0dots, [t0(1) bkPnts], omaTest, searchRange(1), searchRange(2), grParams.estErr);
    end
    t0 = [t0(1) bkPnts];
    disp(' ');
    
elseif strcmp( grParams.regressMethod{nFlag}, 'TimeWarpRegression' )
    disp('****** Time warped geodesic regression (Time-warped GGR) ******');
    paramsRegress.Ys = omaTrain;
    paramsRegress.ts = cntTrain;
    paramsRegress.wi = grParams.weight;
    paramsRegress.nt = grParams.nt;
    paramsRegress.h = 1./paramsRegress.nt;
    paramsRegress.sigmaSqr = grParams.sigmaSqr;
    paramsRegress.alpha = grParams.alpha;
    paramsRegress.stopThreshold = grParams.stopThreshold;
    paramsRegress.deltaT = grParams.deltaT;
    paramsRegress.deltaTWarp = grParams.deltaTWarp;
    paramsRegress.maxReductionSteps = grParams.maxReductionSteps;
    paramsRegress.rho = grParams.rho;
    paramsRegress.nIterMax = grParams.nIterMax;
    paramsRegress.nIterMaxTimeWarp = grParams.nIterMaxTimeWarp;
    paramsRegress.minFunc = grParams.minFunc;  
    paramsRegress.twMinFunc = grParams.twMinFunc;
    paramsRegress.useODE45 = grParams.useODE45;
    paramsRegress.useRansac = grParams.useRansac;
    paramsRegress.nsize = size(paramsRegress.Ys{1});
    paramsRegress.figIdLogisticFunc = grParams.figIdLogisticFunc;
    
    if isfield( grParams, 'maxIterFminconTW' )
        paramsRegress.maxIterFminconTW = grParams.maxIterFminconTW;
    end
    
    if isfield( grParams, 'k' )
        paramsRegress.k = grParams.k;
    end
    
    if isfield( grParams, 'M' )
        paramsRegress.M = grParams.M;
    end
    
    % the ground-truth for comparison
    if isfield( grParams, 't_truth' )
        paramsRegress.t_truth = grParams.t_truth;
    end
    if isfield( grParams, 'Y_truth' )
        paramsRegress.Y_truth = grParams.Y_truth;
    end
    if isfield( grParams, 'pDim' )
        paramsRegress.pDim = grParams.pDim;
    end
    
    % given an initialization, usually for debugging
    if isfield( grParams, 'Y0Init' )
        paramsRegress.Y0Init = grParams.Y0Init;
    end
    if isfield( grParams, 'Y0dotInit' )
        paramsRegress.Y0dotInit = grParams.Y0dotInit;
    end
    
    % time warped regression 
    [Y0{1}, Y0dot{1}, energy, tsWarped, timeWarpParams] = timeWarpedRegressionOnGrassmannManifold( paramsRegress );
    % scale the velocity
    Y0dot{1} = Y0dot{1} ./ ( max(tsWarped) - min(tsWarped) );
    %initVelUsed = initVelUsed ./ ( max(paramsRegress.ts) - min(paramsRegress.ts) );
    t0(1) = min(tsWarped);
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        searchRange = sort(generalisedLogisticFunction([min(cntTruth), max(cntTruth)], timeWarpParams));
        cntTestEst = projectPointsToGeodesicODE45(Y0{1}, Y0dot{1}, t0(1), omaTest, searchRange(1), searchRange(2), grParams.estErr);
        cntTestEst = inverseLogisticFunction(cntTestEst, timeWarpParams);
    end
    disp(' ');
    
elseif strcmp( grParams.regressMethod{nFlag}, 'CubicSplineRegression' )
    disp('****** Cubic spline geodesic regression (Cubic-spline GGR) ******');
    paramsRegress.Ys = omaTrain;
    paramsRegress.ts = cntTrain;
    paramsRegress.wi = grParams.weight;
    paramsRegress.nt = grParams.nt;
    paramsRegress.h = 1./paramsRegress.nt;
    paramsRegress.sigmaSqr = grParams.sigmaSqr;
    paramsRegress.alpha = grParams.alpha;
    paramsRegress.stopThreshold = grParams.stopThreshold;
    paramsRegress.deltaT = grParams.deltaT;
    paramsRegress.deltaTWarp = grParams.deltaTWarp;
    paramsRegress.maxReductionSteps = grParams.maxReductionSteps;
    paramsRegress.rho = grParams.rho;
    paramsRegress.nIterMax = grParams.nIterMax;
    paramsRegress.nIterBoth = grParams.nIterBoth;
    paramsRegress.minFunc = grParams.csMinFunc;  
    paramsRegress.useODE45 = grParams.useODE45;
    paramsRegress.useRansac = grParams.useRansac;
    paramsRegress.nsize = size(paramsRegress.Ys{1});
    paramsRegress.cps = grParams.cps;
    paramsRegress.X10GT = paramsRegress.Ys{1};
    
    if isfield( grParams, 'fidSplineEnergy' )
        paramsRegress.fidSplineEnergy = grParams.fidSplineEnergy;
    else
        paramsRegress.fidSplineEnergy = 500;
    end
    
    if isfield( grParams, 'fidSplineFitting' )
        paramsRegress.fidSplineFitting = grParams.fidSplineFitting;
    else
        paramsRegress.fidSplineFitting = 100;
    end
    
    if isfield(grParams, 'maxIterFmincon')
        paramsRegress.maxIterFmincon = grParams.maxIterFmincon;
        if strcmp(grParams.csMinFunc, 'both')
            paramsRegress.nIterMax = grParams.maxIterFmincon;
        end
    end
    
    % the ground-truth for comparison
    if isfield( grParams, 't_truth' )
        paramsRegress.t_truth = grParams.t_truth;
    end
    if isfield( grParams, 'Y_truth' )
        paramsRegress.Y_truth = grParams.Y_truth;
    end
    if isfield( grParams, 'pDim' )
        paramsRegress.pDim = grParams.pDim;
    end
    
    % time warped regression 
    [X1Opt, X2Opt, X3Opt, X4sOpt, energy] = cubicSplineRegression( paramsRegress );
    Y0{1} = X1Opt;
    disp('Need improvement: Velocity is not scaled, currently it is within [0, 1]');
    % scale the velocity
    Y0dot = cell(length(X4sOpt)+2, 1);
    Y0dot{1} = X2Opt;
    Y0dot{2} = X3Opt;
    for iTmp = 1:length(X4sOpt)
        Y0dot{iTmp+2} = X4sOpt{iTmp};
    end
    t0 = [min(paramsRegress.ts) paramsRegress.cps max(paramsRegress.ts)];
    if ~isfield(grParams, 'predictScalarValue') || grParams.predictScalarValue == 1
        % prediction
        disp('May have problem, because the estimated cubic spline could be non-monotonic');
        searchRange = [min(cntTruth), max(cntTruth)];
        cntTestEst = projectPointsToGeodesicODE45Spline(X1Opt, X2Opt, X3Opt, X4sOpt, t0, omaTest, ...
            searchRange(1), searchRange(2), paramsRegress.nt);
    end
    disp(' ');
else
    disp(grParams.regressMethod{nFlag});
    error('Unknow method');
end
end
