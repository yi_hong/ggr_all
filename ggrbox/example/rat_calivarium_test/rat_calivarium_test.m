% corpus callosum test

close all
clear all

%profile -memory on

%grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
%outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/rat_calivarium';
grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';         % data for regression
outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_093014/rat_calivarium2';  % regression results

%grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression', 'ContinuousPiecewiseRegression', 'ContinuousPiecewiseRegressionWithOptimalBreakpoints'};  
%grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression'};  
%grParams.regressMethod = {'FullRegression', 'TimeWarpRegression'};  
grParams.regressMethod = {'CubicSplineRegression'};
grParams.minValue = 0;                               % the minimal range of the independent value, for plots                
grParams.maxValue = 160;                             % the maximal range of the independent value, for plots

% parameters for compute the cost function
grParams.sigmaSqr = 1;                               % the coefficient for matching term in the cost function
grParams.alpha = 0;                                  % the coefficient for prior knowledge
grParams.nt = 200;
grParams.deltaT = 0.01;
grParams.deltaTBk = 1.0;
grParams.deltaTWarp = 0.01;
grParams.maxReductionSteps = 5;
grParams.rho = 0.5;
grParams.nIterMax = 300;
grParams.nIterMaxBk = 50;
grParams.nIterMaxTimeWarp = 50;
grParams.stopThreshold = 1e-14;
grParams.minFunc = 'linesearch';                     % linesearch, fmincon
grParams.twMinFunc = 'fmincon';
grParams.csMinFunc = 'both';
grParams.nIterBoth = 50;
grParams.useODE45 = true;                            % more stable
grParams.estErr = 0.05;                              % discretizing size for prediction
grParams.useWeightedData = false;                    % weight data using its density
grParams.useRansac = true;                           % randomly choose pairs for pair-wise searching

rParams.fullGGRInitZero = true;                    % use zero to initialize the initial conditions   
grParams.predictScalarValue = 0;                    % 0: don't estimate independent value, 1: predict
grParams.figIdLogisticFunc = 600;                   % plot logistic function
grParams.fidSplineEnergy = 500;

grParams.maxIterFmincon = 300;

grParams.M = 30;
grParams.k = 0.1;

tMinPlot = 7;
tMaxPlot = 150;
nNumShape = 20;

[omas, ages, sigma] = loadRatCalivariumLandmarksAndSVD( grParams );
[ages, index] = sort(ages);
omas = omas(index);
grParams.nSys = length(omas);
%grParams.cps = [(min(ages)+max(ages))/2];
grParams.cps = [50 100];

% permutation test
nPermutation = 0;
rng('default');

R2 = zeros( nPermutation+1, length(grParams.regressMethod) );
errSqr = zeros( grParams.nSys, length(grParams.regressMethod) );

for iPer = 1:nPermutation+1
    
    if iPer > 1
        % reorder the ages
        ages = ages( randperm(length(ages)) );
    end
    
    nPartition = 18; %grParams.nSys;
    for iP = 1:nPartition
        grParams.nTesting = iP:nPartition:grParams.nSys;
        grParams.nSampling = 1:grParams.nSys;
        if nPartition > 1
            grParams.nSampling(grParams.nTesting) = [];
        end
        [ageEst, errEst, tPos, initPnt, initVel, totEnergy, timeWarpParams] = estimateOnFeature( omas, ages, grParams, iP );

        % save the plot and data
        filename = sprintf('%s/rat_calivarium_Part%dOf%d.fig', outputPrefix, iP, nPartition);
        saveas( gca, filename );
        filename = sprintf('%s/rat_calivarium_Part%dOf%d.png', outputPrefix, iP, nPartition);
        saveas( gca, filename );
        filename = sprintf('%s/rat_calivarium_Part%dOf%d.mat', outputPrefix, iP, nPartition);
        save( filename, 'ages', 'ageEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy', 'timeWarpParams' );
        
        % compute the R2 statistic
        R2(iPer, :) = computeR2Statistic( initPnt, initVel, tPos, timeWarpParams, omas, ages, grParams );
        
        % save R2
        for iJ = 1:size(R2, 2);
            pValue = length( find(R2(2:end, iJ) >= R2(1, iJ)) ) / ( size(R2, 1) - 1 );
            fprintf('p-value: %f\n', pValue);
        end
        filename = sprintf('%s/rat_calivarium_pValue_nPermute%d_Part%d_of_%d.txt', outputPrefix, nPermutation, iP, nPartition);
        save(filename, 'R2', 'pValue', '-ascii');

    
        % compute the test error
        for iJ = 1:length(initPnt)
            errSqr(grParams.nTesting, iJ) = computeGeodesicTestError( grParams.regressMethod, initPnt{iJ}, initVel{iJ}, tPos{iJ}, ...
                timeWarpParams{iJ}, omas(grParams.nTesting), ages(grParams.nTesting), grParams.nt );
        end
        
        % plot the shape on geodesic
        plot2DShapeOnGeodesic( initPnt, initVel, tPos, timeWarpParams, tMinPlot, tMaxPlot, sigma, nNumShape, grParams, 2 );
        filename = sprintf('%s/rat_calivarium_shapes_nPermute%d_Part%d_of_%d.fig', outputPrefix, nPermutation, iP, nPartition);
        figure(400), saveas(gca, filename);
    end

end

% save cross-validation error
filename = sprintf( '%s/errSqr_part%d.mat', outputPrefix, nPartition );
save(filename, 'errSqr');

%p = profile('info');
%filename = sprintf('%s/corpus_callosum_profile', outputPrefix);
%profsave(p, filename);
