% system identification
function [oma, cntAvg] = sysIdOnVideoAndComputeIndependentValue( videos, counts, params )

oma = cell(length(videos), 1);
cntAvg = zeros(length(videos), 1);
for iI = 1:length(videos)
    if strcmp( params.sysid, 'dt' )
        sysParams.subtractMean = params.subtractMean;
        sysParams.class = 2;
        if (~params.useWeightedSysid)
            sysTmp = suboptimalSystemID(videos{iI}, params.nSta, sysParams);
            cntAvg(iI) = mean(counts(iI, :));
        else
            weight = gaussmf(1:params.nWinSize, [params.sigmaWeightedSysid params.nWinSize/2]);
            % adjust 
            idTmp = floor(params.winTemporal/2)+1:params.stepTemporal:length(weight)-params.winTemporal+floor(params.winTemporal/2)+1;
            weight = weight( idTmp );
            weight = weight ./ sum(weight);
            sysTmp = weighted_suboptimal_systemID(videos{iI}, params.nSta, weight, sysParams);
            cntAvg(iI) = sum(counts(iI, idTmp) .* weight);
        end
    else
        error( 'Not supported system identification' );
    end
    oma{iI} = omat(sysTmp, params.nObs);
end