% Simple toy example script to demonstrate geodesic regression on the
% Grassmannian manifold 
%
% Strategy:
%
%   Generate 2-D signals (sine,cosine) with various frequencies f_i. These
%   frequencies could be the regression variable.
%
%   We then project the signal into 24-D space and add some noise to make 
%   the problem a little bit more interesting. In principle, these signals
%   can be modeled using a 2-state LDS.
%
%   Yi Hong: yihong@cs.unc.edu
%   Roland Kwitt: rkwitt@gmx.at
%

clear all
close all

%% generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10]
nTotalNum = 25;
%f = sort(rand(1, nTotalNum)*10);
minBoundary = 0.2;
maxBoundary = 9.8;
%minBoundary = 0.5;       % the minimum frequency
%maxBoundary = 10.5;      % the maximum frequency
f = linspace(minBoundary, maxBoundary, nTotalNum);

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% system identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    s = [sin(f(i)*x); cos(f(i)*x)];
    org{i} = s;
    y =  CC*s;
    y = y + 0.01*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weighted_suboptimal_systemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end


%% generated data with breakpoints

keepOrignalData = true;     % use the above data, false: generate new data based on the above data
setBreakpoint = [5];       % the breakpoints, [] means no breakpoint
varLen = [0];                % the variance of the breakpoints (bk-var, bk+var), varLen >= 0

assert( length(setBreakpoint) == length(varLen) );
assert( sum( setBreakpoint - varLen > minBoundary ) + sum( setBreakpoint + varLen < maxBoundary ) == 2*length(setBreakpoint) );

% count the length for each piece for computing the number of the generated data
portion = [minBoundary setBreakpoint maxBoundary];
portion = portion(2:end) - portion(1:end-1);
for iI = 1:length(varLen)
    portion(iI) = portion(iI) - varLen(iI);
    portion(iI+1) = portion(iI+1) - varLen(iI);
end
portion = portion ./ sum(portion);

if ~keepOrignalData

    if ~isempty(setBreakpoint)
        
        % compute the geodesic passing through the given points
        [Y0, Y0dot, t0] = grpairSearchODE45( oma, f, 1, 0, 1, true, false, ones(size(f)) );
        
        % compute the number of points in each piece based on above portion
        numInEachPiece = zeros( length(setBreakpoint)+1, 1 );
        for iI = 1:length(numInEachPiece)-1
            numInEachPiece(iI) = floor( nTotalNum * portion(iI) );
        end
        numInEachPiece(end) = nTotalNum - sum( numInEachPiece(1:end-1) );
        
        % make the breakpoint in increasing order
        setBreakpoint = sort(setBreakpoint);
        
        % move the initial condition to the first breakpoint 
        if t0 > setBreakpoint(1)   
            [~, Ytmp, Ydottmp] = integrateForwardWithODE45( Y0, -Y0dot, [0 (t0-setBreakpoint(1))/2.0 (t0-setBreakpoint(1))] );
            Y0 = Ytmp{end};
            Y0dot = -Ydottmp{end};
        elseif t0 < setBreakpoint(1)
            [~, Ytmp, Ydottmp] = integrateForwardWithODE45( Y0, Y0dot, [0 (setBreakpoint(1)-t0)/2.0 (setBreakpoint(1)-t0)] );
            Y0 = Ytmp{end};
            Y0dot = Ydottmp{end};
        end
        
        % shoot backward to the minimal boundary, this is the first piece
        if varLen(1) <= 0
            tSpanTmp = linspace( setBreakpoint(1), minBoundary, numInEachPiece(1)+1 );
        else
            tSpanTmp = linspace( setBreakpoint(1)-varLen(1), minBoundary, numInEachPiece(1) );
            tSpanTmp = [setBreakpoint(1) tSpanTmp];
        end
        [Ttmp, Ytmp, Ydottmp] = integrateForwardWithODE45( Y0, Y0dot, tSpanTmp);

        % store the ground-truth for testing
        %grParams.Y0Init = Ytmp{end};
        %grParams.Y0dotInit{1} = Ydottmp{end};

        f = [];
        oma = [];
        % the first breakpoint will be covered in the next piece
        for iI = 2:length(Ttmp)
            f(iI-1) = Ttmp(iI);
            oma{iI-1} = Ytmp{iI};
        end
        
        % shoot forward to the maximal boundary
        YdotPre = Y0dot;
        for iK = 1:length(setBreakpoint)
            % change the initial velocity at the breakpoint
            %Y0dot = rand(size(Y0dot));
            Y0dot = 5*Y0dot;

            % make it orthogonal to the previous one
            %Y0dot = Y0dot - trace(Y0dot'*YdotPre)*YdotPre/sqrt(trace(YdotPre'*YdotPre));    

            % projection
            Y0dot = Y0dot - Y0 * (Y0' * Y0dot);
            % keep the normal of the velocity unchanged
            %Y0dot = Y0dot * sqrt(trace(YdotPre' * YdotPre)) / sqrt(trace(Y0dot' * Y0dot));
           
            if iK == length(setBreakpoint)    % last piece
                if varLen <= 0 
                    tSpanTmp = linspace( setBreakpoint(iK), maxBoundary, numInEachPiece(iK+1) );
                else
                    tSpanTmp = linspace( setBreakpoint(iK)+varLen(iK), maxBoundary, numInEachPiece(iK+1) );
                    tSpanTmp = [setBreakpoint(iK) tSpanTmp];
                end
            else
                if varLen <= 0 
                    tSpanTmp = linspace( setBreakpoint(iK), setBreakpoint(iK+1), numInEachPiece(iK+1)+1 );
                else
                    tSpanTmp = linspace( setBreakpoint(iK)+varLen(iK), setBreakpoint(iK+1)-varLen(iK+1), numInEachPiece(iK+1) );
                    tSpanTmp = [setBreakpoint(iK) tSpanTmp setBreakpoint(iK+1)];
                end
            end
            [Ttmp, Ytmp, Ydottmp] = integrateForwardWithODE45( Y0, Y0dot, tSpanTmp );
            

            % store the ground-truth
            %grParams.Y0dotInit{end+1} = Y0dot;

            Y0 = Ytmp{end};
            Y0dot = Ydottmp{end};
            YdotPre = Y0dot;
            
            if varLen <= 0 
                for iI = 1:length(Ttmp)-1
                    f(end+1) = Ttmp(iI);
                    oma{end+1} = Ytmp{iI};
                end
            else
                for iI = 2:length(Ttmp)-1
                    f(end+1) = Ttmp(iI);
                    oma{end+1} = Ytmp{iI};
                end
            end

            if iK == length(setBreakpoint)
                f(end+1) = Ttmp(end);
                oma{end+1} = Ytmp{end};            
            end
        end
        
    else
        % generate data with any initial point
        Y0 = oma{1};
        Y0dot = 0.015*(randn(size(Y0)));
        % projection
        Y0dot = Y0dot - Y0 * (Y0' * Y0dot);
        [Ttmp, Ytmp, Ydottmp] = integrateForwardWithODE45( Y0, Y0dot, linspace( minBoundary, maxBoundary, nTotalNum ) );
        f = Ttmp';
        oma = Ytmp;

        % ground truth
        %grParams.Y0Init = Y0;
        %grParams.Y0dotInit{1} = Y0dot;
    end
    [f, index] = sort(f);
    oma = oma(index);

    grParams.t_truth = f;
    grParams.Y_truth = oma;
    grParams.pDim = pDim;
   
end

%save('tmp.mat', 'f', 'oma', 'Y0InitTmp', 'Y0dotInitTmp');

%tmp = load('tmp.mat');
%f = tmp.f;
%oma = tmp.oma;

% visualize the data
figure(100), hold on
plotGeodesic( oma, f, pDim, '-' );
hold off
title( 'Geodesic shown with eigenvalues' );

%% regression
grParams.regressMethod = {'ContinuousPiecewiseRegressionWithOptimalBreakpoints'}; % five GGRs
%grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression'}; %, ...
    %'ContinuousPiecewiseRegression', 'ContinuousPiecewiseRegressionWithOptimalBreakpoints'}; 
grParams.minValue = 0;            % the minimum frequency for plot
grParams.maxValue = max(f);       % the maximum frequency for plot

grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.deltaT = 0.05;           % the step size for updating the initial conditions
grParams.deltaTBk = 0.5;          % the step size for updating the breakpoints
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 300;          % the number of iterations for updating the initial conditions
grParams.nIterMaxBk = 50;         % the number of iterations for updating the breakpoints
grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'linesearch';  % use linesearch for updating the initial conditions, other methods could be implemented later
grParams.estErr = 0.01;           % the discrete step size for the prejection (prediction) 
grParams.useWeightedData = false; % weighted or unweighted GGR
grParams.useRansac = false;       % use ransac when the size of the training dataset is large
grParams.bks = setBreakpoint+varLen*1.5;     % the breakpoints
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue]; % the breakpoints for full piecewise GGR
grParams.nSys = length(oma);      % the number of points on the Grassmann manifold
grParams.optimizeBreakpoint = 2;  % 1: update initial conditions and breakpoints simultaneously, 2: iteratively

% if one result is available, whether use it to initialize other methods
grParams.useFullInitializePiecewise = false;         % use the result of full GGR to initialize the full piecewise GGR
grParams.useFullInitializeContinuous = false;        % use the full GGR result to initialize the continous piecewise GGR
grParams.usePiecewiseInitializeContinuous = true;    % use the piecewise full GGR results to initialize the continuous
grParams.useContinuousInitializeContinuousOB = true; % use the continuous to initialize the continuous with optimal breakpoints

% divide into 5 partitions, one for testing and the other four for training
nPartition = 5;
absErr2 = zeros( nPartition, length(grParams.regressMethod), 6 ); % 1-3: training, 4-6: testing
predictions = zeros( nTotalNum, length(grParams.regressMethod));  % predictions

for iI = 1:nPartition
    grParams.nTesting = iI:nPartition:grParams.nSys;              % testing set
    grParams.nSampling = 1:grParams.nSys;
    grParams.nSampling(grParams.nTesting) = [];                   %training set

    % geodesic regression on Grassmann manifold and prediction
    [cntEst, err, tPos{iI}, initPnt{iI}, initVel{iI}, totEnergy{iI}] = estimateOnFeature( oma, f', grParams, iI );  
    cntAvg = f';
    truth(:, 1) = f;

    for iJ = 1:length(cntEst)
        % predictions for different GGR
        predictions(grParams.nTesting, iJ) = cntEst{iJ}(grParams.nTesting);
        % training error     
        absErr2(iI, iJ, 1) = sum( abs( cntEst{iJ}(grParams.nSampling) - cntAvg(grParams.nSampling) ) );
        absErr2(iI, iJ, 2) = length(grParams.nSampling);
        absErr2(iI, iJ, 3) = absErr2(iI, iJ, 1) ./ absErr2(iI, iJ, 2);
        % testing error
        absErr2(iI, iJ, 4) = sum( abs( cntEst{iJ}(grParams.nTesting) - cntAvg(grParams.nTesting) ) );
        absErr2(iI, iJ, 5) = length(grParams.nTesting);
        absErr2(iI, iJ, 6) = absErr2(iI, iJ, 4) ./ absErr2(iI, iJ, 5);
    end
end

% absolute mean error for both training and testing  
absMeanErr = zeros( size(absErr2, 2), 2 );
stdErr = zeros( size(absErr2, 2), 2 );
for iJ = 1:size(absMeanErr, 1)
    absMeanErr(iJ, 1) = sum( absErr2(:, iJ, 1) ) ./ sum( absErr2(:, iJ, 2) );
    stdErr(iJ, 1) = std( absErr2(:, iJ, 3) );
    
    absMeanErr(iJ, 2) = sum( absErr2(:, iJ, 4) ) ./ sum( absErr2(:, iJ, 5) );
    stdErr(iJ, 2) = std( absErr2(:, iJ, 6) );
end

%% plot errorbar
figure, barwitherr( stdErr, absMeanErr, 'BarWidth', 0.8);
barmap = [0.3 0.3 1.0; 1.0 0.3 0.3];
colormap(barmap);
legend('Train', 'Test');
set( gca, 'xTickLabel', {'Pairwise searching', 'Full GGR', 'Piecewise GGR', 'Continuous piecewise GGR'} );
%xticklabel_rotate([1 2 3], 45, {'Pairwise search', 'Full regression', 'Piecewise regression'});
box off
grid on
set(gca, 'FontSize', 20);


style = {'k+', 'b+', 'g+', 'ks', 'yo'};

% plot truth-predction
figure, hold on;
for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( truth(:, 1), predictions(:, iJ), style{iJ}, 'LineWidth', 2 );
    plot( grParams.minValue:grParams.maxValue, grParams.minValue:grParams.maxValue, 'k--', 'LineWidth', 2 );
    axis equal
    xlim([grParams.minValue grParams.maxValue]);
    ylim([grParams.minValue grParams.maxValue])
    grid on
    title(grParams.regressMethod{iJ});
    xlabel('True values');
    ylabel('Predictions');
end

% plot sorted frequences
figure, hold on;
[truth(:, 1), idSort] = sort(truth(:, 1));
for iJ = 1:size(predictions, 2)
    predictions(:, iJ) = predictions(idSort, iJ);
end

for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( predictions(:, iJ), style{iJ}, 'LineWidth', 2);
    plot( truth(:, 1), 'r-', 'LineWidth', 2 );
    title(grParams.regressMethod{iJ});
    xlim([0 size(predictions, 1)]);
    grid on
end
hold off

groundTruth = truth;
%save('signal_frequency_prediction.mat', 'groundTruth', 'predictions', 'absMeanErr', 'stdErr');

