% Simple toy example script to demonstrate geodesic regression on the
% Grassmannian manifold 
%
% Strategy:
%
%   Generate 2-D signals (sine,cosine) with various frequencies f_i. These
%   frequencies could be the regression variable.
%
%   We then project the signal into 24-D space and add some noise to make 
%   the problem a little bit more interesting. In principle, these signals
%   can be modeled using a 2-state LDS.
%
%   Yi Hong: yihong@cs.unc.edu
%   Roland Kwitt: rkwitt@gmx.at
%

clear all
close all

%% generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10]
nTotalNum = 25;
minBoundary = 0.5;       % the minimum frequency
maxBoundary = 10.5;      % the maximum frequency
f = linspace(minBoundary, maxBoundary, nTotalNum);

% use the logistic function to change the f
twParams.A = 0.0;
twParams.K = 1.0;
twParams.beta = 1.0;
twParams.m = 1.0;
twParams.k = 1.0;
twParams.M = 5.0;
fOrg = f;
f = generalisedLogisticFunction( fOrg, twParams );
figure(601), plot( fOrg, f, 'r*-' );

f = f*10;

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% system identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    s = [sin(f(i)*x); cos(f(i)*x)];
    org{i} = s;
    y =  CC*s;
    y = y + 0.01*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weighted_suboptimal_systemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end

% plot the ground-truth
figure(602);
subplot(1, 2, 1 ), plotGeodesic( oma, fOrg', pDim, '*-' );
title('Ground truth');
xlabel('Real time points');
ylabel('Eigenvalue of A matrix');


%% regression
grParams.regressMethod = {'FullRegression', 'timeWarpRegression'}; % five GGRs
grParams.minValue = 0;            % the minimum frequency for plot
grParams.maxValue = 11;           % the maximum frequency for plot

grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.deltaT = 0.05;           % the step size for updating the initial conditions
grParams.deltaTBk = 0.5;          % the step size for updating the breakpoints
grParams.deltaTWarp = 0.01;
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 300;          % the number of iterations for updating the initial conditions
grParams.nIterMaxBk = 50;         % the number of iterations for updating the breakpoints
grParams.nIterMaxTimeWarp = 50;
grParams.stopThreshold = 1e-7;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'linesearch';  % use linesearch for updating the initial conditions, other methods could be implemented later
grParams.twMinFunc = 'fmincon';
grParams.estErr = 0.01;           % the discrete step size for the prejection (prediction) 
grParams.useWeightedData = false; % weighted or unweighted GGR
grParams.useRansac = false;       % use ransac when the size of the training dataset is large
grParams.bks = [5];               % the breakpoints
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue]; % the breakpoints for full piecewise GGR
grParams.nSys = length(oma);      % the number of points on the Grassmann manifold
grParams.optimizeBreakpoint = 2;  % 1: update initial conditions and breakpoints simultaneously, 2: iteratively
grParams.pDim = pDim;

% if one result is available, whether use it to initialize other methods
grParams.useFullInitializePiecewise = false;         % use the result of full GGR to initialize the full piecewise GGR
grParams.useFullInitializeContinuous = false;        % use the full GGR result to initialize the continous piecewise GGR
grParams.usePiecewiseInitializeContinuous = true;    % use the piecewise full GGR results to initialize the continuous
grParams.useContinuousInitializeContinuousOB = true; % use the continuous to initialize the continuous with optimal breakpoints

% divide into several partitions, one for testing and the rest for training
nPartition = 1; %grParams.nSys;

R2 = zeros(nPartition, length(grParams.regressMethod));
errSqr = zeros(length(oma), length(grParams.regressMethod));

for iI = 1:nPartition
    
    fprintf('%d/%d\n', iI, nPartition);
    
    grParams.nTesting = iI:nPartition:grParams.nSys;              % testing set
    grParams.nSampling = 1:grParams.nSys;
    if nPartition > 1
        grParams.nSampling(grParams.nTesting) = [];                   %training set
    end
    
    % geodesic regression on Grassmann manifold and prediction
    [cntEst, err, tPos{iI}, initPnt{iI}, initVel{iI}, totEnergy{iI}, timeWarpParams{iI}] = estimateOnFeature( oma, fOrg', grParams, iI );  
    
    % compute R2 statistic
    R2(iI, :) = computeR2Statistic(initPnt{iI}, initVel{iI}, tPos{iI}, timeWarpParams{iI}, oma, fOrg, grParams );
    
    % compute test error
    for iJ = 1:length(initPnt{iI})
        errSqr(grParams.nTesting, iJ) = computeGeodesicTestError( initPnt{iI}{iJ}, initVel{iI}{iJ}, tPos{iI}{iJ}(1), ...
            timeWarpParams{iI}{iJ}, oma(grParams.nTesting), fOrg(grParams.nTesting) );
    end
    
    % plot the regional data and the fitting results
    figure(602), subplot(1, 2, 2), hold on;
    for iJ = 1:length(initPnt{iI})
        % generate geodesic
        if strcmp( grParams.regressMethod{iJ}, 'FullRegression' )
            tSpanTmp = tPos{iI}{iJ}(1):0.1:grParams.maxValue;
            [~, omaGeo] = integrateForwardWithODE45( initPnt{iI}{iJ}{1}, initVel{iI}{iJ}{1}, tSpanTmp );
            plotGeodesic( omaGeo, tSpanTmp', pDim, '-.' );
        elseif strcmp( grParams.regressMethod{iJ}, 'timeWarpRegression' );
            tSpanTmp = inverseLogisticFunction( tPos{iI}{iJ}(1), timeWarpParams{iI}{iJ} ):0.1:grParams.maxValue;
            tSpanWarp = generalisedLogisticFunction( tSpanTmp, timeWarpParams{iI}{iJ} );
            [~, omaGeo] = integrateForwardWithODE45( initPnt{iI}{iJ}{1}, initVel{iI}{iJ}{1}, tSpanWarp );
            plotGeodesic( omaGeo, tSpanTmp', pDim, '--' );
        else
            error( 'Not supported yet' );
        end
    end
    plotGeodesic( oma, fOrg', pDim, 'o');
    hold off
    title('Ground truth');
    xlabel('Real time points');
    ylabel('Eigenvalue of A matrix');
end

mean(errSqr, 1)