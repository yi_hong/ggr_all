% Simple toy example script to demonstrate geodesic regression on the
% Grassmannian manifold 
%
% Strategy:
%
%   Generate 2-D signals (sine,cosine) with various frequencies f_i. These
%   frequencies could be the regression variable.
%
%   We then project the signal into 24-D space and add some noise to make 
%   the problem a little bit more interesting. In principle, these signals
%   can be modeled using a 2-state LDS.
%
%   Yi Hong: yihong@cs.unc.edu
%   Roland Kwitt: rkwitt@gmx.at
%

clear all
close all

%% generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10)
nTotalNum = 25;
minBoundary = 0.2;
maxBoundary = 9.8;
f = linspace(minBoundary, maxBoundary, nTotalNum);

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% system identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    s = [sin(f(i)*x); cos(f(i)*x)];
    org{i} = s;
    y =  CC*s;
    y = y + 0.01*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weighted_suboptimal_systemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end

%% regression
grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression'}; 
grParams.minValue = 0;            % the minimum frequency for plot
grParams.maxValue = max(f);       % the maximum frequency for plot

% the following parameters can remain to be unchanged
grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.deltaT = 0.05;           % the step size for updating the initial conditions
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable

% change if needed
grParams.nIterMax = 300;          % the number of iterations for updating the initial conditions
grParams.minFunc = 'linesearch';  % use linesearch for updating the initial conditions, other methods could be implemented later
grParams.useWeightedData = false; % weighted or unweighted GGR
grParams.useRansac = false;       % use ransac when the size of the training dataset is large
grParams.bks = 0.5*(min(f)+max(f));     % the breakpoints for piecewise regression

grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue]; % the breakpoints for full piecewise GGR
grParams.nSys = length(oma);      % the number of points on the Grassmann manifold

% for prediction
grParams.estErr = 0.01;           % the discrete step size for the prejection (prediction) 

% if one result is available, whether use it to initialize other methods
grParams.useFullInitializePiecewise = false;         % use the result of full GGR to initialize the full piecewise GGR

% divide into 5 partitions, one for testing and the rest four for training
nPartition = 5;
absErr2 = zeros( nPartition, length(grParams.regressMethod), 6 ); % 1-3: training, 4-6: testing
predictions = zeros( nTotalNum, length(grParams.regressMethod));  % predictions

for iI = 1:nPartition
    grParams.nTesting = iI:nPartition:grParams.nSys;              % testing set
    grParams.nSampling = 1:grParams.nSys;
    grParams.nSampling(grParams.nTesting) = [];                   %training set

    % geodesic regression on Grassmann manifold and prediction
    [cntEst, err, tPos{iI}, initPnt{iI}, initVel{iI}, totEnergy{iI}] = estimateOnFeature( oma, f', grParams, iI );  
    cntAvg = f';
    truth(:, 1) = f;

    for iJ = 1:length(cntEst)
        % predictions for different GGR
        predictions(grParams.nTesting, iJ) = cntEst{iJ}(grParams.nTesting);
        % training error     
        absErr2(iI, iJ, 1) = sum( abs( cntEst{iJ}(grParams.nSampling) - cntAvg(grParams.nSampling) ) );
        absErr2(iI, iJ, 2) = length(grParams.nSampling);
        absErr2(iI, iJ, 3) = absErr2(iI, iJ, 1) ./ absErr2(iI, iJ, 2);
        % testing error
        absErr2(iI, iJ, 4) = sum( abs( cntEst{iJ}(grParams.nTesting) - cntAvg(grParams.nTesting) ) );
        absErr2(iI, iJ, 5) = length(grParams.nTesting);
        absErr2(iI, iJ, 6) = absErr2(iI, iJ, 4) ./ absErr2(iI, iJ, 5);
    end
end

% absolute mean error for both training and testing  
absMeanErr = zeros( size(absErr2, 2), 2 );
stdErr = zeros( size(absErr2, 2), 2 );
for iJ = 1:size(absMeanErr, 1)
    absMeanErr(iJ, 1) = sum( absErr2(:, iJ, 1) ) ./ sum( absErr2(:, iJ, 2) );
    stdErr(iJ, 1) = std( absErr2(:, iJ, 3) );
    
    absMeanErr(iJ, 2) = sum( absErr2(:, iJ, 4) ) ./ sum( absErr2(:, iJ, 5) );
    stdErr(iJ, 2) = std( absErr2(:, iJ, 6) );
end

%% plot errorbar
figure, barwitherr( stdErr, absMeanErr, 'BarWidth', 0.8);
barmap = [0.3 0.3 1.0; 1.0 0.3 0.3];
colormap(barmap);
legend('Train', 'Test');
set( gca, 'xTickLabel', {'Pairwise searching', 'Full GGR', 'Piecewise GGR', 'Continuous piecewise GGR'} );
%xticklabel_rotate([1 2 3], 45, {'Pairwise search', 'Full regression', 'Piecewise regression'});
box off
grid on
set(gca, 'FontSize', 20);


style = {'k+', 'b+', 'g+', 'ks', 'yo'};

% plot truth-predction
figure, hold on;
for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( truth(:, 1), predictions(:, iJ), style{iJ}, 'LineWidth', 2 );
    plot( grParams.minValue:grParams.maxValue, grParams.minValue:grParams.maxValue, 'k--', 'LineWidth', 2 );
    axis equal
    xlim([grParams.minValue grParams.maxValue]);
    ylim([grParams.minValue grParams.maxValue])
    grid on
    title(grParams.regressMethod{iJ});
    xlabel('True values');
    ylabel('Predictions');
end

% plot sorted frequences
figure, hold on;
[truth(:, 1), idSort] = sort(truth(:, 1));
for iJ = 1:size(predictions, 2)
    predictions(:, iJ) = predictions(idSort, iJ);
end

for iJ = 1:size(predictions, 2)
    subplot(1, size(predictions, 2), iJ), hold on;
    plot( predictions(:, iJ), style{iJ}, 'LineWidth', 2);
    plot( truth(:, 1), 'r-', 'LineWidth', 2 );
    title(grParams.regressMethod{iJ});
    xlim([0 size(predictions, 1)]);
    grid on
end
hold off

groundTruth = truth;
%save('signal_frequency_prediction.mat', 'groundTruth', 'predictions', 'absMeanErr', 'stdErr');

