% Amy's dataset example

close all
clear all

profile -memory on

grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_011714/amy_AR';

%grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';
%outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_011714/amy';

setup();

grParams.sysid = 'dt';
grParams.useWeightedSysid = false;
grParams.featureName = 'HOOF';
grParams.regressMethod = {'PairSearch', 'FullRegression', 'PiecewiseRegression'};
grParams.minValue = 0;
grParams.maxValue = 400000;

grParams.sigmaSqr = 1;
grParams.alpha = 0;
grParams.nt = 100;                % the number of the discretized grids 
grParams.deltaT = 0.2;           % the step size for updating the initial conditions
grParams.deltaTBk = 0.5;          % the step size for updating the breakpoints
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 100;          % the number of iterations for updating the initial conditions
grParams.nIterMaxBk = 50;         % the number of iterations for updating the breakpoints
grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
grParams.minFunc = 'linesearch';
grParams.useODE45 = true;
grParams.estErr = 100;
grParams.bks = [80000];
grParams.breakpoint = [grParams.minValue, grParams.bks, grParams.maxValue]; % the breakpoints for full piecewise GGR
grParams.useWeightedData = false;
grParams.useRansac = false;
grParams.optimizeBreakpoint = 2;  % 1: update initial conditions and breakpoints simultaneously, 2: iteratively

% if one result is available, whether use it to initialize other methods
grParams.useFullInitializePiecewise = true;          % use the result of full GGR to initialize the full piecewise GGR
grParams.useFullInitializeContinuous = false;        % use the full GGR result to initialize the continous piecewise GGR
grParams.usePiecewiseInitializeContinuous = true;    % use the piecewise full GGR results to initialize the continuous
grParams.useContinuousInitializeContinuousOB = true; % use the continuous to initialize the continuous with optimal breakpoints

grParams.nSta = 10;
grParams.nObs = grParams.nSta;

[oma, fibro] = loadAmyFeatureAndSysId( grParams );
grParams.nSys = length(oma);

nPartition = grParams.nSys;
for iP = 1:nPartition
    grParams.nTesting = iP:nPartition:grParams.nSys;
    grParams.nSampling = 1:grParams.nSys;
    grParams.nSampling(grParams.nTesting) = [];
    
    [fibroEst, err, tPos, initPnt, initVel, totEnergy] = estimateOnFeature(oma, fibro, grParams, iP);
    
    filename = sprintf('%s/amy_wd%d_Part%dOf%d.fig', outputPrefix, grParams.useWeightedData, iP, nPartition);
    saveas(gca, filename);
    filename = sprintf('%s/amy_wd%d_Part%dOf%d.png', outputPrefix, grParams.useWeightedData, iP, nPartition);
    saveas(gca, filename);
    filename = sprintf('%s/amy_wd%d_Part%dOf%d.mat', outputPrefix, grParams.useWeightedData, iP, nPartition);
    save(filename, 'fibro', 'fibroEst', 'grParams', 'err', 'tPos', 'initPnt', 'initVel', 'totEnergy');
end


