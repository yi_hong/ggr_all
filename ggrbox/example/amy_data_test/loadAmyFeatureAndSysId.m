% load features

function [oma, fibro] = loadAmyFeatureAndSysId( params )

if strcmp( params.featureName, 'RAW' )
    disp( 'Not implemented yet, coming soon');
elseif strcmp( params.featureName, 'HOOF' )
    %videoName = {'DR30-1', 'DR30-2', 'DR30-3', 'DR33-1', 'DR33-2', ...
    %    'DR39-1', 'DR39-2', 'DR39-3', 'DR39-4', 'DR3-30-1', 'DR3-30-2', 'DR3-30-3'};
    %fibro = [0, 0, 0, 30000, 30000, 90000, 90000, 90000, 90000, 300000, 300000, 300000]';
    videoName = {'AR30-1', 'AR30-2', 'AR30-3', 'AR30-4', 'AR33-1', 'AR33-2', ...
        'AR39-1', 'AR39-2', 'AR39-3', 'AR3-30-1', 'AR3-30-2', 'AR3-30-3'};
    fibro = [0, 0, 0, 0, 30000, 30000, 90000, 90000, 90000, 300000, 300000, 300000]';
    for iI = 1:length(videoName)
        filename = sprintf( '%s/data/hoof_amy/%s_HOOF.mat', params.inputPrefix, videoName{iI} );
        tmp = load(filename);
        %tmp.imageHOOF(:, end) = [];
        %for iJ = 1:size(tmp.imageHOOF, 2)
        %    tmp.imageHOOF(:, iJ) = tmp.imageHOOF(:, iJ) ./ sum(tmp.imageHOOF(:, iJ));
        %end
        %imgdb{iI} = log(tmp.imageHOOF+1);
        imgdb{iI} = tmp.imageFeature;
    end
end

oma = cell( length(imgdb), 1 );

for iI = 1:length(oma)
    if strcmp( params.sysid, 'dt' )
        sysParams.class = 2;
        sysTmp = suboptimalSystemID(imgdb{iI}, params.nSta, sysParams);
        oma{iI} = omat(sysTmp, params.nObs);
    elseif strcmp( params.sysid, 'n4sid' )
        dataTmp = iddata( (imgdb{iI})', [], 0.1);
        opt = n4sidOptions('Focus', 'prediction', 'Display', 'off');
        sysn4 = n4sid(dataTmp, params.nSta, opt);
        sysTmp.C = sysn4.c;
        sysTmp.A = sysn4.a;
        oma{iI} = omat(sysTmp, params.nObs);
    else
        error( 'Not supported system identification' );
    end
end
