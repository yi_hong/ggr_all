% 
% integrate forward for spline regression in each interval 
% 
% Author: Yi Hong, yihong@cs.unc.edu
%
%

function [X1, X2, X3, X4] = adimat_integrate_forward_each_interval_spline( X10, X20, X30, X4s, params )

X10_tmp = X10;
X20_tmp = X20;
X30_tmp = X30;
%X1 = cell(length(params.ts), 1);
%X2 = cell(length(params.ts), 1);
%X3 = cell(length(params.ts), 1);
%X4 = cell(length(params.ts), 1);
X1 = {};
X2 = {};
X3 = {};
X4 = {};
nIndex = 1;
for iI = 1:length(X4s)
    [X1Tmp, X2Tmp, X3Tmp, X4Tmp] = adimat_integrate_forward_spline( X10_tmp, X20_tmp, ...
        X30_tmp, X4s{iI}, params.Gns(iI), params.h );
    nStart = 1;
    if iI == 1
        %X1 = [X1 X1Tmp];
        %X2 = [X2 X2Tmp];
        %X3 = [X3 X3Tmp];
        %X4 = [X4 X4Tmp];
    else
        nStart = 2;
        %X1 = [X1 X1Tmp(2:end)];
        %X2 = [X2 X2Tmp(2:end)];
        %X3 = [X3 X3Tmp(2:end)];
        %X4 = [X4 X4Tmp(2:end)];
    end
    for iJ = nStart:length(X1Tmp)
        X1{nIndex} = X1Tmp{iJ};
        X2{nIndex} = X2Tmp{iJ};
        X3{nIndex} = X3Tmp{iJ};
        X4{nIndex} = X4Tmp{iJ};
        nIndex = nIndex + 1;
    end
    
    X10_tmp = X1Tmp{end};
    X20_tmp = X2Tmp{end};
    X30_tmp = X3Tmp{end};
end
