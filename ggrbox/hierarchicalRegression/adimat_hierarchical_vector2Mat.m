% 
% mat and vector exchange
% 
function [X1, X2, X3, X4s] = adimat_hierarchical_vector2Mat(Y, nSizeX)

YMat = reshape(Y, [], nSizeX(2));
X1 = YMat(1:nSizeX(1), :);
X2 = YMat(nSizeX(1)+1:2*nSizeX(1), :);
X3 = YMat(2*nSizeX(1)+1:3*nSizeX(1), :);
nX4s = size(YMat, 1) / nSizeX(1) - 3;
X4s = cell(nX4s, 1);
for iJ = 1:length(X4s)
    X4s{iJ} = YMat((2+iJ)*nSizeX(1)+1:(3+iJ)*nSizeX(1), :);
end

end