function [f, g] = adimat_hierarchical_driver(x, opts_all)
    
    params = opts_all.params;
    opts = opts_all.optsAdimat;
    gX1Ctl = opts_all.gX1Ctl;
    
    [g, f] = admDiffRev( @adimat_compute_hierarchical_energy, ...
        1, x, params, opts );

    % project the gradient
    nSizeX = size(params.Ys{1});
    xMat = reshape(x, [], nSizeX(2));
    x0 = xMat(1:nSizeX(1), :);

    gMat = reshape(g, [], nSizeX(2));
    gX1 = gMat(1:nSizeX(1), :);
    gX2 = gMat(nSizeX(1)+1:2*nSizeX(1), :);
    gX3 = gMat(2*nSizeX(1)+1:3*nSizeX(1), :);

    gMat(1:nSizeX(1), :) = gX1 - x0 * (x0' * gX1);
    if gX1Ctl == 0 % don't update the first state
        gMat(1:nSizeX(1), :) = gX1 - gX1;
    end
    gMat(nSizeX(1)+1:2*nSizeX(1), :) = gX2 - x0 * (x0' * gX2);
    gMat(2*nSizeX(1)+1:3*nSizeX(1), :) = gX3 - x0 * (x0' * gX3);

    g = gMat(:);
    
end