% 
% test hierarchical regression on synthetic data
%
% Yi Hong, yihong@cs.unc.edu
%

clear all
%close all

% generate data
nSub = 35;
nPnt = 7;
sigma = 0.6;
[dataPoints, subTime, subIds, dataGT] = genSynDataForHierarchicalRegression(nSub, nPnt, sigma);

% set basic regression parameters
grParams = setRegressParams();


% use hierarchical regression
useHR = true;

if useHR
    % individial trend
    [initPnts, initVels, initTimes] = fitIndividualTrend(dataPoints, subTime, subIds, grParams);

    % group trend
    params = grParams;
    params.Ys = initPnts;
    params.Ps = initVels;
    params.ts = initTimes;
    params.wi = ones(length(params.ts), 1);
    params.cps = (min(params.ts) + max(params.ts))/2;
    params.beta = 0.5;
    params.figId = 1000;   
else 
    params = grParams;
    params.Ys = dataPoints;
    params.ts = subTime;
    params.wi = ones(length(params.ts), 1);
    params.cps = (min(params.ts) + max(params.ts))/2;
    params.figId = 1001;
end

params.useHR = useHR;
params.YsGT = dataGT.YsEst;
params.PsGT = dataGT.PsEst;
params.TsGT = dataGT.TsEst;
params.YsOrg = dataPoints;
params.TsOrg = subTime;
params.IdsOrg = subIds;
[X10, X20, X30, X4s, energy] = adimat_hierarchical_regression( params );

filename = sprintf('%s%d.mat', 'params', useHR);
save(filename, 'params');
