% 
% test longitudinal corpus callosum data
%

clear all 
% close all

filePrefix = '/Users/yihong/Desktop/work/Project/GeodesicAnalysisData/ccLongData';
[ccLM, ages, sigma, gIds, sIds] = loadLongitudinalCCAndSVD( filePrefix );

% test on control data
idCL = find(gIds == 0);

grParams = setRegressParams();
useHR = true;

if useHR 
    [initPnts, initVels, initTimes] = fitIndividualTrend(ccLM(idCL), ages(idCL), sIds(idCL), grParams);
    params = grParams;
    params.Ys = initPnts;
    params.Ps = initVels;
    params.ts = initTimes;
    params.beta = 0.5;
    params.figIdShape = 1000;
else
    params = grParams;
    params.Ys = ccLM(idCL);
    params.ts = ages(idCL);
    params.figIdShape = 1002;
end

params.wi = ones(length(params.ts), 1);
params.cps = (min(params.ts) + max(params.ts))/2;
params.useHR = useHR;
params.sigma = sigma;

[X10, X20, X30, X4s, energy] = adimat_hierarchical_regression( params );

filename = sprintf('%s%d_cc.mat', 'params', useHR);
save(filename, 'params');
