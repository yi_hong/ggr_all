function [energyTotal] = compute_energy_hierarchical( X1, X2, X3, params )

% compute the energy of the initial acceleration, need further checking
energyV = 0;
for iI = 1:length(X3)
    %energyV = energyV + trace( (X3{iI})' * X3{iI} ) * params.h;
    tmp = X3{iI};
    energyV = energyV + sum(tmp(:).^2) * params.h;
end
energyV = energyV * params.alpha;

%X1s = cell(length(params.ts), 1);
%X2s = cell(length(params.ts), 1);
% compute the energy of the data matching (points)
energyS = 0;
for iI = 1:length(params.ts)
	curX1 = X1{params.ts(iI)};
    curX2 = X2{params.ts(iI)};
    
	energyS = energyS + adimat_grarc(curX1, params.Ys{iI}) * params.wi(iI);
    
    if params.useHR
        tmp = adimat_gr_parallel_transport(params.Ys{iI}, params.Ps{iI}, curX1);
        tmp = curX2 - tmp;
        energyS = energyS + sum(tmp(:).^2) * params.beta;
    end
end

% compute the energy of the velocity matching (velocities)
energyS = energyS / params.sigmaSqr;

% compute the total energy
energyTotal = energyV + energyS;

end