% 
% test on facial data
% 
clear all
close all

fileLocation = '/Users/yihong/Desktop/work/Project/longitudinal_data/facial_landmarks/ptsFolder/';
[matLM, vecId, vecAge] = loadFacialLandmarks(fileLocation);
[grassLM, sigma] = projectSVD(matLM);

% count the number of points for each subject
uniqueId = unique(vecId);
numCount = zeros(length(uniqueId), 1);
for iI = 1:length(uniqueId)
    numCount(iI) = length(find(vecId == uniqueId(iI)));
end
figure, bar(numCount);

numThreshold = 6;
deleteIds = uniqueId(find(numCount < numThreshold));
usedLM = grassLM;
usedId = vecId;
usedAge = vecAge;
for iI = 1:length(deleteIds)
    ids = find(usedId == deleteIds(iI));
    usedLM(ids) = [];
    usedId(ids) = [];
    usedAge(ids) = [];
end

% set basic regression parameters
grParams = setRegressParams();


% use hierarchical regression
useHR = true;

if useHR
    % individial trend
    [initPnts, initVels, initTimes] = fitIndividualTrend(usedLM, usedAge, usedId, grParams);

    % group trend
    params = grParams;
    params.Ys = initPnts;
    params.Ps = initVels;
    params.ts = initTimes;
    params.wi = ones(length(params.ts), 1);
    params.cps = (min(params.ts) + max(params.ts))/2;
    params.beta = 0.01;  
else 
    params = grParams;
    params.Ys = usedLM;
    params.ts = usedAge;
    params.wi = ones(length(params.ts), 1);
    params.cps = (min(params.ts) + max(params.ts))/2;
end

params.useHR = useHR;
[X10, X20, X30, X4s, energy, ~, X1s] = adimat_hierarchical_regression( params );

cmap = jet(length(X1s));
figure(1);
for iI = 1:length(X1s)
    plotFace(X1s{iI}*sigma, cmap(iI, :));
    hold on
    xlim([-110, 110]);
    ylim([-110, 110]);
    %drawnow
end
hold off

% filename = sprintf('%s%d.mat', 'params_face_', useHR);
% save(filename, 'params');