% 
% project to the Grassmannian using svd

function [grassLM, sigma] = projectSVD(matLM)

grassLM = cell(length(matLM), 1);
sigma = zeros(size(matLM{1}, 2));

for iI = 1:length(matLM)
    landmarks = matLM{iI};
    landmarks = landmarks - repmat(mean(landmarks, 1), size(landmarks, 1), 1);
    [grassLM{iI}, tmpSigma] = svd(landmarks, 0);
    sigma = sigma + tmpSigma;
end
sigma = sigma ./ length(matLM);

end