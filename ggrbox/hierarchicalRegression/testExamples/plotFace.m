% 
% plot face 
%

function plotFace(landmarks, color)

if nargin < 2
    color = [0, 0, 1];
end

if(size(landmarks, 1) == 68) && (size(landmarks, 2) == 2)
    
    landmarks = fliplr(landmarks);
    landmarks(:, 2) = -landmarks(:, 2);

    plot(landmarks(1:15, 1), landmarks(1:15, 2), 'Color', color);
    hold on
    plot([landmarks(16:21, 1); landmarks(16, 1)], [landmarks(16:21, 2); landmarks(16, 2)], 'Color', color);
    plot([landmarks(22:27, 1); landmarks(22, 1)], [landmarks(22:27, 2); landmarks(22, 2)], 'Color', color);
    plot(landmarks(28:32, 1), landmarks(28:32, 2), 'Color', color);
    plot(landmarks(33:37, 1), landmarks(33:37, 2), 'Color', color);
    plot(landmarks(38:48, 1), landmarks(38:48, 2), 'Color', color);
    plot(landmarks(49:66, 1), landmarks(49:66, 2), 'Color', color);
    plot(landmarks(67:68, 1), landmarks(67:68, 2), 'Color', color);
    hold off
    axis equal 
    
end

end