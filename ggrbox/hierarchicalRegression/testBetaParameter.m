% 
% test beta for multiple times
%

close all
clear all

flagData = 1;

if flagData == 0  % synthetic 
    tmp = load('params041.mat');
    params = tmp.params;
    prefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_HGR/synthetic_data04';
else   % cc 
    tmp = load('params1_cc.mat');
    params = tmp.params;
    prefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_HGR/cc_data';
end

list = [0.0001, 0.001, 0.01, 0.1];
for iI = 1:length(list)
    beta = list(iI);
    fprintf('Beta: %.4f\n', beta);
    params.beta = beta;
    adimat_hierarchical_regression( params );
    if flagData == 0
        figure(params.figId);
    else
        figure(params.figIdShape);
    end
    filename = sprintf('%s_beta%.4f.fig', prefix, beta);
    saveas(gcf, filename);
    filename = sprintf('%s_beta%.4f.png', prefix, beta);
    saveas(gcf, filename);
end