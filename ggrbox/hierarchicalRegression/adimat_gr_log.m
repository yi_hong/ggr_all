function U = adimat_gr_log(X, Y)
% ADIMAT_GR_LOG computes the logarithm map on the Grassmannian
%
% U = ADIMAT_GR_LOG(X, Y) computed the tangent vector at X in the direction of Y.
%
% Adapted from the manopt toolbox.
%
% Modified by Roland Kwitt, 2015

    n = -1; %#ok<NASGU>
    p = -1; %#ok<NASGU>
    u = -1; %#ok<NASGU>
    s = -1; %#ok<NASGU>
    v = -1; %#ok<NASGU>
    
    [n,p] = size(X);
    U = zeros(n, p); %#ok<PREALL>
    
    x = X;
    y = Y;
    ytx = y.'*x;
    At = y.'-ytx*x.';
    Bt = ytx\At;
    % adjusted to work with adimat
    [u,s,v] = svd(Bt');
    
    % reduce size
    u = u(:, 1:p);
    s = diag(s);
    s = s(1:p);
    v = v(:, 1:p);
    
    U = u*diag(atan(s))*v.';
end
