% 
% plot results 
%
function adimat_show_obs_and_curve(timeCurve, X1Curve, X2Curve, timeGT, YsGT, PsGT, timeOrg, YsOrg, IdsOrg)

if length(X1Curve{1}) ~= 2 
    disp('Currently, only support the visualization of 2-D');
    return;
end

% plot the original data
angleOrg = zeros(length(YsOrg), 1);
for iI = 1:length(YsOrg)
    angleOrg(iI) = atan(YsOrg{iI}(2)/YsOrg{iI}(1));
end
uniqueIds = unique(IdsOrg);
cmap = colormap(jet(length(uniqueIds)));
for iJ = 1:length(uniqueIds)
    idTmp = find(IdsOrg == uniqueIds(iJ));
    plot(timeOrg(idTmp), angleOrg(idTmp), 'k+', 'LineWidth', 2, 'Color', cmap(iJ, :));
    hold on;
end

% plot the fitted curve 
angleCurve = zeros(length(X1Curve), 1);
for iI = 1:length(X1Curve)
    angleCurve(iI) = atan(X1Curve{iI}(2)/X1Curve{iI}(1));
end
% plot(timeCurve, angleCurve, '-*b', 'LineWidth', 2);
% hold on;

drawArrow = @(x, y, props) quiver(x(1), y(1), x(2)-x(1), y(2)-y(1), 0, props{:});

% plot the groud truth and the velocity of fitted curve
dt = 0.05;
angleGT = zeros(length(YsGT), 1);
for iI = 1:length(YsGT)
    angleGT(iI) = atan(YsGT{iI}(2)/YsGT{iI}(1));
    
    [~, pos] = min(abs(timeCurve - timeGT(iI)));
    [~, YsTmp, ~] = integrateForwardWithODE45(X1Curve{pos}, X2Curve{pos}, [0 dt/2 dt]);
    angleTmp = atan(YsTmp{end}(2)/YsTmp{end}(1));
    %plot([timeCurve(pos), timeCurve(pos)+dt], [angleCurve(pos), angleTmp], 'b-', 'LineWidth', 2);
    drawArrow([timeCurve(pos), timeCurve(pos)+dt], [angleCurve(pos), angleTmp], ...
        {'MaxHeadSize', 0.8, 'Color', 'b', 'LineWidth', 3});
    hold on
    
    [~, YsTmp, ~] = integrateForwardWithODE45(YsGT{iI}, PsGT{iI}, [0 dt/2 dt]);
    angleTmp = atan(YsTmp{end}(2)/YsTmp{end}(1));
    %plot([timeGT(iI), timeGT(iI)+dt], [angleGT(iI), angleTmp], 'm-', 'LineWidth', 2);
    drawArrow([timeGT(iI), timeGT(iI)+dt], [angleGT(iI), angleTmp], ...
        {'MaxHeadSize', 0.8, 'Color', 'm', 'LineWidth', 3});
end
plot(timeGT, angleGT, 'mo', 'LineWidth', 2);
hold off

end