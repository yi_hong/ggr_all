%
% linesearch using adimat 
% 
function [curY, currentEnergy] = adimat_hierarchical_linesearch(Y0, nSizeX, opts_all)

params = opts_all.params;

curY = Y0;
currentEnergy = adimat_compute_hierarchical_energy(curY(:), params);
fprintf('Energy = %f\n', currentEnergy);
stopIteration = false;

for iI = 1:params.nIterMax
    % use adimat to compute gradiate
    [~, g] = adimat_hierarchical_driver(curY, opts_all);
    [gradX10, gradX20, gradX30, gradX4s] = adimat_hierarchical_vector2Mat(g, nSizeX);
    
    % time step size
    dt = params.deltaT;
    energyReduced = false;
    
    % store current status
    [curX10, curX20, curX30, curX4s] = adimat_hierarchical_vector2Mat(curY, nSizeX);
    gradX10 = -gradX10;
    gradX10 = gradX10 - curX10 * (curX10' * gradX10);
    
    for iJ = 1:params.maxReductionSteps
        [~, Ytmp] = integrateForwardWithODE45(curX10, gradX10, [0 dt/2 dt]);
        X1Opt = Ytmp{end};
        
        X2Opt = curX20 - dt * gradX20;
        X2Opt = X2Opt - curX10 * (curX10' * X2Opt);
        X2Opt = ptEdelman(curX10, gradX10, X2Opt, dt);
        X2Opt = X2Opt - X1Opt * (X1Opt' * X2Opt);
        
        X3Opt = curX30 - dt * gradX30;
        X3Opt = X3Opt - curX10 * (curX10' * X3Opt);
        X3Opt = ptEdelman(curX10, gradX10, X3Opt, dt);
        X3Opt = X3Opt - X1Opt * (X1Opt' * X3Opt);
        
        X4sOpt = cell(size(curX4s));
        for iK = 1:length(curX4s)
            X4sOpt{iK} = curX4s{iK} - dt * gradX4s{iK};
        end
        
        tmpY = [X1Opt; X2Opt; X3Opt];
        for iK = 1:length(X4sOpt)
            tmpY = [tmpY; X4sOpt{iK}];
        end
        energy = adimat_compute_hierarchical_energy(tmpY(:), params);
        
        if energy >= currentEnergy
            fprintf('#');
        else
            energyReduced = true;
            if currentEnergy - energy < params.stopThreshold
                stopIteration = true;
            end
            currentEnergy = energy;
            curY = tmpY(:);
            fprintf('Iter %04d: Energy = %f\n', iI, currentEnergy);
            break;
        end
        dt = dt * params.rho;
    end
    
    if(~energyReduced)
        fprintf('Iteration %d: Could not reduce energy further. Quitting.\n', iI);
        tmpY = [curX10; curX20; curX30];
        for iK = 1:length(curX4s)
            tmpY = [tmpY; curX4s{iK}];
        end
        curY = tmpY(:);
        break;
    end
    
    if stopIteration
        fprintf('Quitting because the decresed energy is smaller than the threshold.\n');
        break;
    end 
end

end

