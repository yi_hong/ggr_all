function energy = adimat_compute_hierarchical_energy( Y, params )
% compute the energy for the group 
    
% % get the size for X1...
% nLen = size(params.Ys{1}, 1);
% X10 = Y(1:nLen, :);
% X20 = Y(nLen+1:2*nLen, :);
% X30 = Y(2*nLen+1:3*nLen, :);
% %X4s = cell(params.nPieces, 1);
% X4s = {};
% for iI = 1:params.nPieces
%     X4s{iI} = Y((2+iI)*nLen+1:(3+iI)*nLen, :);
% end

[X10, X20, X30, X4s] = adimat_hierarchical_vector2Mat(Y, size(params.Ys{1}));

% project X10
% tmp = X10;
[U, S, V] = svd(X10);
S = eye(size(S));
X10 = U*S*V';
X20 = X20 - X10 * (X10' * X20);
X30 = X30 - X10 * (X10' * X30);

% integrate forward
[X1, X2, X3] = adimat_integrate_forward_each_interval_spline( X10, X20, X30, X4s, params );

% compute energy
[energy] = compute_energy_hierarchical( X1, X2, X3, params );
% tmp = tmp - X10;
% energy = energy + sum(tmp(:).^2);

end