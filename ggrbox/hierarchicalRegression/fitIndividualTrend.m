function [initPnts, initVels, initTimes] = fitIndividualTrend(dataPoints, time, subIds, grParams)

subUniqueIds = unique(subIds);
nSub = length(subUniqueIds);

initPnts = cell(nSub, 1);
initVels = cell(nSub, 1);
initTimes = zeros(nSub, 1);

%matlabpool(4);
for iI = 1:nSub
    disp(iI);
    curSubIds = find(subIds == subUniqueIds(iI));
    params = grParams;
    params.Ys = dataPoints(curSubIds);
    params.ts = time(curSubIds);
    params.wi = ones(length(params.ts), 1);
    params.pntY0 = params.Ys{1};
    params.tanY0 = zeros(size(params.pntY0));
    params.pntT0 = min(params.ts);
    params.nsize = size(params.Ys{1});
    [X10, X20, ~] = fullRegressionOnGrassmannManifold(params);
    X20 = X20 ./ (max(params.ts) - min(params.ts));
    [Ts, Ys, Ps] = integrateForwardWithODE45(X10, X20, ...
        [min(params.ts) (min(params.ts)+max(params.ts))/2 max(params.ts)]);
    initPnts{iI} = Ys{2};
    initVels{iI} = Ps{2};
    initTimes(iI) = Ts(2);
end

end