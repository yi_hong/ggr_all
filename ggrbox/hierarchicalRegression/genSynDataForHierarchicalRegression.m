%
% generate nSub subjects, each subject has nPnt points
% 

function [dataPoints, subTime, subIds, dataGT] = genSynDataForHierarchicalRegression(nSub, nPnt, sigma)

if exist('synDataHGR.mat', 'file')
    tmp = load('synDataHGR.mat');
    dataPoints = tmp.dataPoints;
    subTime = tmp.subTime;
    subIds = tmp.subIds;
    dataGT = tmp.dataGT;
    if length(unique(subIds)) == nSub && length(find(subIds == subIds(1))) == nPnt
        return;
    end
end

% generate points along a curve
N = nSub;
% time = linspace(0, 1, N);
t1 = linspace(0, 0.25, round(N/5));
t3 = linspace(0.75, 1, round(N/5));
t2 = linspace(0.25, 0.75, N - round(N/5)*2 + 4);
time = [t1(2:end-1) t2 t3(2:end-1)];
angle = sin(time*3*pi + 0*pi)*0.15;
Ys = cell(N, 1);
for iI = 1:N
    Ys{iI} = [cos(angle(iI)); sin(angle(iI))];
    [Ys{iI}, ~] = qr(Ys{iI}, 0);
    YsGaussNoise{iI} = generateGaussianSamples(Ys{iI}, sigma*0.05);
end
figure(101), subplot(1, 3, 1), plot2DGrass(Ys, YsGaussNoise, time);

% fit the points
grParams = setRegressParams();
grParams.Ys = YsGaussNoise;
grParams.ts = time;
grParams.wi = ones(length(grParams.ts), 1);
grParams.pntY0 = YsGaussNoise{1};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(time);
grParams.nsize = size(YsGaussNoise{1});
grParams.cps = (min(time) + max(time))/2;
grParams.minFunc = 'both';
grParams.nIterMax = 200;
[X10, X20, X30, X40s, ~] = cubicSplineRegression(grParams);
dataGT.X10 = X10;
dataGT.X20 = X20;
dataGT.X30 = X30;
dataGT.X40s = X40s;

% genreate points along the regressed curve
pnt{1} = X10;
vel = {X20, X30};
vel = [vel, X40s];
tPos = [min(time) grParams.cps max(time)];
[TsEst, YsEst, PsEst] = computePointsAlongGeodesicSpline(pnt, vel, ...
    tPos, time, grParams.nt);
figure(101), subplot(1, 3, 2), plot2DGrass(YsEst, YsGaussNoise, time);

dataGT.TsEst = TsEst;
dataGT.YsEst = YsEst;
dataGT.PsEst = PsEst;

% add some noise
YsEstNoise = cell(length(YsEst), 1);
PsEstNoise = cell(length(YsEst), 1);
for iI = 1:length(YsEst)
    YsEstNoise{iI} = generateGaussianSamples(YsEst{iI}, sigma);
    PsEstNoise{iI} = adimat_gr_parallel_transport(YsEst{iI}, PsEst{iI}, YsEstNoise{iI});
end
figure(101), subplot(1, 3, 3), plot2DGrass(YsEst, YsEstNoise, TsEst);

% generate individual subjects 
dataPoints = []; 
subTime = [];
subIds = [];
figure, hold on;
for iI = 1:length(YsEstNoise)
    rng('shuffle');
    timesTmp = 2;
    if iI < 5 || iI > length(YsEstNoise)-5
        timesTmp = 1;
    end
    dt = 0.05 + rand(1) * 0.05 * timesTmp;
    tSpan = linspace(TsEst(iI), TsEst(iI)-dt, 3);
    [~, YsTmp, PsTmp] = integrateForwardWithODE45(YsEstNoise{iI}, PsEstNoise{iI}, tSpan);
    tSpan = linspace(TsEst(iI)-dt, TsEst(iI)+dt, nPnt);
    [~, YsSub, ~] = integrateForwardWithODE45(YsTmp{end}, PsTmp{end}, tSpan);
    YsSubNoise = cell(length(YsSub), 1);
    for iJ = 1:length(YsSub)
        YsSubNoise{iJ} = generateGaussianSamples(YsSub{iJ}, sigma*0.2);
    end
    dataPoints = [dataPoints; YsSubNoise];
    subTime = [subTime; tSpan'];
    subIds = [subIds; ones(length(tSpan), 1) * iI];
    plot2DGrass(YsSub, YsSubNoise, tSpan);
    hold on;
end
plot2DGrass(YsEst, YsEstNoise, TsEst);
hold off

% save data
save('synDataHGR.mat', 'dataPoints', 'subTime', 'subIds', 'dataGT');

end