%
% load the landmarks for corpus callosum
% apply svd on the landmarks to convert them to Grassmannian
%

function [omas, ages, sigma, gIds, sIds] = loadLongitudinalCCAndSVD( filePrefix )

    [landmarks, ages, gIds, sIds] = readLandmarksAndAges( filePrefix );
    
    omas = cell( length(landmarks), 1 );
    sigma = zeros(size(landmarks{1}, 2), size(landmarks{1}, 2));
    for iI = 1:length(landmarks)
        landmarks{iI} = landmarks{iI} - repmat( mean(landmarks{iI}, 1), size(landmarks{iI}, 1), 1 );
        [omas{iI}, tmp, ~] = svd( landmarks{iI}, 0 );
        sigma = sigma + tmp;
    end
    sigma = sigma ./ length(landmarks);
    
    % find the real age
    infoFilename = sprintf('%s/oasis_longitudinal_used.xls', filePrefix);
    [info] = xlsread(infoFilename);

    assert( sum(ages - info(:, 1)) == 0 );
%     realAge = ages / 365;
%     idUnique = unique(sIds);
%     for iI = 1:length(idUnique)
%         curSubjectId = find( sIds == idUnique(iI) );
%         baseAgeId = find( ages(curSubjectId) == 0 );
%         baseAge = info(curSubjectId(baseAgeId), 4);
%         realAge(curSubjectId) = realAge(curSubjectId) + baseAge;
%     end
%     min(realAge - info(:, 4))
%     max(realAge - info(:, 4))
%     ages = realAge;
    ages = info(:, 4);

    function [landmarks, ages, gIds, sIds] = readLandmarksAndAges( filePrefix )
        
        orderId = [1,64,32,48,16,56,24,40,8,60,28,44,12,52,20,36,4,62,30,46,14,54,22,38,6,58,26,42, ...
                   10,50,18,34,2,63,31,47,15,55,23,39,7,59,27,43,11,51,19,35,3,61,29,45,13,53,21,37, ...
                   5,57,25,41,9,49,17,33];
        
        % the age for each corpus
        filename = sprintf('%s/ccLongDataParams.txt', filePrefix );
        [gIds, sIds, ages, ccFilenames] = textread(filename, '%d %d %d %s');
        
        landmarks = cell(length(ccFilenames), 1);
        for iJ = 1:length(ccFilenames)
            nameTmp = sprintf('%s/%s', filePrefix, ccFilenames{iJ});
            [x, y] = textRead(nameTmp, '%f %f');
            landmarks{iJ} = [x(orderId) y(orderId)];
        end
    
    end

end