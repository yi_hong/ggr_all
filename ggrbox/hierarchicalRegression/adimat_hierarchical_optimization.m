function [X1Opt, X2Opt, X3Opt, X4sOpt, energy] = adimat_hierarchical_optimization( params )

[X10, X20, X30, X4s] = initStates();

Y0 = [X10; X20; X30];
for iI = 1:length(X4s)
    Y0 = [Y0; X4s{iI}];
end
[X1, X2] = adimat_integrate_forward_each_interval_spline( X10, X20, X30, X4s, params );
%figure(params.figId), adimat_show_obs_and_curve(params.ts * params.h, params.Ys, (0:params.h:1)', X1);

if isfield(params, 'figId')
    t0 = params.ts_pre(1);
    s0 = params.ts_pre(end) - params.ts_pre(1);
    figure(params.figId), adimat_show_obs_and_curve((0:params.h:1)' * s0 + t0, X1, X2, ...
        params.TsGT, params.YsGT, params.PsGT, params.TsOrg, params.YsOrg, params.IdsOrg);
    drawnow;
end

opt_all.params = params;
f_val = adimat_compute_hierarchical_energy(Y0, params);
opt_all.optsAdimat = admOptions('functionResults', {f_val}, 'i', 1);

opt_opts = adimat_load_optimizer_options();

% fmincon
lb = ones(size(Y0)) * (-1e10);
ub = ones(size(Y0)) * 1e10;
nSizeX = size(params.Ys{1});
opt_opts.fmincon = optimset( opt_opts.fmincon, 'OutputFcn', @outfun );
opt_opts.fmincon = optimset( opt_opts.fmincon, 'MaxIter', 100 );
%opt_opts.fmincon = optimset( opt_opts.fmincon, 'MaxFunEvals', 5000 );

% alternating linesearch and fmincon
% for iI = 1:params.nIterBoth
    
    % use the linear search to do the initialization for fmincon
%     opt_all.gX1Ctl = 1;
%     opt_all.params.nIterMax = 10;
%     [Y, energy] = adimat_hierarchical_linesearch(Y0, nSizeX, opt_all);
%     Y0 = Y;

    opt_all.gX1Ctl = 1;
    [Y, energy] = fmincon( @(Y)adimat_hierarchical_driver(Y, opt_all), Y0(:), ...
        [], [], [], [], lb, ub, @mycon, opt_opts.fmincon );
%     Y0 = Y;

% end

% fminunc
% opt_opts.fminunc = optimset( opt_opts.fminunc, 'OutputFcn', @outfun );
% opt_opts.fminunc = optimset( opt_opts.fminunc, 'MaxIter', 100 );
% [Y, energy] = fminunc( @(Y)adimat_hierarchical_driver(Y, opt_all), Y0(:), opt_opts.fminunc ); 

%nSizeX = size(X10);
Y = reshape(Y, [], nSizeX(2));
X1Opt = Y(1:nSizeX(1), :);
X2Opt = Y(nSizeX(1)+1:2*nSizeX(1), :);
X3Opt = Y(2*nSizeX(1)+1:3*nSizeX(1), :);
for iI = 1:length(X4s)
    X4sOpt{iI} = Y((iI+2)*nSizeX(1)+1:(iI+3)*nSizeX(1), :);
end

    function [X10, X20, X30, X4s] = initStates()
        if isfield(params, 'X10Init')
            X10 = params.X10Init;
        else
            X10 = params.Ys{1};
        end

        if isfield(params, 'X20Init')
            X20 = params.X20Init;
        else
            X20 = zeros(size(X10));
        end

        if isfield(params, 'X30Init')
            X30 = params.X30Init;
        else
            X30 = zeros(size(X10));
        end

        if isfield(params, 'X4sInit')
            X4s = params.X4sInit;
        else
            X4s = cell(length(params.cps)+1, 1);
            for i = 1:length(X4s)
                X4s{i} = zeros(size(X10));
            end
        end
    end

    function [c, ceq] = mycon(Y)
        
        YMat = reshape(Y, [], nSizeX(2));
        X1Tmp = YMat(1:nSizeX(1), :);
        X2Tmp = YMat(nSizeX(1)+1:2*nSizeX(1), :);
        X3Tmp = YMat(2*nSizeX(1)+1:3*nSizeX(1), :);
        
        tmp1 = X1Tmp' * X1Tmp - eye(nSizeX(2));
        tmp2 = X1Tmp' * X2Tmp;
        tmp3 = X1Tmp' * X3Tmp;
        
        tmp = [tmp1; tmp2; tmp3];
        
        c = [];
        ceq = tmp(:);
        
    end

    function dummy = outfun(Y, optimValues, state)
        dummy = 0;
        switch state
            case 'init'
                %hold on;
            case 'iter'
                nSizeXtmp = size(X10);
                X10tmp = Y(1:nSizeXtmp(1), :);
                X20tmp = Y(nSizeXtmp(1)+1:2*nSizeXtmp(1), :);
                X30tmp = Y(2*nSizeXtmp(1)+1:3*nSizeXtmp(1), :);
                for i = 1:length(X4s)
                    X4stmp{i} = Y((i+2)*nSizeXtmp(1)+1:(i+3)*nSizeXtmp(1), :);
                end
                
                if isfield(params, 'figId')
                    [X1Curve, X2Curve] = adimat_integrate_forward_each_interval_spline( X10tmp, X20tmp, X30tmp, X4stmp, params );
                    t0 = params.ts_pre(1);
                    s0 = params.ts_pre(end) - params.ts_pre(1);
                    figure(params.figId), adimat_show_obs_and_curve((0:params.h:1)' * s0 + t0, X1Curve, X2Curve, ...
                        params.TsGT, params.YsGT, params.PsGT, params.TsOrg, params.YsOrg, params.IdsOrg);
                    drawnow;
                end
                %disp([X10tmp' * X10tmp X10tmp' * X20tmp X10tmp' * X30tmp]);
            case 'done'
            otherwise
        end
    end

end