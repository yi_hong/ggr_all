%----------------------------------------------------------------------
% Test time warping method on synthetic data
% 
% Firstly, fit a set of data points, using the results as the ground truth
% Secondly, generate new data points with noise for fitting
% Lastly, compare the fitting results with the ground truth
% 
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/29/2014
%----------------------------------------------------------------------

close all;

%profile -memory on

% Generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10)
nTotalNum = 50;
minBoundary = 0.2;       % the minimum frequency
maxBoundary = 9.8;      % the maximum frequency
f = linspace(minBoundary, maxBoundary, nTotalNum);

% use the logistic function to change the f
twParams.A = 0.0;
twParams.K = 1.0;
twParams.beta = 1.0;
twParams.m = 1.0;
twParams.k = 3.0;
twParams.M = 4.0;
fOrg = f;
fWarped = generalisedLogisticFunction( fOrg, twParams );
figure, plot( fOrg, fWarped, 'r*-' );

% amplify f 
f = (fWarped+0.5)*5;

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% system identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    s = [sin(f(i)*x); cos(f(i)*x)];
    org{i} = s;
    y =  CC*s;
    y = y + 0.01*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weighted_suboptimal_systemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end

% plot the ground-truth
figIdGT = 500;
figure(figIdGT), plotGeodesic( oma, fOrg', pDim, '*' );
title('Ground truth');
xlabel('Real time points');
ylabel('Eigenvalue of A matrix');


%% regression
grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.h = 1.0 / grParams.nt;   % step size
grParams.deltaT = 0.01;           % the step size for updating the initial conditions
grParams.deltaTWarp = 0.01;
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 300;          % the number of iterations for updating the initial conditions
grParams.nIterMaxTimeWarp = 100;
grParams.stopThreshold = 1e-7;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'linesearch';  % use linesearch for updating the initial conditions, other methods could be implemented later
grParams.twMinFunc = 'fmincon';
grParams.useRansac = false;       % use ransac when the size of the training dataset is large
grParams.nSys = length(oma);      % the number of points on the Grassmann manifold

grParams.ts = fOrg';
grParams.Ys = oma;
grParams.wi = ones(length(fOrg), 1);
grParams.t_truth = fWarped;
grParams.Y_truth = grParams.Ys;
grParams.nsize = size(grParams.Ys{1});
grParams.pDim = pDim;

grParams.figIdFitting = 600;
grParams.figIdLogisticFunc = 700;

figure(grParams.figIdLogisticFunc), hold on;
plot( grParams.ts, generalisedLogisticFunction(grParams.ts, twParams), 'c*-', 'LineWidth', 2);
plot( twParams.M*[1 1], [0 generalisedLogisticFunction( twParams.M, twParams ) ], 'c--', 'LineWidth', 2 );
hold off

% perform the time warped regression
[X1Opt, X2Opt, energy, tsWarped, timeWarpParams] = timeWarpedRegressionOnGrassmannManifold( grParams );

% generate data using the results
grParamsFit = grParams;
grParamsFit.ts = inverseLogisticFunction( tsWarped, timeWarpParams );
[~, grParamsFit.Ys] = integrateForwardWithODE45( X1Opt, X2Opt, tsWarped );
grParamsFit.wi = ones(length(grParamsFit.ts), 1);
figure(figIdGT), plotGeodesic( grParamsFit.Ys, grParamsFit.ts, grParams.pDim, '--' );

grParamsFit.t_truth_org = grParamsFit.ts;
grParamsFit.t_truth = tsWarped;
grParamsFit.Y_truth = grParamsFit.Ys;
% add some noise
grParamsFit.Ys = addNoiseToData( grParamsFit.Ys, 0.03 );
grParamsFit.Y_noise = grParamsFit.Ys;
[X1Fit, X2Fit, energyFit, tsWarpedFit, timeWarpParamsFit] = timeWarpedRegressionOnGrassmannManifold( grParamsFit );

[~, YsFit] = integrateForwardWithODE45( X1Fit, X2Fit, tsWarpedFit );

X1Dist = sqrt(grarc(X1Opt, X1Fit));
X1Dist 

X2Dist = norm(X2Opt - X2Fit, 'fro') / norm(X2Opt, 'fro');
X2Dist 

timeWarpParamsDiff = addTwoParams(timeWarpParams, timeWarpParamsFit, -1);
timeWarpParamsDiff

abs(timeWarpParamsFit.k - timeWarpParams.k) / timeWarpParams.k
abs(timeWarpParamsFit.M - timeWarpParams.M) / timeWarpParams.M

meanEnergyFit = energyFit / length(grParamsFit.Ys);
meanEnergyFit

energyOpt = 0;
for iI = 1:length(grParamsFit.Y_truth)
    energyOpt = energyOpt + grarc(grParamsFit.Ys{iI}, grParamsFit.Y_truth{iI});
end
meanEnergyOpt = energyOpt / length(grParamsFit.Y_truth);
meanEnergyOpt

energyErr = 0;
for iI = 1:length(grParamsFit.Y_truth)
    energyErr = energyErr + grarc(YsFit{iI}, grParamsFit.Y_truth{iI});
end
meanEnergyErr = energyErr / length(grParamsFit.Y_truth);
meanEnergyErr
