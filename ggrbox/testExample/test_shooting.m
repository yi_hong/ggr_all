%----------------------------------------------------------------------
%  Test shooting on Grassmannian manifold
%
%  Author: Yi Hong
%  Date: 08-30-2013
%----------------------------------------------------------------------

close all;

dt.A = [0 2; -1 0];
dt.C = eye(2);
Y0 = omat(dt, 2);
dt.A = [0 5; -16 0];
dt.C = eye(2);
Y1 = omat(dt, 2);

% set params 
params.Y0 = Y0;
params.Y1 = Y1;
params.nt = 100;
params.h = 1./params.nt;
params.sigmaSqr = 1;

% do shooting
[tanY] = shootingOnGrassmannManifold( params );

% move forward based on initial conditions
[X1, X2] = integrateForward( params.Y0, tanY, params.nt, params.h );

% plot the results
figure, hold on;
plot( reshape( params.Y0, [], 1 ), 'b', 'LineWidth', 2 );
plot( reshape( params.Y1, [], 1 ), 'r', 'LineWidth', 2 );
cmap = cool(length(X1));
for iI = 1:length(X1)
    plot( reshape( X1{iI}, [], 1 ), '--', 'Color', cmap(iI, :) );
end
hold off
colormap(cmap);
colorbar

