function test_ode45()
close all
clear all

setup();

testNum = 300;
n = 100;
p = 10;
times = zeros(testNum, 3);
matSz = zeros(testNum, 1);
for iI = 1:testNum
    disp(iI);
    [times(iI, 1), times(iI, 2), times(iI, 3)] = integrateForward(n*iI, p);
    matSz(iI) = n*iI*p;
end
figure, hold on;
plot( matSz, times(:, 1), 'r-', 'LineWidth', 2 );
plot( matSz, times(:, 2), 'b-.', 'LineWidth', 2 );
plot( matSz, times(:, 3), 'g--', 'LineWidth', 2 );
legend('Forward', 'Backward', 'Backward(-v)');
xlabel('Matrix size');
ylabel('Time (sec)');
saveas(gca, 'test_ode45_time.png');

    function [t1, t2, t3] = integrateForward(n, p)

        A = rand(n, p);
        B = rand(n, p);

        sys1.C = qr(A);
        sys1.A = rand(p, p);
        oma1 = omat(sys1, p);

        sys2.C = qr(B);
        sys2.A = rand(p, p);
        oma2 = omat(sys2, p);

        [~, ~, ~, Ydot] = grgeo(oma1, oma2, 1, 'v3', 'v2');
        t = cputime;
        integrateForwardWithODE45(oma1, Ydot, [0 0.5 1.0]);
        t1 = cputime-t;

        t = cputime;
        integrateForwardWithODE45(oma1, Ydot, [0 -0.5 -1.0]);
        t2 = cputime-t;

        t = cputime;
        integrateForwardWithODE45(oma1, -Ydot, [0 0.5 1.0]);
        t3 = cputime-t;

    end
end