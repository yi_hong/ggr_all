%-------------------------------------------------------------------------- 
% Test spline regression using synthetic data
% --
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 08/25/2014
%-------------------------------------------------------------------------- 

close all;

%% generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10)
nTotalNum = 50;
minBoundary = 0.2;
maxBoundary = 9.8;
%t = [minBoundary:0.5:4 4.1:0.1:6 6.05:0.5:maxBoundary];
t = linspace(minBoundary, maxBoundary, nTotalNum);
f = 3*(sin(t/0.6)+1.5);

% use the logistic function to change the f
%{
twParams.A = 0.0;
twParams.K = 1.0;
twParams.beta = 1.0;
twParams.m = 1.0;
twParams.k = 5.0;
twParams.M = 5.0;
fOrg = t;
f = generalisedLogisticFunction( fOrg, twParams );
f = 5*(f+0.5);
figure(601), plot( fOrg, f, 'r*-' );
%}

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% system identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    s = [sin(f(i)*x); cos(f(i)*x)];
    org{i} = s;
    y =  CC*s;
    y = y + 0.01*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weighted_suboptimal_systemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end
% plot the generate data
plotGeodesic( oma, t, pDim, '-*' );
Ypnt = oma;
Tpnt = t;

%% perform spline regression
grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0.000;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.h = 1./grParams.nt;
grParams.deltaT = 0.02;           % the step size for updating the initial conditions
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 100;          % the number of iterations for updating the initial conditions
grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'fmincon';  % linesearch, fmincon, both
grParams.maxIterFmincon = 10000;

grParams.ts = Tpnt;
grParams.Ys = Ypnt;
grParams.wi = ones(length(t), 1);
%grParams.cps = [1.7 3.4 5.1 6.8 8.5];
grParams.cps = [2 4 6 8];
%grParams.cps = [3 4:0.5:7 8];
%grParams.cps = linspace(1, 9, 20);
%grParams.cps = [2.5 5 7.5];
%grParams.cps = [4 6];
%grParams.cps = [5];
%grParams.cps = [(grParams.ts(1)+grParams.ts(end))/2];     % control points
grParams.t_truth = Tpnt;
grParams.Y_truth = Ypnt;
grParams.pDim = pDim;
grParams.X10GT = Ypnt{1};

grParams.fidSplineEnergy = 500;
grParams.fidSplineFitting = 100;
grParams.estimateInitialValueSpline = false;
[X1Opt, X2Opt, X3Opt, X4sOpt] = cubicSplineRegression(grParams);

% check the constraints
X1Opt' * X1Opt
X1Opt' * X2Opt
X1Opt' * X3Opt

%% use the above result to generate data with ground truth
cpsTmp = (grParams.cps - min(grParams.ts))/(max(grParams.ts)-min(grParams.ts));
cpsTmp = min( max( round(cpsTmp / grParams.h)+1, 1), grParams.nt+1 );
tmp = [1 cpsTmp grParams.nt+1];
paramsTmp.Gns = tmp(2:end) - tmp(1:end-1);
paramsTmp.h = grParams.h;
[X1, X2, X3, X4, ~] = integrateForwardEachIntervalSpline( X1Opt, X2Opt, X3Opt, X4sOpt, paramsTmp );
for iI = 1:length(X1)
    if iI == 1
        X1s = X1{iI}(1:end);
    else
        X1s = [X1s X1{iI}(2:end)];
    end
end
grParamsGT = grParams;
grParamsGT.ts = grParams.ts;
tsIdTmp = (grParamsGT.ts - min(grParams.ts))/(max(grParams.ts)-min(grParams.ts));
tsIdTmp = min( max( round(tsIdTmp / grParams.h)+1, 1), grParams.nt+1 );
grParamsGT.Ys = X1s(tsIdTmp);
grParamsGT.t_truth = grParamsGT.ts;
grParamsGT.Y_truth = grParamsGT.Ys;
grParamsGT.Ys = addNoiseToData(grParamsGT.Ys, 0.03);
grParamsGT.Y_noise = grParamsGT.Ys;
grParamsGT.minFunc = 'both';  % linesearch, fmincon, both
grParamsGT.nIterBoth = 200;
grParamsGT.maxIterFmincon = 100;

figure, plotGeodesic( grParamsGT.Y_truth, grParamsGT.t_truth, pDim, '-' );
hold on
plotGeodesic( grParamsGT.Ys, grParamsGT.ts, pDim, 'o');
hold off

grParams.estimateInitialValueSpline = false;
[X1Est, X2Est, X3Est, X4sEst, energyEst] = cubicSplineRegression(grParamsGT);
energyEst

energyOpt = 0;
for iI = 1:length(grParamsGT.Ys)
    energyOpt = energyOpt + grarc(grParamsGT.Ys{iI}, grParamsGT.Y_truth{iI});
end
energyOpt

% compute the points using estimate points
[X1_e, ~, ~, ~, ~] = integrateForwardEachIntervalSpline( X1Est, X2Est, X3Est, X4sEst, paramsTmp );
for iI = 1:length(X1_e)
    if iI == 1
        X1s_e = X1_e{iI}(1:end);
    else
        X1s_e = [X1s_e X1_e{iI}(2:end)];
    end
end
X1s_est = X1s_e(tsIdTmp);

% compute distance between two geodesic
energyErr = 0;
for iI = 1:length(X1s_est)
    energyErr = energyErr + grarc(X1s_est{iI}, grParamsGT.Y_truth{iI});
end
meanSqrDist = energyErr/length(X1s_est);
meanSqrDist

X1Dist = sqrt(grarc(X1Est, X1Opt));
X1Dist

X2Dist = norm(X2Est-X2Opt, 'fro') / norm(X2Opt, 'fro');
X2Dist 

X3Dist = norm(X3Est-X3Opt, 'fro') / norm(X3Opt, 'fro');
X3Dist 

errX4s = zeros(length(X4sEst), 1);
for iI = 1:length(X4sEst)
    errX4s(iI) = norm(X4sEst{iI} - X4sOpt{iI}, 'fro') / norm(X4sOpt{iI}, 'fro');
end
X4MeanDist = mean(errX4s);
X4MeanDist

% compute R2
initPnt{1}{1} = X1Opt;
initVel{1}{1} = X2Opt; 
initVel{1}{2} = X3Opt;
initVel{1}(3:2+length(X4sOpt)) = X4sOpt;
tPos{1} = [min(grParamsGT.ts) grParamsGT.cps max(grParamsGT.ts)];

initPnt{2}{1} = X1Est;
initVel{2}{1} = X2Est; 
initVel{2}{2} = X3Est;
initVel{2}(3:2+length(X4sEst)) = X4sEst;
tPos{2} = [min(grParamsGT.ts) grParamsGT.cps max(grParamsGT.ts)];

paramsTmp.regressMethod{1} = 'CubicSplineRegression';
paramsTmp.regressMethod{2} = 'CubicSplineRegression';
paramsTmp.nSampling = 1:length(grParamsGT.Y_truth);
paramsTmp.nt = grParamsGT.nt;
R2 = computeR2Statistic( initPnt, initVel, tPos, [], grParamsGT.Ys, grParamsGT.ts, paramsTmp );
R2