function test_spline_GGR_and_Su12a_on_synthetic_data()

    %----------------------------------------------------------------------
    % Configure toy data generation    
    %----------------------------------------------------------------------
    fNoise = [0.1 0.2 0.3 0.4];
    outDir = '/tmp';
    for noiseI = 1:length(fNoise)
    
        clear discreteParams;
        clear toyParams;
        clear grParams;
        
        toyParams.seed = 1234;
        toyParams.nTotalNum = 20;    
        toyParams.fNoise = fNoise(noiseI);
        toyParams.sNoise = 0.1;
        toyData = gen_spline_synthetic_data( toyParams );

        %----------------------------------------------------------------------
        % Configure Hong et al. spline fitting   (GGR) 
        %----------------------------------------------------------------------
        
        grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
        grParams.alpha = 0.000;           % alpha for balancing the prior knowledge of the slope of the geodesic
        grParams.nt = 100;                % the number of the discretized grids
        grParams.deltaT = 0.02;           % the step size for updating the initial conditions
        grParams.maxReductionSteps = 10;  % the number of times for tracing back
        grParams.rho = 0.5;               % the step size for tracing back
        grParams.nIterMax = 100;          % the number of iterations for updating the initial conditions
        grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
        grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
        grParams.minFunc = 'fmincon';     % linesearch, fmincon, both
        grParams.maxIterFmincon = 10000;
        grParams.h = 1./grParams.nt;
        
        grParams.ts = toyData.tt;
        grParams.Ys = toyData.Y_c;
        grParams.wi = ones(length(toyData.tt), 1);
        grParams.cps = 5;
        grParams.t_truth = toyData.tt;
        grParams.Y_truth = toyData.Y_c;
        grParams.pDim = toyData.pDim;
        grParams.X10GT = toyData.Y_c{1};
        
        grParams.fidSplineEnergy = 500;
        grParams.fidSplineFitting = 100;
        grParams.estimateInitialValueSpline = false;
        [X1Opt, X2Opt, X3Opt, X4sOpt, ~, grParams] = cubicSplineRegression(grParams);
        [X1tmp, ~, X3tmp, ~, ~] = integrateForwardEachIntervalSpline( X1Opt, X2Opt, X3Opt, X4sOpt, grParams );
        [~, Su12aCurve] = computeEnergySpline( X1tmp, X3tmp, grParams );
        close all;
      
        %----------------------------------------------------------------------
        % Configure Su et al. discrete spline fitting   
        %----------------------------------------------------------------------
        
        discreteParams.tau = 1:toyParams.nTotalNum;  % Sample at 20 locations
        discreteParams.lambda1 = 0.5;                % Data weighting
        discreteParams.lambda2 = 2;                  % Smoothness weighting
        discreteParams.n = size(toyData.Y_c{1},1);   % R^n
        discreteParams.p = size(toyData.Y_c{1},2);   % Subspace dim
        discreteParams.P = zeros(discreteParams.n,discreteParams.p, ...
            toyParams.nTotalNum);
        for i=1:toyParams.nTotalNum; discreteParams.P(:,:,i) = toyData.Y_c{i};end;
        
        % Generate initial curve (between P(1) and P(2))
        tmpTv = adimat_gr_log(discreteParams.P(:,:,1),discreteParams.P(:,:,2));
        tmpTime = linspace(0,1,toyParams.nTotalNum);
        initialCurve = zeros(discreteParams.n,discreteParams.p,toyParams.nTotalNum);
        for i=1:toyParams.nTotalNum
            initialCurve(:,:,i) = ...
                adimat_gr_exp(discreteParams.P(:,:,1),tmpTime(i)*tmpTv);
        end
        
        % Setup optimization
        x0 = initialCurve(:);
        opt_opts = adimat_load_optimizer_options();
        f_val = adimat_compute_regression_energy( x0, discreteParams );
        opts.optsAdimat = admOptions( 'functionResults', {f_val}, 'i', 1 );
        
        % Setup optimization
        opt_opts.fminunc = optimset( opt_opts.fminunc, 'OutputFcn', @outfun);
        opt_opts.fminunc = optimset( opt_opts.fminunc, 'MaxIter', 300);
        discreteParams.colors = parula(opt_opts.fminunc.MaxIter+2);
        opts.params = discreteParams;
        
        % Optimize
        Hong15aCurve = fminunc( @( x )adimat_driver( x, opts ), ...
            x0, opt_opts.fminunc );
        close all;
        
        %----------------------------------------------------------------------
        % Data preparation for outputting
        %----------------------------------------------------------------------
        n = discreteParams.n;
        p = discreteParams.p;
        T = toyParams.nTotalNum;

        % Convert to nxpxT matrices
        curve0 = zeros(n,p,T);
        curve1 = zeros(n,p,T);
        for i=1:T 
            curve0(:,:,i) = toyData.Z_c{i};
        end;
        for i=1:T
            curve1(:,:,i) = Su12aCurve{i};
        end
        curve2 = reshape(Hong15aCurve,n,p,T);

        % Get lambda_1(A), i.e., 1st eigenvalue of dyanmic matrix A
        lamCurve0 = max_evs(curve0, toyData.pDim);
        lamCurve1 = max_evs(curve1, toyData.pDim);
        lamCurve2 = max_evs(curve2, toyData.pDim);
        
        curveData = [...
            real(lamCurve0) ...
            real(lamCurve1) ...
            real(lamCurve2) ...
            imag(lamCurve0) ...
            imag(lamCurve1) ...
            imag(lamCurve2)];
        dataFile = fullfile(outDir, ...
            sprintf('%s_%.3f.txt', mfilename, fNoise(noiseI)));
        dlmwrite(dataFile, curveData, 'delimiter', ' ');
        close all;
    end
    
    %----------------------------------------------------------------------
    % Plotting progress of Su et al.   
    %----------------------------------------------------------------------
    function dummy = outfun(x, optimValues, state)
        dummy = 0;
        switch state
            case 'init'
                hold on;
            case 'iter'
                tmp = reshape(x,...
                    [discreteParams.n,discreteParams.p,toyParams.nTotalNum]);
                adimat_show_curve(tmp, 1:toyParams.nTotalNum, 24, '--', ...
                    discreteParams.colors(optimValues.iteration+1,:));
                drawnow;
            case 'done'
            otherwise
        end
    end
end