%--------------------------------------------------------------------------
% Test full regression using synthetic data and compare to two competitors!
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/23/2014
%--------------------------------------------------------------------------

% Setup
close all;
rng(1234);

% Generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10]
nTotalNum = 50;
minBoundary = 0.2;
maxBoundary = 9.8;
t = linspace(minBoundary, maxBoundary, nTotalNum);
f = t;

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% system identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    s = [sin(f(i)*x); cos(f(i)*x)];
    org{i} = s;
    y =  CC*s;
    y = y + 0.01*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weighted_suboptimal_systemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end
% Plot the generate data
plotGeodesic( oma, t, pDim, '-*' );
Ypnt = oma;
Tpnt = t;

% Setup GGR
grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.h = 1./grParams.nt;
grParams.deltaT = 0.05;           % the step size for updating the initial conditions
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 500;          % the number of iterations for updating the initial conditions
grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'linesearch';  % use linesearch for updating the initial conditions, other methods could be implemented later

grParams.ts = Tpnt;
grParams.Ys = Ypnt;
grParams.wi = ones(length(t), 1);
grParams.t_truth = Tpnt;
grParams.Y_truth = Ypnt;
grParams.pDim = pDim;
grParams.useRansac = false;

% initialize initial conditions
grParams.pntY0 = Ypnt{end};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(grParams.ts);
grParams.nsize = size(Ypnt{1});

% Generate the ground truth
[X10GT, X20GT, energy] = fullRegressionOnGrassmannManifold( grParams );
X20GT = X20GT ./ (max(grParams.ts) - min(grParams.ts));

% Generate data using the ground  truth
nPoint = 50;
spac = linspace(minBoundary, maxBoundary, nPoint);
[tGT, yGT, ydotGT] = integrateForwardWithODE45(X10GT, X20GT, spac);
checkConstraints(yGT, X10GT, X20GT);

% Add noise to data
yGTNoise = addNoiseToData(yGT, 0.03);
figure, plotGeodesic( yGT, tGT, pDim, '-*' );

% Setup GGR params
grParamsFit = grParams;
grParamsFit.ts = tGT;
grParamsFit.Ys = yGTNoise;
grParamsFit.wi = ones(length(grParamsFit.ts), 1);
grParamsFit.Y_noise = yGTNoise;
grParamsFit.t_truth = tGT;
grParamsFit.Y_truth = yGT;

% Initialize 
grParamsFit.pntY0 = grParamsFit.Ys{end};
grParamsFit.tanY0 = zeros(size(grParamsFit.pntY0));
grParamsFit.pntT0 = min(grParamsFit.ts);
grParamsFit.nsize = size(grParamsFit.Ys{1});
[X10Fit, X20Fit, energyFit] = fullRegressionOnGrassmannManifold( grParamsFit );
X20Fit = X20Fit ./ (max(grParamsFit.ts) - min(grParamsFit.ts));
[tEst, yEst, ~] = integrateForwardWithODE45(X10Fit, X20Fit, spac);

% Fit geodesic using [Rentmeesters11a]
cdc_time = (tGT-min(tGT))./(max(tGT(:) - min(tGT(:))));
[p0,v0] = cdc_init(yGTNoise,cdc_time,50);
[X1Rentmeesters11a,X2Rentmeesters11a,~,~,C] = ...
    cdc_gd(yGTNoise,p0,v0,cdc_time,1e-6,100,0.5,1);

% Difference in initial conditions
X1DistRentmeesters11a = sqrt(grarc(X1Rentmeesters11a, X10GT));
X2DistRentmeesters11a = norm(X2Rentmeesters11a/...
    (max(grParams.ts) - min(grParams.ts)) - X20GT,'fro')/norm(X20GT, 'fro');

% Compute points on fitted geodesic
pointsRentmeesters11a = cell(length(cdc_time), 1);
manifold = grassmannfactory(...
    size(X1Rentmeesters11a,1),size(X1Rentmeesters11a,2));
for i=1:length(cdc_time)
    pointsRentmeesters11a{i} = manifold.exp(...
        X1Rentmeesters11a,cdc_time(i)*X2Rentmeesters11a);
end

% Diff. in initial conditions
X1Dist = sqrt(grarc(X10Fit, X10GT));
X2Dist = norm(X20Fit-X20GT, 'fro') / norm(X20GT, 'fro');

% Data vs. Est.
meanEnergyFit = energyFit / length(grParamsFit.Ys);

% GT vs. Data
energyOpt = 0;
for iI = 1:length(grParamsFit.Ys)
    energyOpt = energyOpt + grarc( ...
        grParamsFit.Ys{iI}, grParamsFit.Y_truth{iI} );
end
meanEnergyOpt = energyOpt / length(grParamsFit.Ys);

% GT vs. Est.
diffSum = 0;
for iI = 1:length(grParamsFit.Y_truth{iI})
    diffSum = diffSum + grarc( yEst{iI}, grParamsFit.Y_truth{iI} );
end
meanErr = diffSum / length(grParamsFit.Y_truth);

fprintf('**** Ours ***\n');
fprintf('diff(X1)=%.5f\n', X1Dist);
fprintf('diff(X2)=%.5f\n', X2Dist);
fprintf('GT vs. Data.: %.5f\n', meanEnergyOpt); % same for all
fprintf('Data vs. Est: %.5f\n', meanEnergyFit);
fprintf('GT vs. Est.: %.5f\n', meanErr);
fprintf('**** Ours ***\n');

% GT vs. Est.
diffSumRentmeesters11a = 0;
for iI = 1:length(grParamsFit.Y_truth{iI})
    diffSumRentmeesters11a = diffSumRentmeesters11a + ...
        grarc( pointsRentmeesters11a{iI}, grParamsFit.Y_truth{iI} );
end
meanErrRentmeesters11a = ...
    diffSumRentmeesters11a / length(grParamsFit.Y_truth);

% Est. vs Data
diffSumRentmeesters11a = 0;
for iI = 1:length(grParamsFit.Ys{iI})
    diffSumRentmeesters11a = diffSumRentmeesters11a + ...
        grarc( pointsRentmeesters11a{iI}, grParamsFit.Ys{iI} );
end
meanEnergyFitRentmeesters11a = ...
    diffSumRentmeesters11a/ length(grParamsFit.Y_truth);


fprintf('**** vs. Rentmeesters11a ***\n');
fprintf('diff(X1)=%.5f\n', X1DistRentmeesters11a);
fprintf('diff(X2)=%.5f\n', X2DistRentmeesters11a);
fprintf('GT vs. Data.: %.5f\n', meanEnergyOpt);
fprintf('Data vs. Est: %.5f\n', meanEnergyFitRentmeesters11a);
fprintf('GT vs. Est.: %.5f\n', meanErrRentmeesters11a);
fprintf('**** vs. Rentmeesters11a ***\n');










