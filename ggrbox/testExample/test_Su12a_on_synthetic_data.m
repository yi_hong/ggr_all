function res = test_Su12a_on_synthetic_data()

    toyParams.seed = 1234;
    toyParams.nTotalNum = 20;
    toyParams.fNoise = 5;
    toyParams.sNoise = 0.1;
    toyData = gen_spline_synthetic_data( toyParams );

    params.tau = 1:toyParams.nTotalNum;  % Sample at 20 locations
    params.lambda1 = 2;                  % Data weighting 
    params.lambda2 = 0.2;                % Smoothness weighting
    params.n = size(toyData.Y_c{1},1);   % R^n 
    params.p = size(toyData.Y_c{1},2);   % Subspace dim
    params.P = zeros(params.n,params.p, ...
        toyParams.nTotalNum);
    for i=1:toyParams.nTotalNum; 
        params.P(:,:,i) = toyData.Y_c{i};
    end;

    % Generate initial curve (between P(1) and P(2))
    tmpTv = adimat_gr_log(params.P(:,:,1),params.P(:,:,2));
    tmpTime = linspace(0,1,toyParams.nTotalNum);
    initialCurve = zeros(...
        params.n,...
        params.p,...
        toyParams.nTotalNum);
    for i=1:toyParams.nTotalNum
        initialCurve(:,:,i) = adimat_gr_exp(...
            params.P(:,:,1),tmpTime(i)*tmpTv);
    end
    
    adimat_show_curve( params.P, 1:20, 24, '-', [0 0 0]);
    drawnow;
    
    % Setup optimization
    x0 = initialCurve(:);
    opt_opts = adimat_load_optimizer_options();
    f_val = adimat_compute_regression_energy( x0, params );

    opts.optsAdimat = admOptions( 'functionResults', {f_val}, 'i', 1 );
 
    % Optimize
    opt_opts.fminunc = optimset( opt_opts.fminunc, 'OutputFcn', @outfun);
    opt_opts.fminunc = optimset( opt_opts.fminunc, 'MaxIter', 100);
    params.colors = parula(opt_opts.fminunc.MaxIter+2);    
    opts.params = params; 

    res = fminunc( @( x )adimat_driver( x, opts ), x0, opt_opts.fminunc );
    
    function dummy = outfun(x, optimValues, state)
        dummy = 0;
        switch state
            case 'init'
                hold on;
            case 'iter'
                tmp = reshape(x,[params.n,params.p,toyParams.nTotalNum]);
                adimat_show_curve(tmp, 1:toyParams.nTotalNum, 24, '--', ...
                    params.colors(optimValues.iteration+1,:));
                drawnow;
            case 'done'
            otherwise
        end
    end
end

