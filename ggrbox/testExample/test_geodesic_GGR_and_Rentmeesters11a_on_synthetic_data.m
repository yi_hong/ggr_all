%-------------------------------------------------------------------------- 
% Compare the geodesic fitting method of Rentmeesters (CDC'11) to the
% proposed GGR method.
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 09/28/2014
%-------------------------------------------------------------------------- 

close all;

addpath(genpath('../data'));

% prepare data
clear cc
load cc.mat

xAge = cc.ages;
time = (xAge-min(xAge))./(max(xAge(:) - min(xAge(:))));
m = length(time);
 
Q = cell(m,1);
sigma = zeros(size(cc.data{1},2), size(cc.data{1},2));
for i=1:m
    [U,sTmp,~] = svd(cc.data{i},0);
    Q{i} = U;
    sigma = sigma + sTmp;
end
sigma = sigma ./ m;

%% ggr
grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.h = 1./grParams.nt;
grParams.deltaT = 0.02;           % the step size for updating the initial conditions
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 1000;          % the number of iterations for updating the initial conditions
grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'linesearch';  % use linesearch for updating the initial conditions, other methods could be implemented later
grParams.ts = time;
grParams.Ys = Q;
grParams.wi = ones(length(grParams.ts), 1);
grParams.useRansac = false;

% initialize initial conditions
grParams.pntY0 = grParams.Ys{1};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(grParams.ts);
grParams.nsize = size(grParams.Ys{1});

tic 
[pntY, tanY, energy] = fullRegressionOnGrassmannManifold( grParams );
t1 = toc;


%% cdc
tic
% pick initial geodesic
[p0,v0] = cdc_init(Q,time,50);
% run GD algorithm
[p_opt,v_opt,~,~,C] = cdc_gd(Q,p0,v0,time,1e-6,100,0.5,1);
t2 = toc;

N = size(Q{1},1); % pts
d = size(Q{1},2); % dim
G = grassmannfactory(N,d);

%% generate points 
plotPoints = 50;
spac = linspace(0,1,plotPoints);

% cdc
pointCDC = cell(plotPoints, 1);
for i=1:plotPoints
    xCDC = G.exp(p_opt,spac(i)*v_opt);
    pointCDC{i} = xCDC;
end

% ggr
[~, pointGGR] = integrateForwardWithODE45(pntY, tanY, spac);

% plot error
pntErr = zeros(plotPoints, 1);
for i=1:plotPoints
    pntErr(i) = sqrt(grarc(pointGGR{i}, pointCDC{i}));
end
figure, plot(spac, pntErr);

sqrt(grarc(pntY, p_opt))
%norm(pntY - p_opt, 'fro')
norm(tanY - v_opt, 'fro')