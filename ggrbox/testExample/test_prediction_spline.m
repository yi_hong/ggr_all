%----------------------------------------------------------------------
% Test estimating independent value using spline
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 10/11/2014
%----------------------------------------------------------------------

close all;

% Generate data using system identification
step=0.05; x = 0:step:10*pi;

% Frequencies within (0, 10)
nTotalNum = 50;
minBoundary = 0.2;
maxBoundary = 9.8;
t = linspace(minBoundary, maxBoundary, nTotalNum);
f = 3*(sin(t/6)+1.5);

pDim = 24;
nSta = 2;
kObs = 2;

% Linear projector into 24-D
[CC, ~, ~] = svd(randn(pDim, nSta), 0);
 
sig = cell(length(f),1); % 24-d signals
sys = cell(length(f),1); % LDS
oma = cell(length(f),1); % Finite observ. matrices
org = cell(length(f),1); % Original signal

% System identification
useWeightedSystemIdentification = false;
for i=1:length(f)
    %fn = f(i)+0.2*randn();
    s = [sin(f(i)*x); cos(f(i)*x)];
    %s = [sin(fn*x); cos(fn*x)];    
    org{i} = s;
    y =  CC*s;
    y = y + 0.05*rand(size(y));
    sig{i} = y;
    params.class = 2;
    
    if ( ~useWeightedSystemIdentification )
        % use uniform weights
        weights = ones( 1, size( y, 2 ) );
        weights = weights/sum(weights);      % normalize
    else
        % create Gaussian weights for test
        gVal = linspace( -3, 3, size( y,2 ) );
        weights = exp( -gVal.^2 );
        weights = weights/sum(weights);      % normalize
    end
    sys{i} = weighted_suboptimal_systemID(sig{i},nSta,weights,params);
    oma{i} = omat(sys{i}, kObs);
end
% plot the generate data
plotGeodesic( oma, t, pDim, '-*' );
Ypnt = oma;
Tpnt = t;

%% perform spline regression
grParams.sigmaSqr = 1;            % sigma square for balancing the mismatching term
grParams.alpha = 0.000;               % alpha for balancing the prior knowledge of the slope of the geodesic
grParams.nt = 100;                % the number of the discretized grids 
grParams.h = 1./grParams.nt;
grParams.deltaT = 0.02;           % the step size for updating the initial conditions
grParams.maxReductionSteps = 10;  % the number of times for tracing back
grParams.rho = 0.5;               % the step size for tracing back
grParams.nIterMax = 100;          % the number of iterations for updating the initial conditions
grParams.stopThreshold = 1e-6;    % the threshold for stopping the iterations
grParams.useODE45 = true;         % chose ODE45 for solving the PDEs, more stable
grParams.minFunc = 'fmincon';  % linesearch, fmincon, both
grParams.maxIterFmincon = 10000;

grParams.ts = Tpnt;
grParams.Ys = Ypnt;
grParams.wi = ones(length(t), 1);
grParams.cps = [5];
grParams.t_truth = Tpnt;
grParams.Y_truth = Ypnt;
grParams.pDim = pDim;
grParams.X10GT = Ypnt{1};

grParams.fidSplineEnergy = 500;
grParams.fidSplineFitting = 100;
grParams.estimateInitialValueSpline = false;
[X1Opt, X2Opt, X3Opt, X4sOpt] = cubicSplineRegression(grParams);

% test prediction
%cntEst = projectPointsToGeodesicODE45Spline(X1Opt, X2Opt, X3Opt, X4sOpt, ...
%    [min(grParams.ts) grParams.cps max(grParams.ts)], grParams.Ys, 0, 10, grParams.nt);
%figure, plot(grParams.ts, cntEst);
