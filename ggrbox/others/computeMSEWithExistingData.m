% 
% read data to compute mean squared testing error
%
% Author: Yi Hong, yihong@cs.unc.edu
% Date: 10/10/2014
%

%function computeMSEWithExistingData()

nFlag = 2; % 1: corpus_callosum, 2:rat_calivarium
nMethod = 1;

if nFlag == 1
    fileLocation = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/corpus_callosum2';
    nPartition = 32;
elseif nFlag == 2
    fileLocation = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_092914/rat_calivarium2';
    nPartition = 18;
else
    error('Unknown type');
end


errSqr = zeros(nPartition, nMethod);
for iI = 1:nPartition
    
    if nFlag == 1
        filename = sprintf('%s/corpus_callosum_Part%dOf%d.mat', fileLocation, iI, nPartition);
    else
        filename = sprintf('%s/rat_calivarium_Part%dOf%d.mat', fileLocation, iI, nPartition);
    end
    
    tmp = load(filename);
    grParams = tmp.grParams;
    initPnt = tmp.initPnt;
    initVel = tmp.initVel;
    tPos = tmp.tPos;
    timeWarpParams = tmp.timeWarpParams;
    
    grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
    if nFlag == 1
        [omas, ages, sigma] = loadCorpusCallosumFeatureAndSVD( grParams );
    else
        [omas, ages, sigma] = loadRatCalivariumLandmarksAndSVD( grParams );
    end
    [ages, index] = sort(ages);
    omas = omas(index);

    assert(length(initPnt) == nMethod);
    for iJ = 1:length(initPnt)
        errSqr(grParams.nTesting, iJ) = computeGeodesicTestError( grParams.regressMethod{iJ}, initPnt{iJ}, ...
            initVel{iJ}, tPos{iJ}, timeWarpParams{iJ}, omas(grParams.nTesting), ages(grParams.nTesting), grParams.nt );
    end
end

mean(errSqr)