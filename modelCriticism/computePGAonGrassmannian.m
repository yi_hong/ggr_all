function [Ymean, pc, latent] = computePGAonGrassmannian(Ys)

% compute the mean 
Ymean = grfmean(Ys, 1e-6);
   
% compute the covariance matrix
[n, p] = size(Ys{1});
cov = zeros(n*p, n*p);

for iI = 1:length(Ys)
    [~, ~, ~, vTmp] = grgeo(Ymean, Ys{iI}, 1, 'v3', 'v2');
    
    vList = reshape(vTmp, [], 1);
    for j = 1:length(vList)
        for k = 1:length(vList)
            cov(j, k) = cov(j, k) + vList(j) * vList(k);
        end
    end
end

cov = cov / (length(Ys)-1);

[pc, ~, latent] = princomp(cov);
    
end