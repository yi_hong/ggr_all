function [stat, boot_stat] = evaluateFitOnGrassmannian(Obs, Est, time, nTrials, gkParams, nType)

N = length(Obs);
assert(N == length(Est) && N == length(time));

% normalize time
time = (time - min(time))/(max(time)-min(time));

%gkParams.gamma = computePairwiseMedian(time);

error = zeros(N, 1);
if nType == 1  
    Rnd = cell(N, 1);
    Rnd1 = cell(N, 1);
    Rnd2 = cell(N, 1);
end

%---Estimate of Std. Dev.---%
for iI = 1:N
    error(iI) = sqrt(grarc(Obs{iI}, Est{iI}));
end
%sig = std(error);
%sig = sqrt(sum(error.^2)/(N-2));
sig = sqrt(sum(error.^2)/(N-1));
%sig = sqrt(sum(error.^2)/N);
sum(error.^2)

% robust standard deviation of residuals
% nPicked = round(N*0.6827);
% sig = sqrt(sum(error(1:nPicked).^2)/(nPicked-1));

stat = zeros(nTrials, 1);
% dist1 = zeros(nTrials, 1);
for iT = 1:nTrials
    fprintf('stat: %d/%d\n', iT, nTrials);
    switch nType 
        case 1
            %---Generate random samples for stat---%
            for iI = 1:N
                Rnd{iI} = generateGaussianSamples(Est{iI}, sig);
            end
            %---Make sure it is Gaussian distributed residues---%
            %[bGauss, ~] = checkResidues(Est, Rnd);
            %assert(bGauss == 0); 
        case 2
            %---Compute PGA and sample---%
            [Ymean, pc, latent] = computePGAonGrassmannian(Obs);
            [Rnd] = generateSamplesPGA(Est, sig, Ymean, pc, latent, 1);
        otherwise
            error('Unknown type to generate Gaussian samples on the Grassmannian');
    end

    %---Compute statistic---%
    stat(iT) = grassRegMMD(Obs, Rnd, time, time, gkParams);
%     for iJ = 1:length(Obs)
%         dist1(iT) = dist1(iT) + grarc(Obs{iJ}, Rnd{iJ});
%     end

    % plot witness function
    if iT == 1
        figure, estimateWitnessFuncGrassmann(Obs, Rnd, time, time, 100, 100, gkParams, Est);
    end
end

boot_stat = zeros(nTrials, 1);
% dist2 = zeros(nTrials, 1);
for iT = 1:nTrials
    fprintf('bootStat: %d/%d\n', iT, nTrials);
    
    switch nType
        case 1
            for iI = 1:N
                Rnd1{iI} = generateGaussianSamples(Est{iI}, sig);
                Rnd2{iI} = generateGaussianSamples(Est{iI}, sig);
            end
            %[bGauss1, ~] = checkResidues(Est, Rnd1);
            %[bGauss2, ~] = checkResidues(Est, Rnd2);
            %assert(bGauss1 == 0 && bGauss2 == 0);
        case 2
            Rnd1 = generateSamplesPGA(Est, sig, Ymean, pc, latent, 1);
            Rnd2 = generateSamplesPGA(Est, sig, Ymean, pc, latent, 1);
        otherwise
            error('Unknown type to generate Gaussian samples on the Grassmannian');
    end
    boot_stat(iT) = grassRegMMD(Rnd1, Rnd2, time, time, gkParams);
%     for iJ = 1:length(Rnd1)
%         dist2(iT) = dist2(iT) + grarc(Rnd1{iJ}, Rnd2{iJ});
%     end
end

end
