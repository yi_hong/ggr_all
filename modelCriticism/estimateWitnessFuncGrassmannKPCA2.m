function estimateWitnessFuncGrassmannKPCA2(A, B, timeA, timeB, xSize, ySize, gkParams, Est)

pc1 = 1;
pc2 = 2;

data = [A; B; Est];
time = [timeA; timeB; timeB];
assert(length(data) >= max(pc1, pc2));

[eigVectorTmp] = constructNormalizedKernelMatrix(data, time, gkParams, max(pc1, pc2));
eigVector = [eigVectorTmp(:, pc1) eigVectorTmp(:, pc2)];

[basisA, basisB, basisE] = precomputeBasis(data, time, A, timeA, B, timeB, Est, timeB, gkParams);
% projection
scoreA = (eigVector' * basisA)'; 
scoreB = (eigVector' * basisB)';
scoreE = (eigVector' * basisE)';

xAxis = linspace(min([scoreA(:, 1); scoreB(:, 1); scoreE(:, 1)]), ...
                 max([scoreA(:, 1); scoreB(:, 1); scoreE(:, 1)]), xSize);
yAxis = linspace(min([scoreA(:, 2); scoreB(:, 2); scoreE(:, 2)]), ...
                 max([scoreA(:, 2); scoreB(:, 2); scoreE(:, 2)]), ySize);

% compute witness function, sampling             
witnessFunc = zeros(length(yAxis), length(xAxis));
for iJ = 1:size(witnessFunc, 1)
    for iI = 1:size(witnessFunc, 2)
        sample = [xAxis(iI), yAxis(iJ)];
        witnessFunc(iJ, iI) = mean(sample * scoreA') - mean(sample * scoreB');
    end
end

hold on
imagesc(xAxis, yAxis, witnessFunc);
plot(scoreA(:, 1), scoreA(:, 2), 'm*', 'LineWidth', 2);
plot(scoreB(:, 1), scoreB(:, 2), 'bs', 'LineWidth', 2);
plot(scoreE(:, 1), scoreE(:, 2), 'k-', 'LineWidth', 2);
set(gca, 'YDir', 'normal');
hold off
colormap(b2r(min(min(witnessFunc)), max(max(witnessFunc))));
colorbar

end

function [eigVector] = constructNormalizedKernelMatrix(data, time, gkParams, nPCs)

    N = length(data);
    matK = size(N, N);
    matI = ones(N, N) / N;
    
    % compute the original kernel matrix
    for iI = 1:N
        for iJ = 1:N
            matK(iI, iJ) = xKernel(time(iI), time(iJ), gkParams.gamma) * ...
                gKernel(data{iI}, data{iJ}, gkParams);
        end
    end
    
    % centering
    matKT = matK - matI * matK - matK * matI + matI * matK * matI;

    % compute the first nPCs eigvalues and eigvectors
    [eigVector, eigValue] = eigs(matKT, nPCs);
    eigValue = diag(eigValue);
    
    % normalize alphas
    for iI = 1:length(eigValue)
        if abs(eigValue(iI)) > 1e-7
            tmp = eigVector(:, iI);
            eigVector(:, iI) = tmp / norm(tmp) / sqrt(abs(eigValue(iI)));
        end
    end
end

function [basisA, basisB, basisE] = precomputeBasis(data, time, A, timeA, B, timeB, Est, timeE, gkParams)

    basisA = zeros(length(data), length(A));
    basisB = zeros(length(data), length(B));
    basisE = zeros(length(data), length(Est));
    
    for iI = 1:length(data)
        for iJ = 1:length(A)
            basisA(iI, iJ) = xKernel(time(iI), timeA(iJ), gkParams.gamma) ...
                * gKernel(data{iI}, A{iJ}, gkParams);
        end
        for iJ = 1:length(B)
            basisB(iI, iJ) = xKernel(time(iI), timeB(iJ), gkParams.gamma) ...
                * gKernel(data{iI}, B{iJ}, gkParams);
        end
        for iJ = 1:length(Est)
            basisE(iI, iJ) = xKernel(time(iI), timeE(iJ), gkParams.gamma) ...
                * gKernel(data{iI}, Est{iJ}, gkParams);
        end
    end

end

% function den = estimateKernelDensity(score, sample, eigVector, gkParams)
% 
%     N = length(Obs);
%     denSum = 0;
%     for iN = 1:N
%         for iI = 1:length(sample)
%             tmp = 0;
%             for iJ = 1:size(eigVector, 1)
%                 tmp = tmp + eigVector(iJ, iI) * xKernel(time(iJ), timeO(iN), gkParams.gamma) ...
%                     * gKernel(data{iJ}, Obs{iN}, gkParams);
%             end
%             denSum = denSum + sample(iI) * tmp;
%         end
%     end
%     den = denSum / N;
%     
% end

