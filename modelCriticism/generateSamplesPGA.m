function [YsSample] = generateSamplesPGA(Est, sigma, Ymean, pc, latent, type)

[m, n] = size(Est{1});
YsSample = cell(length(Est), 1);
%var = latent(1:nEig);
var = latent;
var = var ./ var(1);
for iI = 1:length(Est)
    switch type
        case 1
            % generate Gaussian distributed tangent vector
            sRnd = normrnd(zeros(length(latent), 1), var, [length(latent), 1]);
        case 2
            % generate Poisson distributed tangent vector
            sRnd = poissrnd(var, [length(latent), 1]);
        otherwise
            error('Unknown type for sampling');
    end
    tan = zeros(size(pc, 1), 1);
    for iJ = 1:length(sRnd)
        tan = tan + sRnd(iJ) * pc(:, iJ);
    end
    
    %tan = tan / norm(tan) * sigma;
    tan = tan * sigma;
    tanVec = reshape(tan, [m, n]);
    
    % parallel transport
    [~, ~, ~, vTmp] = grgeo(Ymean, Est{iI}, 1, 'v3', 'v2');
    tanSample = ptEdelman(Ymean, vTmp, tanVec, 1.0);
    
    % shoot forward
    [~, Ytmp, ~] = integrateForwardWithODE45(Est{iI}, tanSample, [0, 1]);
    YsSample{iI} = Ytmp{end};
end

end