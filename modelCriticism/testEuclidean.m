function [testStat, MMDs, p] = testEuclidean(mat_file,seed,do_plot)

    rng(seed);
    tmp = load(mat_file);
    
    X = tmp.X;
    y = tmp.y;
    
    X_data = double(X);
    y_data = double(y);

    coef = polyfit(double(X),double(y),1);
    ymu = double(X_data)*coef(1) + coef(2);
    ys2 = var(double(y) - double(X)*coef(1) - coef(2));

    X_post = double(X_data);
    y_post = ymu + sqrt(ys2) .* randn(size(ymu));

    if do_plot
        figure,
        plot(X_post, y_post, 'ro'); hold on;
        plot(X_data, y_data, 'go'); 
        plot(X_data, ymu, 'black', 'Linewidth',2);hold off;
    end

    A = [X_data, y_data];
    B = [X_post, y_post];
    fitB = [X_data, ymu];

    std_A = std(A);

    fitB = fitB ./ repmat(std_A, size(fitB, 1), 1);
    B = B ./ repmat(std_A, size(B, 1), 1);
    A = A ./ repmat(std_A, size(A, 1), 1);

    %d1 = sqrt(sq_dist(A', A'));
    %d2 = sqrt(sq_dist(B', B'));

    params.sig = 0.25;
    
    % estimate witness function
    figure, estimateWitnessFuncEuclidean(A, B, 300, 300, params.sig, fitB);
    
    params.shuff = 1000;
    MMDs = zeros(params.shuff,1);
    for b = 1:params.shuff
        y_post_1 = ymu + sqrt(ys2) .* randn(size(ymu));
        y_post_2 = ymu + sqrt(ys2) .* randn(size(ymu));

        C = [X_post, y_post_1];
        D = [X_post, y_post_2];

        C = C ./ repmat(std_A, size(C, 1), 1);
        D = D ./ repmat(std_A, size(D, 1), 1);

        m=size(C,1);
        n=size(D,1);
        K = rbf_dot(C,C,params.sig);
        L = rbf_dot(D,D,params.sig);
        KL = rbf_dot(C,D,params.sig);
        testStat = (1/m^2) * sum(sum(K)) - (2 / (m * n)) * sum(sum(KL)) + ...
                   (1/n^2) * sum(sum(L));
        testStat = sqrt(testStat);
        % Save
        MMDs(b) = testStat;
    end

    % Calculate MMD stat and p-value
    m=size(A,1);
    n=size(B,1);
    K = rbf_dot(A,A,params.sig);
    L = rbf_dot(B,B,params.sig);
    KL = rbf_dot(A,B,params.sig);
    testStat = (1/m^2) * sum(sum(K)) - (2 / (m * n)) * sum(sum(KL)) + ...
               (1/n^2) * sum(sum(L));
    testStat = sqrt(testStat);
    %p = sum(testStat < MMDs) / length(MMDs);
    p = sum(MMDs >= testStat) / length(MMDs);
    
end
