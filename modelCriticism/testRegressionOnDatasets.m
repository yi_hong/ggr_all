% testing regression on corpus callosum
close all
clear all

rng('shuffle');
%outPrefix = '/Users/yihong/Desktop/work/Project/model_criticism_results/poster';
outPrefix = '/playpen/Project/modelCriticism/miccai2015_results/poster';
bSave = true;
fileSavedTime = '092715';

nDataSet = 2; % 1: corpus callosum, 2: rat calvarium

bFileExists = true;
if bFileExists == false
    switch nDataSet
        case 1
            regressionOnCorpusCallosum();
        case 2
            regressionOnRatCalvarium();
        otherwise
            error('Unknown dataset');
    end
end

switch nDataSet
    case 1
        %inPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_022215/corpus_callosum';
        inPrefix = './mat_files';
        filename = sprintf('%s/corpus_callosum_Part1Of1.mat', inPrefix);
    case 2
        %outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_022215/rat_calvarium';
        inPrefix = './mat_files';
        filename = sprintf('%s/rat_calvarium_Part1Of1.mat', inPrefix);
    otherwise
        error('Unknown dataset');
end

tmp = load(filename);

nTrials = 1000;
nType = 1; % Gaussam sampling, 1: random tangent 2: based on PCA (2 does not work, use 1)
gkParams.beta = 1;
gkParams.type = 4;  % Laplace, p
gkParams.p = size(tmp.omas{1}, 2);
gkParams.gamma = 0.3;

regressMethod = tmp.grParams.regressMethod;
stat = cell(length(regressMethod), 1);
bootStat = cell(length(regressMethod), 1);
pValue = cell(length(regressMethod), 1);
R2 = zeros(length(regressMethod), 1);

for iI = 1:length(regressMethod)
    [~, omaEst] = computeGeodesicTestError( regressMethod{iI}, tmp.initPnt{iI}, ...
        tmp.initVel{iI}, tmp.tPos{iI}, tmp.timeWarpParams{iI}, tmp.omas, tmp.ages, tmp.grParams.nt );

    % model criticism 
    [stat{iI}, bootStat{iI}] = evaluateFitOnGrassmannian(tmp.omas, omaEst, ...
        tmp.ages, nTrials, gkParams, nType);
    
    R2(iI) = 1 - computeVar(tmp.omas, omaEst, 3) / computeVar(tmp.omas, tmp.omas, 1);
end

figure;
for iI = 1:length(stat)
    tmpValue = zeros(length(stat{iI}), 1);
    for iJ = 1:length(stat{iI})
        tmpValue(iJ) = sum(bootStat{iI} >= stat{iI}(iJ)) / length(bootStat{iI});
    end
    pValue{iI} = tmpValue;
    subplot(length(stat), 2, (iI-1)*2+1), histTwoVectors(bootStat{iI}, stat{iI}, 100);
    subplot(length(stat), 2, (iI-1)*2+2), hist(pValue{iI}, 100);
end

if bSave == true
    filename = sprintf('%s/modelCriticism_dataset%d_%dTrials_kerenl%d_beta%d_%s.fig', ...
        outPrefix, nDataSet, nTrials, gkParams.type, gkParams.beta, fileSavedTime);
    saveas(gca, filename);
end

if bSave == true
    filename = sprintf('%s/results_dataset%d_%dTrials_kernel%d_beta%d_%s.mat', ...
        outPrefix, nDataSet, nTrials, gkParams.type, gkParams.beta, fileSavedTime);
    save(filename, 'stat', 'bootStat', 'pValue', 'R2');
end
