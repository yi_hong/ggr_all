function saveMFD(timeX, dataY, timeFileName, dataFileName, dim)

    % make sure enough (time,data) tuple are present
    assert(length(timeX) == size(dataY, 2) && ...
           mod(size(dataY, 1), dim) == 0);

    % data file
    fid = fopen(dataFileName, 'wt');
    assert(fid > 0);

    % see 'ManifoldFileReader.cxx'
    fprintf(fid, 'GFBR1\n'); 
    fprintf(fid, 'manifold=KendallShapeSpace\n');
    fprintf(fid, 'primitive=point\n');
    fprintf(fid, 'dimParameters=%d %d\n', dim, size(dataY, 1)/dim);
    fprintf(fid, 'numElements=%d\n', size(dataY, 2));
    fprintf(fid, 'format=text\n');
    fprintf(fid, 'data:\n');

    for iJ = 1:size(dataY, 2)
        for iI = 1:size(dataY, 1)
            fprintf(fid, '%.10f ', dataY(iI, iJ));
        end
        fprintf(fid, '\n');
    end

    fclose(fid);
    % DONE writing data
    
    % time point file
    fid = fopen(timeFileName, 'wt');
    assert(fid > 0);
    
    fprintf(fid, 'GFBR1\n'); 
    fprintf(fid, 'manifold=Euclidean\n');
    fprintf(fid, 'primitive=point\n');
    fprintf(fid, 'dimParameters=1\n');
    fprintf(fid, 'numElements=%d\n', size(dataY,2));
    fprintf(fid, 'format=text\n');
    fprintf(fid, 'data:\n');
    for iI = 1:length(timeX)
        fprintf(fid, '%.10f\n', timeX(iI));
    end
    fclose(fid);
end