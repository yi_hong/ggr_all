function estimateWitnessFuncGrassmann(A, B, timeA, timeB, xSize, ySize, gkParams, Est)

if ~(size(A{1}, 1) == 2 && size(A{1}, 2) == 1)
    % use kernel-PCA to extract principal componenent for visualization
    nType = 3;
    if nType == 1
        estimateWitnessFuncGrassmannKPCA(A, B, timeA, timeB, xSize, ySize, gkParams, Est);  % time + 1PC
    elseif nType == 2
        estimateWitnessFuncGrassmannKPCA2(A, B, timeA, timeB, xSize, ySize, gkParams, Est);  % 2PCs
    end
    return;
end

angleA = zeros(length(A), 1);
angleB = zeros(length(B), 1);
angleE = zeros(length(Est), 1);
for iI = 1:length(A)
    angleA(iI) = atan(A{iI}(2)/A{iI}(1));
end
for iI = 1:length(B)
    angleB(iI) = atan(B{iI}(2)/B{iI}(1));
end
for iI = 1:length(Est)
    angleE(iI) = atan(Est{iI}(2)/Est{iI}(1));
end

time = linspace(min(min(timeA), min(timeB)), max(max(timeA), max(timeB)), xSize);
angle = linspace(min(min(angleA), min(angleB)), max(max(angleA), max(angleB)), ySize);

witnessFunc = zeros(length(angle), length(time));
for iJ = 1:size(witnessFunc, 1)
    for iI = 1:size(witnessFunc, 2)
        sample = [cos(angle(iJ)); sin(angle(iJ))];
        witnessFunc(iJ, iI) = estimateKernelDensity(A, timeA, sample, time(iI), gkParams) ...
            - estimateKernelDensity(B, timeB, sample, time(iI), gkParams);
    end
end

hold on
imagesc(time, angle, witnessFunc);
plot(timeA, angleA, '*', 'LineWidth', 2, 'color', [193/255 75/255 83/255]);
plot(timeB, angleB, 's', 'LineWidth', 2, 'color', [73/255 112 /255 166/255]);
plot(timeB, angleE, '-', 'LineWidth', 4, 'Color', [73/255 112 /255 166/255]);
set(gca,'YDir','normal');
hold off
colormap(b2r(min(min(witnessFunc)), max(max(witnessFunc))));
colorbar


    function den = estimateKernelDensity(Obs, timeO, Sample, timeS, gkParams)
        N = length(Obs);
        denSum = 0;
        for iN = 1:N
            denSum = denSum + ( xKernel(timeO(iN), timeS, gkParams.gamma) * ...
                gKernel(Obs{iN}, Sample, gkParams) );
        end
        den = denSum / N;
    end

end

