function [Y1] = generatePoissonSamples(Y0, gamma)
    [m, n] = size(Y0);
    Tan = poissrnd(gamma*100, [m, n])/100.0;
    Tan = (eye(size(Y0, 1)) - Y0*Y0')*Tan;
    [~, Ytmp, ~] = integrateForwardWithODE45(Y0, Tan, [0, 1]);
    Y1 = Ytmp{end};
end