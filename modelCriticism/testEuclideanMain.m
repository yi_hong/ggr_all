% 
% test of model criticism for Euclidean case
% 
close all
clear all

outPrefix = '/Users/yihong/Desktop/work/Project/model_criticism_results';
%outPrefix = '/playpen/Project/modelCriticism/miccai2015_results';

dataSet = 2;
nTrials = 1000;
seed = 4556;
do_plot = 0;

if dataSet == 1
    mat_file = 'mat_files/linear.mat';
elseif dataSet == 2
    mat_file = 'mat_files/solar.mat';
else
    error('Unknown dataset');
end

[testStats, MMDs, pValues] = testEuclideanModified(mat_file, seed, nTrials, do_plot);

figure, subplot(1, 2, 1), histTwoVectors(MMDs, testStats, 100);
subplot(1, 2, 2), hist(pValues, 100);

filename = sprintf('%s/euclideanData_dataset%d_%dTrials.fig', outPrefix, dataSet, nTrials);
saveas(gca, filename);

filename = sprintf('%s/euclideanData_dataset%d_%dTrials.mat', outPrefix, dataSet, nTrials);
save(filename, 'testStats', 'MMDs', 'pValues');
