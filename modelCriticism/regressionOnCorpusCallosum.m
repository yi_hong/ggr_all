function regressionOnCorpusCallosum()
% corpus callosum data

% close all
% clear all

%profile -memory on

grParams.inputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results';
outputPrefix = '/Users/yihong/Desktop/work/Project/GGR_data_results/results_022215/corpus_callosum';
%grParams.inputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results';         % data for regression
%outputPrefix = '/playpen/Project/dynamicalSystem/GGR_data_results/results_093014/corpus_callosum2';  % regression results

grParams.regressMethod = {'FullRegression', 'TimeWarpRegression', 'CubicSplineRegression'};
grParams.minValue = 0;                 % the minimal range of the independent value, for plots                
grParams.maxValue = 100;               % the maximal range of the independent value, for plots

% parameters for compute the cost function
grParams.sigmaSqr = 1;                             % the coefficient for matching term in the cost function
grParams.alpha = 0;                                % the coefficient for prior knowledge
grParams.nt = 100;
grParams.deltaT = 0.03;
grParams.deltaTWarp = 0.01;
grParams.maxReductionSteps = 10;
grParams.rho = 0.5;
grParams.nIterMax = 300;
grParams.nIterMaxTimeWarp = 50;
grParams.stopThreshold = 1e-14;
grParams.minFunc = 'linesearch';                    % linesearch, fmincon
grParams.twMinFunc = 'fmincon';
grParams.csMinFunc = 'both';                        % linesearch, fmincon, both(linesearch+fmincon)
grParams.nIterBoth = 50;
grParams.useODE45 = true;                           % more stable
grParams.estErr = 0.05;                             % discretizing size for prediction
grParams.useWeightedData = false;                   % weight data using its density
grParams.useRansac = false;                         % randomly choose pairs for pair-wise searching
grParams.fullGGRInitZero = true;                    % use zero to initialize the initial conditions   
grParams.predictScalarValue = 0;                    % 0: don't estimate independent value, 1: predict
grParams.figIdLogisticFunc = 600;                   % plot logistic function
grParams.fidSplineEnergy = 500;
grParams.maxIterFmincon = 300;

grParams.M = 65;
grParams.k = 0.5;

tMinPlot = 19;
tMaxPlot = 90;
nNumShape = 50;

[omas, ages, sigma] = loadCorpusCallosumFeatureAndSVD( grParams );
[ages, index] = sort(ages);
omas = omas(index);
grParams.nSys = length(omas);
grParams.cps = (min(ages)+max(ages))/2;
%grParams.cps = [45 65];

% permulation test
nPermutation = 0;
rng('shuffle');

R2 = zeros( nPermutation+1, length(grParams.regressMethod) );

for iPer = 1:nPermutation+1
    
    if iPer > 1
        % reorder the ages
        ages = ages( randperm(length(ages)) );
    end

    nPartition = 1; 
    for iP = 1:nPartition
        grParams.nTesting = iP:nPartition:grParams.nSys;
        grParams.nSampling = 1:grParams.nSys;
        if nPartition > 1
            grParams.nSampling(grParams.nTesting) = [];
        end
        [ageEst, errEst, tPos, initPnt, initVel, totEnergy, timeWarpParams] = estimateOnFeature( omas, ages, grParams, iP );

        % save the plot and data
        filename = sprintf('%s/corpus_callosum_Part%dOf%d.fig', outputPrefix, iP, nPartition);
        saveas( gca, filename );
        filename = sprintf('%s/corpus_callosum_Part%dOf%d.png', outputPrefix, iP, nPartition);
        saveas( gca, filename );
        filename = sprintf('%s/corpus_callosum_Part%dOf%d.mat', outputPrefix, iP, nPartition);
        save( filename, 'omas', 'ages', 'ageEst', 'grParams', 'errEst', 'tPos', 'initPnt', 'initVel', 'totEnergy', 'timeWarpParams' );

        % compute the R2 statistic
        R2(iPer, :) = computeR2Statistic( initPnt, initVel, tPos, timeWarpParams, omas, ages, grParams );
        for iJ = 1:size(R2, 2);
            pValue = length( find(R2(2:end, iJ) >= R2(1, iJ)) ) / ( size(R2, 1) - 1 );
            fprintf('p-value: %f\n', pValue);
        end
        filename = sprintf('%s/corpus_callosum_pValue_nPermute%d_Part%d_of_%d.txt', outputPrefix, nPermutation, iP, nPartition);
        save(filename, 'R2', 'pValue', '-ascii');
        
        % plot shape on geodesic
        plot2DShapeOnGeodesic( initPnt, initVel, tPos, timeWarpParams, tMinPlot, tMaxPlot, sigma, nNumShape, grParams, 1 );
        filename = sprintf('%s/corpus_callosum_shapes_nPermute%d_Part%d_of_%d.fig', outputPrefix, nPermutation, iP, nPartition);
        figure(400), saveas(gca, filename);
    end
end

end
