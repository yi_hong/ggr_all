function testRandomData(mat_file,num,sig,out_dir,config)
    GenerateRandomGaussianData_binary = config.GenerateRandomGaussianData_binary;

    tmp = load(mat_file);
    C = tmp.C;
    
    % set mean shape + normalize
    x = C(:,:,9)'; % broken bone
    x = x(:,1)+sqrt(-1)*x(:,2);
    x = x - mean(x);
    x = x/norm(x);

    broken_bone_org_filename = fullfile(...
        out_dir, ...
        'broken_bone_org.mfd');
    saveSinglePointMFD([real(x); imag(x)], broken_bone_org_filename,2);
    broken_bone_rng_filename = fullfile(...
        out_dir, ...
        'broken_bone_rng.mfd');
    exec = [GenerateRandomGaussianData_binary ...
        ' -n ' num2str(num) ...
        ' -o ' broken_bone_rng_filename ...
        ' -m ' broken_bone_org_filename ...
        ' -s ' num2str(sig) ... 
        ' -f text'];
    [result,~] = system ( exec );
    assert(result==0);
    X = readMultiplePointsMFD(broken_bone_rng_filename);
    error = zeros(num,1);
    for i=1:num
        T = kslmap(X(:,i),x);
        error(i) = norm(T);
    end
    fprintf('est(error): %.10f\n', std(error));
    fprintf('std(error): %.10f\n', sig);
end









