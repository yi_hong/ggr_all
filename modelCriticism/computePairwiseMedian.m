% 
% compute pairwise median
% 

function med = computePairwiseMedian(x)

res = [];
for iI = 1:length(x)
    for iJ = iI+1:length(x)
        res = [res abs(x(iI) - x(iJ))];
    end
end

med = median(res);

end