
%% Corpus Callosum
%[X,time] = genCCData('cc.mat');

%% Toy data (non-geodesic)
[X,time] = genNonGeodesicData('toyShapes.mat',40,0);
N = size(X,2);

%% Toy data (on geodesic + Gaussian noise)
%[X,time] = genGeodesicData('toyShapes.mat',40,0.001,0);
%N = size(X,2);

%% CONFIG
runs = 50;      % # random trials
leave_out = 10; % # left-out samples / trial
do_plot = 0;    % plot results
out_dir = '~/Desktop/ModelCritisism/scratch'; % write do this directory
ComputeGeodesicRegression_binary = ...
    '/Users/rkwitt/Software/manifoldstatistics/Build/Applications/ComputeGeodesicRegression';

if do_plot
    figure;
    hold;
end

D = cell(runs,1);
errors = zeros(runs, leave_out);
W = zeros(N,runs);
for i=1:runs
    disp(i);
    [trn,tst] = crossvalind('LeaveMOut', N, leave_out);
    trn_data_cv = X(:,trn);
    trn_time_cv = time(trn);
    
    gres_filename = ['gres_' num2str(i,'%.3d') '.mfd'];
    data_filename = ['data_' num2str(i,'%.3d') '.mfd'];
    time_filename = ['time_' num2str(i,'%.3d') '.mfd'];
    
    saveMFD(...
        trn_time_cv, ...
        trn_data_cv, ...
        fullfile(out_dir,time_filename), ...
        fullfile(out_dir,data_filename), ...
        2);
    
    exec = [...
        ComputeGeodesicRegression_binary ...
        ' -x ' fullfile(out_dir, time_filename) ...
        ' -y ' fullfile(out_dir, data_filename) ...
        ' -o ' fullfile(out_dir, gres_filename) ...
        ' -f text'];
    system ( exec );
       
    [x0,x1] = readMFD(fullfile(out_dir, gres_filename));
    
    D{i} = x0;
    if do_plot
        plot(real(x0),imag(x0),'r+','Linewidth',1);
    end
    
    tst_data_cv = X(:,tst);
    tst_time_cv = time(tst);
    assert(size(tst_data_cv,2)==length(tst_time_cv));
 
    E = []; % estimates @ training times
    T = []; % targets @ training times

    for t=1:size(trn_data_cv,2)
        e_pt = ksemap(x0,trn_time_cv(t)*x1);
        t_pt = reshape(trn_data_cv(:,t), [length(x0) 2]);
        t_pt = t_pt(:,1)+sqrt(-1)*t_pt(:,2);
        E = [E e_pt];
        T = [T t_pt];
    end
    
    assert(size(tst_data_cv,2)==length(tst_time_cv));
    w = zeros(size(tst_data_cv,2),1);
    pos = find(tst==1);
    for t=1:size(tst_data_cv,2);
        q_pt = reshape(tst_data_cv(:,t),[length(x0) 2]);
        q_pt = q_pt(:,1)+sqrt(-1)*q_pt(:,2);
        
        w0 = 0;
        for k=1:size(E,2)
            w0 = w0 + ...
                xKernel(tst_time_cv(t),trn_time_cv(k),1) ...
                *ksGeoK(q_pt,E(:,k),[]);
        end
        w1 = 0;
        for k=1:size(T,2)
            w1 = w1 + ...
                xKernel(tst_time_cv(t),trn_time_cv(k),1) ...
                *ksGeoK(q_pt,T(:,k),[]);
        end
        w(t) = w0 - w1;
        W(pos(t),i) = w(t);
    end
end

if do_plot
    axis equal;
    box on;
end




