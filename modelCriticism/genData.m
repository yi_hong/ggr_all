function [X,Xsmall,params] = genData

    %rng(1234);
    tDim = 20;
    nTimePoints = 5;
    smallSample = 5;
    dim = 64;
    
    N = ones(nTimePoints,1)*512;
    X = cell(nTimePoints,1);
    Xsmall = cell(nTimePoints,1);
    params = cell(nTimePoints,1);
    
    %fixs = 2^3;
    
    for s=1:length(N)
        mu = ones(dim,1).*s;
        sigma = ones(dim,1);
        sigma(tDim) = 2^s;
        %sigma(tDim+1) = 2^s/2;
        
        sigma = diag(sigma);
        
        X{s} = mvnrnd(mu,sigma,N(s));
        Xsmall{s} = X{s}(randperm(N(s),smallSample),:);
        params{s}.mu = mu;
        params{s}.sigma = sigma;
    end

    % plot
    if 0
       colors = jet(length(X));
       figure;hold;
       for i=1:length(X)
           plot(X{i}(:,1),X{i}(:,2),'+', ....
               'color',colors(i,:));
           pause
       end
       %pause
    end
end