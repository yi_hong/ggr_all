function [value] = gKernel(X, Y, param)

switch param.type
    case 1
        % RBF, bc
        assert( param.beta > 0 );
        %disp('Grassmann kernel: RBF, BC');
        value = exp(param.beta * abs(det(X' * Y)));
    case 2
        % RBF, p
        assert( param.beta > 0 );
        %disp('Grassmann kernel: RBF, P');
        value = exp(param.beta * norm(X' * Y, 'fro').^2);
    case 3 
        % Laplace, bc 
        assert( param.beta > 0 );
        %disp('Grassmann kernel: Laplace, BC');
%         if norm(X-Y, 'fro') < 1e-9
%             value = 1;
%         else
            value = exp(-param.beta * sqrt( 1 - abs(det(X' * Y)) ));
            value = real(value);
%         end
%         if abs(imag(value)) < 1e-7
%             value = real(value);
%         end
    case 4 
        % Laplace, p
        assert( param.beta > 0 );
        %disp('Grassmann kernel: Laplace, P');
%         if norm(X-Y, 'fro') < 1e-6
%             value = 1;
%         else
            value = exp(-param.beta * sqrt(param.p - norm(X'*Y, 'fro').^2));
            value = real(value);
%         end
%         if abs(imag(value)) < 1e-7
%             value = real(value);
%         end
    case 5
        % Binomial, bc
        assert( param.beta > 1 );
        %disp('Grassmann kernel: Binomial, BC');
        value = (param.beta - abs(det(X'*Y))).^(-params.alpha);
    case 6
        % Binomial, p
        assert( param.beta > param.p );
        %disp('Grassmann kernel: Binomial, P');
        value = (param.beta - norm(X'*Y, 'fro').^2).^(-params.alpha);
    otherwise
        error('Unknown types of grassmann kernel');
end