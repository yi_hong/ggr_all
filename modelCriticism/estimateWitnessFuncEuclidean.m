function estimateWitnessFuncEuclidean(A, B, xSize, ySize, sig, fitB)

% assume we have x and y dimension
if size(A, 2) ~= 2 || size(B, 2) ~= 2
    disp('Unsupported dimension of witness function');
    return;
end

minX = min(min(A(:, 1)), min(B(:, 1)));
maxX = max(max(A(:, 1)), max(B(:, 1)));

minY = min(min(A(:, 2)), min(B(:, 2)));
maxY = max(max(A(:, 2)), max(B(:, 2)));

xSpace = linspace(minX, maxX, xSize);
ySpace = linspace(minY, maxY, ySize);

witnessFunc = zeros(length(ySpace), length(xSpace));
for iJ = 1:size(witnessFunc, 1)
    for iI = 1:size(witnessFunc, 2)
        sample = [xSpace(iI) ySpace(iJ)];
        witnessFunc(iJ, iI) = mean(rbf_dot(sample, A, sig)) - ...
            mean(rbf_dot(sample, B, sig));
%         witnessFunc(iJ, iI) = mean(rbf_dot(sample(1), A(:, 1), sig) .* rbf_dot(sample(2), A(:, 2), sig)) ...
%             - mean(rbf_dot(sample(1), B(:, 1), sig) .* rbf_dot(sample(2), B(:, 2), sig));
    end
end

hold on
imagesc(xSpace, ySpace, witnessFunc);
plot(A(:, 1), A(:, 2), '*', 'color', [193/255 75/255 83/255]);
plot(B(:, 1), B(:, 2), 's', 'color', [73/255 112 /255 166/255]);
plot(fitB(:, 1), fitB(:, 2), '-', 'LineWidth', 4, 'Color', [73/255 112 /255 166/255]);
set(gca,'YDir','normal');
hold off
colormap(b2r(min(min(witnessFunc)), max(max(witnessFunc))));
colorbar
