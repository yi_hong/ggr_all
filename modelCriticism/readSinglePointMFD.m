function X = readSinglePointMFD(filename)

    fid = fopen(filename, 'r');
   
    X = [];
    dataFlag = 0;
    while 1
        x = fgets(fid);
        if x<0
            break;
        end
        
        x = strtrim(x);
        parts = strsplit(x);
        
        key = parts{1};
        if strcmp(key, 'manifold')
            assert(strcmp(parts{end}, 'KendallShapeSpace'));
        elseif strcmp(key, 'dimParameters')
            dim1 = str2double(parts{end-1});
            dim2 = str2double(parts{end-0});
        elseif strcmp(key, 'numElements')
        elseif strcmp(key, 'primitive')
        elseif strcmp(key, 'format')
        end
        
        if strcmp(x,'data:')
            dataFlag = 1;
            continue; 
        end
      
        if dataFlag
            assert(dim1 > 0);
            assert(dim2 > 0);
            nums = strsplit(x);
            assert(length(nums) == dim1*dim2);
            
            tmp = [];
            for i=1:length(nums);
                z = str2double(nums{i});
                tmp = [tmp z];
            end
            X = [X; tmp];
        end
    end
    assert(size(X,1)==1);
    X = reshape(X,[dim2 dim1]);
    X = X(:,1)+sqrt(-1)*X(:,2);
    fclose(fid);
end