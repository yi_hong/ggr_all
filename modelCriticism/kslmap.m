function retT = kslmap(p,q)
% Computes log-map in Kendall shape space
%
% Author: Roland Kwitt

    assert(~isreal(p), 'p not complex!');
    assert(~isreal(q), 'q not complex!');

    x = p;
    y = q;

    yRot = palign(x',y');
    cosTheta = innerProduct(x,yRot);
    t = yRot - x*cosTheta;
    
    len = vnorm(t);
    
    if (len<1e-12 || cosTheta >= 1 || cosTheta <= -1)
        retT = zeros(length(p),1);
    else
        retT = (t* (acos(cosTheta)/len));
    end
end

%function p = proj(x,y)
%    p = x*dot(x,y)/norm(x)^2;
%end

function n = vnorm(t)
    n = sqrt(innerProduct(t,t));
end

function result = innerProduct(u,v)
    result = real(dot(u,v));
end

function qRot = palign(p,q)
    assert(~isreal(p), 'p not complex!');
    assert(~isreal(q), 'q not complex!');

    p_m = [real(p); imag(p)];
    q_m = [real(q); imag(q)];

    % solve procrustes
    M = p_m*q_m';
    [U,~,V] = svd(M,0);

    % compute rotated q
    R = U*V';
    qRot = R*q_m;

    % back to complex
    qRot = (qRot(1,:) + sqrt(-1)*qRot(2,:))';
end