function [data,time] = genGeodesicData(mat_file,N,sigma,do_plot,scratch_dir,config)

    out_dir = scratch_dir; %'/Users/rkwitt/Desktop/ModelCritisism/scratch';
    GenerateRandomGaussianData_binary = config.GenerateRandomGaussianData_binary;
        %'/Users/rkwitt/Software/manifoldstatistics/Build/Applications/GenerateRandomGaussianData';
    assert(sigma>0);
    
    % load file with toy shapes
    tmp = load(mat_file,'C'); 
    C = tmp.C;

    x1 = C(:,:,1)'; % bone
    x2 = C(:,:,9)'; % broken bone
    X = {x1, x2};

    time = linspace(0,1,N-1); % bone->flower

    % scale/translation normalization
    Y = cell(length(X),1);
    for i=1:length(X)
        Y{i} = X{i}(:,1)+sqrt(-1)*X{i}(:,2);
        Y{i} = Y{i}-mean(Y{i});
        Y{i} = Y{i}/norm(Y{i});
        assert((norm(Y{i})-1.0)<1e-12);
    end

    x1 = Y{1}; % beg
    x2 = Y{2}; % end
    tangent = kslmap(x1,x2); % tangent: beg->end
    
    data = [];
    for i=1:N-1
        % map to point on geodesic beg->end (+normalize)
        u = ksemap(x1,time(i)*tangent);
        u = u - mean(u);
        u = u/norm(u);
        assert((norm(u)-1.0)<1e-12);
        
        rng_geo_filename = fullfile(out_dir, ['rng_geo_' num2str(i, '%.3d') '.mfd']);
        pnt_geo_filename = fullfile(out_dir, ['pnt_geo_' num2str(i, '%.3d') '.mfd']);
        saveSinglePointMFD(...
            [real(u); imag(u)],...
            pnt_geo_filename,2);
        exec = [GenerateRandomGaussianData_binary ...
            ' -n ' num2str(1) ...          % generate one point
            ' -o ' rng_geo_filename ...    % name of file with random point
            ' -m ' pnt_geo_filename ...    % name of file with point on geodesic
            ' -s ' num2str(sigma) ...      % sigma of Gaussian
            ' -f text'];
        system ( exec );   
        
        % read random point (+normalize)
        rng_pnt = readSinglePointMFD(rng_geo_filename);
        rng_pnt = rng_pnt - mean(rng_pnt);
        rng_pnt = rng_pnt/norm(rng_pnt);
        assert((norm(rng_pnt)-1.0)<1e-12);
        rng_pnt_real = [real(rng_pnt); imag(rng_pnt)];
        data = [data rng_pnt_real];
    end
    
    if do_plot
        figure;
        for i=1:size(data,2)
            tmp = reshape(data(:,i),[100 2]);
            plot(tmp(:,1),tmp(:,2),'blue','Linewidth',2);
            axis equal;
            axis tight;
            out_png = fullfile(out_dir, ['geodesic_shape_' num2str(i,'%.3d') '.png'])
            export_fig(out_png,'-transparent');
            close all;
        end
    end
end






