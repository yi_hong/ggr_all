function k = ksGeoK(x,y,varargin)
    %t0 = abs(dot(x,y));
    %assert(t0<pi/2);
   % k = exp(-acos(t0)/10);
   d2F = 1-abs(dot(x,y))^2;
   k = exp(-d2F);
end