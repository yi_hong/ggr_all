function k = xKernel(x,y,param)
    assert(length(param)==1);
    sigma = param(1);
    k = exp(-norm(x-y)^2/(2*sigma^2));
end