function [x0,x1] = readMFD(filename)
% only works for output of 'ComputeGeodesicRegression' binary
    fid = fopen(filename, 'r');
   
    X = [];
    dataFlag = 0;
    while 1
        x = fgets(fid);
        if x<0
            break;
        end
        
        x = strtrim(x);
        parts = strsplit(x);
        
        key = parts{1};
        if strcmp(key, 'manifold')
            assert(strcmp(parts{end}, 'KendallShapeSpace'));
        elseif strcmp(key, 'dimParameters')
            dim1 = str2double(parts{end-1});
            dim2 = str2double(parts{end-0});
        elseif strcmp(key, 'numElements')
        elseif strcmp(key, 'primitive')
        elseif strcmp(key, 'format')
        end
        
        if strcmp(x,'data:')
            dataFlag = 1;
            continue; 
        end
      
        if dataFlag
            assert(dim1 > 0);
            assert(dim2 > 0);
            nums = strsplit(x);
            assert(length(nums) == dim1*dim2);
            
            tmp = [];
            for i=1:length(nums);
                z = str2double(nums{i});
                tmp = [tmp z];
            end
            X = [X; tmp];
        end
    end
    % expect base + tangent
    assert(size(X,1)==2);
    x0 = reshape(X(1,:),[dim2 dim1]);
    x1 = reshape(X(2,:),[dim2 dim1]);
    % make complex
    x0 = x0(:,1)+sqrt(-1)*x0(:,2);
    x1 = x1(:,1)+sqrt(-1)*x1(:,2); 
    fclose(fid);
end