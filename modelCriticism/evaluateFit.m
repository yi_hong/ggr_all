function [stat,boot_stat] = evaluateFit(data,time,n_trials,out_dir,config)
    
    % --CONFIGURE---%
    if (isfield(config, 'seed') && config.seed > 0)
        fprintf('[%s]: Setting seed to %d\n', mfilename, config.seed);
        rng(config.seed);
    end
    verbose = config.verbose;
    do_plot = config.do_plot;
    ComputeGeodesicRegression_binary = config.ComputeGeodesicRegression_binary;
    GenerateRandomGaussianData_binary = config.GenerateRandomGaussianData_binary;
    % --CONFIGURE---%
    
    
    
    N = size(data,2);
    data_filename = fullfile(out_dir, 'target_data.mfd');
    time_filename = fullfile(out_dir, 'target_time.mfd');
    gres_filename = fullfile(out_dir, 'target_gres.mfd');
    
    saveMFD(time, data, time_filename, data_filename, 2);
    exec = [...
        ComputeGeodesicRegression_binary ...
        ' -x ' time_filename ...
        ' -y ' data_filename ...
        ' -o ' gres_filename ...
        ' -f text'];
    [result,~]=system ( exec );
    assert(result==0);
    
    [x0,x1] = readMFD(gres_filename);
    
    error = zeros(N,1);
    est_geo_filename = cell(N,1);   % filenames of points on geodesic (our model)
    rng_geo_filename = cell(N,1);   % filenames of samples from model
    O = zeros(size(data,1)/2,N);    % (O)bserverd points 
    E = zeros(size(data,1)/2,N);    % (E)stimated points on geodesic
    R = zeros(size(data,1)/2,N);    % (R)andom sample from model
    for i=1:N
        obs_i = r2c(data(:,i));
        est_i = ksemap(x0,time(i)*x1);
        est_geo_filename{i} = fullfile(out_dir, ['est_geo_' num2str(i, '%.3d') '.mfd']);
        saveSinglePointMFD(...
            [real(est_i); imag(est_i)],...
            est_geo_filename{i},2);
        error(i) = norm(kslmap(obs_i,est_i));
        O(:,i) = obs_i;
        E(:,i) = est_i;
    end
    %---ESTIMATE OF STD. DEV---%
    sig = std(error);
    %---ESTIMATE OF STD. DEV---%
    for i=1:N
        rng_geo_filename{i} = fullfile(out_dir, ['rng_geo_' num2str(i, '%.3d') '.mfd']);
        exec = [GenerateRandomGaussianData_binary ...
                ' -n ' num2str(1) ...
                ' -o ' rng_geo_filename{i} ...
                ' -m ' est_geo_filename{i} ...
                ' -s ' num2str(sig) ... 
                ' -f text'];
        [result,~] = system ( exec );
        assert(result==0);
        R(:,i) = readSinglePointMFD(rng_geo_filename{i});
        if do_plot
            figure; hold on;
            plot(real(O(:,i)),imag(O(:,i)),'black-','Linewidth',3);
            plot(real(E(:,i)),imag(E(:,i)),'blue-');
            plot(real(R(:,i)),imag(R(:,i)),'red-');
            axis equal;
            axis tight;
            hold off;
            fprintf('Press key to continue ...\n');
            pause;
            close all;
        end
    end
    
    %---COMPUTE STATISTIC---%
    stat = ksRegMMD(R,O,time,time);
    %---COMPUTE STATISTIC---%
    if verbose
        fprintf('[%s]: estimated variance: %.10f\n', mfilename, sig);
        fprintf('[%s]: test-statistic T = %.10f\n', mfilename, stat);
    end
    
    %---ESTIMATE H0---%
    boot_stat = zeros(n_trials,1);
    rng_geo_H0_filename = cell(n_trials,N);
    nSamples = 1;
%     n_subTrials = round(n_trials/nSamples);
%     assert(n_trials == nSamples * n_subTrials);
    for b=1:n_trials %n_subTrials
        tic
        Z0 = zeros(size(data,1)/2,N);
        Z1 = zeros(size(data,1)/2,N);
%         Z0 = cell(nSamples, 1);
%         Z1 = cell(nSamples, 1);
%         for j=1:nSamples
%             Z0{j} = zeros(size(data,1)/2,N);
%             Z1{j} = zeros(size(data,1)/2,N);
%         end
        for i=1:N
            rng_geo_H0_filename{b,i} = fullfile(out_dir, ['rng_geo_trial_' num2str(b, '%.3d') '_pnt_' num2str(i, '%.3d')  '.mfd']);
            % generate 2 samples
            exec = [GenerateRandomGaussianData_binary ...
                ' -n ' num2str(2*nSamples) ...
                ' -o ' rng_geo_H0_filename{b,i} ...
                ' -m ' est_geo_filename{i} ...
                ' -s ' num2str(sig) ... 
                ' -f text'];
            [status,~] = system ( exec );
            assert(status==0);
            
            tmp = readMultiplePointsMFD(rng_geo_H0_filename{b,i});
            assert(size(tmp,2)==2*nSamples);
            Z0(:,i) = tmp(:,1);
            Z1(:,i) = tmp(:,2);
%             for j=1:nSamples
%                 Z0{j}(:, i) = tmp(:, (j-1)*2+1);
%                 Z1{j}(:, i) = tmp(:, (j-1)*2+2);
%             end
        end
        boot_stat(b) = ksRegMMD(Z0,Z1,time,time);
%         for j=1:nSamples
%             boot_stat((b-1)*nSamples+j) = ksRegMMD(Z0{j},Z1{j},time,time);
%         end
        if verbose
            fprintf('[%s]: boot statistic [run=%d] stat=%.10f\n', ...
                mfilename, b*nSamples, boot_stat(b*nSamples));
        end
        toc
    end
end

function stat = ksRegMMD(X,Y,timeX,timeY)
    N = size(X,2);
    M = size(Y,2);
   
    %kXX
    K = eye(size(X,2),size(X,2));
    for i=1:size(X,2)
        for j=1:i-1
            K(i,j)=xKernel(timeX(i),timeX(j),1)* ...
                real(ksGeoK(X(:,i),X(:,j)));
            K(j,i) = K(i,j);
        end
    end
    %kYY
    L = eye(size(Y,2),size(Y,2));
    for i=1:size(Y,2)
        for j=1:i-1
            L(i,j)=xKernel(timeY(i),timeY(j),1)* ...
                real(ksGeoK(Y(:,i),Y(:,j)));
            L(j,i) = L(i,j);
        end
    end
    % kXY
    V = zeros(size(X,2),size(Y,2));
    for i=1:size(X,2)
        for j=1:size(Y,2)
            V(i,j)=xKernel(timeX(i),timeY(j),1)* ...
                real(ksGeoK(X(:,i),Y(:,j)));
        end
    end
    %---MMD---%
    stat = ...
        1 / N^2*sum(sum(K)) - ... 
        2 / (N * M)*sum(sum(V)) + ...
        1 / M^2*sum(sum(L));
    %---MMD---%    
end

function c = r2c(r)
% real vector to complex vector
    assert(isreal(r));
    assert(mod(length(r),2)==0);
    c = reshape(r,[length(r)/2 2]);
    c = c(:,1)+sqrt(-1)*c(:,2);
end


