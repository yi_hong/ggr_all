% test function
% geodesic fitting, residues are Gaussian/Non-Gaussian distributed
clear all
close all

% generate two points on Grassmannian
Y0 = [2; 1];
[Y0, ~] = qr(Y0, 0);
Y1 = [7; 9];
[Y1, ~] = qr(Y1, 0);

% generate points along the geodesic defined by those two points 
N = 50;
[~, ~, ~, vTmp] = grgeo(Y0, Y1, 1, 'v3', 'v2');
[time, Ys, ~] = integrateForwardWithODE45(Y0, vTmp, linspace(0, 1, N));
figure, plot2DGrass(Ys);

% add noise to the "clean" data
sigma = 0.05;
gamma = 0.2;
nType = 1;
switch nType 
    case 1
        YsGaussNoise = cell(length(Ys), 1);
        YsPoissNoise = cell(length(Ys), 1);
        for iI = 1:length(Ys)
            YsGaussNoise{iI} = generateGaussianSamples(Ys{iI}, sigma);
            YsPoissNoise{iI} = generatePoissonSamples(Ys{iI}, gamma);
        end
%         [h1, p1] = checkResidues(Ys, YsGaussNoise);
%         [h2, p2] = checkResidues(Ys, YsPoissNoise);
    case 2
        [Ymean, pc, latent] = computePGAonGrassmannian(Ys);
        YsGaussNoise = generateSamplesPGA(Ys, sigma, Ymean, pc, latent, 1);
        YsPoissNoise = generateSamplesPGA(Ys, sigma, Ymean, pc, latent, 2);
    otherwise
        error('Unknown type to generate Gaussian samples');
end
figure, hold on
subplot(1, 2, 1), plot2DGrass(Ys, YsGaussNoise, time);
subplot(1, 2, 2), plot2DGrass(Ys, YsPoissNoise, time);
hold off

% fit 
grParams = setRegressParams();
grParams.Ys = YsGaussNoise;
grParams.ts = time;
grParams.wi = ones(length(grParams.ts), 1);
grParams.pntY0 = YsGaussNoise{1};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(time);
grParams.nsize = size(YsGaussNoise{1});

[X10, X20, energy] = fullRegressionOnGrassmannManifold(grParams);
X20 = X20 ./ (max(grParams.ts) - min(grParams.ts));
[~, YsGaussEst, ~] = integrateForwardWithODE45(X10, X20, time);
figure(150), subplot(1, 2, 1), plot2DGrass(YsGaussEst, grParams.Ys, time);

grParams.Ys = YsPoissNoise;
grParams.pntY0 = YsPoissNoise{1};
[X10, X20, energy] = fullRegressionOnGrassmannManifold(grParams);
X20 = X20 ./ (max(grParams.ts) - min(grParams.ts));
[~, YsPoissEst, ~] = integrateForwardWithODE45(X10, X20, time);
figure(150), subplot(1, 2, 2), plot2DGrass(YsPoissEst, grParams.Ys, time);


% model creticism
nTrials = 1000;
gkParams.beta = 1;
gkParams.type = 1;
[statGauss, bootStatGauss] = evaluateFitOnGrassmannian(YsGaussNoise, YsGaussEst, time, nTrials, gkParams, nType);
pGauss = sum(bootStatGauss >= statGauss) / length(bootStatGauss);
figure, subplot(1, 2, 1), hist(bootStatGauss, 100);
hold on
yLim = get(gca, 'YLim');
plot(statGauss*[1 1], yLim, 'r-', 'LineWidth', 2);
hold off


[statPoiss, bootStatPoiss] = evaluateFitOnGrassmannian(YsPoissNoise, YsPoissEst, time, nTrials, gkParams, nType);
pPoiss = sum(bootStatPoiss >= statPoiss) / length(bootStatPoiss);
subplot(1, 2, 2), hist(bootStatPoiss, 100);
hold on
yLim = get(gca, 'YLim');
plot(statPoiss*[1 1], yLim, 'r-', 'LineWidth', 2);
hold off

