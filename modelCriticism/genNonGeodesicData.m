function [data,time] = genNonGeodesicData(mat_file,N,do_plot)

    out_dir = '~/Desktop/ModelCritisism/scratch';
    tmp = load(mat_file,'C'); 
    C = tmp.C;

    x1 = C(:,:,1)'; % bone
    x2 = C(:,:,500)'; % flower
    x3 = C(:,:,9)'; % broken bone
    assert(mod(N,2)==0);
    X = {x1,x2,x3};

    t12 = linspace(0,1,N/2); % bone->flower
    t23 = linspace(0,1,N/2); % flower->broken bone
    
    % scale/translation normalization
    Y = cell(length(X),1);
    for i=1:length(X)
        Y{i} = X{i}(:,1)+sqrt(-1)*X{i}(:,2);
        Y{i} = Y{i}-mean(Y{i});
        Y{i} = Y{i}/norm(Y{i});
        assert((norm(Y{i})-1.0)<1e-12);
    end

    x1 = Y{1};
    x2 = Y{2};
    x3 = Y{3};

    
    D = [];
    tangent12 = kslmap(x1,x2);
    for i=1:N/2
        u = ksemap(x1,t12(i)*tangent12);
        assert((norm(u)-1.0)<1e-12);
        u = [real(u); imag(u)];
        D = [D u];
    end
    
    E = [];
    u = ksemap(x1,t12(end)*tangent12);
    tangent23 = kslmap(u,x3);
    for i=1:N/2
        v = ksemap(u,t23(i)*tangent23);
        assert((norm(v)-1.0)<1e-12);
        v = [real(v); imag(v)];
        E = [E v];
    end
    D(:,end) = [];
    t12 = t12/2;
    t23 = t23/2;
    time = [t12(1:end-1)';t23(:)+0.5]; % time 
    data = [D E]; % all shapes

    if do_plot
        figure;
        for i=1:size(data,2)
            tmp = reshape(data(:,i),[100 2]);
            plot(tmp(:,1),tmp(:,2),'blue','Linewidth',2);
            axis equal;
            axis tight;
            out_png = fullfile(out_dir, ['nonGeodesic_shape_' num2str(i,'%.3d') '.png']);
            export_fig(out_png,'-transparent');
            close all;
        end
    end
end






