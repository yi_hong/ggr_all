function saveSinglePointMFD(dataY, dataFileName, dim)

    % data file
    fid = fopen(dataFileName, 'wt');
    assert(fid > 0);

    % see 'ManifoldFileReader.cxx'
    fprintf(fid, 'GFBR1\n'); 
    fprintf(fid, 'manifold=KendallShapeSpace\n');
    fprintf(fid, 'primitive=point\n');
    fprintf(fid, 'dimParameters=%d %d\n', dim, size(dataY, 1)/dim);
    fprintf(fid, 'numElements=1\n');
    fprintf(fid, 'format=text\n');
    fprintf(fid, 'data:\n');

    for iJ = 1:size(dataY, 2)
        for iI = 1:size(dataY, 1)
            fprintf(fid, '%.10f ', dataY(iI, iJ));
        end
        fprintf(fid, '\n');
    end

    fclose(fid);   
end