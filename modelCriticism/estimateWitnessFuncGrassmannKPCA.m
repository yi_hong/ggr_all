function estimateWitnessFuncGrassmannKPCA(A, B, timeA, timeB, xSize, ySize, gkParams, Est)

nPC = 1;

data = [A; B; Est];
assert(length(data) >= nPC);
[eigVectorTmp] = constructNormalizedKernelMatrix(data, gkParams, nPC);

eigVector = eigVectorTmp(:, nPC);
[basisA, basisB, basisE] = precomputeBasis(data, A, B, Est, gkParams);
scoreA = (eigVector' * basisA)';
scoreB = (eigVector' * basisB)';
scoreE = (eigVector' * basisE)';

time = linspace(min([timeA; timeB]), max([timeA; timeB]), xSize);
pcs = linspace(min([scoreA; scoreB; scoreE]), max([scoreA; scoreB; scoreE]), ySize);

witnessFuncs = zeros(length(time), length(pcs));
for iJ = 1:size(witnessFuncs, 1)
    for iI = 1:size(witnessFuncs, 2)
        sampleTime = time(iJ);
        samplePC = pcs(iI);
        witnessFuncs(iJ, iI) = estimateKernelDensity(scoreA, timeA, samplePC, sampleTime, gkParams) ...
            - estimateKernelDensity(scoreB, timeB, samplePC, sampleTime, gkParams);
    end
end

%subplot(2, 1, 1), hold on;
hold on
imagesc(time, pcs, witnessFuncs);
plot(timeA, scoreA, 'm*', 'LineWidth', 2);
plot(timeB, scoreB, 'bs', 'LineWidth', 2);
plot(timeB, scoreE, 'k-', 'LineWidth', 2);
set(gca, 'YDir', 'normal');
hold off
colormap(b2r(min(min(witnessFuncs)), max(max(witnessFuncs))));
colorbar

% witnessFuncsClean = cleanWitnessFunctionWithBand(witnessFuncs, ...
%     [timeA;timeB;timeB], [scoreA; scoreB; scoreE], time, pcs, 100);
% subplot(2, 1, 2), hold on;
% imagesc(time, pcs, witnessFuncsClean);
% plot(timeA, scoreA, 'm*', 'LineWidth', 2);
% plot(timeB, scoreB, 'bs', 'LineWidth', 2);
% plot(timeB, scoreE, 'k-', 'LineWidth', 2);
% set(gca, 'YDir', 'normal');
% hold off
% colormap(b2r(min(min(witnessFuncs)), max(max(witnessFuncs))));
% colorbar

end

function [eigVector] = constructNormalizedKernelMatrix(data, gkParams, nPCs)

    N = length(data);
    matK = size(N, N);
    matI = ones(N, N) / N;

    % compute the original kernel matrix
    for iI = 1:N
        for iJ = 1:N
            matK(iI, iJ) = gKernel(data{iI}, data{iJ}, gkParams);
        end
    end

    % centering
    matKT = matK - matI * matK - matK * matI + matI * matK * matI;

    % compute the first nPCs eigvalues and eigvectors
    [eigVector, eigValue] = eigs(matKT, nPCs);
    eigValue = diag(eigValue);

    % normalize alphas
    for iI = 1:length(eigValue)
        if abs(eigValue(iI)) > 1e-7
            tmp = eigVector(:, iI);
            eigVector(:, iI) = tmp / norm(tmp) / sqrt(abs(eigValue(iI)));
        end
    end
    
end

function [basisA, basisB, basisE] = precomputeBasis(data, A, B, Est, gkParams)

    basisA = zeros(length(data), length(A));
    basisB = zeros(length(data), length(B));
    basisE = zeros(length(data), length(Est));
    
    for iI = 1:length(data)
        for iJ = 1:length(A)
            basisA(iI, iJ) = gKernel(data{iI}, A{iJ}, gkParams);
        end
        for iJ = 1:length(B)
            basisB(iI, iJ) = gKernel(data{iI}, B{iJ}, gkParams);
        end
        for iJ = 1:length(Est)
            basisE(iI, iJ) = gKernel(data{iI}, Est{iJ}, gkParams);
        end
    end
    
end

function den = estimateKernelDensity(score, timeO, samplePC, timeS, gkParams)

    assert(length(score) == length(timeO));
    N = length(score);
    denSum = 0;
    for iN = 1:N
        denSum = denSum + samplePC * score(iN) * xKernel(timeO(iN), timeS, gkParams.gamma);
    end
    den = denSum / N;
    
end