function [X,time] = genCCData(mat_file)

    tmp = load(mat_file);
    cc = tmp.cc;
    data = cc.data; % landmark-based shape representation
    time = (cc.ages - min(cc.ages)) / (max(cc.ages) - min(cc.ages)); % normalize to [0,1]
    X = [];
    N = length(data);
    for i=1:N
       % translation/scale normalization
       x = cc.data{i}(:,1) + sqrt(-1)*cc.data{i}(:,2);
       y = x/norm(x);
       assert(mean(y) < 1e-9);
       assert((norm(y)-1.0) < 1e-9);
       a = [real(y); imag(y)];
       X = [X a]; 
    end
end