% 
% remove regions in witness function where no observtion exists
% 
function witnessFuncClean = cleanWitnessFunctionWithBand(witnessFuncs, ...
    xData, yData, xAxis, yAxis, nStep)

mask = zeros(size(witnessFuncs));

xStart = min(xAxis);
winSize = (max(xAxis) - min(xAxis))/20;
deltaX = (max(xAxis) - min(xAxis)) / nStep;
for iI = 1:nStep
    xEnd = xStart + winSize;
    if iI == nStep
        xEnd = max(xAxis);
    end
    
    % find the data within [xStart, xEnd]
    idInside = find(xData >= xStart & xData <= xEnd);
    yStart = min(yData(idInside));
    yEnd = max(yData(idInside));
    
    idx = find(xAxis >= xStart & xAxis <= xEnd);
    if ~isempty(idInside)
        idy = find(yAxis >= yStart & yAxis <= yEnd);
        mask(min(idy):max(idy), min(idx):max(idx)) = 1;
    end
    xStart = xStart + deltaX;
end

witnessFuncClean = witnessFuncs .* mask;

end