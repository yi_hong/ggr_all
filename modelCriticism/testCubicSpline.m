% test function
clear all
close all

rng('shuffle');
outPrefix = '/Users/yihong/Desktop/work/Project/model_criticism_results/poster';
%outPrefix = '/playpen/Project/modelCriticism/miccai2015_results/poster';
bSave = true;
fileSavedTime = '092715';

% generate angles
N = 51;
time = linspace(0, 1, N)';
angle = sin(time*1.8*pi + 0.1*pi);
Ys = cell(N, 1);
YsGaussNoise = cell(N, 1);
sigma = 0.05;
for iI = 1:N
    Ys{iI} = [cos(angle(iI)); sin(angle(iI))];
    [Ys{iI}, ~] = qr(Ys{iI}, 0);
    YsGaussNoise{iI} = generateGaussianSamples(Ys{iI}, sigma);
end
figure, subplot(1, 2, 1), plot(time, angle, '-r*', 'LineWidth', 2);
subplot(1, 2, 2), plot2DGrass(Ys, YsGaussNoise, time);
if bSave == true
    filename = sprintf('%s/generatedSplineDataWithNoise_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end

% fit 
grParams = setRegressParams();
grParams.Ys = YsGaussNoise;
grParams.ts = time;
grParams.wi = ones(length(grParams.ts), 1);
grParams.pntY0 = YsGaussNoise{1};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(time);
grParams.nsize = size(YsGaussNoise{1});

[X10, X20, ~] = fullRegressionOnGrassmannManifold(grParams);
X20 = X20 ./ (max(grParams.ts) - min(grParams.ts));
[~, YsEstLinear, ~] = integrateForwardWithODE45(X10, X20, time);
figure(150), subplot(1, 2, 1), plot2DGrass(YsEstLinear, grParams.Ys, time);

grParams.cps = (min(time) + max(time))/2;
grParams.minFunc = 'both';
grParams.nIterMax = 100;
grParams.X10GT = YsGaussNoise{1};
[~, ~, ~, vTmp] = grgeo(YsGaussNoise{1}, YsGaussNoise{end}, 1, 'v3', 'v2');
grParams.X20GT = vTmp;
[X10, X20, X30, X40s, ~] = cubicSplineRegression(grParams);
pnt{1} = X10;
vel = {X20, X30};
vel = [vel, X40s];
tPos = [min(time) grParams.cps max(time)];
[~, YsEstSpline] = computePointsAlongGeodesicSpline(pnt, vel, tPos, time, grParams.nt);
figure(150), subplot(1, 2, 2), plot2DGrass(YsEstSpline, grParams.Ys, time);
if bSave == true
    filename = sprintf('%s/generatedSplineDataFitting_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end

% model creticism
%nTrials = 1000;
nTrials = 1000;
gkParams.beta = 1;
gkParams.type = 4;
gkParams.gamma = 0.1; %computePairwiseMedian(time); 
gkParams.p = size(YsGaussNoise{1}, 2);
[statLinear, bootStatLinear] = evaluateFitOnGrassmannian(YsGaussNoise, ...
    YsEstLinear, time, nTrials, gkParams, 1);
if bSave == true
    filename = sprintf('%s/witnessFunctionSplineLinear_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end
pLinear = zeros(length(statLinear), 1);
for iI = 1:length(statLinear)
    pLinear(iI) = sum(bootStatLinear >= statLinear(iI)) / length(bootStatLinear);
end
% compute R^2 statistics
R2Linear = 1 - computeVar(YsGaussNoise, YsEstLinear, 3) / computeVar(YsGaussNoise, YsGaussNoise, 1);
figure(101), subplot(2, 2, 1), histTwoVectors(bootStatLinear, statLinear, 100);
subplot(2, 2, 2), hist(pLinear, 100);

% yLim = get(gca, 'YLim');
% plot(statLinear*[1 1], yLim, 'r-', 'LineWidth', 2);
% hold off

[statSpline, bootStatSpline] = evaluateFitOnGrassmannian(YsGaussNoise, ...
    YsEstSpline, time, nTrials, gkParams, 1);
if bSave == true
    filename = sprintf('%s/witnessFunctionSplineSpline_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end
pSpline = zeros(length(statSpline), 1);
for iI = 1:length(statSpline)
    pSpline(iI) = sum(bootStatSpline >= statSpline(iI)) / length(bootStatSpline);
end
R2Spline = 1 - computeVar(YsGaussNoise, YsEstSpline, 3) / computeVar(YsGaussNoise, YsGaussNoise, 1);
figure(101), subplot(2, 2, 3), histTwoVectors(bootStatSpline, statSpline, 100);
subplot(2, 2, 4), hist(pSpline, 100);

if bSave == true
    filename = sprintf('%s/generatedSplineDataModelCriticism_%dTrials_%s.fig', outPrefix, nTrials, fileSavedTime);
    saveas(gca, filename);
end

if bSave == true
    filename = sprintf('%s/generatedSplineData_%dTrials_%s.mat', outPrefix, nTrials, fileSavedTime);
    save(filename, 'R2Linear', 'R2Spline', 'X10', 'X20', 'X30', 'X40s', 'Ys', ...
        'YsGaussNoise', 'YsEstLinear', 'YsEstSpline', 'bootStatLinear', 'bootStatSpline', ...
        'pLinear', 'pSpline', 'statLinear', 'statSpline', 'tPos', 'time', 'gkParams');
end
