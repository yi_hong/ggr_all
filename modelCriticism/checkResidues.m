function [h, p] = checkResidues(Est, Rnd)
    assert(length(Est) == length(Rnd));
    
    error = zeros(length(Est), 1);
    for iI = 1:length(Est)
        error(iI) = sqrt(grarc(Est{iI}, Rnd{iI}));
    end
    
    [h, p] = jbtest(error);
    %[h, p] = lillietest(error);
    
    figure, hist(error, 20);
end