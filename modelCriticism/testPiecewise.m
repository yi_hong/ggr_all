% test function
% geodesic fitting, residues are Gaussian/Non-Gaussian distributed
clear all
close all

rng('shuffle');
%outPrefix = '/Users/yihong/Desktop/work/Project/model_criticism_results/poster';
outPrefix = '/playpen/Project/modelCriticism/miccai2015_results/poster';
bSave = true;
fileSavedTime = '092715';

% generate two points on Grassmannian
Y0 = [1; 2];
[Y0, ~] = qr(Y0, 0);
Y1 = [3; 4];
[Y1, ~] = qr(Y1, 0);
Y2 = [9; 1];
[Y2, ~] = qr(Y2, 0);

N = 26;
% generate one geodesic
[~, ~, ~, vTmp] = grgeo(Y0, Y2, 1, 'v3', 'v2');
[timeOne, YsOne, ~] = integrateForwardWithODE45(Y0, vTmp, linspace(0, 1, N*2-1));

% generate two pieces of geodesics
% the first piece
[~, ~, ~, vTmp] = grgeo(Y0, Y1, 1, 'v3', 'v2');
[time1, Ys1, ~] = integrateForwardWithODE45(Y0, vTmp, linspace(0, 1, N));
% the second piece
[~, ~, ~, vTmp] = grgeo(Y1, Y2, 1, 'v3', 'v2');
[time2, Ys2, ~] = integrateForwardWithODE45(Y1, vTmp, linspace(0, 1, N));
timeTwo = [time1/2; time2(2:end)/2+0.5]; 
YsTwo = [Ys1; Ys2(2:end)];

figure, subplot(1, 2, 1), plot2DGrass(YsOne);
subplot(1, 2, 2), plot2DGrass(YsTwo);
if bSave == true
    filename = sprintf('%s/generatedPiecewiseDataNoNoise_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end

% add noise to the "clean" data
sigma = 0.05;
YsGaussNoiseOne = cell(length(YsOne), 1);
YsGaussNoiseTwo = cell(length(YsTwo), 1);
for iI = 1:length(YsOne)
    YsGaussNoiseOne{iI} = generateGaussianSamples(YsOne{iI}, sigma);
end
for iI = 1:length(YsTwo)
    YsGaussNoiseTwo{iI} = generateGaussianSamples(YsTwo{iI}, sigma);
end
figure, subplot(1, 2, 1), plot2DGrass(YsOne, YsGaussNoiseOne, timeOne);
subplot(1, 2, 2), plot2DGrass(YsTwo, YsGaussNoiseTwo, timeTwo);
if bSave == true
    filename = sprintf('%s/generatedPiecewiseDataWithNoise_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end

% fit 
grParams = setRegressParams();
grParams.Ys = YsGaussNoiseOne;
grParams.ts = timeOne;
grParams.wi = ones(length(grParams.ts), 1);
grParams.pntY0 = YsGaussNoiseOne{1};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(timeOne);
grParams.nsize = size(YsGaussNoiseOne{1});

[X10, X20, energy] = fullRegressionOnGrassmannManifold(grParams);
X20 = X20 ./ (max(grParams.ts) - min(grParams.ts));
[~, YsGaussEstOne, ~] = integrateForwardWithODE45(X10, X20, timeOne);
figure(150), subplot(1, 2, 1), plot2DGrass(YsGaussEstOne, grParams.Ys, timeOne);

grParams.Ys = YsGaussNoiseTwo;
grParams.ts = timeTwo;
grParams.wi = ones(length(grParams.ts), 1);
grParams.pntY0 = YsGaussNoiseTwo{1};
grParams.tanY0 = zeros(size(grParams.pntY0));
grParams.pntT0 = min(timeTwo);
grParams.nsize = size(YsGaussNoiseTwo{1});
[X10, X20, energy] = fullRegressionOnGrassmannManifold(grParams);
X20 = X20 ./ (max(grParams.ts) - min(grParams.ts));
[~, YsGaussEstTwo, ~] = integrateForwardWithODE45(X10, X20, timeTwo);
figure(150), subplot(1, 2, 2), plot2DGrass(YsGaussEstTwo, grParams.Ys, timeTwo);
if bSave == true
    filename = sprintf('%s/generatedPiecewiseDataFitting_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end

% model creticism
%nTrials = 5000;
nTrials = 10000;
gkParams.beta = 1;
gkParams.type = 4;
gkParams.gamma = 0.1; %computePairwiseMedian([timeOne; timeTwo]); 
gkParams.p = size(YsGaussNoiseOne{1}, 2);
[statGaussOne, bootStatGaussOne] = evaluateFitOnGrassmannian(YsGaussNoiseOne, ...
    YsGaussEstOne, timeOne, nTrials, gkParams, 1);
if bSave == true
    filename = sprintf('%s/witnessFunctionPiecewiseOne_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end
pGaussOne = zeros(length(statGaussOne), 1);
for iI = 1:length(statGaussOne)
    pGaussOne(iI) = sum(bootStatGaussOne >= statGaussOne(iI)) / length(bootStatGaussOne);
end
R2GaussOne = 1 - computeVar(YsGaussNoiseOne, YsGaussEstOne, 3) / computeVar(YsGaussNoiseOne, YsGaussNoiseOne, 1);
figure(101), subplot(2, 2, 1), histTwoVectors(bootStatGaussOne, statGaussOne, 100);
subplot(2, 2, 2), hist(pGaussOne, 100);

gkParams.p = size(YsGaussNoiseTwo{1}, 2);
[statGaussTwo, bootStatGaussTwo] = evaluateFitOnGrassmannian(YsGaussNoiseTwo, ...
    YsGaussEstTwo, timeTwo, nTrials, gkParams, 1);
if bSave == true
    filename = sprintf('%s/witnessFunctionPiecewiseTwo_%s.fig', outPrefix, fileSavedTime);
    saveas(gca, filename);
end
pGaussTwo = zeros(length(statGaussTwo), 1);
for iI = 1:length(statGaussTwo)
    pGaussTwo(iI) = sum(bootStatGaussTwo >= statGaussTwo(iI)) / length(bootStatGaussTwo);
end
R2GaussTwo = 1 - computeVar(YsGaussNoiseTwo, YsGaussEstTwo, 3) / computeVar(YsGaussNoiseTwo, YsGaussNoiseTwo, 1);
figure(101), subplot(2, 2, 3), histTwoVectors(bootStatGaussTwo, statGaussTwo, 100);
subplot(2, 2, 4), hist(pGaussTwo, 100);
if bSave == true
    filename = sprintf('%s/generatedPiecewiseDataModelCriticism_%dTrials_%s.fig', outPrefix, nTrials, fileSavedTime);
    saveas(gca, filename);
end

if bSave == true
    filename = sprintf('%s/generatedPiecewiseData_%dTrials_%s.mat', outPrefix, nTrials, fileSavedTime);
    save(filename, 'R2GaussOne', 'R2GaussTwo', 'YsGaussNoiseOne', 'YsGaussNoiseTwo', ...
        'YsGaussEstOne', 'YsGaussEstTwo', 'bootStatGaussOne', 'bootStatGaussTwo', ...
        'pGaussOne', 'pGaussTwo', 'statGaussOne', 'statGaussTwo', 'timeOne', 'timeTwo', 'gkParams');
end

