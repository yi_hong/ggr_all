function p = ksemap(x,v)
% Compute exp-map in Kendall shape space
%
% Author: Roland Kwitt
    
    theta = vnorm(v);
    if (theta<1e-12)
        p = x;
        return;
    end
    q = cos(theta)*x + (sin(theta)/theta)*v;
    p = q/vnorm(q);
end

function n = vnorm(t)
    n = sqrt(innerProduct(t,t));
end

function result = innerProduct(u,v)
    result = real(dot(u,v));
end
