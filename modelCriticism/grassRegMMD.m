function [stat] = grassRegMMD(X, Y, timeX, timeY, gkParams)
    N = length(X);
    M = length(Y);
    
    % kXX
    K = eye(N, N);
    for iI = 1:N
        for iJ = 1:iI
            K(iI, iJ) = xKernel(timeX(iI), timeX(iJ), gkParams.gamma) * ...
                        gKernel(X{iI}, X{iJ}, gkParams);
            K(iJ, iI) = K(iI, iJ);
        end
    end
    
    % kYY
    L = eye(M, M);
    for iI = 1:M
        for iJ = 1:iI
            L(iI, iJ) = xKernel(timeY(iI), timeY(iJ), gkParams.gamma) * ...
                        gKernel(Y{iI}, Y{iJ}, gkParams);
            L(iJ, iI) = L(iI, iJ);
        end
    end
    
    % kXY
    V = zeros(N, M);
    for iI = 1:N
        for iJ = 1:M
            V(iI, iJ) = xKernel(timeX(iI), timeY(iJ), gkParams.gamma) * ...
                        gKernel(X{iI}, Y{iJ}, gkParams);
        end
    end
    
    %---MMD---%
    stat = 1 / (N^2) * sum(sum(K)) - ...
           2 / (N*M) * sum(sum(V)) + ...
           1 / (M^2) * sum(sum(L));
       
    stat = sqrt(stat);
end
