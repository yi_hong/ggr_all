function histTwoVectors(X, Y, nBins)

[nb, xb] = hist(X, nBins);
stairs(xb, nb, '-b');
%set(bh, 'FaceColor', 'b', 'EdgeColor', 'w');

hold on
[nb, xb] = hist(Y, nBins);
stairs(xb, nb, '-r');
%set(bh, 'FaceColor', 'r', 'EdgeColor', 'w');
hold off

end