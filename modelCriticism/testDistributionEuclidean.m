% 
% test of model criticism for Euclidean case
% 
close all
clear all

%outPrefix = '/Users/yihong/Desktop/work/Project/model_criticism_results';
outPrefix = '/playpen/Project/modelCriticism/miccai2015_results';

nTest = 10000;
dataSet = 1;
seed = 4556;
do_plot = 1;

if dataSet == 1
    mat_file = 'mat_files/linear.mat';
elseif dataSet == 2
    mat_file = 'mat_files/solar.mat';
else
    error('Unknown dataset');
end

testStat = zeros(nTest, 1);
MMDs = zeros(nTest, 1000);
pValues = zeros(nTest, 1);
for iI = 1:nTest
    fprintf('Test %d/%d\n', iI, nTest);
    tic
    [testStat(iI), MMDs(iI, :), pValues(iI)] = testEuclidean(mat_file, iI*100, do_plot);
    toc
end

figure, subplot(1, 2, 1), hist(testStat, nTest);
subplot(1, 2, 2), hist(pValues, nTest);

filename = sprintf('%s/euclideanData_dataset%d_%dtest.fig', outPrefix, dataSet, nTest);
saveas(gca, filename);

filename = sprintf('%s/euclideanData_dataset%d_%dtest.mat', outPrefix, dataSet, nTest);
save(filename, 'testStat', 'MMDs', 'pValues');
